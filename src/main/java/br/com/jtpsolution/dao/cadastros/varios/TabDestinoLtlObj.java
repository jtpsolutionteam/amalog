package br.com.jtpsolution.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.NumberFormat;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.cadastros.empresa.TabEmpresaObj;
import br.com.jtpsolution.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_destino_ltl", schema = Constants.SCHEMA)
public class TabDestinoLtlObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_destino_ltl")
	// @NotNull(message = "DestinoLtl campo obrigatório!")
	private Integer cdDestinoLtl;
	
	@ManyToOne
	@JoinColumn(name = "cd_empresa")
	private TabEmpresaObj tabEmpresaObj;	

	@Column(name = "tx_origem_ltl")
	@Size(max = 45, message = "DestinoLtl tamanho máximo de 45 caracteres")
	private String txOrigemLtl;
	
	@Column(name = "tx_destino_ltl")
	// @NotEmpty(message = "DestinoLtl campo obrigatório!")
	@Size(max = 45, message = "DestinoLtl tamanho máximo de 45 caracteres")
	private String txDestinoLtl;
	
	@Column(name = "vl_importacao")
	// @NotEmpty(message = "Importacao campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlImportacao;

	@Column(name = "vl_exportacao")
	// @NotEmpty(message = "Exportacao campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlExportacao;

	@Column(name = "ck_rota_amalog")
	// @NotNull(message = "ckRotaAmalog campo obrigatório!")
	private Integer ckRotaAmalog;

	@Column(name = "ck_ativo")
	// @NotNull(message = "ckAtivo campo obrigatório!")
	private Integer ckAtivo;
	
	@Column(name = "vl_qtde_dias")
	private Integer vlQtdeDias;
	
	@Column(name = "cd_impexp")
	private Integer cdImpexp;
	
	@ManyToOne
	@JoinColumn(name = "cd_via_transporte")
	private TabViaTransporteObj tabViaTransporteObj;
	

	

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txDestinoLtl))
			txDestinoLtl = txDestinoLtl.toUpperCase();
	}

}
