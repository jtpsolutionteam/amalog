package br.com.jtpsolution.modulos.dashboard.controller;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.jtpsolution.dao.TransactionUtilBean;
import br.com.jtpsolution.dao.propostas.TabPropostaNewObj;
import br.com.jtpsolution.dao.propostas.VwTabPropostaDashboardObj;
import br.com.jtpsolution.dao.propostas.VwTabPropostaNewObj;
import br.com.jtpsolution.dao.propostas.repository.TabPropostaNewRepository;
import br.com.jtpsolution.dao.propostas.repository.VwTabPropostaDashboardRepository;
import br.com.jtpsolution.modulos.microled.service.MicroledService;
import br.com.jtpsolution.modulos.propostas.service.TabPropostaService;
import br.com.jtpsolution.modulos.util.geralcontroller.allController;
import br.com.jtpsolution.security.DadosUser;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.teste.teste;

@Controller
public class DashboardDtaController extends allController {

	@Autowired
	private UsuarioBean tabUsuarioService;

	@Autowired
	private TabPropostaService tabPropostaService;

	@Autowired
	private VwTabPropostaDashboardRepository vwTabPropostaDashboardRepository;

	@Autowired
	private TabPropostaNewRepository tabPropostaNewRepository;

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private TransactionUtilBean transactionUtilBean;
	
	@Autowired
	private MicroledService microledService;

	@Autowired
	private teste test;

	@RequestMapping("/dashboarddta")
	public ModelAndView dashboard() {

		
		//microledService.statusLote();
		
		DadosUser user = tabUsuarioService.DadosUsuario();

		ModelAndView mv = new ModelAndView("dashboard/dashboarddta");

		if (user.getCdIdioma().equals("PT")) {

			//List<TabPropostaNewObj> listAgDta2 = tabPropostaNewRepository.listDashQuery(1, 4);
			List<TabPropostaNewObj> listAgDta = executaQuery(4, "dtLiberacaoDocumentos", "Asc", " and t.ckDesistenciaVistoria = 1 ");
			List<TabPropostaNewObj> listAgDtaRegistro = executaQuery(5, "dtDtaSolicitacao", "Asc", "");
			List<TabPropostaNewObj> listAgDtaParametrizacao = executaQuery(6, "dtDtaRegistro", "Asc", "");
			List<TabPropostaNewObj> listAgDtaCarregamento = executaQuery(7, "dtDtaParametrizacao", "Asc", "");
			List<TabPropostaNewObj> listAgDtaDesembaraco = executaQuery(9, "dtDtaCarregamento", "Asc", "");
			List<TabPropostaNewObj> listAgDtaInicioTransito = executaQuery(8, "dtDtaDesembaraco", "Asc", "");
			List<TabPropostaNewObj> listAgDtaChegada = executaQuery(10, "dtDtaInicioTransito", "Asc", "");
			List<TabPropostaNewObj> listAgDtaConclusao = executaQuery(11, "dtDtaChegadaTransito", "Asc", "");
			List<TabPropostaNewObj> listDtaConclusao = executaQuery(12, "dtDtaConclusaoTransito", "Desc", "");

			List<TabPropostaNewObj> listPropostaDoctosPendentes = executaQueryPropostaDoctosPendentes("pendências");

			List<TabPropostaNewObj> listPropostaUploadOK = PropostaUploadOK(1);

			List<TabPropostaNewObj> listPropostaUploadOKReconferencia = listarPropostaUploadOKReconferencia(1);

			List<TabPropostaNewObj> listPropostaDoctosAceitosSemDesistenciaVistoria = listarPropostaDoctosAceitosSemDesisteciaVistoria();

			// List<VwTabPropostaNewObj> listPropostaFaturadasPagas =
			// listarPropostasFaturadasPagas(true);
			List<TabPropostaNewObj> listPropostaFaturadasEmAberto = listarPropostasFaturadasPagas("DTA");

			mv.addObject("listAgDta", listAgDta);
			mv.addObject("listAgDtaRegistro", listAgDtaRegistro);
			mv.addObject("listAgDtaParametrizacao", listAgDtaParametrizacao);
			mv.addObject("listAgDtaDesembaraco", listAgDtaDesembaraco);
			mv.addObject("listAgDtaCarregamento", listAgDtaCarregamento);
			mv.addObject("listAgDtaInicioTransito", listAgDtaInicioTransito);
			mv.addObject("listAgDtaChegada", listAgDtaChegada);
			mv.addObject("listAgDtaConclusao", listAgDtaConclusao);
			mv.addObject("listDtaConclusao", listDtaConclusao);

			mv.addObject("listUploadOK", listPropostaUploadOK);

			mv.addObject("listPropostaDoctosPendentes", listPropostaDoctosPendentes);

			mv.addObject("listUploadOKReconferencia", listPropostaUploadOKReconferencia);

			mv.addObject("listDoctosAceitosSemDesistenciaVistoria", listPropostaDoctosAceitosSemDesistenciaVistoria);

			// mv.addObject("listPropostaFaturadasPagas", listPropostaFaturadasPagas);
			mv.addObject("listPropostaFaturadasEmAberto", listPropostaFaturadasEmAberto);

		}
		// geraboletos();

		// Properties properties = System.getProperties();
		// System.out.println(properties.toString());

		return mv;

	}

	private String getStrSql() {
		
		String StrSql = "select distinct t from TabPropostaNewObj t "
				+ "join fetch t.tabUsuarioObj tabUsuarioObj  "
				+ "join fetch t.tabOrigemObj tabOrigemObj  " 
				+ "join fetch t.tabDestinoObj tabDestinoObj  "
				+ "left join fetch t.tabOrigemLtlObj tabOrigemLtlObj  "
				+ "left join fetch t.tabDestinoLtlObj tabDestinoLtlObj  "
				+ "left join fetch t.tabStatusObj tabStatusObj  "
				+ "left join fetch t.tabTipoCarregamentoObj tabTipoCarregamentoObj  "
				+ "left join fetch t.tabFreehandObj tabFreehandObj  "
				+ "join fetch t.tabClienteObj tabClienteObj  "				
				+ "join fetch t.listPropostaGrupoAcesso tg ";
				
		
		return StrSql;
		
	}
	
	private List<TabPropostaNewObj> executaQuery(Integer cdStatus, String txOrdem, String txAscDesc, String txAndWhere) {

		// Criteria criteria =
		// entityManager.unwrap(Session.class).createCriteria(VwTabPropostaDashboardObj.class);

		// adicionarFiltro(cdStatus, txOrdem, criteria, txAscDesc);
		DadosUser user = tabUsuarioService.DadosUsuario();

		String StrSql = getStrSql() 
				+ "where tg.cdGrupoAcesso = " + user.getCdGrupoAcesso()
				+ " and t.tabStatusObj.cdStatus = " + cdStatus + txAndWhere + " order by t."
				+ txOrdem;

		List<TabPropostaNewObj> list = (List<TabPropostaNewObj>) transactionUtilBean.querySelect(StrSql,
				TabPropostaNewObj.class);

		// return criteria.list();
		return list;

	}

	private List<TabPropostaNewObj> executaQueryPropostaDoctosPendentes(String TxCheckUploadDoctos) {

		DadosUser user = tabUsuarioService.DadosUsuario();

		String StrSql = getStrSql() 
				+ "where tg.cdGrupoAcesso = " + user.getCdGrupoAcesso()
				+ " and t.txCheckUploadDoctos like '%" + TxCheckUploadDoctos + "%' order by t.dtProposta";

		List<TabPropostaNewObj> list = (List<TabPropostaNewObj>) transactionUtilBean.querySelect(StrSql,
				TabPropostaNewObj.class);

		// return criteria.list();
		return list;

	}

	private List<TabPropostaNewObj> PropostaUploadOK(Integer ckUploadDoctos) {

		DadosUser user = tabUsuarioService.DadosUsuario();

		String StrSql = getStrSql() 
				+ "where tg.cdGrupoAcesso = " + user.getCdGrupoAcesso()
				+ " and t.ckUploadDoctos = " + ckUploadDoctos 
				+ " and t.dtLiberacaoDocumentos is null and t.txCheckUploadDoctos not like '%pendência%' and t.ckReconferencia = 0 and (t.txBl is not null or t.txReserva is not null) and t.txTerminalMar = 'Terminal'";

		List<TabPropostaNewObj> list = (List<TabPropostaNewObj>) transactionUtilBean.querySelect(StrSql,
				TabPropostaNewObj.class);

		// return criteria.list();
		return list;

	}

	private void adicionarFiltro(Integer cdStatus, String txOrdem, Criteria criteria, String txAscDesc) {

		DadosUser user = tabUsuarioService.DadosUsuario();

		criteria.add(Restrictions.eq("cdGrupoAcesso", user.getCdGrupoAcesso()));

		criteria.add(Restrictions.eq("cdStatus", cdStatus));

		criteria.add(Restrictions.eq("ckDesistenciaVistoria", 1));

		if (cdStatus == 12) {

			Date dt = GeneralParser.retornaDataAnteriorDiasCorridos(new Date(), 10);
			Date dtFinal = GeneralParser.retornaDataPosteriorDiasCorridos(new Date(), 3);
			criteria.add(Restrictions.between("dtDtaConclusaoTransito", dt, dtFinal));
		}

		if (txAscDesc.equals("Asc")) {
			criteria.addOrder(Order.asc(txOrdem));
		} else {
			criteria.addOrder(Order.desc(txOrdem));
		}

	}

	public List<VwTabPropostaDashboardObj> listarPropostaDoctosPendentes(String txCheckUploadDoctos) {

		DadosUser user = tabUsuarioService.DadosUsuario();

		return vwTabPropostaDashboardRepository.findByTxCheckUploadDoctosContainingAndCdGrupoAcessoOrderByDtPropostaAsc(
				txCheckUploadDoctos, user.getCdGrupoAcesso());

	}

	public List<VwTabPropostaDashboardObj> listarPropostaUploadOK(Integer ckUploadDoctos) {

		DadosUser user = tabUsuarioService.DadosUsuario();

		return vwTabPropostaDashboardRepository.findByListCkUploadOKQuery(ckUploadDoctos, user.getCdGrupoAcesso());

	}

	public List<TabPropostaNewObj> listarPropostaUploadOKReconferencia(Integer ckUploadDoctos) {

		DadosUser user = tabUsuarioService.DadosUsuario();

		String StrSql = getStrSql() 
				+ "where tg.cdGrupoAcesso = " + user.getCdGrupoAcesso()
				+ " and t.ckUploadDoctos = " + ckUploadDoctos
				+ " and t.dtLiberacaoDocumentos is null and t.txCheckUploadDoctos not like '%pendência%' and t.ckReconferencia = 1 and (t.txBl is not null or t.txReserva is not null) and t.txTerminalMar = 'Terminal' order by t.dtProposta";

		List<TabPropostaNewObj> list = (List<TabPropostaNewObj>) transactionUtilBean.querySelect(StrSql,
				TabPropostaNewObj.class);

		return list;

	}

	public List<TabPropostaNewObj> listarPropostaDoctosAceitosSemDesisteciaVistoria() {

		DadosUser user = tabUsuarioService.DadosUsuario();

		String StrSql = getStrSql() 
				+ "where tg.cdGrupoAcesso = " + user.getCdGrupoAcesso()
				+ " and t.dtLiberacaoDocumentos is not null and t.ckDesistenciaVistoria = 2 order by t.dtLiberacaoDocumentos";

		List<TabPropostaNewObj> list = (List<TabPropostaNewObj>) transactionUtilBean.querySelect(StrSql,
				TabPropostaNewObj.class);

		return list;
	}

	public List<TabPropostaNewObj> listarPropostasFaturadasPagas(String txTipoProposta) {

		DadosUser user = tabUsuarioService.DadosUsuario();

		String StrSql = getStrSql() 
				+ "where tg.cdGrupoAcesso = " + user.getCdGrupoAcesso()
				+ " and t.dtBoletoVencto is not null and t.dtBoletoPagto is null and t.ckBoletoCancelado is null and t.txTipoProposta = '"
				+ txTipoProposta + "'";

		List<TabPropostaNewObj> list = (List<TabPropostaNewObj>) transactionUtilBean.querySelect(StrSql,
				TabPropostaNewObj.class);

		return list;
	}
}
