package br.com.jtpsolution.modulos.robodta.obj;

import java.io.Serializable;
import java.time.LocalDateTime;


public class Carregamento implements Serializable{

	private String dtCarregamento;
	private String txInterveniente;
	
	public String getTxInterveniente() {
		return txInterveniente;
	}
	public void setTxInterveniente(String txInterveniente) {
		this.txInterveniente = txInterveniente;
	}
	public String getDtCarregamento() {
		return dtCarregamento;
	}
	public void setDtCarregamento(String dtCarregamento) {
		this.dtCarregamento = dtCarregamento;
	}
	
	
}
