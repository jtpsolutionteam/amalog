package br.com.jtpsolution.dao.cadastros.varios.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.cadastros.varios.TabOrigemLtlImpexpObj;



public interface TabOrigemLtlImpexpRepository extends JpaRepository<TabOrigemLtlImpexpObj, Integer> {

	@Query("select t from TabOrigemLtlImpexpObj t where t.cdImpexp = ?1 and t.tabOrigemLtlObj.ckAtivo = ?2 order by t.tabOrigemLtlObj.txOrigemLtl")
	List<TabOrigemLtlImpexpObj> findByCkAtivoQuery(Integer cdImpexp, Integer ckAtivo);

}