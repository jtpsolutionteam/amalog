package br.com.jtpsolution.dao.propostas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.propostas.TabPropostaNovaObj;



public interface TabPropostaNovaRepository extends JpaRepository<TabPropostaNovaObj, Integer> {

	@Query("select distinct t from TabPropostaNovaObj t "
			+ "left join fetch t.tabUsuarioObj tabUsuarioObj  "
			+ "left join fetch t.tabOrigemObj tabOrigemObj  " 
			+ "left join fetch t.tabDestinoObj tabDestinoObj  "
			+ "left join fetch t.tabOrigemLtlObj tabOrigemLtlObj  "
			+ "left join fetch t.tabDestinoLtlObj tabDestinoLtlObj  "
			+ "left join fetch t.tabStatusObj tabStatusObj  "
			+ "left join fetch t.tabTipoCarregamentoObj tabTipoCarregamentoObj  "
			+ "left join fetch t.tabFreehandObj tabFreehandObj  "
			+ "join fetch t.tabClienteObj tabClienteObj  " 	
			+ "join fetch t.listPropostaGrupoAcesso tg "
			+ "where t.cdProposta = ?1 and tg.cdGrupoAcesso = ?2")
	TabPropostaNovaObj findByCdPropostaAndCdGrupoAcessoQuery(Integer cdProposta, Integer cdGrupoAcesso);

	@Query("select distinct t from TabPropostaNovaObj t "
			+ "left join fetch t.tabUsuarioObj tabUsuarioObj  "
			+ "left join fetch t.tabOrigemObj tabOrigemObj  " 
			+ "left join fetch t.tabDestinoObj tabDestinoObj  "
			+ "left join fetch t.tabOrigemLtlObj tabOrigemLtlObj  "
			+ "left join fetch t.tabDestinoLtlObj tabDestinoLtlObj  "
			+ "left join fetch t.tabStatusObj tabStatusObj  "
			+ "left join fetch t.tabTipoCarregamentoObj tabTipoCarregamentoObj  "
			+ "left join fetch t.tabFreehandObj tabFreehandObj  "
			+ "join fetch t.tabClienteObj tabClienteObj  " 	
			+ "where t.cdProposta = ?1")
	TabPropostaNovaObj findByCdPropostaQuery(Integer cdProposta);
	
}