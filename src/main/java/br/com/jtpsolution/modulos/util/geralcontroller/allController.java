package br.com.jtpsolution.modulos.util.geralcontroller;

import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.administracao.TabCamposIdiomaObj;
import br.com.jtpsolution.dao.administracao.TabMenuSubObj;
import br.com.jtpsolution.dao.administracao.TabTelasObj;
import br.com.jtpsolution.dao.administracao.vw.VwTabBotaoObj;
import br.com.jtpsolution.dao.administracao.vw.VwTabCamposObj;
import br.com.jtpsolution.dao.administracao.vw.VwTabMenuObj;
import br.com.jtpsolution.modulos.administrador.service.TabBotaoService;
import br.com.jtpsolution.modulos.administrador.service.TabCamposIdiomaService;
import br.com.jtpsolution.modulos.administrador.service.TabCamposService;
import br.com.jtpsolution.modulos.administrador.service.TabMenuService;
import br.com.jtpsolution.modulos.administrador.service.TabTelasService;
import br.com.jtpsolution.security.DadosUser;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.Validator;

public class allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabTelasService tabTelasService;

	@Autowired
	private TabMenuService tabMenuService;
	
	@Autowired
	private TabCamposIdiomaService tabCamposIdiomaService;

	@ModelAttribute("listMenus")
	public List<VwTabMenuObj> listMenus() {

		DadosUser user = tabUsuariosService.DadosUsuario();

		List<VwTabMenuObj> list = user.getListTabMenu();
		return list;

	}
	
	
	public void listCamposIdioma(ModelAndView mv) {
		
		DadosUser user = tabUsuariosService.DadosUsuario();

		if (!Validator.isBlankOrNull(user.getCdIdioma())) {
		
			List<TabCamposIdiomaObj> list = tabCamposIdiomaService.listarController(getClass().getSimpleName(), user.getCdIdioma());
			
			for (TabCamposIdiomaObj atual : list) {
				
				mv.addObject("lb"+atual.getTxObjCampo(), atual.getTxCampo());
			}
		
		}
	}
	
	public ModelAndView verificaPermissao(String txUrlTela) {
 		
 		ModelAndView mv = new ModelAndView(txUrlTela);
 		
 		String txUrlPermissao = permissaoEndpoint(txUrlTela); 
 		if (!txUrlPermissao.equals(txUrlTela)) {
 			mv = new ModelAndView("redirect:/permissaonegada/"+pathtela());
 		}
 		
 		return mv;
 	}

	public String permissaoEndpoint(String txUrl) {

		DadosUser user = tabUsuariosService.DadosUsuario();

		List<VwTabMenuObj> list = user.getListTabMenu();

		String txEndPoint = Constants.TEMPLATE_PATH_UTIL+"/permissaonegada/permissaonegada";
		for (VwTabMenuObj atual : list) {

			if (!Validator.isBlankOrNull(atual.getTxUrlPage())) {
				if (txUrl.contains(atual.getTxUrlPage())) {
					txEndPoint = txUrl;
					break;
				}
			}else {
				for (TabMenuSubObj atualSub : atual.getListaSubMenus()) {
					if (!Validator.isBlankOrNull(atualSub.getTxUrlPage())) {
						if (txUrl.contains(atualSub.getTxUrlPage())) {
							txEndPoint = txUrl;
							break;
						}
					}
				}
			}
		}

		return txEndPoint;
	}

	// Dados da Tela/Usuário

	@ModelAttribute("nomeSistema")
	public String NomeSistema() {
		return Constants.NOME_SISTEMA_LOCAL;
	}

	@ModelAttribute("pathtela")
	public String pathtela() {

		String txTela = "Sem cadastro - " + getClass().getSimpleName();
		TabTelasObj Tab = tabTelasService.consultarController(getClass().getSimpleName());
		if (Tab != null) {
			txTela = Tab.getTxTela();
		}
		return txTela;
	}

	@ModelAttribute("cdIdioma")
	public String cdIdioma() {

		DadosUser user = tabUsuariosService.DadosUsuario();
		
		return user.getCdIdioma() ;
	}
	
	@ModelAttribute("txService")
	public String txService() {

		return getClass().getSimpleName().replace("Controller", "Service");
	}

	@ModelAttribute("txUsuario")
	public String txNomeUsuario() {
		DadosUser user = tabUsuariosService.DadosUsuario();
		return user.getTxApelido().toUpperCase();
	}

	@ModelAttribute("cdGrupoVisao")
	public Integer cdGrupoVisao() {
		DadosUser user = tabUsuariosService.DadosUsuario();
		return user.getCdGrupoVisao();
	}
	
	@ModelAttribute("cdGrupoAcesso")
	public Integer cdGrupoAcesso() {
		DadosUser user = tabUsuariosService.DadosUsuario();
		return user.getCdGrupoAcesso();
	}

	// Dados de permissões
	@Autowired
	private TabBotaoService tabBotaoService;

	@ModelAttribute("listPermissaoBotao")
	public @ResponseBody List<VwTabBotaoObj> listPermissaoBotao() {
		DadosUser user = tabUsuariosService.DadosUsuario();

		return tabBotaoService.pesquisaPermissoes(user.getCdGrupoVisao(), getClass().getSimpleName());
	}

	@Autowired
	private TabCamposService tabCamposService;

	@ModelAttribute("listPermissaoCampos")
	public @ResponseBody List<VwTabCamposObj> listPermissaoCampos() {
		DadosUser user = tabUsuariosService.DadosUsuario();

		return tabCamposService.pesquisaPermissoes(user.getCdGrupoVisao(), getClass().getSimpleName());
	}

}
