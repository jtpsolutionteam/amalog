package br.com.jtpsolution.util.criptografia;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
 
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
 
/**
 * It's necessary to set the static attribute char[] PASSWORD. It will be use to encrypt and decrypt
 * a text. This basically means initialing a javax.crypto.Cipher with algorithm 
 * "PBEWithMD5AndDES" and getting a key from javax.crypto.SecretKeyFactory 
 * with the same algorithm.
 * @author Willian Antunes
 * @version 1.0.0
 * @see <a href="http://stackoverflow.com/questions/1132567/encrypt-password-in-configuration-files-java">Encrypt Password in Configuration Files</a>
 */

public class CriptografiaService {

	public static void main(String[] args) throws GeneralSecurityException, IOException {
		// TODO Auto-generated method stub
		
		
		CriptografiaService s = new CriptografiaService();
		
		//System.out.println("Criptografado: "+s.encrypt("fabio é um gayzola"));
		
		System.out.println("Descriptografado: "+s.decrypt("ftPeAQGO9unZ9m5izO1ePymRWAEt2/bk"));
		
		
		
		
	}
	
	
	    private static final char[] PASSWORD = "ColocarSeuPasswordAqui".toCharArray();
	    private static final byte[] SALT = {
	        (byte) 0xde, (byte) 0x33, (byte) 0x10, (byte) 0x12,
	        (byte) 0xde, (byte) 0x33, (byte) 0x10, (byte) 0x12,
	    };
	     
	    private static final Logger logger = LogManager.getLogger(CriptografiaService.class);
	 
	    public static String encrypt(String property) throws GeneralSecurityException, UnsupportedEncodingException 
	    {
	     //logger.debug("Calling method encrypt(String property)...");
	      
	        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
	        SecretKey key = keyFactory.generateSecret(new PBEKeySpec(PASSWORD));
	        Cipher pbeCipher = Cipher.getInstance("PBEWithMD5AndDES");
	        pbeCipher.init(Cipher.ENCRYPT_MODE, key, new PBEParameterSpec(SALT, 20));
	        return base64Encode(pbeCipher.doFinal(property.getBytes("UTF-8")));
	    }
	 
	    public static String decrypt(String property) throws GeneralSecurityException, IOException 
	    {
	     //logger.debug("Calling method decrypt(String property)...");
	      
	        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
	        SecretKey key = keyFactory.generateSecret(new PBEKeySpec(PASSWORD));
	        Cipher pbeCipher = Cipher.getInstance("PBEWithMD5AndDES");
	        pbeCipher.init(Cipher.DECRYPT_MODE, key, new PBEParameterSpec(SALT, 20));
	        return new String(pbeCipher.doFinal(base64Decode(property)), "UTF-8");
	    }
	     
	    private static String base64Encode(byte[] bytes) 
	    {
	     //logger.debug("Calling method base64Encode(byte[] bytes)...");
	      
	     return Base64.getEncoder().encodeToString(bytes);
	    }
	     
	    private static byte[] base64Decode(String property) throws IOException 
	    {
	     //logger.debug("Calling method base64Decode(String property)...");
	      
	        return Base64.getDecoder().decode(property);
	    } 
	}
	


