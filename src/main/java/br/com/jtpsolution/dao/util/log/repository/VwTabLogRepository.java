package br.com.jtpsolution.dao.util.log.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.util.log.VwTabLogObj;




public interface VwTabLogRepository extends JpaRepository<VwTabLogObj, Integer> {

	List<VwTabLogObj> findByTxServiceAndTxCampoAndTxRefOrderByDtDataDesc(String txService, String txCampo, String txRef);
	
	
	
}
