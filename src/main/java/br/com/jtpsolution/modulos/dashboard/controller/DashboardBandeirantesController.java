package br.com.jtpsolution.modulos.dashboard.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.jtpsolution.dao.propostas.VwTabPropostaObj;
import br.com.jtpsolution.modulos.propostas.service.TabPropostaService;
import br.com.jtpsolution.modulos.util.geralcontroller.allController;
import br.com.jtpsolution.security.DadosUser;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralParser;

@Controller
public class DashboardBandeirantesController extends allController {

	@Autowired
	private UsuarioBean tabUsuarioService;

	@Autowired
	private TabPropostaService tabPropostaService;

	@PersistenceContext
	private EntityManager entityManager;

	@RequestMapping("/dashboardbandeirantes")
	public ModelAndView dashboard(HttpServletRequest request) {

		ModelAndView mv = new ModelAndView("dashboard/dashboardbandeirantes");
		mv.addObject("ckChat", false);
		
		
		Integer vlTot = (request.getSession().getAttribute("vlTot") != null
				? GeneralParser.parseInt(request.getSession().getAttribute("vlTot").toString())
				: 1);

		switch (vlTot) {
		case 1: {
			List<VwTabPropostaObj> listAgDta = executaQuery(4, "dtLiberacaoDocumentos", "Asc");
			mv.addObject("listAgDta", listAgDta);
			request.getSession().setAttribute("vlTot", vlTot+1);
			break;
		}
		case 2: {
			List<VwTabPropostaObj> listAgDtaRegistro = executaQuery(5, "dtDtaSolicitacao", "Asc");
			mv.addObject("listAgDtaRegistro", listAgDtaRegistro);
			request.getSession().setAttribute("vlTot", vlTot+1);
			break;
		}
		case 3: {
			List<VwTabPropostaObj> listAgDtaParametrizacao = executaQuery(6, "dtDtaRegistro", "Asc");
			mv.addObject("listAgDtaParametrizacao", listAgDtaParametrizacao);
			request.getSession().setAttribute("vlTot", vlTot+1);
			break;
		}
		case 4: {

			List<VwTabPropostaObj> listAgDtaCarregamento = executaQuery(7, "dtDtaParametrizacao", "Asc");
			mv.addObject("listAgDtaCarregamento", listAgDtaCarregamento);
			request.getSession().setAttribute("vlTot", vlTot+1);
			break;
		}
		case 5: {

			List<VwTabPropostaObj> listAgDtaDesembaraco = executaQuery(9, "dtDtaCarregamento", "Asc");
			mv.addObject("listAgDtaDesembaraco", listAgDtaDesembaraco);
			request.getSession().setAttribute("vlTot", vlTot+1);
			break;
		}
		case 6: {
			List<VwTabPropostaObj> listAgDtaInicioTransito = executaQuery(8, "dtDtaDesembaraco", "Asc");
			mv.addObject("listAgDtaInicioTransito", listAgDtaInicioTransito);
			request.getSession().setAttribute("vlTot", vlTot+1);
			break;
		}
		case 7: {
			List<VwTabPropostaObj> listAgDtaChegada = executaQuery(10, "dtDtaInicioTransito", "Asc");
			mv.addObject("listAgDtaChegada", listAgDtaChegada);
			request.getSession().setAttribute("vlTot", vlTot+1);
			break;
		}
		case 8: {
			List<VwTabPropostaObj> listAgDtaConclusao = executaQuery(11, "dtDtaChegadaTransito", "Asc");
			mv.addObject("listAgDtaConclusao", listAgDtaConclusao);
			request.getSession().setAttribute("vlTot", vlTot+1);
			break;
		}
		case 9: {
			List<VwTabPropostaObj> listDtaConclusao = executaQuery(12, "dtDtaConclusaoTransito", "Desc");
			mv.addObject("listDtaConclusao", listDtaConclusao);
			request.getSession().setAttribute("vlTot", vlTot+1);
			break;
		}
		case 10: {
			request.getSession().setAttribute("vlTot", 1);
			break;
		}

		}

		return mv;

	}

	private List<VwTabPropostaObj> executaQuery(Integer cdStatus, String txOrdem, String txAscDesc) {

		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(VwTabPropostaObj.class);

		adicionarFiltro(cdStatus, txOrdem, criteria, txAscDesc);

		return criteria.list();
	}

	private void adicionarFiltro(Integer cdStatus, String txOrdem, Criteria criteria, String txAscDesc) {

		DadosUser user = tabUsuarioService.DadosUsuario();

		criteria.add(Restrictions.eq("cdGrupoAcesso", user.getCdGrupoAcesso()));

		criteria.add(Restrictions.eq("cdStatus", cdStatus));

		if (txAscDesc.equals("Asc")) {
			criteria.addOrder(Order.asc(txOrdem));
		} else {
			criteria.addOrder(Order.desc(txOrdem));
		}

	}

}
