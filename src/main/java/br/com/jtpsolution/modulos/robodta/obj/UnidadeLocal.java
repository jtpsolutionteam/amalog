package br.com.jtpsolution.modulos.robodta.obj;

import java.io.Serializable;


public class UnidadeLocal implements Serializable{

	
	private String cdUnidade;
	private String txUnidade;
	public String getCdUnidade() {
		return cdUnidade;
	}
	public void setCdUnidade(String cdUnidade) {
		this.cdUnidade = cdUnidade;
	}
	public String getTxUnidade() {
		return txUnidade;
	}
	public void setTxUnidade(String txUnidade) {
		this.txUnidade = txUnidade;
	}
	
	
}
