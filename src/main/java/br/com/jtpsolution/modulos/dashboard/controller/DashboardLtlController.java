package br.com.jtpsolution.modulos.dashboard.controller;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.jtpsolution.dao.propostas.VwTabPropostaNewObj;
import br.com.jtpsolution.dao.propostas.VwTabPropostaObj;
import br.com.jtpsolution.dao.propostas.repository.VwTabPropostaNewRepository;
import br.com.jtpsolution.modulos.propostas.service.TabPropostaService;
import br.com.jtpsolution.modulos.util.geralcontroller.allController;
import br.com.jtpsolution.security.DadosUser;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.teste.teste;


@Controller
public class DashboardLtlController extends allController {

	@Autowired
	private UsuarioBean tabUsuarioService;
	
	@Autowired
	private TabPropostaService tabPropostaService;
	
	@Autowired
	private VwTabPropostaNewRepository vwTabPropostaNewRepository;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private teste test;

	@RequestMapping("/dashboardltl")
	public ModelAndView dashboard() {
		
		
		DadosUser user  = tabUsuarioService.DadosUsuario();
	
		ModelAndView mv = new ModelAndView("dashboard/dashboardltl");

		
		if (user.getCdIdioma().equals("PT")) {
			
	
			List<VwTabPropostaNewObj> listPropostaDoctosPendentes = listarPropostaDoctosPendentes("pendências");
			
			List<VwTabPropostaNewObj> listPropostaUploadOK = listarPropostaUploadOK(1);
	
			List<VwTabPropostaNewObj> listPropostaUploadOKReconferencia = listarPropostaUploadOKReconferencia(1);
			
			List<VwTabPropostaNewObj> listPropostaFaturadasEmAberto = listarPropostasFaturadasPagas(false);
			
			mv.addObject("listUploadOK", listPropostaUploadOK);
			mv.addObject("listUploadOKReconferencia", listPropostaUploadOKReconferencia);
			
			mv.addObject("listPropostaDoctosPendentes", listPropostaDoctosPendentes);
	
			mv.addObject("listPropostaFaturadasEmAberto", listPropostaFaturadasEmAberto);
			
			
			List<VwTabPropostaNewObj> listAgDta = executaQuery(14, "dtLiberacaoDocumentos","Asc");
			mv.addObject("listAgDta", listAgDta);
			
		}
      
		return mv;
		
		
	}
	
	
	
	private List<VwTabPropostaNewObj> executaQuery(Integer cdStatus, String txOrdem, String txAscDesc) {
 		
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(VwTabPropostaNewObj.class);
		
		adicionarFiltro(cdStatus, txOrdem, criteria, txAscDesc);
 		
 		return criteria.list();
 	}

	
	
	private void adicionarFiltro(Integer cdStatus, String txOrdem, Criteria criteria, String txAscDesc) {
		
		  DadosUser user  = tabUsuarioService.DadosUsuario();	
		  
	      criteria.add(Restrictions.eq("cdGrupoAcesso", user.getCdGrupoAcesso()));
		  
		  criteria.add(Restrictions.eq("cdStatus", cdStatus));
	      
		  if (txAscDesc.equals("Asc")) {
		    criteria.addOrder(Order.asc(txOrdem));
		  }else {
			criteria.addOrder(Order.desc(txOrdem));
		  }
		  
	  }

	
	public List<VwTabPropostaNewObj> listarPropostaDoctosPendentes(String txCheckUploadDoctos) {
		
		DadosUser user  = tabUsuarioService.DadosUsuario();
		
		return vwTabPropostaNewRepository.findByTxCheckUploadDoctosContainingAndTxTipoPropostaAndCdGrupoAcessoOrderByDtPropostaAsc(txCheckUploadDoctos, "LTL", user.getCdGrupoAcesso());
		
	}
	

	public List<VwTabPropostaNewObj> listarPropostaUploadOK(Integer ckUploadDoctos) {
		
		DadosUser user  = tabUsuarioService.DadosUsuario();
		
		return vwTabPropostaNewRepository.findByListCkUploadOKLtlQuery(ckUploadDoctos, user.getCdGrupoAcesso());
		
	}
	
	public List<VwTabPropostaNewObj> listarPropostaUploadOKReconferencia(Integer ckUploadDoctos) {
		
		DadosUser user  = tabUsuarioService.DadosUsuario();
		
		return vwTabPropostaNewRepository.findByListCkUploadOKReconfereciaLtlQuery(ckUploadDoctos, user.getCdGrupoAcesso());
		
	}
	
	
	public List<VwTabPropostaNewObj> listarPropostasFaturadasPagas(boolean ckPagas) {
		
		DadosUser user  = tabUsuarioService.DadosUsuario();
		
		if (ckPagas) {
			return vwTabPropostaNewRepository.findByListDtBoletoPagtoPagosQuery(user.getCdGrupoAcesso(), "LTL");
		}else {
			return vwTabPropostaNewRepository.findByListDtBoletoPagtoEmAbertoQuery(user.getCdGrupoAcesso(), "LTL");	
		}
		
		
	}
}
