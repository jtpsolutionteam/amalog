package br.com.jtpsolution.modulos.propostas.controller;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.cadastros.varios.TabStatusObj;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabStatusRepository;
import br.com.jtpsolution.dao.propostas.TabPropostaObj;
import br.com.jtpsolution.dao.propostas.VwTabPropostaObj;
import br.com.jtpsolution.modulos.propostas.bean.TabFiltroPropostaObj;
import br.com.jtpsolution.modulos.propostas.service.TabPropostaService;
import br.com.jtpsolution.modulos.util.geralcontroller.allController;
import br.com.jtpsolution.security.DadosUser;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.regras.RegrasBean;

@Controller
@RequestMapping("/propostabaixaarmazenagem")
public class TabPropostaBaixaArmazenagemController extends allController {


	@Autowired
	private TabPropostaService tabService;
	
	@Autowired
	private RegrasBean regrasBean;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private UsuarioBean tabUsuarioService;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_PROPOSTA+"/propostabaixaarmazenagem";
	
 	@GetMapping	
	public ModelAndView pesquisa(TabFiltroPropostaObj tabFiltroPropostaObj, HttpServletRequest request) {
		
		tabFiltroPropostaObj = verificaFiltro(tabFiltroPropostaObj, request);
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject(new TabPropostaObj());			
		mv.addObject("tabFiltroPropostaObj", tabFiltroPropostaObj);
		mv.addObject("listadados", retornoFiltro(tabFiltroPropostaObj));
		
		
		return mv;
	}
	
	private List<?> retornoFiltro(TabFiltroPropostaObj tabFiltroPropostaObj) {
 		
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(VwTabPropostaObj.class);
		
		adicionarFiltro(tabFiltroPropostaObj, criteria);
 		
 		return criteria.list();
 	}


	private TabFiltroPropostaObj verificaFiltro(TabFiltroPropostaObj tabFiltroPropostaObj, HttpServletRequest request) {
		
		try {
			
			if (Validator.isObjetoNull(tabFiltroPropostaObj)) {
				if (request.getSession().getAttribute(getClass().getSimpleName()) != null) {					
					String jsonFiltro = request.getSession().getAttribute(getClass().getSimpleName()).toString();
					ObjectMapper mapper = new ObjectMapper();
					tabFiltroPropostaObj = mapper.readValue(jsonFiltro, TabFiltroPropostaObj.class);	
				}
			}else {
				request.getSession().setAttribute(getClass().getSimpleName(),
						GeneralParser.convertObjJson(tabFiltroPropostaObj));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return tabFiltroPropostaObj;
	}

	
	private void adicionarFiltro(TabFiltroPropostaObj filtro, Criteria criteria) {
		
		  boolean ckFiltro = false;
		  
		  DadosUser user  = tabUsuarioService.DadosUsuario();	
		  
		 // if (user.getCdGrupoVisao() < 3) {
		    criteria.add(Restrictions.eq("cdGrupoAcesso", user.getCdGrupoAcesso()));
		 // }else {
		//	criteria.add(Restrictions.eq("cdUsuario", user.getCdUsuario()));  
		 // }
		  
		  if (filtro != null) {
			  
			  if (!StringUtils.isEmpty(filtro.getTxPropostaFiltro())) {
				  criteria.add(Restrictions.like("txProposta", "%"+filtro.getTxPropostaFiltro()+"%"));
				  ckFiltro = true;
			  }
			  
			  if (!StringUtils.isEmpty(filtro.getTxTipoCampoFiltro()) && !StringUtils.isEmpty(filtro.getDtPropostaInicialFiltro()) && !StringUtils.isEmpty(filtro.getDtPropostaFinalFiltro())) {
				  criteria.add(Restrictions.between(filtro.getTxTipoCampoFiltro(), filtro.getDtPropostaInicialFiltro(), filtro.getDtPropostaFinalFiltro()));
				  ckFiltro = true;
			  }
			 
			  /*
			  if (!ckFiltro) {
				  criteria.add(Restrictions.eq("cdStatus", -1));
			  }*/
			  if (!StringUtils.isEmpty(filtro.getCdStatusFiltro())) {
				  criteria.add(Restrictions.eq("cdStatus", filtro.getCdStatusFiltro()));
			     ckFiltro = true;
			  }else {
				  criteria.add(Restrictions.isNotNull("txNumeroDta"));
				  criteria.add(Restrictions.isNull("dtBaixaPagtoArmazenagem"));
			  }
			  
			  criteria.addOrder(Order.desc("dtProposta"));
		  }
	  }
 	
	
	
	@RequestMapping("/baixapagto/{cdProposta}/{txAcao}")	
	public @ResponseBody String gravarcondicao(@PathVariable Integer cdProposta, @PathVariable String txAcao, HttpServletRequest request) {

		TabPropostaObj Tab = tabService.consultarSimple(cdProposta);
		
		if (Tab != null) {
		
		    if (txAcao.equals("on")) {
				Tab.setDtBaixaPagtoArmazenagem(new Date());
		    }else {
		    	Tab.setDtBaixaPagtoArmazenagem(null);
		    }
		
		    tabService.gravar(Tab, request.getParameter("submitFields"));
		
		}
		
		return "OK";
	}

	
	@RequestMapping("/baixaquimico/{cdProposta}/{txAcao}")	
	public @ResponseBody String baixaquimico(@PathVariable Integer cdProposta, @PathVariable String txAcao, HttpServletRequest request) {

		TabPropostaObj Tab = tabService.consultarSimple(cdProposta);
		
		if (Tab != null) {
		
		    if (txAcao.equals("on")) {
				Tab.setCkQuimico(1);
		    }else {
		    	Tab.setCkQuimico(2);
		    	
		    }
		
		    tabService.gravar(Tab, request.getParameter("submitFields"));
		
		}
		
		return "OK";
	}

	@RequestMapping("/baixaaverbacao/{cdProposta}/{txAcao}")	
	public @ResponseBody String baixaaverbacao(@PathVariable Integer cdProposta, @PathVariable String txAcao, HttpServletRequest request) {

		TabPropostaObj Tab = tabService.consultarSimple(cdProposta);
		
		if (Tab != null) {
		
		    if (txAcao.equals("on")) {
				Tab.setDtAverbacao(new Date());
				if (!Validator.isBlankOrNull(Tab.getDtDtaSolicitacao()) && Validator.isBlankOrNull(Tab.getDtDtaCarregamento())) {
					TabStatusObj tabStatusObj = new TabStatusObj();
					tabStatusObj.setCdStatus(13);												
					Tab.setTabStatusObj(tabStatusObj);
				}
		    }else {
		    	Tab.setDtAverbacao(null);
		    	
		    }
		
		    tabService.gravar(Tab, request.getParameter("submitFields"));
		
		}
		
		return "OK";
	}
	

	@Autowired
	private TabStatusRepository tabStatusRepository;
	
	@ModelAttribute("selectcdstatus")
	public List<TabStatusObj> selectcdstatus() {
		return tabStatusRepository.findAll();
	}
	
}
