package br.com.jtpsolution.modulos.cadastros.documentosclassificacao.service;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.jtpsolution.dao.cadastros.varios.TabDocumentosClassificacaoObj;
import br.com.jtpsolution.dao.cadastros.varios.VwTabDocumentosClassificacaoObj;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabDocumentosClassificacaoRepository;
import br.com.jtpsolution.dao.cadastros.varios.repository.VwTabDocumentosClassificacaoRepository;
import br.com.jtpsolution.dao.util.log.TabLogObj;
import br.com.jtpsolution.exceptions.ErrosConstraintException;
import br.com.jtpsolution.security.DadosUser;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralUtil;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.log.LogBean;
import br.com.jtpsolution.util.logErro.JTPLogErroBean;

@Service
public class TabDocumentosClassificacaoService {

	@Autowired
	private JTPLogErroBean logErroBean;
	
	@Autowired
	private TabDocumentosClassificacaoRepository  tabRepository;
	
	@Autowired
	private VwTabDocumentosClassificacaoRepository  tabVwRepository;
	
	@Autowired
	private UsuarioBean tabUsuarioService;
	
	@Autowired
	private LogBean tabLogBeanService;

	/*
	@Autowired 
	private TabFiltro.....Bean tabFiltroService;
	
	*/
	
	
	public List<VwTabDocumentosClassificacaoObj> listar() {
		return tabVwRepository.findOrderByCdDoctoClassificacaoQuery();
		
	}
	
	public List<VwTabDocumentosClassificacaoObj> listar(Integer cdTipoDta) {
		return tabVwRepository.findOrderByCdClassificacaoTipoDtaValidarQuery(cdTipoDta);
		
	}

	public List<VwTabDocumentosClassificacaoObj> listar(Integer cdTipoDta, Integer ckValidar) {
		return tabVwRepository.findOrderByCdClassificacaoTipoDtaValidarQuery(cdTipoDta, ckValidar);
		
	}

	
	public TabDocumentosClassificacaoObj gravar(TabDocumentosClassificacaoObj Tab) {
		
		boolean ckAlteracao = false;
				
		DadosUser user  = tabUsuarioService.DadosUsuario();

		
		TabDocumentosClassificacaoObj tAtual = new TabDocumentosClassificacaoObj();
		TabDocumentosClassificacaoObj tNovo = Tab;
		
		//tNovo.setCdUsuario(null);
		List<TabLogObj> listaLog = null;
		
		if (Tab.getCdDoctoClassificacao() != null && !Validator.isBlankOrNull(Tab.getCdDoctoClassificacao())) {
			ckAlteracao = true;
			//Log de campos -- Sempre antes da gravação por causa da persistencia.
			tAtual = tabRepository.findOne(Tab.getCdDoctoClassificacao());
			//listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
			listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdDoctoClassificacao()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
		}
		
		  try {	
		   tNovo = tabRepository.save(tNovo);			  
		  }catch (ConstraintViolationException ex) {
			  
			 throw new ErrosConstraintException(GeneralUtil.MessageExceptionConstraint(ex), ex.getConstraintViolations());			  
		  }
		  
			//Log de campos
		  if (!ckAlteracao) {
			//tabLogBeanService.LogBean(String.valueOf(tNovo.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  	tabLogBeanService.LogBean(String.valueOf(tNovo.getCdDoctoClassificacao()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  }else {
			  tabLogBeanService.GravarListaLog(listaLog);
		  }
		return tNovo;
		
	}

	public TabDocumentosClassificacaoObj consultar(Integer cdDoctoClassificacao) {
	  TabDocumentosClassificacaoObj Tab = tabRepository.findOne(cdDoctoClassificacao);
	 
	   return Tab;
	 
	}

	
		public void excluir(Integer cdDoctoClassificacao) {

		  
		   tabRepository.delete(cdDoctoClassificacao);
			 
		}
		
	

	
}
