package br.com.jtpsolution.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.cadastros.cliente.TabClienteObj;
import br.com.jtpsolution.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_origem", schema = Constants.SCHEMA)
public class TabOrigemObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_origem")
	// @NotNull(message = "Origem campo obrigatório!")
	private Integer cdOrigem;

	@Column(name = "tx_origem")
	@NotEmpty(message = "Origem campo obrigatório!")
	@Size(max = 45, message = "Origem tamanho máximo de 45 caracteres")
	private String txOrigem;

	@Column(name = "ck_ativo")
	// @NotNull(message = "Origem campo obrigatório!")
	private Integer ckAtivo;
	
	@Column(name = "vl_dta_importacao")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlDtaImportacao;
	
	@Column(name = "vl_exportacao")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlExportacao;
	
	@Column(name = "ck_rota_amalog")
	// @NotNull(message = "Origem campo obrigatório!")
	private Integer ckRotaAmalog;
	
	@ManyToOne
	@JoinColumn(name = "cd_armazem")
	private TabClienteObj tabArmazemObj;
	
	
	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txOrigem))
			txOrigem = txOrigem.toUpperCase();
	}

}
