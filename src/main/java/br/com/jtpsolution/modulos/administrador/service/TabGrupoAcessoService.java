package br.com.jtpsolution.modulos.administrador.service;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.jtpsolution.dao.administracao.TabGrupoAcessoObj;
import br.com.jtpsolution.dao.administracao.repository.TabGrupoAcessoRepository;
import br.com.jtpsolution.dao.util.log.TabLogObj;
import br.com.jtpsolution.exceptions.ErrosConstraintException;
import br.com.jtpsolution.security.DadosUser;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralUtil;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.log.LogBean;
import br.com.jtpsolution.util.logErro.JTPLogErroBean;

@Service
public class TabGrupoAcessoService {

	@Autowired
	private JTPLogErroBean logErroBean;
	
	@Autowired
	private TabGrupoAcessoRepository  tabRepository;
	

	@Autowired
	private UsuarioBean tabUsuarioService;
	
	@Autowired
	private LogBean tabLogBeanService;


	
	
	public List<TabGrupoAcessoObj> listar() {
		return tabRepository.findListGrupoAcessoQuery();
		
	}
	
	/* Modelo tela filho 
	
		public List<VwTabGrupoAcessoObj> listar(String CampoRelacionamento) {

		DadosUser user  = tabUsuarioService.DadosUsuario();

		return tabVwRepository.findByTxNrefQuery(CampoRelacionamento, user.getCdGrupoAcesso());
		
	}
	*/
	
	/* Modelo - Tela Pesquisar
			public Page<VwTabGrupoAcessoObj> telapesquisar(TabFiltroObj tabFiltroObj, Pageable pageable) {

			Page<VwTabGrupoAcessoObj> Tab = tabFiltroService.filtrar(tabFiltroObj, pageable);
			
			return Tab;

		}
	*/


	public TabGrupoAcessoObj gravar(TabGrupoAcessoObj Tab) {
		
		boolean ckAlteracao = false;
				
		DadosUser user  = tabUsuarioService.DadosUsuario();

		
		TabGrupoAcessoObj tAtual = new TabGrupoAcessoObj();
		TabGrupoAcessoObj tNovo = Tab;
		
		//tNovo.setCdUsuario(null);
		List<TabLogObj> listaLog = null;
		
		if (Tab.getCdGrupoAcesso() != null && !Validator.isBlankOrNull(Tab.getCdGrupoAcesso())) {
			ckAlteracao = true;
			//Log de campos -- Sempre antes da gravação por causa da persistencia.
			tAtual = tabRepository.findOne(Tab.getCdGrupoAcesso());
			//listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
			listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdGrupoAcesso()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
		}
		
		  try {	
		   tNovo = tabRepository.save(tNovo);			  
		  }catch (ConstraintViolationException ex) {
			  
			 throw new ErrosConstraintException(GeneralUtil.MessageExceptionConstraint(ex), ex.getConstraintViolations());			  
		  }
		  
			//Log de campos
		  if (!ckAlteracao) {
			//tabLogBeanService.LogBean(String.valueOf(tNovo.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  	tabLogBeanService.LogBean(String.valueOf(tNovo.getCdGrupoAcesso()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  }else {
			  tabLogBeanService.GravarListaLog(listaLog);
		  }
		return tNovo;
		
	}

	public TabGrupoAcessoObj consultar(Integer cdGrupoAcesso) {
	 TabGrupoAcessoObj Tab = tabRepository.findOne(cdGrupoAcesso);
	 
	   return Tab;
	 
	}

	
	/* Se tela tiver excluir
		public void excluir(Integer CdGrupoAcesso) {

		   DadosUser user  = tabUsuarioService.DadosUsuario();

		   TabGrupoAcessoObj Tab = tabRepository.findOne(CdGrupoAcesso);
		   Tab.setDtCancelamento(new Date());
		   Tab.setCdUsuarioCancelamento(user.getCdUsuario());
		   tabRepository.save(Tab);
			 
		}
		
	*/	
	

	
}
