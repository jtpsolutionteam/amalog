package br.com.jtpsolution.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.jtpsolution.Constants;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_financeiro_status", schema = Constants.SCHEMA)
public class TabFinanceiroStatusObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_financ_status")
	private Integer cdFinancStatus;
	
	@Column(name = "tx_financ_status")
	private String txFinancStatus;
	
	@Column(name = "tx_financ_campo")
	private String txFinancCampo;
	
	
	
}
