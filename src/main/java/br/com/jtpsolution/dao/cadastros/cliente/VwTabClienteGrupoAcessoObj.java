package br.com.jtpsolution.dao.cadastros.cliente;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.util.Validator;

@Entity
@Table(name = "vw_tab_cliente_grupo_acesso", schema = Constants.SCHEMA)
public class VwTabClienteGrupoAcessoObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_cliente_grupo_acesso")
	// @NotNull(message = "ClienteGrupoAcesso campo obrigatório!")
	private Integer cdClienteGrupoAcesso;

	@Column(name = "cd_cliente")
	// @NotNull(message = "Cliente campo obrigatório!")
	private Integer cdCliente;

	@Column(name = "cd_grupo_acesso")
	// @NotNull(message = "GrupoAcesso campo obrigatório!")
	private Integer cdGrupoAcesso;

	@Column(name = "tx_grupo_acesso")
	// @NotEmpty(message = "GrupoAcesso campo obrigatório!")
	@Size(max = 45, message = "GrupoAcesso tamanho máximo de 45 caracteres")
	private String txGrupoAcesso;
	
	@Column(name = "tx_cliente")
	private String txCliente;

	@Column(name = "tx_cnpj")
	private String txCnpj;
	
	@Column(name = "ck_cliente")
	private Integer ckCliente;

	
	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txGrupoAcesso))
			txGrupoAcesso = txGrupoAcesso.toUpperCase();
	}

	public Integer getCdClienteGrupoAcesso() {
		return cdClienteGrupoAcesso;
	}

	public void setCdClienteGrupoAcesso(Integer cdClienteGrupoAcesso) {
		this.cdClienteGrupoAcesso = cdClienteGrupoAcesso;
	}

	public Integer getCdCliente() {
		return cdCliente;
	}

	public void setCdCliente(Integer cdCliente) {
		this.cdCliente = cdCliente;
	}

	public Integer getCdGrupoAcesso() {
		return cdGrupoAcesso;
	}

	public void setCdGrupoAcesso(Integer cdGrupoAcesso) {
		this.cdGrupoAcesso = cdGrupoAcesso;
	}

	public String getTxGrupoAcesso() {
		return txGrupoAcesso;
	}

	public void setTxGrupoAcesso(String txGrupoAcesso) {
		this.txGrupoAcesso = txGrupoAcesso;
	}

	public String getTxCliente() {
		return txCliente;
	}

	public void setTxCliente(String txCliente) {
		this.txCliente = txCliente;
	}

	public String getTxCnpj() {
		return txCnpj;
	}

	public void setTxCnpj(String txCnpj) {
		this.txCnpj = txCnpj;
	}

	public Integer getCkCliente() {
		return ckCliente;
	}

	public void setCkCliente(Integer ckCliente) {
		this.ckCliente = ckCliente;
	}

}
