package br.com.jtpsolution.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.cadastros.varios.TabTipoCarregamentoLtlObj;



public interface TabTipoCarregamentoLtlRepository extends JpaRepository<TabTipoCarregamentoLtlObj, Integer> {



}