package br.com.jtpsolution.modulos.microled.obj;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VwTabEtapasAvBlCntrObj {

	private String txGrPaga;
	
	private String txBloqueioBl;
	
	private String txIcmsEntrega;
	
	private String txIcmsSefaz;
	
	private String txMapa;
	
	private String txBloqueioCNTR;
	
	
	
	
	
}
