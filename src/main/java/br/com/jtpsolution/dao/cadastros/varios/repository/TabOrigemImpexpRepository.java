package br.com.jtpsolution.dao.cadastros.varios.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.cadastros.varios.TabOrigemImpexpObj;



public interface TabOrigemImpexpRepository extends JpaRepository<TabOrigemImpexpObj, Integer> {

	@Query("select t from TabOrigemImpexpObj t where t.cdImpexp = ?1 and t.tabOrigemObj.ckAtivo = ?2 order by t.tabOrigemObj.txOrigem")
	List<TabOrigemImpexpObj> findByCkAtivoQuery(Integer cdImpexp, Integer ckAtivo);

}