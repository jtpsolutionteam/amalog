package br.com.jtpsolution.dao.propostas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.propostas.VwTabPropostaDocumentosObj;



public interface VwTabPropostaDocumentosRepository extends JpaRepository<VwTabPropostaDocumentosObj, Integer> {

	List<VwTabPropostaDocumentosObj> findByCdProposta(Integer cdProposta);

}