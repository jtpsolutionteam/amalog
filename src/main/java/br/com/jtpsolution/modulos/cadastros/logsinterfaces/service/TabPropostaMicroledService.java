package br.com.jtpsolution.modulos.cadastros.logsinterfaces.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.jtpsolution.dao.propostas.TabPropostaMicroledObj;
import br.com.jtpsolution.dao.propostas.repository.TabPropostaMicroledRepository;
import br.com.jtpsolution.security.DadosUser;
import br.com.jtpsolution.security.UsuarioBean;

@Service
public class TabPropostaMicroledService {

	@Autowired
	private TabPropostaMicroledRepository tabRepository;

	@Autowired
	private UsuarioBean tabUsuarioService;

	public TabPropostaMicroledObj gravar(TabPropostaMicroledObj Tab) {

		boolean ckAlteracao = false;

		DadosUser user = tabUsuarioService.DadosUsuario();

		TabPropostaMicroledObj tNovo = tabRepository.save(Tab);

		return tNovo;

	}

	public TabPropostaMicroledObj consultar(Integer CdCodigo) {
		TabPropostaMicroledObj Tab = tabRepository.findOne(CdCodigo);

		return Tab;

	}

}
