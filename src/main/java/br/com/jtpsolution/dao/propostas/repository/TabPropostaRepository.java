package br.com.jtpsolution.dao.propostas.repository;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.propostas.TabPropostaObj;
import br.com.jtpsolution.dao.propostas.VwTabPropostaNewObj;
import br.com.jtpsolution.dao.propostas.VwTabPropostaObj;



public interface TabPropostaRepository extends JpaRepository<TabPropostaObj, Integer> {

	TabPropostaObj findByTxBl(String txBl);

	@Query("select t from TabPropostaObj t where t.cdProposta = ?1")
	List<TabPropostaObj> findByCdPropostaQuery(Integer cdProposta);
	
	@Query("select t from TabPropostaObj t where t.tabTipoCarregamentoObj.cdTipoCarregamento in(1,3) and t.txLote is not null and t.dtAceite is not null and t.dtDtaConclusaoTransito is null")
	List<TabPropostaObj> findByAceiteLoteConclusaoTransitoNullQuery();
	
	@Query("select t from TabPropostaObj t where t.tabTipoCarregamentoObj.cdTipoCarregamento in(1,3) and t.txNumeroDta is not null and t.txNumeroDta <> '' and t.dtDtaConclusaoTransito is null")
	List<TabPropostaObj> findByConclusaoTransitoNullQuery();

	@Query("select t from TabPropostaObj t where t.dtBoletoVencto is not null and t.dtBoletoPagto is null and t.ckBoletoCancelado is null")
	List<TabPropostaObj> findByListDtBoletoPagtoEmAbertoQuery();

	@Query("select t from TabPropostaObj t where t.dtDtaInicioTransito is not null and t.dtBoletoVencto is null")
	List<TabPropostaObj> findByListDtBoletoCandidatosFaturadosQuery();
	
	@Query("select t from TabPropostaObj t where t.tabTipoCarregamentoObj.cdTipoCarregamento in(2,4) and t.txReserva is not null and t.txNumeroDta is not null and t.txReserva = t.txNumeroDta and t.tabStatusObj.cdStatus != 12")
	List<TabPropostaObj> findByListAgConclusaoDATQuery();
	
	@Query("select t from TabPropostaObj t where t.tabTipoCarregamentoObj.cdTipoCarregamento in (1,3) and txTipoProposta = 'DTA' and dtDtaRegistro is not null and dtDtaCarregamento is null and dtDoctosLiberadosRegistroDta is null")
	List<TabPropostaObj> findByListStatusMicroledQuery();

	@Query("select distinct t from TabPropostaObj t "
			+ "left join fetch t.tabUsuarioObj tabUsuarioObj  "
			+ "left join fetch t.tabOrigemObj tabOrigemObj  " 
			+ "left join fetch t.tabDestinoObj tabDestinoObj  "
			+ "left join fetch t.tabOrigemLtlObj tabOrigemLtlObj  "
			+ "left join fetch t.tabDestinoLtlObj tabDestinoLtlObj  "
			+ "left join fetch t.tabStatusObj tabStatusObj  "
			+ "left join fetch t.tabTipoCarregamentoObj tabTipoCarregamentoObj  "
			+ "left join fetch t.tabFreehandObj tabFreehandObj  "
			+ "join fetch t.tabClienteObj tabClienteObj  " 	
			+ "join fetch t.listPropostaGrupoAcesso tg "
			+ "where t.cdProposta = ?1 and tg.cdGrupoAcesso = ?2")
	TabPropostaObj findByCdPropostaAndCdGrupoAcessoQuery(Integer cdProposta, Integer cdGrupoAcesso);
	
	
	@Query("select distinct t from TabPropostaObj t "
			+ "left join fetch t.tabUsuarioObj tabUsuarioObj  "
			+ "left join fetch t.tabOrigemObj tabOrigemObj  " 
			+ "left join fetch t.tabDestinoObj tabDestinoObj  "
			+ "left join fetch t.tabOrigemLtlObj tabOrigemLtlObj  "
			+ "left join fetch t.tabDestinoLtlObj tabDestinoLtlObj  "
			+ "left join fetch t.tabStatusObj tabStatusObj  "
			+ "left join fetch t.tabTipoCarregamentoObj tabTipoCarregamentoObj  "
			+ "left join fetch t.tabFreehandObj tabFreehandObj  "
			+ "join fetch t.tabClienteObj tabClienteObj  " 				
			+ "where t.cdProposta = ?1")
	TabPropostaObj findByCdPropostaNewQuery(Integer cdProposta);

	TabPropostaObj findByTxBlAndTxTipoProposta(String txBl, String txTipoProposta);
	
	TabPropostaObj findByTxReserva(String txReversa);
	
}