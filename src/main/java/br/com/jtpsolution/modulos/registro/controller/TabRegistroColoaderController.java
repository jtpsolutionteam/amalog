package br.com.jtpsolution.modulos.registro.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.cadastros.coloader.TabColoaderDespachanteObj;
import br.com.jtpsolution.dao.cadastros.usuario.TabUsuarioObj;
import br.com.jtpsolution.modulos.registro.service.TabRegistroColoaderService;
import br.com.jtpsolution.modulos.registro.service.TabRegistroService;
import br.com.jtpsolution.security.GeraPass;

@Controller
@RequestMapping("/registrocoloader")
public class TabRegistroColoaderController{


	@Autowired
	private TabRegistroColoaderService tabService;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_REGISTRO+"/registrocoloader";
	
	@GetMapping
	public ModelAndView novo() {
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		mv.addObject(new TabColoaderDespachanteObj());
		return mv;
	}
	
	
	@RequestMapping(value = "/gravar", method = RequestMethod.POST)
	public ModelAndView gravar(@Validated TabColoaderDespachanteObj tabColoaderDespachanteObj, Errors erros) {
		ModelAndView mv = new ModelAndView(txUrlTela);
		
		if (erros.hasErrors()) {
		  mv.addObject("listaErros", erros.getAllErrors());
		  return mv;
		}
		
		TabColoaderDespachanteObj tabColoader = tabService.consultar(tabColoaderDespachanteObj.getTxCnpj());
		
		if (tabColoader != null) {
			mv.addObject("tabColoaderDespachanteObj",tabColoaderDespachanteObj);
			mv.addObject("txMensagem", "CNPJ já cadastrado no sistema!");
			mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
			return mv;	
		}
		
		TabUsuarioObj tabUsuario = tabService.consultarEmail(tabColoaderDespachanteObj.getTxEmail());
			
		if (tabUsuario != null) {
			mv.addObject("tabUsuarioObj",tabColoaderDespachanteObj);
			mv.addObject("txMensagem", "Email já cadastrado no sistema!");
			mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
			return mv;	
		}
		
		if (!tabColoaderDespachanteObj.getTxEmail().equals(tabColoaderDespachanteObj.getTxConfEmail())) {
			mv.addObject("tabColoaderDespachanteObj",tabColoaderDespachanteObj);
			mv.addObject("txMensagem", "Emails diferente!");
			mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
			return mv;			
		}
		

		if (!tabColoaderDespachanteObj.getTxSenha().equals(tabColoaderDespachanteObj.getTxConfSenha())) {
			mv.addObject("tabColoaderDespachanteObj",tabColoaderDespachanteObj);
			mv.addObject("txMensagem", "Senhas diferente!");
			mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
			return mv;						
		}

		TabColoaderDespachanteObj Tab = tabService.gravar(tabColoaderDespachanteObj);
		
		if (Tab != null) {
			
			TabUsuarioObj tabUsu = new TabUsuarioObj();
			tabUsu.setCdGrupoAcesso(34);
			tabUsu.setCdGrupoVisao(7);
			tabUsu.setCkAtivo(1);
			tabUsu.setTxApelido(tabColoaderDespachanteObj.getTxNomesimples());
			tabUsu.setTxCnpj(tabColoaderDespachanteObj.getTxCnpj());
			tabUsu.setTxEmail(tabColoaderDespachanteObj.getTxEmail());
			tabUsu.setTxNome(tabColoaderDespachanteObj.getTxNome());
			tabUsu.setTxSenha(new GeraPass().BCryptPass(tabColoaderDespachanteObj.getTxSenha()));
			tabService.gravar(tabUsu);
			
		}
		
				
	    mv = new ModelAndView("redirect:/login");		
		return mv;
	}
   
	/*
	@RequestMapping(value = "/pesquisar/{txPesquisar}", method = RequestMethod.GET)
	public ModelAndView pesquisar(@PathVariable String txPesquisar) {
	
	   TabUsuarioObj TabView = tabService.pesquisa(txPesquisar);

		ModelAndView mv = new ModelAndView(txUrlTela);	
		if (TabView != null) {
		  mv.addObject("tabUsuarioObj",TabView);
		}else {
		  mv.addObject(new TabUsuarioObj());	
		  mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
		  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		
		return mv;
	}*/
	
}
