package br.com.jtpsolution.modulos.propostas.listener;

import java.util.Date;
import java.util.HashMap;

import javax.persistence.PreUpdate;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.AutowireHelper;
import br.com.jtpsolution.dao.propostas.TabPropostaLogBoletoObj;
import br.com.jtpsolution.dao.propostas.TabPropostaObj;
import br.com.jtpsolution.dao.propostas.repository.TabPropostaLogBoletoRepository;
import br.com.jtpsolution.modulos.iugu.PagamentosIuguService;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.GeneralUtil;
import br.com.jtpsolution.util.Validator;

public class GeraBoletoListener {

	//@Autowired
	PagamentosIuguService pagamentosIuguService = new PagamentosIuguService(); 
	
	@Autowired
	private TabPropostaLogBoletoRepository tabPropostaLogBoletoRepository;
	
	@PreUpdate
	public void geraboleto(TabPropostaObj tabPropostaObj) {
		
		AutowireHelper.autowire(this, this.tabPropostaLogBoletoRepository);
		
		if (!Validator.isBlankOrNull(tabPropostaObj.getDtDtaInicioTransito()) && tabPropostaObj.getCkRotaAmalog() == 1 && Validator.isBlankOrNull(tabPropostaObj.getDtBoletoVencto())) {
			
			//System.out.println("Listener Boleto Iugu");
			
			Date dtVencto = GeneralParser.retornaDataPosteriorDiasCorridosUteis(new Date(), 7);
			
			
			HashMap h = new HashMap();
			h.put("api_token", Constants.IUGU_KEY_PRODUCAO);
			h.put("email", tabPropostaObj.getTxEmailPagador()); //receb.getTx_email()); 
			h.put("due_date", GeneralParser.format_dateBRMySql(dtVencto));
			h.put("payable_with", "bank_slip");				
			h.put("items[][description]", "Amalog Proposta "+tabPropostaObj.getTxProposta());
			h.put("items[][quantity]", "1");
			if (tabPropostaObj.getVlFrete() != null) {
 			  h.put("items[][price_cents]", GeneralUtil.TiraNaonumero(GeneralParser.doubleToStringMoney((tabPropostaObj.getVlFrete()))));				
			}else {
			  h.put("items[][price_cents]", GeneralUtil.TiraNaonumero(GeneralParser.doubleToStringMoney((tabPropostaObj.getVlFretePrevisto()))));	
			}
 			h.put("payer[cpf_cnpj]", GeneralUtil.TiraNaonumero(tabPropostaObj.getTxCnpj()));
			h.put("payer[name]", GeneralParser.convertISO8859ToUTF8(tabPropostaObj.getTxNomePagador()));
			h.put("payer[phone_prefix]", "13");
			h.put("payer[phone]", "32191571");
			
			h.put("payer[custom_variables][name]", "cdProposta");
			h.put("payer[custom_variables][value]", tabPropostaObj.getCdProposta());
			h.put("notification_url", "https://amalog.com.br/retornosiugu/"+tabPropostaObj.getCdProposta());
			
				
			h.put("notification_url", "https://amalog.com.br/retornosiugu/"+tabPropostaObj.getCdProposta());				
			h.put("payer[address][street]", tabPropostaObj.getTxEndereco());
			h.put("payer[address][number]", tabPropostaObj.getTxNumero());
			h.put("payer[address][district]", tabPropostaObj.getTxBairro());
			h.put("payer[address][city]", tabPropostaObj.getTxCidade());
			h.put("payer[address][state]", tabPropostaObj.getTxUf());
			h.put("payer[address][country]", "Brasil");
			h.put("payer[address][zip_code]", tabPropostaObj.getTxCep());
						
			pagamentosIuguService.CriarFatura(h);
			String txFaturaId = pagamentosIuguService.getTxIdFatura();
			
			if (pagamentosIuguService.getTxMensagem().contains("sucesso")) {
				
				tabPropostaObj.setDtBoletoVencto(dtVencto);
				tabPropostaObj.setTxBoletoId(txFaturaId);
				tabPropostaObj.setTxBoletoLinhaDigitavel(pagamentosIuguService.getTxBoletoLinhaDigitavel());
				tabPropostaObj.setTxBoletoUrl(pagamentosIuguService.getTxBoletoUrl());
				tabPropostaObj.setTxBoletoErros(null);
				//System.out.println(txFaturaId);
				
				TabPropostaLogBoletoObj Tab = new TabPropostaLogBoletoObj();
				Tab.setCdProposta(tabPropostaObj.getCdProposta());
				Tab.setDtVencto(dtVencto);
				Tab.setDtEnvio(new Date());
				tabPropostaLogBoletoRepository.save(Tab);
			
			}else {
				
				tabPropostaObj.setTxBoletoErros(pagamentosIuguService.getTxMensagem());
			}
			
			
			
			
		}
		
		
		
	}
	
}
