package br.com.jtpsolution.modulos.iugu.controller;

import java.util.Date;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.jtpsolution.dao.propostas.TabPropostaObj;
import br.com.jtpsolution.dao.propostas.repository.TabPropostaRepository;



@Controller
@RequestMapping("/retornosiugu")
public class IuguRecebimentosController {

	@Autowired
	private TabPropostaRepository tabPropostaRepository;
	
	
	@RequestMapping(value = "/{cdProposta}", method = RequestMethod.POST)
	public ResponseEntity receivedAdesao(@PathVariable Integer cdProposta, HttpServletRequest request) {
		try {
			
			
			Enumeration<?> en = request.getParameterNames();

			String tx = "";
			while (en.hasMoreElements()) {
				String paramName = (String) en.nextElement();
				String paramValue = request.getParameter(paramName);
				
				tx += paramName+"="+paramValue+";";
			}
			
			TabPropostaObj Tab = tabPropostaRepository.findOne(cdProposta);
			
			if (Tab != null) {
				if (tx.contains("data[status]=paid")) {
					Tab.setDtBoletoPagto(new Date());
				}			
			}
			
			Tab.setTxIuguRetornos(tx);
			tabPropostaRepository.save(Tab);
			
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return ResponseEntity.status(HttpStatus.OK).body("OK");
		
	}

	


}
