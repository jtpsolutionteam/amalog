package br.com.jtpsolution.dao.cadastros.varios.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.cadastros.varios.TabLtlObj;



public interface TabLtlRepository extends JpaRepository<TabLtlObj, Integer> {


	@Query("select t from TabLtlObj t where vlDe <= ?1 and tabEmpresaObj.cdEmpresa = ?2 and tabViaTransporteObj.cdViaTransporte = ?3 and tabDestinoLtlObj.cdDestinoLtl = ?4 order by vlDe desc")
	List<TabLtlObj> findByPrecosQuery(double vlPeso, Integer cdEmpresa, Integer cdViaTransporte, Integer cdDestinoLtl);
	
}