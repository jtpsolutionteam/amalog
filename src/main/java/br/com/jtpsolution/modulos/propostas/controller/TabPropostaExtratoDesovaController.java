package br.com.jtpsolution.modulos.propostas.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.propostas.TabPropostaAvariasObj;
import br.com.jtpsolution.dao.propostas.VwTabPropostaObj;
import br.com.jtpsolution.modulos.propostas.service.TabPropostaAvariasService;
import br.com.jtpsolution.modulos.propostas.service.TabPropostaService;
import br.com.jtpsolution.modulos.util.geralcontroller.allController;
import br.com.jtpsolution.security.UsuarioBean;

@Controller
@RequestMapping("/propostaextratodesova")
public class TabPropostaExtratoDesovaController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabPropostaService tabService;
	
	@Autowired
	private TabPropostaAvariasService tabPropostaAvariasService;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_PROPOSTA+"/propostaextratodesova";
	
	
	@RequestMapping(value = "/{txHashProposta}")
	public ModelAndView show(@PathVariable String txHashProposta) {
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		
		VwTabPropostaObj tab = tabService.consultar(txHashProposta);
		mv.addObject("tabPropostaObj", tab);
		
		List<TabPropostaAvariasObj> lstAvarias = tabPropostaAvariasService.listar(tab.getCdProposta());
		mv.addObject("listaAvarias", lstAvarias);
		
		
		return mv;
	}
	

	
}
