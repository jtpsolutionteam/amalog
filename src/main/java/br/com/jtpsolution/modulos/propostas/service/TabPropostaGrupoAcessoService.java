package br.com.jtpsolution.modulos.propostas.service;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.jtpsolution.dao.propostas.TabPropostaGrupoAcessoObj;
import br.com.jtpsolution.dao.propostas.repository.TabPropostaGrupoAcessoRepository;
import br.com.jtpsolution.dao.util.log.TabLogObj;
import br.com.jtpsolution.exceptions.ErrosConstraintException;
import br.com.jtpsolution.security.DadosUser;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralUtil;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.log.LogBean;
import br.com.jtpsolution.util.logErro.JTPLogErroBean;

@Service
public class TabPropostaGrupoAcessoService {

	@Autowired
	private JTPLogErroBean logErroBean;

	@Autowired
	private TabPropostaGrupoAcessoRepository tabRepository;

	@Autowired
	private UsuarioBean tabUsuarioService;

	@Autowired
	private LogBean tabLogBeanService;

	/*
	 * @Autowired private TabFiltro.....Bean tabFiltroService;
	 * 
	 */

	public List<TabPropostaGrupoAcessoObj> listar() {
		return tabRepository.findAll();

	}


	public TabPropostaGrupoAcessoObj gravar(TabPropostaGrupoAcessoObj Tab) {

		boolean ckAlteracao = false;

		DadosUser user = tabUsuarioService.DadosUsuario();

		TabPropostaGrupoAcessoObj tAtual = new TabPropostaGrupoAcessoObj();
		TabPropostaGrupoAcessoObj tNovo = Tab;

		// tNovo.setCdUsuario(null);
		List<TabLogObj> listaLog = null;

		if (Tab.getCdPropostaGrupoAcesso() != null && !Validator.isBlankOrNull(Tab.getCdPropostaGrupoAcesso())) {
			ckAlteracao = true;
			// Log de campos -- Sempre antes da gravação por causa da persistencia.
			tAtual = tabRepository.findOne(Tab.getCdPropostaGrupoAcesso());
			// listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdUsuario()),
			// user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
			listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdPropostaGrupoAcesso()), user.getCdUsuario(),
					getClass().getSimpleName(), 0, 2, tAtual, Tab);
		}

		try {
			tNovo = tabRepository.save(tNovo);
		} catch (ConstraintViolationException ex) {

			throw new ErrosConstraintException(GeneralUtil.MessageExceptionConstraint(ex),
					ex.getConstraintViolations());
		}

		// Log de campos
		if (!ckAlteracao) {
			// tabLogBeanService.LogBean(String.valueOf(tNovo.getCdUsuario()),
			// user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
			tabLogBeanService.LogBean(String.valueOf(tNovo.getCdPropostaGrupoAcesso()), user.getCdUsuario(),
					getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		} else {
			tabLogBeanService.GravarListaLog(listaLog);
		}
		return tNovo;

	}

	public TabPropostaGrupoAcessoObj consultar(Integer CdPropostaGrupoAcesso) {
		TabPropostaGrupoAcessoObj Tab = tabRepository.findOne(CdPropostaGrupoAcesso);

		return Tab;

	}
	
	public TabPropostaGrupoAcessoObj consultarPropostaGrupoAcesso(Integer cdProposta, Integer cdGrupoAcesso, boolean ckColoader) {
		
		if (!ckColoader) {
		  TabPropostaGrupoAcessoObj Tab = tabRepository.findByCdPropostaAndCdGrupoAcesso(cdProposta, cdGrupoAcesso);
		  return Tab;
		}else {
		  TabPropostaGrupoAcessoObj Tab = tabRepository.findByCdPropostaAndCdGrupoAcessoAndCkColoader(cdProposta, cdGrupoAcesso, 1);	
		  return Tab;
		}

	}

	

	/*
	 * Se tela tiver excluir public void excluir(Integer CdPropostaGrupoAcesso) {
	 * 
	 * DadosUser user = tabUsuarioService.DadosUsuario();
	 * 
	 * TabPropostaGrupoAcessoObj Tab = tabRepository.findOne(CdPropostaGrupoAcesso);
	 * Tab.setDtCancelamento(new Date());
	 * Tab.setCdUsuarioCancelamento(user.getCdUsuario()); tabRepository.save(Tab);
	 * 
	 * }
	 * 
	 */

	

}
