package br.com.jtpsolution.modulos.cadastros.documentosclassificacao.service;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.jtpsolution.dao.cadastros.varios.TabDocumentosClassificacaoLtlObj;
import br.com.jtpsolution.dao.cadastros.varios.TabDocumentosClassificacaoObj;
import br.com.jtpsolution.dao.cadastros.varios.VwTabDocumentosClassificacaoLtlObj;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabDocumentosClassificacaoLtlRepository;
import br.com.jtpsolution.dao.cadastros.varios.repository.VwTabDocumentosClassificacaoLtlRepository;
import br.com.jtpsolution.dao.util.log.TabLogObj;
import br.com.jtpsolution.exceptions.ErrosConstraintException;
import br.com.jtpsolution.security.DadosUser;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralUtil;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.log.LogBean;
import br.com.jtpsolution.util.logErro.JTPLogErroBean;

@Service
public class TabDocumentosClassificacaoLtlService {

	@Autowired
	private JTPLogErroBean logErroBean;
	
	@Autowired
	private TabDocumentosClassificacaoLtlRepository  tabRepository;
	
	@Autowired
	private VwTabDocumentosClassificacaoLtlRepository  tabVwRepository;
	
	@Autowired
	private UsuarioBean tabUsuarioService;
	
	@Autowired
	private LogBean tabLogBeanService;

	/*
	@Autowired 
	private TabFiltro.....Bean tabFiltroService;
	
	*/
	
	
	public List<VwTabDocumentosClassificacaoLtlObj> listar() {
		return tabVwRepository.findOrderByCdDoctoClassificacaoQuery();
		
	}
	
	public List<VwTabDocumentosClassificacaoLtlObj> listar(Integer cdTipo) {
		return tabVwRepository.findOrderByCdClassificacaoTipoValidarQuery(cdTipo);
		
	}

	public List<VwTabDocumentosClassificacaoLtlObj> listar(Integer cdTipoDta, Integer ckValidar) {
		return tabVwRepository.findOrderByCdClassificacaoTipoValidarQuery(cdTipoDta, ckValidar);
		
	}

	
	public TabDocumentosClassificacaoLtlObj gravar(TabDocumentosClassificacaoLtlObj Tab) {
		
		boolean ckAlteracao = false;
				
		DadosUser user  = tabUsuarioService.DadosUsuario();

		
		TabDocumentosClassificacaoLtlObj tAtual = new TabDocumentosClassificacaoLtlObj();
		TabDocumentosClassificacaoLtlObj tNovo = Tab;
		
		//tNovo.setCdUsuario(null);
		List<TabLogObj> listaLog = null;
		
		if (Tab.getCdDoctoClassificacao() != null && !Validator.isBlankOrNull(Tab.getCdDoctoClassificacao())) {
			ckAlteracao = true;
			//Log de campos -- Sempre antes da gravação por causa da persistencia.
			tAtual = tabRepository.findOne(Tab.getCdDoctoClassificacao());
			//listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
			listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdDoctoClassificacao()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
		}
		
		  try {	
		   tNovo = tabRepository.save(tNovo);			  
		  }catch (ConstraintViolationException ex) {
			  
			 throw new ErrosConstraintException(GeneralUtil.MessageExceptionConstraint(ex), ex.getConstraintViolations());			  
		  }
		  
			//Log de campos
		  if (!ckAlteracao) {
			//tabLogBeanService.LogBean(String.valueOf(tNovo.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  	tabLogBeanService.LogBean(String.valueOf(tNovo.getCdDoctoClassificacao()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  }else {
			  tabLogBeanService.GravarListaLog(listaLog);
		  }
		return tNovo;
		
	}

	public TabDocumentosClassificacaoLtlObj consultar(Integer cdDoctoClassificacao) {
	  TabDocumentosClassificacaoLtlObj Tab = tabRepository.findOne(cdDoctoClassificacao);
	 
	   return Tab;
	 
	}

	
		public void excluir(Integer cdDoctoClassificacao) {

		  
		   tabRepository.delete(cdDoctoClassificacao);
			 
		}
		
	

	
}
