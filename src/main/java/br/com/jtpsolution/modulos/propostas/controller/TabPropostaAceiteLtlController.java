package br.com.jtpsolution.modulos.propostas.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.modulos.propostas.service.TabPropostaService;
import br.com.jtpsolution.modulos.util.geralcontroller.allController;
import br.com.jtpsolution.security.UsuarioBean;

@Controller
@RequestMapping("/propostaaceiteltl")
public class TabPropostaAceiteLtlController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabPropostaService tabService;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_PROPOSTA+"/propostaaceiteltl";
	
	
	@GetMapping
	public ModelAndView show() {
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		
		return mv;
	}
	

	
}
