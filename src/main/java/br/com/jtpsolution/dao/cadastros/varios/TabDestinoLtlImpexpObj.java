package br.com.jtpsolution.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import br.com.jtpsolution.Constants;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_destino_ltl_impexp", schema = Constants.SCHEMA)
public class TabDestinoLtlImpexpObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_destino_impexp")
	// @NotNull(message = "DestinoImpexp campo obrigatório!")
	private Integer cdDestinoImpexp;

	@ManyToOne
	@JoinColumn(name = "cd_destino_ltl")
	private TabDestinoLtlObj tabDestinoLtlObj;

	@Column(name = "cd_impexp")
	@NotNull(message = "Impexp campo obrigatório!")
	private Integer cdImpexp;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
	}

}
