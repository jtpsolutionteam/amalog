package br.com.jtpsolution.modulos.robodta.obj;

import java.io.Serializable;


public class RecintoAduaneiro implements Serializable{


	private String cdRecinto;
	private String txRecinto;
	public String getCdRecinto() {
		return cdRecinto;
	}
	public void setCdRecinto(String cdRecinto) {
		this.cdRecinto = cdRecinto;
	}
	public String getTxRecinto() {
		return txRecinto;
	}
	public void setTxRecinto(String txRecinto) {
		this.txRecinto = txRecinto;
	}
	
	
	
}
