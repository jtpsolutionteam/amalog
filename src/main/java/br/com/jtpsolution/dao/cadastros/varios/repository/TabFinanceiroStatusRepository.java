package br.com.jtpsolution.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.cadastros.varios.TabFinanceiroStatusObj;



public interface TabFinanceiroStatusRepository extends JpaRepository<TabFinanceiroStatusObj, Integer> {

	

}