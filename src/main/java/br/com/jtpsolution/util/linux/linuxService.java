package br.com.jtpsolution.util.linux;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class linuxService {

	public static void main(String[] args) {
		
		
		
	}
	
	
	public String consultarServico(String command) throws IOException {
		
		String txServico = "";
	         
	        final ArrayList<String> commands = new ArrayList<String>();
	        commands.add("/bin/bash");
	        commands.add("-c");
	        commands.add(command);
	         
	        BufferedReader br = null;        
	         
	        try {                        
	            final ProcessBuilder p = new ProcessBuilder(commands);
	            final Process process = p.start();
	            final InputStream is = process.getInputStream();
	            final InputStreamReader isr = new InputStreamReader(is);
	            br = new BufferedReader(isr);
	             
	            String line;            
	            while((line = br.readLine()) != null) {
	            	if (line.contains("pts/0")) {
	                  txServico = line;
	            	}
	            }
	        } catch (IOException ioe) {
	            System.out.println("Erro ao executar comando shell" + ioe.getMessage());
	            throw ioe;
	        } finally {
	            secureClose(br);
	        }
		
	        
	      return txServico;  
	        
	}

	
	  private void secureClose(final Closeable resource) {
	        try {
	            if (resource != null) {
	                resource.close();
	            }
	        } catch (IOException ex) {
	            //log.severe("Erro = " + ex.getMessage());
	        }
	    }
	
}
