package br.com.jtpsolution.modulos.administrador.controller;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.administracao.TabCamposIdiomaObj;
import br.com.jtpsolution.dao.administracao.TabTelasObj;
import br.com.jtpsolution.mensagens.TabRetornoSucessoObj;
import br.com.jtpsolution.modulos.administrador.bean.TabFiltroCamposIdiomaObj;
import br.com.jtpsolution.modulos.administrador.service.TabCamposIdiomaService;
import br.com.jtpsolution.modulos.administrador.service.TabTelasService;
import br.com.jtpsolution.modulos.util.geralcontroller.allController;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.regras.RegrasBean;

@Controller
@RequestMapping("/admcamposidioma")
public class TabCamposIdiomaController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabCamposIdiomaService tabService;

	@Autowired
	private RegrasBean regrasBean;

	@PersistenceContext
	private EntityManager entityManager;

	private String txUrlTela = Constants.TEMPLATE_PATH_ADM + "/admcamposidioma";

	@GetMapping	
	public ModelAndView pesquisa(TabFiltroCamposIdiomaObj tabFiltroCamposIdiomaObj, HttpServletRequest request) {

		tabFiltroCamposIdiomaObj = verificaFiltro(tabFiltroCamposIdiomaObj, request);
		
		ModelAndView mv = new ModelAndView(txUrlTela);		
		mv.addObject(new TabCamposIdiomaObj());
		mv.addObject("tabFiltroCamposIdiomaObj", tabFiltroCamposIdiomaObj);
		mv.addObject("listadados", retornoFiltro(tabFiltroCamposIdiomaObj));
		mv.addObject("cdTela", tabFiltroCamposIdiomaObj.getCdTelaFiltro());
		return mv;
	}
	
	

 	
	private TabFiltroCamposIdiomaObj verificaFiltro(TabFiltroCamposIdiomaObj tabFiltroCamposIdiomaObj, HttpServletRequest request) {
		
		try {
			
			if (Validator.isObjetoNull(tabFiltroCamposIdiomaObj)) {
				if (request.getSession().getAttribute(getClass().getSimpleName()) != null) {					
					String jsonFiltro = request.getSession().getAttribute(getClass().getSimpleName()).toString();
					ObjectMapper mapper = new ObjectMapper();
					tabFiltroCamposIdiomaObj = mapper.readValue(jsonFiltro, TabFiltroCamposIdiomaObj.class);	
				}
			}else {
				request.getSession().setAttribute(getClass().getSimpleName(),
						GeneralParser.convertObjJson(tabFiltroCamposIdiomaObj));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return tabFiltroCamposIdiomaObj;
	}

	private List<?> retornoFiltro(TabFiltroCamposIdiomaObj tabFiltroCamposIdiomaObj) {

		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(TabCamposIdiomaObj.class);

		adicionarFiltro(tabFiltroCamposIdiomaObj, criteria);

		return criteria.list();
	}

	private void adicionarFiltro(TabFiltroCamposIdiomaObj filtro, Criteria criteria) {

		boolean ckFiltro = false;

		if (filtro != null) {

			if (filtro.getCdTelaFiltro() != null) {
				criteria.add(Restrictions.eq("tabTelasObj.cdTela", filtro.getCdTelaFiltro()));
				ckFiltro = true;
			}

			if (!ckFiltro) {
				criteria.add(Restrictions.eq("tabTelasObj.cdTela", 0));
			}

			criteria.addOrder(Order.asc("txCampo"));
		}
	}

	@RequestMapping(value = "/gravarmodal", method = RequestMethod.POST)
	public @ResponseBody List<?> gravarmodal(@Validated TabCamposIdiomaObj tabCamposIdiomaObj, Errors erros,
			HttpServletRequest request) {

		List<ObjectError> error = new ArrayList<ObjectError>();
		if (erros.hasErrors()) {
			// mv.addObject("listaErros", erros.getAllErrors());
			error.addAll(erros.getAllErrors());
			return error;
		} else {
			error = regrasBean.verificaregras(getClass().getSimpleName(), request);
			if (!error.isEmpty()) {
				return error;
			}
		}

		TabCamposIdiomaObj Tab = tabService.gravar(tabCamposIdiomaObj);

		List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();
		TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
		tSuccess.setCd_mensagem(1);
		tSuccess.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
		success.add(tSuccess);

		return success;
	}

	@RequestMapping(value = "/novo/{cdTela}")
	public ModelAndView novo(@PathVariable Integer cdTela, 
			HttpServletRequest request) {
		
		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject(new TabCamposIdiomaObj());
		mv.addObject("tabFiltroCamposIdiomaObj", verificaFiltro(new TabFiltroCamposIdiomaObj(), request));
		mv.addObject("listadados", retornoFiltro(verificaFiltro(new TabFiltroCamposIdiomaObj(), request)));
		mv.addObject("txEdit", "edit");
		mv.addObject("cdTela", cdTela);		
		return mv;
	}

	@RequestMapping(value = "/consultar/{cdCampoIdioma}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable Integer cdCampoIdioma, HttpServletRequest request) {

		TabCamposIdiomaObj TabView = tabService.consultar(cdCampoIdioma);
		TabFiltroCamposIdiomaObj tF = verificaFiltro(new TabFiltroCamposIdiomaObj(), request);

		ModelAndView mv = new ModelAndView(txUrlTela);
		if (TabView != null) {
			mv.addObject("tabFiltroCamposIdiomaObj", tF);
			mv.addObject("tabCamposIdiomaObj", TabView);
			mv.addObject("listadados", retornoFiltro(verificaFiltro(tF, request)));
			mv.addObject("txEdit", "edit");
			mv.addObject("cdTela", TabView.getTabTelasObj().getCdTela());			
		} else {
			mv.addObject("tabFiltroCamposIdiomaObj", tF);
			mv.addObject(new TabCamposIdiomaObj());
			mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
			mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}

	@RequestMapping(value = "/excluir/{cdCampo}", method = RequestMethod.GET)
	public @ResponseBody String excluir(@PathVariable Integer cdCampo, HttpServletRequest request) {

		tabService.excluir(cdCampo);

		return "OK!";
	}

	
	@Autowired
	private TabTelasService tabTelasService;
	
	@ModelAttribute("selectcdtela")
	public List<TabTelasObj> selectcdtela() {
		return tabTelasService.listar();
	}
	
}
