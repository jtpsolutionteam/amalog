package br.com.jtpsolution.dao.propostas;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "vw_tab_proposta_new", schema = Constants.SCHEMA)
public class VwTabPropostaNewObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_proposta") 
	//@NotNull(message = "Proposta campo obrigatório!")
	private Integer cdProposta; 
	 
	@Column(name = "cd_usuario") 
	//@NotNull(message = "Usuario campo obrigatório!")
	private Integer cdUsuario; 
	 
	@Column(name = "cd_cliente") 
	//@NotNull(message = "Cliente campo obrigatório!")
	private Integer cdCliente;
	
	@Column(name = "tx_cliente") 
	//@NotNull(message = "Cliente campo obrigatório!")
	private String txCliente;
	
	@Column(name = "tx_importador") 
	//@NotNull(message = "Cliente campo obrigatório!")
	private String txImportador;
	 
	@Column(name = "dt_proposta") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtProposta; 
	 
	@Column(name = "tx_proposta") 
	//@NotEmpty(message = "Proposta campo obrigatório!")
	@Size(max = 15, message = "Proposta tamanho máximo de 15 caracteres") 
	private String txProposta; 
	 
	@Column(name = "cd_status") 
	//@NotNull(message = "Status campo obrigatório!")
	private Integer cdStatus; 
	 
	@Column(name = "cd_tipo_carregamento") 
	//@NotNull(message = "TipoCarregamento campo obrigatório!")
	private Integer cdTipoCarregamento; 
	 
	@Column(name = "tx_tipo_carregamento") 
	//@NotEmpty(message = "TipoCarregamento campo obrigatório!")
	@Size(max = 100, message = "TipoCarregamento tamanho máximo de 100 caracteres") 
	private String txTipoCarregamento; 
	 
	@Column(name = "cd_destino") 
	//@NotNull(message = "Destino campo obrigatório!")
	private Integer cdDestino; 
	 
	@Column(name = "cd_grupo_acesso") 
	//@NotNull(message = "GrupoAcesso campo obrigatório!")
	private Integer cdGrupoAcesso; 
	 
	@Column(name = "ck_desistencia_vistoria") 
	//@NotNull(message = "ckDesistenciaVistoria campo obrigatório!")
	private Integer ckDesistenciaVistoria; 
	 
	@Column(name = "dt_liberacao_documentos") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtLiberacaoDocumentos; 
	 
	@Column(name = "dt_dta_solicitacao") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtDtaSolicitacao; 
	 
	@Column(name = "dt_dta_registro") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtDtaRegistro; 
	 
	@Column(name = "dt_dta_carregamento") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtDtaCarregamento; 
	 
	@Column(name = "dt_dta_parametrizacao") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtDtaParametrizacao; 
	
	@Column(name = "dt_prev_carregamento") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtPrevCarregamento;
	 
	@Column(name = "tx_dta_canal") 
	//@NotEmpty(message = "DtaCanal campo obrigatório!")
	@Size(max = 30, message = "DtaCanal tamanho máximo de 30 caracteres") 
	private String txDtaCanal; 
	 
	@Column(name = "dt_dta_desembaraco") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtDtaDesembaraco; 
	 
	@Column(name = "dt_dta_inicio_transito") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtDtaInicioTransito; 
	 
	@Column(name = "dt_dta_chegada_transito") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtDtaChegadaTransito; 
	 
	@Column(name = "dt_dta_conclusao_transito") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtDtaConclusaoTransito; 
	 
	@Column(name = "dt_dta_ultima_consulta") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtDtaUltimaConsulta; 
	 
	@Column(name = "ck_upload_doctos") 
	//@NotNull(message = "ckUploadDoctos campo obrigatório!")
	private Integer ckUploadDoctos; 
	 
	@Column(name = "tx_check_upload_doctos") 
	//@NotEmpty(message = "CheckUploadDoctos campo obrigatório!")
	@Size(max = 500, message = "CheckUploadDoctos tamanho máximo de 500 caracteres") 
	private String txCheckUploadDoctos; 
	 
	@Column(name = "ck_reconferencia") 
	//@NotNull(message = "ckReconferencia campo obrigatório!")
	private Integer ckReconferencia; 
	 
	@Column(name = "tx_bl") 
	//@NotEmpty(message = "Bl campo obrigatório!")
	@Size(max = 30, message = "Bl tamanho máximo de 30 caracteres") 
	private String txBl; 
	 
	@Column(name = "tx_reserva") 
	//@NotEmpty(message = "Reserva campo obrigatório!")
	@Size(max = 15, message = "Reserva tamanho máximo de 15 caracteres") 
	private String txReserva; 
	 
	@Column(name = "tx_terminal_mar") 
	//@NotEmpty(message = "TerminalMar campo obrigatório!")
	@Size(max = 10, message = "TerminalMar tamanho máximo de 10 caracteres") 
	private String txTerminalMar; 

	@Column(name = "tx_numero_dta") 	
	private String txNumeroDta; 
	
	@Column(name = "tx_origem") 	
	private String txOrigem; 
	
	@Column(name = "tx_destino") 	
	private String txDestino; 

	 
	@Column(name = "dt_boleto_vencto") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtBoletoVencto; 
	 
	@Column(name = "dt_boleto_pagto") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtBoletoPagto; 
	
	@Column(name = "tx_boleto_url") 	
	private String txBoletoUrl;
	
	@Column(name = "tx_due") 	
	private String txDue;
	
	@Column(name = "tx_nfs") 	
	private String txNfs;
	
	@Column(name = "tx_freehand")
	private String txFreehand;
	
	@Column(name = "tx_tipo_proposta") 
	//@NotEmpty(message = "TipoProposta campo obrigatório!")
	@Size(max = 20, message = "TipoProposta tamanho máximo de 20 caracteres") 
	private String txTipoProposta; 
	 
	@Column(name = "cd_origem_ltl") 
	//@NotNull(message = "OrigemLtl campo obrigatório!")
	private Integer cdOrigemLtl; 
	 
	@Column(name = "cd_destino_ltl") 
	//@NotNull(message = "DestinoLtl campo obrigatório!")
	private Integer cdDestinoLtl; 
	 
	@Column(name = "tx_origem_ltl") 
	//@NotEmpty(message = "OrigemLtl campo obrigatório!")
	@Size(max = 45, message = "OrigemLtl tamanho máximo de 45 caracteres") 
	private String txOrigemLtl; 
	 
	@Column(name = "tx_destino_ltl") 
	//@NotEmpty(message = "DestinoLtl campo obrigatório!")
	@Size(max = 45, message = "DestinoLtl tamanho máximo de 45 caracteres") 
	private String txDestinoLtl; 
	
	@Column(name = "tx_di") 
	private String txDi; 
	
	@Column(name = "ck_boleto_cancelado") 
	private Integer ckBoletoCancelado;
	
	@Column(name = "tx_referencia") 
	private String txReferencia;
	
	@Column(name = "dt_doctos_liberados_registro_dta") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtDoctosLiberadosRegistroDta;

	@Column(name = "tx_url_bl") 
	private String txUrlBl;

	@Column(name = "tx_url_extrato_dta") 
	private String txUrlExtratoDta;

	@Column(name = "tx_url_ficha_emergencia") 
	private String txUrlFichaEmergencia;

	@Column(name = "tx_url_ficha_emergencia_band") 
	private String txUrlFichaEmergenciaBand;
	
	@Column(name = "ck_anvisa") 
	private Integer ckAnvisa;

	@Column(name = "ck_imo_perigoso") 
	private Integer ckImoPerigoso;
	
	@Column(name = "dt_pendencia_dta_carregamento") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtPendenciaDtaCarregamento;

	@Column(name = "tx_pendencia_dta_carregamento") 
	private String txPendenciaDtaCarregamento;
	
	@Column(name = "tx_situacao_ce_master") 
	private String txSituacaoCeMaster;
	
	@Column(name = "tx_email_taxa_informativo") 
	private String txEmailTaxaInformativo;

	@Column(name = "tx_lote") 
	private String txLote;
	
	@Column(name = "tx_status_lote_microled") 
	private String txStatusLoteMicroled;
	
	@Column(name = "dt_ultimo_status_lote_microled") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtUltimoStatusLoteMicroled;
	
	@Column(name = "dt_averbacao") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtAverbacao; 
	
	
	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txProposta))
			txProposta = txProposta.toUpperCase();
		if (!Validator.isBlankOrNull(txTipoCarregamento))
			txTipoCarregamento = txTipoCarregamento.toUpperCase();
		if (!Validator.isBlankOrNull(txDtaCanal))
			txDtaCanal = txDtaCanal.toUpperCase();
	}

}
