package br.com.jtpsolution.dao.cadastros.varios.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.cadastros.varios.TabDestinoLtlObj;



public interface TabDestinoLtlRepository extends JpaRepository<TabDestinoLtlObj, Integer> {

	List<TabDestinoLtlObj> findByTabEmpresaObjCdEmpresaAndCdImpexpAndTabViaTransporteObjCdViaTransporteOrderByTxDestinoLtlAsc(Integer cdEmpresa, Integer cdImpexp, Integer cdViaTransporte);

	List<TabDestinoLtlObj> findByTabEmpresaObjCdEmpresa(Integer cdEmpresa);
}