package br.com.jtpsolution.modulos.robosiscarga.obj;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TabCeMercanteItemObj {

	private String txCe;
	
	private Integer cdItem;
	
	private String txEmbalagem;
	
	private String txMarca;
	
	private String txContramarca;
	
	private String vlQtdeVolumes;

	private String vlPesoItem;
	
	private String vlM3Item;
	
	private String txMercadoriaPerigosa;
	
	private String txClasseMercadoriaPerigosa;
	
	private String txSituacaoItem;
	
	
}
