package br.com.jtpsolution.dao.propostas;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import br.com.jtpsolution.Constants;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_proposta_grupo_acesso", schema = Constants.SCHEMA)
public class TabPropostaGrupoAcessoObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_proposta_grupo_acesso")
	// @NotNull(message = "PropostaGrupoAcesso campo obrigatório!")
	private Integer cdPropostaGrupoAcesso;

	@Column(name = "cd_proposta")
	@NotNull(message = "Proposta campo obrigatório!")
	private Integer cdProposta;

	@Column(name = "cd_grupo_acesso")
	@NotNull(message = "GrupoAcesso campo obrigatório!")
	private Integer cdGrupoAcesso;
	
	@Column(name = "ck_coloader")
	private Integer ckColoader;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
	}

}
