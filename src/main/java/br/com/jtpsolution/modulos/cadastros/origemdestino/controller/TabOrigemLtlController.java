package br.com.jtpsolution.modulos.cadastros.origemdestino.controller;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.cadastros.cliente.repository.TabClienteRepository;
import br.com.jtpsolution.dao.cadastros.varios.TabOrigemLtlObj;
import br.com.jtpsolution.mensagens.TabRetornoSucessoObj;
import br.com.jtpsolution.modulos.cadastros.origemdestino.service.TabOrigemLtlService;
import br.com.jtpsolution.modulos.util.geralcontroller.allController;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.regras.RegrasBean;

@Controller
@RequestMapping("/origemltl")
public class TabOrigemLtlController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabOrigemLtlService tabService;
	
	@Autowired
	private RegrasBean regrasBean;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_CADASTROS+"/origemdestino/origemltl";
	
	
	@GetMapping
	public ModelAndView show() {
		
		ModelAndView mv = new ModelAndView(txUrlTela);
	
		List<TabOrigemLtlObj> listOrigem = tabService.listar();
		mv.addObject(new TabOrigemLtlObj());
		mv.addObject("listadados", listOrigem);
		
		return mv;
	}
 	
 	
 
 	
	
	@RequestMapping(value = "/gravarmodal", method = RequestMethod.POST)
	public @ResponseBody List<?> gravarmodal(@Validated TabOrigemLtlObj tabOrigemLtlObj, Errors erros, HttpServletRequest request) {

		List<ObjectError> error = new ArrayList<ObjectError>();
		if (erros.hasErrors()) {
		  //mv.addObject("listaErros", erros.getAllErrors());
		  error.addAll(erros.getAllErrors());
		  return error;
		}else {
			error = regrasBean.verificaregras(getClass().getSimpleName(),request);
			if (!error.isEmpty()) {
				  return error;	
			}	  
		}
		
		
	    TabOrigemLtlObj Tab = tabService.gravar(tabOrigemLtlObj);
	   
	    List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();		
	    TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
	    tSuccess.setCd_mensagem(1);
	    tSuccess.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
	    success.add(tSuccess);
	    
		return success;
	}
	
	
	@RequestMapping(value = "/novo")
	public ModelAndView novo(HttpServletRequest request) {
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		mv.addObject(new TabOrigemLtlObj());
		mv.addObject("listadados", tabService.listar());
		mv.addObject("txEdit", "edit");
		return mv;
	}
	

	@RequestMapping(value = "/consultar/{cdOrigem}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable Integer cdOrigem, HttpServletRequest request) {
	
	   TabOrigemLtlObj TabView = tabService.consultar(cdOrigem);
	   
		ModelAndView mv = new ModelAndView(txUrlTela);	
		if (TabView != null) {
		  mv.addObject("tabOrigemLtlObj",TabView);
		  mv.addObject("listadados", tabService.listar());
		  mv.addObject("txEdit", "edit");
		}else {		  	
		  mv.addObject(new TabOrigemLtlObj());	
		  mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
		  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}
	
	/*
	@RequestMapping(value = "/excluir/{CdOrigem}", method = RequestMethod.GET)
	public @ResponseBody String excluir(@PathVariable Integer CdOrigem, HttpServletRequest request) {

		tabService.excluir(CdOrigem);

		return "OK!";
	}*/

	@Autowired
	private TabClienteRepository tabClienteRepository;
	
	@ModelAttribute("selectcdcliente")
	public List<?> selectcdcliente() {

		return tabClienteRepository.findAll(GeneralParser.sortByIdAsc("txCliente"));
	}
	
}
