package br.com.jtpsolution.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.cadastros.varios.TabTaxaDolarObj;



public interface TabTaxaDolarRepository extends JpaRepository<TabTaxaDolarObj, Integer> {



}