package br.com.jtpsolution.modulos.iugu;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.GeneralUtil;

@Service
public class PagamentosIuguService {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		HashMap h = new HashMap();
		h.put("api_token", "cf4bcecbfaaad3c34edd9aefc5a5f6a2");
		h.put("email", "contato@jtpsolution.com.br"); //receb.getTx_email()); 
		h.put("due_date", GeneralParser.format_dateBR2(new Date()));
		h.put("payable_with", "credit_card");
		h.put("items[][description]", "Adesão Shark");
		h.put("items[][quantity]", "1");
		h.put("items[][price_cents]", GeneralUtil.TiraNaonumero(GeneralParser.doubleToStringMoney((GeneralParser.parseDouble("100,00")))));

		h.put("payer[cpf_cnpj]", "04746455767");
		h.put("payer[name]", GeneralParser.convertISO8859ToUTF8("Sergio Augusto Peralta"));
		h.put("payer[phone_prefix]", "13");
		h.put("payer[phone]", "981732300");
		//h.put("payer[email]", receb.getTx_email());

		//h.put("payer[address][street]", receb.getTx_endereco());
		//h.put("payer[address][number]", "82");
		//h.put("payer[address][city]", receb.getTx_cidade());
		//h.put("payer[address][state]", receb.getTx_uf());
		//h.put("payer[address][country]", "Brasil");
		//h.put("payer[address][zip_code]", "11060400");
		
		h.put("payer[custom_variables][name]", "cd_finance");
		h.put("payer[custom_variables][value]", "1");
		
		h.put("notification_url", "https://sharktradercompany/depositoiugu/1/dshddshdshk");
		
		//System.out.println(new PagamentosIuguService().CriarFatura(h));
		
		h.clear();
		h.put("account_id", "0D88A5B74B204B3283C1114954DEB957");
		h.put("method", "credit_card");
		h.put("test", "true");
		h.put("data[number]", "5536360359348414");
		h.put("data[verification_value]", "772");
		h.put("data[first_name]", "sergio");
		h.put("data[last_name]", "santos");
		h.put("data[month]", "10");
		String tx_ano_cartao = "26";
		if (tx_ano_cartao.length()<3) {
			h.put("data[year]", (GeneralParser.parseInt(tx_ano_cartao)+2000));	
		}else {
			h.put("data[year]", tx_ano_cartao);
		}
		
		//System.out.println(new PagamentosIuguService().GerarTokenPagto(h));
		
		/*
		h.clear();
		h.put("api_token", receb.getTx_cc_token_live_iugu());
		h.put("token", idTokenCartao);
		h.put("restrict_payment_method", "true");
		h.put("invoice_id", idFatura);
		h.put("months", vl_qtd_parcelas);
		*/
		
		
	}
	
	
	private String txIdFatura;
	
	private String txBoletoLinhaDigitavel;
	
	private String txBoletoBarcode;
	
	private String txBoletoUrl;
	
	private String txIdToken;
	
	private String txMensagem;
	
	
	public void CriarFatura(HashMap tx_hash) {
		
		String txFaturaId = "";
		
		String parametros = "";
		try {
			
			// vamos obter uma view dos mapeamentos
	           Set set = tx_hash.entrySet();

	           // obtemos um iterador
	           Iterator i = set.iterator();

	           // e finalmente exibimos todas as chaves e seus valores
	           while(i.hasNext()){
	            Map.Entry entrada = (Map.Entry)i.next();
	            //System.out.println("Chave: " + entrada.getKey() +
	            //" - Valor: " + entrada.getValue());
		        parametros += entrada.getKey()+"="+entrada.getValue()+"&";
	           }
			
	        parametros = parametros.substring(0,parametros.length()-1);
			
			RestTemplate restTemplate = new RestTemplate();
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.add("User-Agent", "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/6.0;)");

			HttpEntity<String> entity = new HttpEntity<String>(headers);
			
			String url = "https://api.iugu.com/v1/invoices?"+parametros;
			ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);

			//System.out.println(response.getBody());
			
			JSONObject json = new JSONObject(response.getBody());
			
			if (!json.isNull("id")) {
				this.setTxIdFatura(json.getString("id"));
				this.setTxBoletoUrl(json.getString("secure_url"));
				if (response.getBody().contains("digitable_line")) {
				  json = json.getJSONObject("bank_slip");
				  this.setTxBoletoLinhaDigitavel(json.getString("digitable_line"));
				  this.setTxBoletoBarcode(json.getString("barcode_data"));				 
				  this.setTxMensagem("criada com sucesso");
				}else {
					this.setTxMensagem("criada com sucesso"); 
				}
			}else {
				this.setTxMensagem("erroFatura");				 
			}
			
			
			
		}catch (HttpClientErrorException ex) {	
			//System.out.println(ex.getResponseBodyAsString());
			this.setTxMensagem("erroFatura: "+GeneralParser.convertUTF8TOIso8859(ex.getResponseBodyAsString()));	
		}catch (JSONException exj) {
			
		}
		
		
	}
	
	
	public void GerarTokenPagto(HashMap tx_hash) {
		
		
		
		String txTokenId = "";
		
		try {
			
			String parametros = "";
			
			// vamos obter uma view dos mapeamentos
	           Set set = tx_hash.entrySet();

	           // obtemos um iterador
	           Iterator i = set.iterator();

	           // e finalmente exibimos todas as chaves e seus valores
	           while(i.hasNext()){
	            Map.Entry entrada = (Map.Entry)i.next();
	            //System.out.println("Chave: " + entrada.getKey() +
	            //" - Valor: " + entrada.getValue());
		        parametros += entrada.getKey()+"="+entrada.getValue()+"&";
	           }
			
	        parametros = parametros.substring(0,parametros.length()-1);
			
			RestTemplate restTemplate = new RestTemplate();
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.add("User-Agent", "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/6.0;)");

			HttpEntity<String> entity = new HttpEntity<String>(headers);
			
			String url = "https://api.iugu.com/v1/payment_token?"+parametros;
			ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);
			
			//System.out.println(response.getBody());
			
			JSONObject json = new JSONObject(response.getBody());
			
			if (!json.isNull("id")) {
			  this.setTxIdToken(json.getString("id"));
			  this.setTxMensagem("criada com sucesso");
			}else {
				this.setTxMensagem("Erro ao validar gerar o token do Cartão de Crédito! Favor verificar e enviar novamente...");
			}
			
			
			
		}catch (Exception ex) {
			this.setTxMensagem("Erro ao validar os dados do Cartão de Crédito! Favor verificar e enviar novamente...");
		}
		
		
	}
	
	
	
	public String FazerPagamento(HashMap tx_hash) {
		
		String txRetorno = "";
		
		try {
			
			String parametros = "";
			
			// vamos obter uma view dos mapeamentos
	           Set set = tx_hash.entrySet();

	           // obtemos um iterador
	           Iterator i = set.iterator();

	           // e finalmente exibimos todas as chaves e seus valores
	           while(i.hasNext()){
	            Map.Entry entrada = (Map.Entry)i.next();
	            //System.out.println("Chave: " + entrada.getKey() +
	            //" - Valor: " + entrada.getValue());
		        parametros += entrada.getKey()+"="+entrada.getValue()+"&";
	           }
			
	        parametros = parametros.substring(0,parametros.length()-1);
			
			RestTemplate restTemplate = new RestTemplate();
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.add("User-Agent", "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/6.0;)");

			HttpEntity<String> entity = new HttpEntity<String>(headers);
			
			String url = "https://api.iugu.com/v1/charge?"+parametros;
			ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);
			
			//System.out.println(response.getBody());
			
			JSONObject json = new JSONObject(response.getBody());
			
			
				if (json.getBoolean("success")) {
				  txRetorno = "Pagamento realizado com sucesso!";
				}else {
					txRetorno = "Pagamento não Aprovado!";	
				}
			
		}catch (Exception ex) {
			//ex.printStackTrace();
			txRetorno = "erroPagamento";
		}
		
		return txRetorno;
	}
	
	
	public void CancelarFatura(String txFaturaId) {
		
		String txRetorno = "";
		
		try {
			
			
			RestTemplate restTemplate = new RestTemplate();
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.add("User-Agent", "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/6.0;)");

			HttpEntity<String> entity = new HttpEntity<String>(headers);
			
			String url = "https://api.iugu.com/v1/invoices/"+txFaturaId+"/cancel";
			restTemplate.put(url, entity);
			
			
		}catch (Exception ex) {
			//ex.printStackTrace();
			txRetorno = "erroPagamento";
		}
		
		//System.out.println("Fatura cancelada: "+txFaturaId);
	}	

	public String getTxIdFatura() {
		return txIdFatura;
	}

	public void setTxIdFatura(String txIdFatura) {
		this.txIdFatura = txIdFatura;
	}

	public String getTxBoletoLinhaDigitavel() {
		return txBoletoLinhaDigitavel;
	}

	public void setTxBoletoLinhaDigitavel(String txBoletoLinhaDigitavel) {
		this.txBoletoLinhaDigitavel = txBoletoLinhaDigitavel;
	}

	public String getTxBoletoBarcode() {
		return txBoletoBarcode;
	}

	public void setTxBoletoBarcode(String txBoletoBarcode) {
		this.txBoletoBarcode = txBoletoBarcode;
	}

	public String getTxIdToken() {
		return txIdToken;
	}

	public void setTxIdToken(String txIdToken) {
		this.txIdToken = txIdToken;
	}

	public String getTxMensagem() {
		return txMensagem;
	}

	public void setTxMensagem(String txMensagem) {
		this.txMensagem = txMensagem;
	}

	public String getTxBoletoUrl() {
		return txBoletoUrl;
	}

	public void setTxBoletoUrl(String txBoletoUrl) {
		this.txBoletoUrl = txBoletoUrl;
	}



}
