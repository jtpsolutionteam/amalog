package br.com.jtpsolution.modulos.cadastros.valoresfixos.controller;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.cadastros.empresa.TabEmpresaObj;
import br.com.jtpsolution.dao.cadastros.varios.TabValoresFixosObj;
import br.com.jtpsolution.mensagens.TabRetornoSucessoObj;
import br.com.jtpsolution.modulos.cadastros.empresa.service.TabEmpresaService;
import br.com.jtpsolution.modulos.cadastros.valoresfixos.bean.TabFiltroValoresFixosObj;
import br.com.jtpsolution.modulos.cadastros.valoresfixos.service.TabValoresFixosService;
import br.com.jtpsolution.modulos.util.geralcontroller.allController;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.regras.RegrasBean;

@Controller
@RequestMapping("/valoresfixos")
public class TabValoresFixosController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabValoresFixosService tabService;
	
	@Autowired
	private RegrasBean regrasBean;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_CADASTROS+"/valoresfixos/valoresfixos";
	
	/*
	@GetMapping
	public ModelAndView pesquisa(TabFiltroValoresFixosObj tabFiltroValoresFixosObj, Errors errors, @PageableDefault(size = 20) Pageable pageable, HttpServletRequest httpServletRequest) {
		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject("tabFiltroValoresFixosObj", tabFiltroValoresFixosObj);
		
		PageWrapper<TabValoresFixosObj> TabView = new PageWrapper<>(tabService.telapesquisar(tabFiltroValoresFixosObj, pageable),httpServletRequest);
		
		if (TabView.getPage() != null) {	
		  mv.addObject("listadados",TabView);
		  mv.addObject(new TabValoresFixosObj());
		}else {	
		  mv.addObject(new TabValoresFixosObj());	
		  mv.addObject("txMensagem", Constants.FILTRO_NAO_EXISTE); 	
		}
		
		return mv;
	}
 	*/
 	
 	@GetMapping
	public ModelAndView pesquisa(TabFiltroValoresFixosObj tabFiltroValoresFixosObj, HttpServletRequest request) {
		
		tabFiltroValoresFixosObj = verificaFiltro(tabFiltroValoresFixosObj, request);
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject(new TabValoresFixosObj());			
		mv.addObject("tabFiltroValoresFixosObj", tabFiltroValoresFixosObj);
		mv.addObject("listadados", retornoFiltro(tabFiltroValoresFixosObj));
		
		
		return mv;
	}
	
	private List<?> retornoFiltro(TabFiltroValoresFixosObj tabFiltroValoresFixosObj) {
 		
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(TabValoresFixosObj.class);
		
		adicionarFiltro(tabFiltroValoresFixosObj, criteria);
 		
 		return criteria.list();
 	}


	private TabFiltroValoresFixosObj verificaFiltro(TabFiltroValoresFixosObj tabFiltroValoresFixosObj, HttpServletRequest request) {
		
		try {
			
			if (Validator.isObjetoNull(tabFiltroValoresFixosObj)) {
				if (request.getSession().getAttribute(getClass().getSimpleName()) != null) {					
					String jsonFiltro = request.getSession().getAttribute(getClass().getSimpleName()).toString();
					ObjectMapper mapper = new ObjectMapper();
					tabFiltroValoresFixosObj = mapper.readValue(jsonFiltro, TabFiltroValoresFixosObj.class);	
				}
			}else {
				request.getSession().setAttribute(getClass().getSimpleName(),
						GeneralParser.convertObjJson(tabFiltroValoresFixosObj));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return tabFiltroValoresFixosObj;
	}

	
	private void adicionarFiltro(TabFiltroValoresFixosObj filtro, Criteria criteria) {
		
		  boolean ckFiltro = false;
		  
		  if (filtro != null) {
			
			  if (!StringUtils.isEmpty(filtro.getCdEmpresaFiltro()) && !StringUtils.isEmpty(filtro.getCdImpExpFiltro())) {

				    criteria.add(Restrictions.eq("tabEmpresaObj.cdEmpresa", filtro.getCdEmpresaFiltro()));
				    criteria.add(Restrictions.eq("cdImpexp", filtro.getCdImpExpFiltro()));
					ckFiltro = true;
			  }
			  
			  
			  if (!ckFiltro) {
				  criteria.add(Restrictions.eq("cdImpexp", 0));
			  }
			  
			 
		  }
	  }
 	
	
	@RequestMapping(value = "/gravarmodal", method = RequestMethod.POST)
	public @ResponseBody List<?> gravarmodal(@Validated TabValoresFixosObj tabValoresFixosObj, Errors erros, HttpServletRequest request) {

		List<ObjectError> error = new ArrayList<ObjectError>();
		if (erros.hasErrors()) {
		  //mv.addObject("listaErros", erros.getAllErrors());
		  error.addAll(erros.getAllErrors());
		  return error;
		}else {
			error = regrasBean.verificaregras(getClass().getSimpleName(),request);
			if (!error.isEmpty()) {
				  return error;	
			}	  
		}
		
	    TabValoresFixosObj Tab = tabService.gravar(tabValoresFixosObj);
	   
	    List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();		
	    TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
	    tSuccess.setCd_mensagem(1);
	    tSuccess.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
	    success.add(tSuccess);
	    
		return success;
	}
	
	
	@RequestMapping(value = "/novo")
	public ModelAndView novo(HttpServletRequest request) {
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		mv.addObject(new TabValoresFixosObj());
		mv.addObject("tabFiltroValoresFixosObj", verificaFiltro(new TabFiltroValoresFixosObj(), request));
		mv.addObject("listadados", retornoFiltro(verificaFiltro(new TabFiltroValoresFixosObj(), request)));
		mv.addObject("txEdit", "edit");
		return mv;
	}
	

	@RequestMapping(value = "/consultar/{cdTabela}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable Integer cdTabela, HttpServletRequest request) {
	
	   TabValoresFixosObj TabView = tabService.consultar(cdTabela);
	   TabFiltroValoresFixosObj tF = verificaFiltro(new TabFiltroValoresFixosObj(), request);

	   
		ModelAndView mv = new ModelAndView(txUrlTela);	
		if (TabView != null) {
		  mv.addObject("tabFiltroValoresFixosObj",tF);	
		  mv.addObject("tabValoresFixosObj",TabView);
		  mv.addObject("listadados", retornoFiltro(verificaFiltro(tF, request)));
		  mv.addObject("txEdit", "edit");
		}else {
		  mv.addObject("tabFiltroValoresFixosObj",tF);	
		  mv.addObject(new TabValoresFixosObj());	
		  mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
		  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}
	
	/*
	@RequestMapping(value = "/excluir/{CdTabela}", method = RequestMethod.GET)
	public @ResponseBody String excluir(@PathVariable Integer CdTabela, HttpServletRequest request) {

		tabService.excluir(CdTabela);

		return "OK!";
	}*/
	
	@Autowired
	private TabEmpresaService tabEmpresaService;
	
	@ModelAttribute("selectcdempresa")	
	public List<TabEmpresaObj> selectcdempresa() {
		return tabEmpresaService.listar();
	}
	
}
