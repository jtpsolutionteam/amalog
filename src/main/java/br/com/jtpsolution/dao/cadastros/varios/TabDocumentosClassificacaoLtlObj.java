package br.com.jtpsolution.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_documentos_classificacao_ltl", schema = Constants.SCHEMA)
public class TabDocumentosClassificacaoLtlObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_docto_classificacao")
	// @NotNull(message = "DoctoClassificacao campo obrigatório!")
	private Integer cdDoctoClassificacao;

	@Column(name = "tx_docto_classificacao")
	@NotEmpty(message = "Classificação campo obrigatório!")
	@Size(max = 100, message = "Docto Classificação tamanho máximo de 200 caracteres")
	private String txDoctoClassificacao;

	@Column(name = "cd_tipo")
	@NotNull(message = "Tipo  campo obrigatório!")
	private Integer cdTipo;
	
	@Column(name = "ck_validar")
	@NotNull(message = "Validar? campo obrigatório!")
	private Integer ckValidar;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txDoctoClassificacao))
			txDoctoClassificacao = txDoctoClassificacao.toUpperCase();
	}

	
}
