package br.com.jtpsolution.modulos.uploaddoctosbl.controller;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.cadastros.varios.VwTabDocumentosClassificacaoLtlObj;
import br.com.jtpsolution.dao.cadastros.varios.repository.VwTabDocumentosClassificacaoLtlRepository;
import br.com.jtpsolution.dao.cadastros.varios.repository.VwTabDocumentosClassificacaoRepository;
import br.com.jtpsolution.dao.propostas.TabPropostaDocumentosObj;
import br.com.jtpsolution.dao.propostas.TabPropostaObj;
import br.com.jtpsolution.dao.propostas.VwTabPropostaDocumentosObj;
import br.com.jtpsolution.dao.propostas.VwTabPropostaObj;
import br.com.jtpsolution.dao.propostas.repository.TabPropostaDocumentosRepository;
import br.com.jtpsolution.dao.propostas.repository.TabPropostaRepository;
import br.com.jtpsolution.dao.propostas.repository.VwTabPropostaDocumentosRepository;
import br.com.jtpsolution.dao.propostas.repository.VwTabPropostaRepository;
import br.com.jtpsolution.mensagens.TabRetornoSucessoObj;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.GeneralUtil;
import br.com.jtpsolution.util.Validator;

@Controller
@RequestMapping("/uploaddoctosbl")
public class TabUploadDoctosBlController {


	@Autowired
	private TabPropostaRepository tabPropostaRepository;
	
	@Autowired
	private VwTabPropostaRepository vwTabPropostaRepository;

	@Autowired
	private TabPropostaDocumentosRepository tabPropostaDocumentosRepository;
	
	@Autowired
	private VwTabPropostaDocumentosRepository vwTabPropostaDocumentosRepository;
	
	@Autowired
	private VwTabDocumentosClassificacaoRepository vwTabDocumentosClassificacaoRepository;
	
	@Autowired
	private VwTabDocumentosClassificacaoLtlRepository vwTabDocumentosClassificacaoLtlRepository;

	
	private String txUrlTela = Constants.TEMPLATE_PATH_PROPOSTA+"/uploaddoctosbl";

	@GetMapping
	public ModelAndView show() {
		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject(new TabPropostaObj());
		mv.addObject(new TabPropostaDocumentosObj());
		
		return mv;
	}
	
	
	@RequestMapping(value = "/gravar", method = RequestMethod.POST)
	public ModelAndView gravar(@Validated TabPropostaObj tabPropostaObj, Errors erros, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject(new TabPropostaDocumentosObj());
		
		
		if (erros.hasErrors()) {		  
		  mv.addObject("listaErros", erros.getAllErrors());
		  return mv;
		}
				
//		mv.addObject("listaDocumentos",listDocumentos);
//		mv.addObject("selectcddoctoclassificacao", selectcdDoctoClassificacao(TabView.getCdTipoCarregamento()));				
//		mv.addObject("tabPropostaObj",TabView);
		mv.addObject("txMensagem", Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
		mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_SUCCESS);
		return mv;
	}
	
	
	@RequestMapping(value = "/consultar", method = RequestMethod.GET)
	public ModelAndView consultar(@RequestParam String txBl, @RequestParam String txNomeUploadDoctoBl, @RequestParam String txEmailUploadDoctoBl, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView(txUrlTela);	

		if (Validator.isBlankOrNull(txBl) || Validator.isBlankOrNull(txNomeUploadDoctoBl) || Validator.isBlankOrNull(txEmailUploadDoctoBl)) {
			  mv.addObject(new TabPropostaObj());				 
			  mv.addObject(new TabPropostaDocumentosObj());
			  mv.addObject("txMensagem", "No. BL/AWB, Nome e Email obrigatórios!");
			  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
			  return mv;
		}

		
		if (!Validator.isBlankOrNull(txBl)) {
		
		 VwTabPropostaObj TabView = vwTabPropostaRepository.findByTxBlAndTxTipoProposta(txBl,"LTL");
		 if (TabView == null) {
			 TabView = vwTabPropostaRepository.findByTxReserva(txBl);
		 }
		 
		 
		if (TabView != null) {
		  List<VwTabPropostaDocumentosObj> listDocumentos = vwTabPropostaDocumentosRepository.findByCdProposta(TabView.getCdProposta());
		  mv.addObject("selectcddoctoclassificacao", selectcdDoctoClassificacao(TabView.getCdTipoCarregamento()));	
		  TabView.setTxNomeUploadDoctoBl(txNomeUploadDoctoBl);
		  TabView.setTxEmailUploadDoctoBl(txEmailUploadDoctoBl);
		  mv.addObject("tabPropostaObj",TabView);
		  mv.addObject(new TabPropostaDocumentosObj());
		  mv.addObject("listaDocumentos",listDocumentos);
		  mv.addObject("cdProposta",TabView.getCdProposta());
		}else {
		  mv.addObject(new TabPropostaObj());	
		  mv.addObject(new TabPropostaDocumentosObj());
		  mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
		  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
	   }else {
			  mv.addObject(new TabPropostaObj());				 
			  mv.addObject(new TabPropostaDocumentosObj());
			  mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
			  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);		   
	   }
		return mv;
	}
	
	
	@RequestMapping(value = "/gravarmodaldocumentos", headers = ("content-type=multipart/*"), method = RequestMethod.POST)
	public @ResponseBody List<?> gravarmodaldocumentos(@RequestParam("file") MultipartFile[] inputFiles, @Validated TabPropostaDocumentosObj tabPropostaDocumentosObj, Errors erros, HttpServletRequest request) {

		ModelAndView mv = new ModelAndView(txUrlTela);

		List<ObjectError> error = new ArrayList<ObjectError>();
		if (erros.hasErrors()) {
		  //mv.addObject("listaErros", erros.getAllErrors());
		  error.addAll(erros.getAllErrors());
		  return error;
		}
		
	
		//FileInfo fileInfo = new FileInfo();
		String originalFilename = "";
		
		Integer cdProposta = GeneralParser.parseInt(request.getParameter("cdProposta"));
		String txNomeUpload = request.getParameter("txNomeUploadDoctoBl");
		String txEmailUpload = request.getParameter("txEmailUploadDoctoBl");

		if (tabPropostaDocumentosObj.getCdProposta() != null && tabPropostaDocumentosObj.getCdValidado() == null) {
			TabPropostaDocumentosObj tabP = tabPropostaDocumentosRepository.findByCdPropostaAndTabDocumentosClassificacaoObjCdDoctoClassificacao(cdProposta, tabPropostaDocumentosObj.getTabDocumentosClassificacaoObj().getCdDoctoClassificacao());
	
			if (tabP != null) {
				ObjectError obj = new ObjectError("cdClassificacao", "Um Documento já existe com essa classificação!");
				error.add(obj);
				return error;
			}
		}
		
		
		boolean ckUpload = false;
		for(MultipartFile inputFile : inputFiles) {
		
		if (!inputFile.isEmpty()) {
			try {
				
				ckUpload = true;
				//CalcRandom c = new CalcRandom();
				//String txRef= c.getloginAleatoria(10);
				
				originalFilename = inputFile.getOriginalFilename().replaceAll(" ", "_");
				
				String pathDir = Constants.PATH_UPLOAD +"propostas/"+cdProposta+"/";

				File destinationFile = new File(pathDir);
				if (!destinationFile.exists()) {
					destinationFile.mkdirs();
				}

				File arquivo = new File(pathDir + originalFilename);
				if (arquivo.exists()) {
					
					GeneralUtil g = new GeneralUtil();
					g.DeletarArquivo(pathDir + originalFilename);
				}

				if (inputFile.getBytes() != null) {
					tabPropostaDocumentosObj.setDtCriacao(new Date());
					tabPropostaDocumentosObj.setTxNomeArquivo(originalFilename);
					// tabDoctosAnexosObj.setTxArquivo(encodedString);
					tabPropostaDocumentosObj.setTxUrl("/proposta/download/"+cdProposta+"?txNomeArquivo="+originalFilename);
					tabPropostaDocumentosObj = tabPropostaDocumentosRepository.save(tabPropostaDocumentosObj);

					FileOutputStream outStream = null; //new FileOutputStream(pathDir + originalFilename);
					byte [] bufferedbytes= new byte[1024];
					
					BufferedInputStream fileInputStream= new BufferedInputStream(inputFile.getInputStream());
			        outStream=new FileOutputStream(arquivo);
			        int count=0;
			        while((count=fileInputStream.read(bufferedbytes))!=-1) {
			         outStream.write(bufferedbytes,0,count);
			        }
			        outStream.close();
					//fos.write(inputFile.getBytes());
					//fos.close();
					
				} else {
					//Erro
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				//Erro
			}
		  }
		}
			
		TabPropostaObj tabP = tabPropostaRepository.getOne(cdProposta);
		
		if (tabP != null) {
		  tabP.setTxNomeUploadDoctoBl(txNomeUpload);
		  tabP.setTxEmailUploadDoctoBl(txEmailUpload);
		  tabPropostaRepository.save(tabP);
		}
		
		
		List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();		
	    TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
	    tSuccess.setCd_mensagem(1);
	    tSuccess.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
	    success.add(tSuccess);
	    
	    return success;
	}

	
	@RequestMapping(value = "/excluirdocumento/{cdPropostaDocumento}", method = RequestMethod.GET)
	public @ResponseBody String excluirdocumento(@PathVariable Integer cdPropostaDocumento) {
	
	   TabPropostaDocumentosObj TabView = tabPropostaDocumentosRepository.findOne(cdPropostaDocumento);
		
	   String pathDir = Constants.PATH_UPLOAD +"propostas/"+TabView.getCdProposta()+"/";
	   
	   String txNomeArquivo = TabView.getTxNomeArquivo();
	   
	   File arquivo = new File(pathDir + txNomeArquivo);
		if (arquivo.exists()) {
			
			GeneralUtil g = new GeneralUtil();
			g.DeletarArquivo(pathDir + txNomeArquivo);
		}
	   
		tabPropostaDocumentosRepository.delete(cdPropostaDocumento);
	    
		return "OK";
	}
	
	
	@RequestMapping(value = "/download/{cdProposta}")
    public @ResponseBody HttpEntity<InputStreamResource> getFile(@PathVariable Integer cdProposta,  @RequestParam String txNomeArquivo) throws IOException {

		
		String pathDir = Constants.PATH_UPLOAD +"propostas/"+cdProposta+"/";
	    
    	File destinationFile = new File(pathDir + txNomeArquivo);
        InputStreamResource resource = new InputStreamResource(new FileInputStream(destinationFile));

        MediaType m = null;
        if (txNomeArquivo.toLowerCase().contains(".jpg")) {
        	m = MediaType.IMAGE_JPEG;
        }else if (txNomeArquivo.toLowerCase().contains(".gif")) {
        	m = MediaType.IMAGE_GIF;
        }else if (txNomeArquivo.toLowerCase().contains(".png")) {
        	m = MediaType.IMAGE_PNG;
        }else if (txNomeArquivo.toLowerCase().contains(".pdf")) {
        	m = MediaType.APPLICATION_PDF;
        }else {
        	m = MediaType.APPLICATION_OCTET_STREAM;
        }
        
        return ResponseEntity.ok()
              .header(HttpHeaders.CONTENT_DISPOSITION,
                    "attachment;filename=" + txNomeArquivo)
              .contentType(m).contentLength(destinationFile.length())
              .body(resource);
    }
	
	
	public List<VwTabDocumentosClassificacaoLtlObj> selectcdDoctoClassificacao(Integer cdTipo) {		
		return vwTabDocumentosClassificacaoLtlRepository.findOrderByCdClassificacaoTipoValidarQuery(cdTipo);
	}

	
}
