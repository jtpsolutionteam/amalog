package br.com.jtpsolution.dao.administracao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.administracao.TabCamposIdiomaObj;



public interface TabCamposIdiomaRepository extends JpaRepository<TabCamposIdiomaObj, Integer> {

	List<TabCamposIdiomaObj> findByTabTelasObjTxControllerAndCdIdioma(String txController, String cdIdioma);

}