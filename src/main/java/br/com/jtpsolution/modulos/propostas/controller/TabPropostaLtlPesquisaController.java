package br.com.jtpsolution.modulos.propostas.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.cadastros.varios.TabStatusObj;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabStatusRepository;
import br.com.jtpsolution.dao.propostas.TabPropostaObj;
import br.com.jtpsolution.dao.propostas.VwTabPropostaObj;
import br.com.jtpsolution.modulos.propostas.bean.TabFiltroPropostaObj;
import br.com.jtpsolution.modulos.propostas.service.TabPropostaService;
import br.com.jtpsolution.modulos.util.geralcontroller.allController;
import br.com.jtpsolution.security.DadosUser;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.regras.RegrasBean;

@Controller
@RequestMapping("/propostaltlpesquisa")
public class TabPropostaLtlPesquisaController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabPropostaService tabService;
	
	@Autowired
	private RegrasBean regrasBean;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private UsuarioBean tabUsuarioService;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_PROPOSTA+"/propostaltlpesquisa";
	
 	@GetMapping	
	public ModelAndView pesquisa(TabFiltroPropostaObj tabFiltroPropostaObj, HttpServletRequest request) {
		
		tabFiltroPropostaObj = verificaFiltro(tabFiltroPropostaObj, request);
		
		
		ModelAndView mv = verificaPermissao(txUrlTela);
		mv.addObject(new TabPropostaObj());			
		mv.addObject("tabFiltroPropostaObj", tabFiltroPropostaObj);
		mv.addObject("listadados", retornoFiltro(tabFiltroPropostaObj));
		
		
		return mv;
	}
 	
 	
	
	private List<?> retornoFiltro(TabFiltroPropostaObj tabFiltroPropostaObj) {
 		
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(VwTabPropostaObj.class);
		
		adicionarFiltro(tabFiltroPropostaObj, criteria);
 		
 		return criteria.list();
 	}


	private TabFiltroPropostaObj verificaFiltro(TabFiltroPropostaObj tabFiltroPropostaObj, HttpServletRequest request) {
		
		try {
			
			if (Validator.isObjetoNull(tabFiltroPropostaObj)) {
				if (request.getSession().getAttribute(getClass().getSimpleName()) != null) {					
					String jsonFiltro = request.getSession().getAttribute(getClass().getSimpleName()).toString();
					ObjectMapper mapper = new ObjectMapper();
					tabFiltroPropostaObj = mapper.readValue(jsonFiltro, TabFiltroPropostaObj.class);	
				}
			}else {
				request.getSession().setAttribute(getClass().getSimpleName(),
						GeneralParser.convertObjJson(tabFiltroPropostaObj));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return tabFiltroPropostaObj;
	}

	
	private void adicionarFiltro(TabFiltroPropostaObj filtro, Criteria criteria) {
		
		  boolean ckFiltro = false;
		  
		  DadosUser user  = tabUsuarioService.DadosUsuario();	
		  
		 // if (user.getCdGrupoVisao() < 3) {
		    criteria.add(Restrictions.eq("cdGrupoAcesso", user.getCdGrupoAcesso()));
		    
		    criteria.add(Restrictions.eq("txTipoProposta", "LTL"));
		 // }else {
		//	criteria.add(Restrictions.eq("cdUsuario", user.getCdUsuario()));  
		 // }
		  
		  if (filtro != null) {
			  
			  if (!StringUtils.isEmpty(filtro.getTxPropostaFiltro())) {
				  criteria.add(Restrictions.like("txProposta", "%"+filtro.getTxPropostaFiltro()+"%"));
				  ckFiltro = true;
			  }
			  
			  if (!StringUtils.isEmpty(filtro.getTxTipoCampoFiltro()) && !StringUtils.isEmpty(filtro.getDtPropostaInicialFiltro()) && !StringUtils.isEmpty(filtro.getDtPropostaFinalFiltro())) {
				  criteria.add(Restrictions.between(filtro.getTxTipoCampoFiltro(), filtro.getDtPropostaInicialFiltro(), filtro.getDtPropostaFinalFiltro()));
				  ckFiltro = true;
			  }
			  
			  if (!StringUtils.isEmpty(filtro.getCdStatusFiltro())) {
				  criteria.add(Restrictions.eq("cdStatus", filtro.getCdStatusFiltro()));
				  ckFiltro = true;
			  }
			  
			  
			  /*
			  if (!ckFiltro) {
				  criteria.add(Restrictions.between("dtProposta", GeneralParser.retornaDataAnteriorDiasCorridos(new Date(), 40), filtro.getDtPropostaFinalFiltro()));
			  }*/
			  
			  criteria.addOrder(Order.desc("dtProposta"));
		  }
	  }
 	

	@Autowired
	private TabStatusRepository tabStatusRepository;
	
	@ModelAttribute("selectcdstatus")
	public List<TabStatusObj> selectcdstatus() {
		return tabStatusRepository.findByTxTipoStatusQuery("LTL");
	}
	
}
