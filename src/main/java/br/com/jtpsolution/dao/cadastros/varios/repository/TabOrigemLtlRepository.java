package br.com.jtpsolution.dao.cadastros.varios.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.cadastros.varios.TabOrigemLtlObj;



public interface TabOrigemLtlRepository extends JpaRepository<TabOrigemLtlObj, Integer> {

	List<TabOrigemLtlObj> findByCkAtivoOrderByTxOrigemLtlAsc(Integer ckAtivo);


}