package br.com.jtpsolution.dao.cadastros.varios.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.cadastros.varios.TabDocumentosClassificacaoLtlObj;
import br.com.jtpsolution.dao.cadastros.varios.TabDocumentosClassificacaoObj;



public interface TabDocumentosClassificacaoLtlRepository extends JpaRepository<TabDocumentosClassificacaoLtlObj, Integer> {

	@Query("select t from TabDocumentosClassificacaoLtlObj t order by t.cdDoctoClassificacao")
	List<TabDocumentosClassificacaoObj> findOrderByCdDoctoClassificacaoQuery();

}