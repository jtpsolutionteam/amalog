package br.com.jtpsolution.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.cadastros.empresa.TabEmpresaObj;
import br.com.jtpsolution.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_ltl", schema = Constants.SCHEMA)
public class TabLtlObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_ltl")
	// @NotNull(message = "Ltl campo obrigatório!")
	private Integer cdLtl;

	@Column(name = "tx_descricao_ltl")
	@NotEmpty(message = "Descrição campo obrigatório!")
	@Size(max = 45, message = "Descrição tamanho máximo de 45 caracteres")
	private String txDescricaoLtl;

	@Column(name = "vl_valor")
	// @NotEmpty(message = "Valor campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlValor;

	@Column(name = "ck_ativo")
	// @NotNull(message = "ckAtivo campo obrigatório!")
	private Integer ckAtivo;

	@Column(name = "vl_de")
	// @NotEmpty(message = "De campo obrigatório!")
	@NumberFormat(pattern = "#,##0.000")
	private Double vlDe;

	@Column(name = "vl_ate")
	// @NotEmpty(message = "Ate campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlAte;

	@Column(name = "cd_tipo_calculo")
	// @NotEmpty(message = "TipoCalculo campo obrigatório!")
	@Size(max = 10, message = "Tipo Cálculo tamanho máximo de 10 caracteres")
	private String cdTipoCalculo;
	
	@ManyToOne
	@JoinColumn(name = "cd_empresa")
	private TabEmpresaObj tabEmpresaObj;
	
	@ManyToOne
	@JoinColumn(name = "cd_via_transporte")
	private TabViaTransporteObj tabViaTransporteObj;
	
	@Column(name = "cd_impexp")
	private Integer cdImpexp;
	
	@ManyToOne
	@JoinColumn(name = "cd_destino_ltl")
	private TabDestinoLtlObj tabDestinoLtlObj;
	
	@Column(name = "vl_pedagio")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlPedagio;


	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txDescricaoLtl))
			txDescricaoLtl = txDescricaoLtl.toUpperCase();
		if (!Validator.isBlankOrNull(cdTipoCalculo))
			cdTipoCalculo = cdTipoCalculo.toUpperCase();
	}

}
