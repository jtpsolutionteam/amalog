package br.com.jtpsolution.modulos.robosiscarga.service;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.conexaorest.restconnect;
import br.com.jtpsolution.modulos.robosiscarga.obj.TabCeMercanteObj;

@Service
public class RoboSiscargaService {

	
	
	
	
	public TabCeMercanteObj getNrCeMercante(String txCe) {
		
		TabCeMercanteObj tabCe = new TabCeMercanteObj();
		
		try {
		
		  String txReturnCe = restconnect.enviaRestGet(Constants.ROBO_SISCARGA+"/"+txCe); 
		
		  ObjectMapper mapper = new ObjectMapper();
		  tabCe = mapper.readValue(txReturnCe, TabCeMercanteObj.class);
		  
		}catch (Exception ex) {
			//ex.printStackTrace();
		}
		
		return tabCe;
	}
	
}
