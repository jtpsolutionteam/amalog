package br.com.jtpsolution.modulos.propostas.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.propostas.VwTabPropostaObj;
import br.com.jtpsolution.modulos.propostas.service.TabPropostaService;
import br.com.jtpsolution.modulos.util.geralcontroller.allController;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralParser;

@Controller
@RequestMapping("/propostainformativo")
public class TabPropostaInformativoController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabPropostaService tabService;
	

	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_PROPOSTA+"/propostainformativotaxa";
	
	
	@RequestMapping(value = "/{txHashProposta}")
	public ModelAndView show(@PathVariable String txHashProposta) {
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		
		VwTabPropostaObj tab = tabService.consultar(txHashProposta);
		mv.addObject("tabPropostaObj", tab);
		mv.addObject("txBomDia", GeneralParser.mostraBomDiaTardeNoite());
		
		return mv;
	}
	

	
}
