package br.com.jtpsolution.dao.propostas;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.jtpsolution.Constants;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "vw_tab_proposta_documentos", schema = Constants.SCHEMA)
public class VwTabPropostaDocumentosObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_proposta_documento")
	// @NotNull(message = "PropostaDocumento campo obrigatório!")
	private Integer cdPropostaDocumento;

	@Column(name = "cd_proposta")
	// @NotNull(message = "Proposta campo obrigatório!")
	private Integer cdProposta;

	@Column(name = "tx_nome_arquivo")
	private String txNomeArquivo;

	@Column(name = "tx_url")
	private String txUrl;

	@Column(name = "cd_classificacao")
	// @NotNull(message = "Classificacao campo obrigatório!")
	private Integer cdClassificacao;

	@Column(name = "tx_classificacao")
	// @NotEmpty(message = "Classificacao campo obrigatório!")
	@Size(max = 100, message = "Classificacao tamanho máximo de 100 caracteres")
	private String txClassificacao;
	
	@Column(name = "cd_validado")
	private Integer cdValidado;

	@Column(name = "tx_pendencia")
	private String txPendencia;
	
	@Column(name = "dt_criacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtCriacao;
	
	@Column(name = "dt_validacao")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtValidacao;

	@Column(name = "cd_usuario_criacao")
	private Integer cdUsuarioCriacao;
	
	@Column(name = "cd_usuario_validacao")
	private Integer cdUsuarioValidacao;
	
	@Column(name = "tx_usuario_criacao")
	private String txUsuarioCriacao;
	
	@Column(name = "tx_usuario_validacao")
	private String txUsuarioValidacao;
	
	@Column(name = "tx_obs")
	private String txObs;

	@Transient
	private String tabDocumentosClassificacaoObj;
	
	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		
	}


	

}
