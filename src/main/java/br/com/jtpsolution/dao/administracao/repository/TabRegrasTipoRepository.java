package br.com.jtpsolution.dao.administracao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.administracao.TabRegrasTipoObj;




public interface TabRegrasTipoRepository extends JpaRepository<TabRegrasTipoObj, Integer> {



}