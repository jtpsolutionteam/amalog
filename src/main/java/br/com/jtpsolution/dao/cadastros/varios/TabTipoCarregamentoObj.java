package br.com.jtpsolution.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.util.Validator;

@Entity
@Table(name = "tab_tipo_carregamento", schema = Constants.SCHEMA)
public class TabTipoCarregamentoObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_tipo_carregamento")
	// @NotNull(message = "TipoCarrgamento campo obrigatório!")
	private Integer cdTipoCarregamento;

	@Column(name = "tx_tipo_carregamento")
	// @NotEmpty(message = "TipoCarregamento campo obrigatório!")
	@Size(max = 100, message = "TipoCarregamento tamanho máximo de 100 caracteres")
	private String txTipoCarregamento;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txTipoCarregamento))
			txTipoCarregamento = txTipoCarregamento.toUpperCase();
	}

	
	public String getTxTipoCarregamento() {
		return txTipoCarregamento;
	}

	public void setTxTipoCarregamento(String txTipoCarregamento) {
		this.txTipoCarregamento = txTipoCarregamento;
	}


	public Integer getCdTipoCarregamento() {
		return cdTipoCarregamento;
	}


	public void setCdTipoCarregamento(Integer cdTipoCarregamento) {
		this.cdTipoCarregamento = cdTipoCarregamento;
	}
	
	

}
