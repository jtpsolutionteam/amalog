package br.com.jtpsolution.modulos.cadastros.origemdestino.service;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.jtpsolution.dao.cadastros.varios.TabOrigemObj;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabOrigemRepository;
import br.com.jtpsolution.dao.util.log.TabLogObj;
import br.com.jtpsolution.exceptions.ErrosConstraintException;
import br.com.jtpsolution.security.DadosUser;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.GeneralUtil;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.log.LogBean;
import br.com.jtpsolution.util.logErro.JTPLogErroBean;

@Service
public class TabOrigemService {

	@Autowired
	private JTPLogErroBean logErroBean;
	
	@Autowired
	private TabOrigemRepository  tabRepository;
	

	@Autowired
	private UsuarioBean tabUsuarioService;
	
	@Autowired
	private LogBean tabLogBeanService;

	/*
	@Autowired 
	private TabFiltro.....Bean tabFiltroService;
	
	*/
	
	
	public List<TabOrigemObj> listar() {
		return tabRepository.findAll(GeneralParser.sortByIdAsc("txOrigem"));
		
	}
	
	/* Modelo tela filho 
	
		public List<VwTabOrigemObj> listar(String CampoRelacionamento) {

		DadosUser user  = tabUsuarioService.DadosUsuario();

		return tabVwRepository.findByTxNrefQuery(CampoRelacionamento, user.getCdGrupoAcesso());
		
	}
	*/
	
	/* Modelo - Tela Pesquisar
			public Page<VwTabOrigemObj> telapesquisar(TabFiltroObj tabFiltroObj, Pageable pageable) {

			Page<VwTabOrigemObj> Tab = tabFiltroService.filtrar(tabFiltroObj, pageable);
			
			return Tab;

		}
	*/


	public TabOrigemObj gravar(TabOrigemObj Tab) {
		
		boolean ckAlteracao = false;
				
		DadosUser user  = tabUsuarioService.DadosUsuario();

		
		TabOrigemObj tAtual = new TabOrigemObj();
		TabOrigemObj tNovo = Tab;
		
		//tNovo.setCdUsuario(null);
		List<TabLogObj> listaLog = null;
		
		if (Tab.getCdOrigem() != null && !Validator.isBlankOrNull(Tab.getCdOrigem())) {
			ckAlteracao = true;
			//Log de campos -- Sempre antes da gravação por causa da persistencia.
			tAtual = tabRepository.findOne(Tab.getCdOrigem());
			//listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
			listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdOrigem()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
		}
		
		  try {	
			  
		   tNovo.setTabArmazemObj(tNovo.getTabArmazemObj().getCdCliente() != null ? tNovo.getTabArmazemObj() : null);	 
		   tNovo = tabRepository.save(tNovo);			  
		  }catch (ConstraintViolationException ex) {
			  
			 throw new ErrosConstraintException(GeneralUtil.MessageExceptionConstraint(ex), ex.getConstraintViolations());			  
		  }
		  
			//Log de campos
		  if (!ckAlteracao) {
			//tabLogBeanService.LogBean(String.valueOf(tNovo.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  	tabLogBeanService.LogBean(String.valueOf(tNovo.getCdOrigem()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  }else {
			  tabLogBeanService.GravarListaLog(listaLog);
		  }
		return tNovo;
		
	}

	public TabOrigemObj consultar(Integer CdOrigem) {
	 TabOrigemObj Tab = tabRepository.findOne(CdOrigem);
	 
	   return Tab;
	 
	}

	
	/* Se tela tiver excluir
		public void excluir(Integer CdOrigem) {

		   DadosUser user  = tabUsuarioService.DadosUsuario();

		   TabOrigemObj Tab = tabRepository.findOne(CdOrigem);
		   Tab.setDtCancelamento(new Date());
		   Tab.setCdUsuarioCancelamento(user.getCdUsuario());
		   tabRepository.save(Tab);
			 
		}
		
	*/	
	

}
