package br.com.jtpsolution.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.cadastros.varios.TabInformativoObj;



public interface TabInformativoRepository extends JpaRepository<TabInformativoObj, Integer> {


	TabInformativoObj findByCdCliente(Integer cdCliente);

}