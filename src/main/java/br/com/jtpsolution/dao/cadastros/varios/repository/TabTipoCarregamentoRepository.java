package br.com.jtpsolution.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.cadastros.varios.TabTipoCarregamentoObj;



public interface TabTipoCarregamentoRepository extends JpaRepository<TabTipoCarregamentoObj, Integer> {



}