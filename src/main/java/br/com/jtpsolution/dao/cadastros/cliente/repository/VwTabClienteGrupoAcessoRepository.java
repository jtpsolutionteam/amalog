package br.com.jtpsolution.dao.cadastros.cliente.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.cadastros.cliente.VwTabClienteGrupoAcessoObj;



public interface VwTabClienteGrupoAcessoRepository extends JpaRepository<VwTabClienteGrupoAcessoObj, Integer> {

	List<VwTabClienteGrupoAcessoObj> findByCdCliente(Integer cdCliente);

	List<VwTabClienteGrupoAcessoObj> findByTxCnpj(String txCnpj);
	
	VwTabClienteGrupoAcessoObj findByTxCnpjAndCkCliente(String txCnpj, Integer ckCliente);
	
	List<VwTabClienteGrupoAcessoObj> findByCdGrupoAcessoOrderByTxClienteAsc(Integer cdGrupoAcesso);

	VwTabClienteGrupoAcessoObj findByCdClienteAndCkCliente(Integer cdCliente, Integer ckCliente);

	
}