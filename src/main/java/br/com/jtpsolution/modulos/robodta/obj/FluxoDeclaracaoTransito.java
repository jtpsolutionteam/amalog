package br.com.jtpsolution.modulos.robodta.obj;

import java.io.Serializable;


public class FluxoDeclaracaoTransito implements Serializable{

	
	private SolicitacaoRegistro solicitacaoRegistro;
	private UnidadeRecinto origem;
	private UnidadeRecinto destino;
	private NomeCnpj beneficiario;
	private NomeCnpj transportador;
	
	private InformacoesDossie informacoesDossie;
	
	private InformacoesVeiculo informacoesVeiculo;
	private Recepcao recepcao;
	private Concessao concessao;
	
	private Carregamento carregamento;
	
	private InformacoesElementosSeguranca informacoesElementosSeguranca;
	
	private Desembaraco desembaraco;
	
	private ChegadaTransito chegadaTransito;
	private IntegridadeTransito integridadeTransito;
	private Armazenamento armazenamento;
	private ConclusaoTransito conclusaoTransito;
	
	private SelecaoParametrizada selecaoParametrizada;

	public SolicitacaoRegistro getSolicitacaoRegistro() {
		return solicitacaoRegistro;
	}

	public void setSolicitacaoRegistro(SolicitacaoRegistro solicitacaoRegistro) {
		this.solicitacaoRegistro = solicitacaoRegistro;
	}

	public UnidadeRecinto getOrigem() {
		return origem;
	}

	public void setOrigem(UnidadeRecinto origem) {
		this.origem = origem;
	}

	public UnidadeRecinto getDestino() {
		return destino;
	}

	public void setDestino(UnidadeRecinto destino) {
		this.destino = destino;
	}

	public NomeCnpj getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(NomeCnpj beneficiario) {
		this.beneficiario = beneficiario;
	}

	public NomeCnpj getTransportador() {
		return transportador;
	}

	public void setTransportador(NomeCnpj transportador) {
		this.transportador = transportador;
	}

	public InformacoesDossie getInformacoesDossie() {
		return informacoesDossie;
	}

	public void setInformacoesDossie(InformacoesDossie informacoesDossie) {
		this.informacoesDossie = informacoesDossie;
	}

	public InformacoesVeiculo getInformacoesVeiculo() {
		return informacoesVeiculo;
	}

	public void setInformacoesVeiculo(InformacoesVeiculo informacoesVeiculo) {
		this.informacoesVeiculo = informacoesVeiculo;
	}

	public Recepcao getRecepcao() {
		return recepcao;
	}

	public void setRecepcao(Recepcao recepcao) {
		this.recepcao = recepcao;
	}

	public Concessao getConcessao() {
		return concessao;
	}

	public void setConcessao(Concessao concessao) {
		this.concessao = concessao;
	}

	public Carregamento getCarregamento() {
		return carregamento;
	}

	public void setCarregamento(Carregamento carregamento) {
		this.carregamento = carregamento;
	}

	public InformacoesElementosSeguranca getInformacoesElementosSeguranca() {
		return informacoesElementosSeguranca;
	}

	public void setInformacoesElementosSeguranca(InformacoesElementosSeguranca informacoesElementosSeguranca) {
		this.informacoesElementosSeguranca = informacoesElementosSeguranca;
	}

	public Desembaraco getDesembaraco() {
		return desembaraco;
	}

	public void setDesembaraco(Desembaraco desembaraco) {
		this.desembaraco = desembaraco;
	}

	public ChegadaTransito getChegadaTransito() {
		return chegadaTransito;
	}

	public void setChegadaTransito(ChegadaTransito chegadaTransito) {
		this.chegadaTransito = chegadaTransito;
	}

	public IntegridadeTransito getIntegridadeTransito() {
		return integridadeTransito;
	}

	public void setIntegridadeTransito(IntegridadeTransito integridadeTransito) {
		this.integridadeTransito = integridadeTransito;
	}

	public Armazenamento getArmazenamento() {
		return armazenamento;
	}

	public void setArmazenamento(Armazenamento armazenamento) {
		this.armazenamento = armazenamento;
	}

	public ConclusaoTransito getConclusaoTransito() {
		return conclusaoTransito;
	}

	public void setConclusaoTransito(ConclusaoTransito conclusaoTransito) {
		this.conclusaoTransito = conclusaoTransito;
	}

	public SelecaoParametrizada getSelecaoParametrizada() {
		return selecaoParametrizada;
	}

	public void setSelecaoParametrizada(SelecaoParametrizada selecaoParametrizada) {
		this.selecaoParametrizada = selecaoParametrizada;
	}

	
	
}
