package br.com.jtpsolution.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.NumberFormat;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.cadastros.cliente.TabClienteObj;
import br.com.jtpsolution.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_destino", schema = Constants.SCHEMA)
public class TabDestinoObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_destino")
	// @NotNull(message = "Destino campo obrigatório!")
	private Integer cdDestino;

	@Column(name = "tx_destino")
	// @NotEmpty(message = "Destino campo obrigatório!")
	@Size(max = 45, message = "Destino tamanho máximo de 45 caracteres")
	private String txDestino;
	
	@Column(name = "vl_dta_importacao")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlDtaImportacao;
	
	@Column(name = "vl_exportacao")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlExportacao;
	
	@Column(name = "ck_ativo")
	private Integer ckAtivo;
	
	@Column(name = "ck_rota_amalog")
	private Integer ckRotaAmalog;
	
	@ManyToOne
	@JoinColumn(name = "cd_armazem")
	private TabClienteObj tabArmazemObj;
	
	@Column(name = "cd_codigo_ibge")
	private Integer cdCodigoIbge;
	
	@Column(name = "vl_icms")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlIcms;
	
	@Column(name = "vl_gris")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlGris;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txDestino))
			txDestino = txDestino.toUpperCase();
	}

	

}
