package br.com.jtpsolution.util;

import java.io.Serializable;
import java.util.regex.Pattern;

public class CalcRandom implements Serializable {

    private String[] codigo,codigo2;
    private int indicecodigo;

    public CalcRandom () {
      codigo = new String[34];
      codigo2 = new String[10];

      codigo[0] = "A";
      codigo[1] = "B";
      codigo[2] = "C";
      codigo[3] = "D";
      codigo[4] = "E";
      codigo[5] = "F";
      codigo[6] = "G";
      codigo[7] = "H";
      codigo[8] = "I";
      codigo[9] = "J";

      codigo[10] = "K";
      codigo[11] = "L";
      codigo[12] = "M";
      codigo[13] = "N";
      codigo[14] = "O";
      codigo[15] = "P";
      codigo[16] = "Q";
      codigo[17] = "R";
      codigo[18] = "S";
      codigo[19] = "T";

      codigo[20] = "U";
      codigo[21] = "V";
      codigo[22] = "X";
      codigo[23] = "Z";
      codigo[24] = "1";
      codigo[25] = "2";
      codigo[26] = "3";
      codigo[27] = "4";
      codigo[28] = "5";
      codigo[29] = "6";

      codigo[30] = "7";
      codigo[31] = "8";
      codigo[32] = "9";
      codigo[33] = "0";

      codigo2[0] = "1";
      codigo2[1] = "2";
      codigo2[2] = "3";
      codigo2[3] = "4";
      codigo2[4] = "5";
      codigo2[5] = "6";

      codigo2[6] = "7";
      codigo2[7] = "8";
      codigo2[8] = "9";
      codigo2[9] = "0";

      indicecodigo = 0;

    }




    public String getloginAleatoria (int quantX) {
      String cd_codigo = "", ncodigo="";
      int i = 0;

      for (int o=0; o < quantX; o++) {

         i = (int) (Math.random() * codigo.length);


           String arr_items[] = Pattern.compile(codigo[i]).split(cd_codigo);
           String[] ar_labels = new String[arr_items.length];

           if (ar_labels.length == 1) {

             cd_codigo += codigo[i];

           }

           if (ar_labels.length > 1) {
            o--;
           }
           ncodigo = codigo[i];
      }

      return cd_codigo;
    }


    public String getCodigoAleatorio (int quantX) {
      String cd_codigo = "", ncodigo="";
      int i = 0;

      for (int o=0; o < quantX; o++) {

         i = (int) (Math.random() * codigo2.length);

           String arr_items[] = Pattern.compile(codigo2[i]).split(cd_codigo);
           String[] ar_labels = new String[arr_items.length];

           if (ar_labels.length == 1) {

             cd_codigo += codigo2[i];

           }

           if (ar_labels.length > 1) {

             //System.out.println(codigo2[i]);
             o--;
           }
           ncodigo = codigo2[i];
      }

      return cd_codigo;
    }
	
	
	
}
