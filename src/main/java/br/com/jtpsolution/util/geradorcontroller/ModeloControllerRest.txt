
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.mensagens.TabRetornoSucessoObj;

@RestController
@RequestMapping("/#TabObj")
public class #Classe {

	@Autowired
	private #ServiceService tabService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<#TabObj>> listar() {

		return ResponseEntity.status(HttpStatus.OK).body(tabService.listar());
		
	}
	
	@RequestMapping(value="/consultar", method = RequestMethod.POST)
	public ResponseEntity<List<?>> consultar(@RequestBody #TabJoinObj TabJoin) {
		
		   return ResponseEntity.status(HttpStatus.OK).body(tabService.listar());
		
	}
	
	@RequestMapping(value= "/{#Field}", method = RequestMethod.GET)
	public ResponseEntity<?> consultar(@PathVariable("#Field") #TipoField #Field) {

	 #TabObj Tab = tabService.consultar(#Field);

	 return ResponseEntity.status(HttpStatus.OK).body(Tab);
	 
	}
	
	
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<TabRetornoSucessoObj> incluir(@Valid @RequestBody #TabJoinObj TabJoin) {
		   tabService.incluir(TabJoin);	
		
		   TabRetornoSucessoObj tRet = new TabRetornoSucessoObj();
		   tRet.setTx_chave(String.valueOf(TabJoin.get#TabObj().get#Field()));
		   tRet.setCd_mensagem(1);
		   tRet.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
		   tRet.setDt_system(System.currentTimeMillis());

		   return ResponseEntity.status(HttpStatus.OK).body(tRet);
		
	}

	

	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<TabRetornoSucessoObj> alterar(@Valid @RequestBody #TabJoinObj TabJoin) {
	   tabService.alterar(TabJoin);
	   
	   TabRetornoSucessoObj tRet = new TabRetornoSucessoObj();
	   tRet.setTx_chave(String.valueOf(TabJoin.get#TabObj().get#Field()));
	   tRet.setCd_mensagem(1);
	   tRet.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
	   tRet.setDt_system(System.currentTimeMillis());

	   return ResponseEntity.status(HttpStatus.OK).body(tRet);
	}

	
	
}
