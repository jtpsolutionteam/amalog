package br.com.jtpsolution.util.regras;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.ObjectError;

import br.com.jtpsolution.dao.TransactionUtilBean;
import br.com.jtpsolution.dao.administracao.repository.VwTabRegrasRepository;
import br.com.jtpsolution.dao.administracao.vw.VwTabRegrasObj;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.logErro.JTPLogErroBean;

@Service
public class RegrasBean {

	@Autowired
	private JTPLogErroBean logErroBean;

	@Autowired
	private UsuarioBean tabUsuarioService;

	@Autowired 
	private VwTabRegrasRepository vwTabRegrasRepository;
	
	@Autowired
	private TransactionUtilBean transactionUtilBean;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	
	
	public List<ObjectError> verificaregras(String txController, HttpServletRequest obj) {

		List<ObjectError> listError = new ArrayList<ObjectError>();
		try {

			// 1 = NotNull
			// 2 = Condição
			// 3 = Condição Data menor
			// 4 = Obrigatório Se
			// 5 = Validador Caracter Especial
			// 6 = KeyViolation

			List<VwTabRegrasObj> listRegras = vwTabRegrasRepository.findByTxControllerOrderByCdOrdemAsc(txController);
			ObjectError objError = null;

			if (!listRegras.isEmpty()) {
				for (VwTabRegrasObj atual : listRegras) {

					String txObjCampo = atual.getTxObjcampo();
					String txObjCampoCond = atual.getTxObjcampoCond();
					String txObjCampoChave = atual.getTxCampoChave();

					switch (atual.getCdTipo()) {
					case 1: {
						String txFieldValue = retornaValueObj(obj, txObjCampo);
						if (Validator.isBlankOrNull(txFieldValue)) {
							objError = new ObjectError("NotNull", atual.getTxCampo() + " campo obrigatório!");
							listError.add(objError);
						}
						break;
					}
					case 2: {
						String txFieldValue = retornaValueObj(obj, txObjCampo);
						String txFieldCondValue = retornaValueObj(obj, txObjCampoCond);
						if (!Validator.isBlankOrNull(txFieldValue) && Validator.isBlankOrNull(txFieldCondValue)) {
							objError = new ObjectError("Condicao",
									atual.getTxCampo() + " não pode ser preenchido antes do " + atual.getTxCampoCond());
							listError.add(objError);
						}
						break;
					}
					case 3: {
						String txFieldValue = retornaValueObj(obj, txObjCampo);
						String txFieldCondValue = retornaValueObj(obj, txObjCampoCond);
						if (!Validator.isBlankOrNull(txFieldValue) && !Validator.isBlankOrNull(txFieldCondValue)) {
							if (Validator.isValidDate(txFieldValue) && Validator.isValidDate(txFieldCondValue)) {
								long dtObjCampo = GeneralParser.dateToLong(txFieldValue);
								long dtObjCampocond = GeneralParser.dateToLong(txFieldCondValue);
								if (dtObjCampo < dtObjCampocond) {
									objError = new ObjectError("CondicaoNotDataMenor",
											atual.getTxCampo() + " não pode ser menor que o " + atual.getTxCampoCond());
									listError.add(objError);
								}
							} else {
								objError = new ObjectError("CondicaoNotDataMenor",
										atual.getTxCampo() + " ou " + atual.getTxCampoCond() + " - Data Inválida!");
								listError.add(objError);
							}
						}
						break;
					}
					case 4: {
                         String txFieldValue = obj.getParameter(txObjCampo);
                         String txFieldCondValue = obj.getParameter(txObjCampoCond);
                         if (Validator.isBlankOrNull(txFieldValue) && !Validator.isBlankOrNull(txFieldCondValue)) {                      
                                  objError = new ObjectError("NotNull",atual.getTxCampo()+" campo obrigatório!");                        
                                  listError.add(objError);
                         }
                         break;
					 }
					case 5: {
						String txFieldValue = retornaValueObj(obj, txObjCampo);
						if (!Validator.isBlankOrNull(txFieldValue)) {
							if (!Validator.validarStringCaracterEspecial(txFieldValue)) {
								objError = new ObjectError("CaracterEspecial",
										atual.getTxCampo() + " possuí caracteres especiais não permitidos!");
								listError.add(objError);
							}
						}
						break;
					}					
					case 6: {

						String txCampoChave = retornaValueObj(obj, txObjCampoChave);
						String txFieldValue = retornaValueObj(obj, txObjCampo);
						String txAux = atual.getTxAux();
						String txFieldValue2 = retornaValueObj(obj, txObjCampoCond);
						List l = null;
						if (!Validator.isBlankOrNull(txFieldValue) && !Validator.isBlankOrNull(txAux) ) {
							
							String txFields = txObjCampoChave;
							
								if (Validator.isBlankOrNull(txFieldValue2)) {								
									l = queryStr(txAux, txFields, txObjCampo, txFieldValue);
								}else {
									l = queryStr(txAux, txFields, txObjCampo, txFieldValue);
								}
							
								if (Validator.isBlankOrNull(txCampoChave)) {
									if (!l.isEmpty()) {										
											objError = new ObjectError("KeyViolation",
													atual.getTxCampo() + " " + atual.getTxMensagem());
											listError.add(objError);										
									}	
								}else {
									if (!l.isEmpty()) {
										String txFieldChave = l.get(0).toString();																	
										if (!txFieldChave.equals(txCampoChave)) {
											objError = new ObjectError("KeyViolation",
													atual.getTxCampo() + " " + atual.getTxMensagem());
											listError.add(objError);
										}
									}
								}
						}
						
						
						/*
						if (Validator.isBlankOrNull(txCampoChave)) {
							String txFieldValue = retornaValueObj(obj, txObjCampo);
							if (!Validator.isBlankOrNull(txFieldValue)) {

								String txAux = atual.getTxAux();
								List l = null;
								if (!Validator.isBlankOrNull(txObjCampo)
										&& (!Validator.isBlankOrNull(txObjCampoCond))) {
									String txFieldValue2 = retornaValueObj(obj, txObjCampoCond);
									l = queryStr(txAux, txObjCampo, txFieldValue, txObjCampoCond, txFieldValue2);
								} else {
									l = queryStr(txAux, txObjCampo, txFieldValue);
								}

								if (!l.isEmpty()) {
									objError = new ObjectError("KeyViolation",
											atual.getTxCampo() + " " + atual.getTxMensagem());
									listError.add(objError);
								}
							}
						}*/
						break;
					}
					}
				}
			}

		} catch (Exception ex) {
			logErroBean.addLog(getClass().getSimpleName(), ex.getMessage(), ex);
		}

		return listError;

	}

	
	private String retornaValueObj(HttpServletRequest request, String txField) {

		String txValue = "";

		try {

			if (request.getParameter(txField) != null) {
				txValue = request.getParameter(txField);
			}

		} catch (Exception ex) {
			//ex.printStackTrace();
		}

		//System.out.println(txValue);
		return txValue;
	}

	
	
	private String retornaValueObj(Object obj, String txField) {

		String txValue = "";

		try {

			Field[] campos = obj.getClass().getDeclaredFields();

			for (int i = 0; i < campos.length; i++) {

				campos[i].setAccessible(true);

				String fieldName = campos[i].getName();
				String fieldType = campos[i].getType().toString().toLowerCase();

				if (fieldName.equals(txField)) {
					txValue = campos[i].get(obj).toString();
					if (fieldType.contains("date")) {
						txValue = GeneralParser.format_dateUS(txValue, "dd/MM/yyyy");
					}
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		//System.out.println(txValue);
		return txValue;
	}

	private List queryStr(String txTabela, String txField, String txValue) {

		String StrSql = "select t from " + txTabela + " t where t." + txField + " = '" + txValue + "'";

		List l = transactionUtilBean.querySelect(StrSql);

		return l;

	}

	private List queryStr(String txTabela, String txField, String txValue, String txField2, String txValue2) {

		String StrSql = "select t from " + txTabela + " t where t." + txField + " = '" + txValue + "' and t." + txField2
				+ " = '" + txValue2 + "'";

		List l = transactionUtilBean.querySelect(StrSql);

		return l;

	}
	
	
	private List queryStr(String txTabela, String txFields, String txFieldWhere, String txValue, String txFieldWhere2, String txValue2) {

		String StrSql = "select "+txFields+" from " + txTabela + " where " + txFieldWhere + " = '" + txValue + "' and " + txFieldWhere2 + " = '" + txValue2+"'";

		List l = transactionUtilBean.querySelect(StrSql);

		return l;

	}
	
	
	private List queryStr(String txTabela, String txFields, String txFieldWhere, String txValue) {

		String StrSql = "select "+txFields+" from " + txTabela + " where " + txFieldWhere + " = '" + txValue + "'";

		List l = transactionUtilBean.querySelect(StrSql);

		return l;

	}

}
