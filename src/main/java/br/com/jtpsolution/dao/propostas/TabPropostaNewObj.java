package br.com.jtpsolution.dao.propostas;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.cadastros.cliente.TabClienteGrupoAcessoObj;
import br.com.jtpsolution.dao.cadastros.cliente.TabClienteObj;
import br.com.jtpsolution.dao.cadastros.usuario.TabUsuarioObj;
import br.com.jtpsolution.dao.cadastros.varios.TabDestinoLtlObj;
import br.com.jtpsolution.dao.cadastros.varios.TabDestinoObj;
import br.com.jtpsolution.dao.cadastros.varios.TabOrigemLtlObj;
import br.com.jtpsolution.dao.cadastros.varios.TabOrigemObj;
import br.com.jtpsolution.dao.cadastros.varios.TabStatusObj;
import br.com.jtpsolution.dao.cadastros.varios.TabTipoCarregamentoObj;
import br.com.jtpsolution.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_proposta", schema = Constants.SCHEMA)
public class TabPropostaNewObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_proposta") 
	//@NotNull(message = "Proposta campo obrigatório!")
	private Integer cdProposta; 
	
	@ManyToOne
	@JoinColumn(name = "cd_usuario") 
	private TabUsuarioObj tabUsuarioObj; 
	
	@ManyToOne
	@JoinColumn(name = "cd_cliente") 
	//@NotNull(message = "Cliente campo obrigatório!")
	private TabClienteObj tabClienteObj;
	
	@ManyToOne
	@JoinColumn(name = "cd_freehand") 
	//@NotNull(message = "Cliente campo obrigatório!")
	private TabClienteObj tabFreehandObj;
	
	@Column(name = "tx_importador") 
	//@NotNull(message = "Cliente campo obrigatório!")
	private String txImportador;
	 
	@Column(name = "dt_proposta") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtProposta; 
	
	@Column(name = "dt_validade") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtValidade; 
	 
	@Column(name = "tx_proposta") 
	//@NotEmpty(message = "Proposta campo obrigatório!")
	@Size(max = 15, message = "Proposta tamanho máximo de 15 caracteres") 
	private String txProposta; 
	
	@Column(name = "tx_lote") 
	//@NotEmpty(message = "Proposta campo obrigatório!")
	@Size(max = 15, message = "Proposta tamanho máximo de 15 caracteres") 
	private String txLote; 
	 
	@ManyToOne
	@JoinColumn(name = "cd_status") 	
	private TabStatusObj tabStatusObj; 
	 
	@ManyToOne
	@JoinColumn(name = "cd_tipo_carregamento") 
	//@NotNull(message = "TipoCarregamento campo obrigatório!")
	private TabTipoCarregamentoObj tabTipoCarregamentoObj; 
	 
	@ManyToOne 
	@JoinColumn(name = "cd_origem") 
	private TabOrigemObj tabOrigemObj; 
	
	@ManyToOne 
	@JoinColumn(name = "cd_destino") 
	private TabDestinoObj tabDestinoObj; 
	 
	@Column(name = "ck_desistencia_vistoria") 
	private Integer ckDesistenciaVistoria; 
	 
	@Column(name = "dt_liberacao_documentos") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtLiberacaoDocumentos; 
	 
	@Column(name = "dt_dta_solicitacao") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtDtaSolicitacao; 
	 
	@Column(name = "dt_dta_registro") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtDtaRegistro; 
	 
	@Column(name = "dt_dta_carregamento") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtDtaCarregamento; 
	 
	@Column(name = "dt_dta_parametrizacao") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtDtaParametrizacao; 
	
	@Column(name = "dt_prev_carregamento") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtPrevCarregamento;
	 
	@Column(name = "tx_dta_canal") 
	//@NotEmpty(message = "DtaCanal campo obrigatório!")
	@Size(max = 30, message = "DtaCanal tamanho máximo de 30 caracteres") 
	private String txDtaCanal; 
	 
	@Column(name = "dt_dta_desembaraco") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtDtaDesembaraco; 
	 
	@Column(name = "dt_dta_inicio_transito") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtDtaInicioTransito; 
	 
	@Column(name = "dt_dta_chegada_transito") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtDtaChegadaTransito; 
	 
	@Column(name = "dt_dta_conclusao_transito") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtDtaConclusaoTransito; 
	 
	@Column(name = "dt_dta_ultima_consulta") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtDtaUltimaConsulta; 
	 
	@Column(name = "ck_upload_doctos") 
	//@NotNull(message = "ckUploadDoctos campo obrigatório!")
	private Integer ckUploadDoctos; 
	 
	@Column(name = "tx_check_upload_doctos") 
	//@NotEmpty(message = "CheckUploadDoctos campo obrigatório!")
	@Size(max = 500, message = "CheckUploadDoctos tamanho máximo de 500 caracteres") 
	private String txCheckUploadDoctos; 
	 
	@Column(name = "ck_reconferencia") 
	//@NotNull(message = "ckReconferencia campo obrigatório!")
	private Integer ckReconferencia; 
	 
	@Column(name = "tx_bl") 
	//@NotEmpty(message = "Bl campo obrigatório!")
	@Size(max = 30, message = "Bl tamanho máximo de 30 caracteres") 
	private String txBl; 
	 
	@Column(name = "tx_reserva") 
	//@NotEmpty(message = "Reserva campo obrigatório!")
	@Size(max = 15, message = "Reserva tamanho máximo de 15 caracteres") 
	private String txReserva; 
	 
	@Column(name = "tx_terminal_mar") 
	//@NotEmpty(message = "TerminalMar campo obrigatório!")
	@Size(max = 10, message = "TerminalMar tamanho máximo de 10 caracteres") 
	private String txTerminalMar; 

	@Column(name = "tx_numero_dta") 	
	private String txNumeroDta; 
	 
	@Column(name = "dt_boleto_vencto") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtBoletoVencto; 
	 
	@Column(name = "dt_boleto_pagto") 
	@DateTimeFormat(pattern = "dd/MM/yyyy") 
	@Temporal(TemporalType.DATE) 
	private Date dtBoletoPagto; 
	
	@Column(name = "tx_boleto_url") 	
	private String txBoletoUrl;
	
	@Column(name = "tx_due") 	
	private String txDue;
	
	@Column(name = "tx_nfs") 	
	private String txNfs;
	
	@Column(name = "tx_tipo_proposta") 
	//@NotEmpty(message = "TipoProposta campo obrigatório!")
	@Size(max = 20, message = "TipoProposta tamanho máximo de 20 caracteres") 
	private String txTipoProposta; 
	 
	@ManyToOne
	@JoinColumn(name = "cd_origem_ltl") 
	private TabOrigemLtlObj tabOrigemLtlObj; 
	 
	@ManyToOne
	@JoinColumn(name = "cd_destino_ltl") 
	private TabDestinoLtlObj tabDestinoLtlObj; 
	
	@Column(name = "tx_di") 
	private String txDi; 
	
	@Column(name = "ck_boleto_cancelado") 
	private Integer ckBoletoCancelado;
	
	@Column(name = "tx_referencia") 
	private String txReferencia;
	
	@Column(name = "dt_doctos_liberados_registro_dta") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtDoctosLiberadosRegistroDta;

	
	@Column(name = "ck_anvisa") 
	private Integer ckAnvisa;

	@Column(name = "ck_imo_perigoso") 
	private Integer ckImoPerigoso;
	
	@Column(name = "dt_pendencia_dta_carregamento") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtPendenciaDtaCarregamento;

	@Column(name = "tx_pendencia_dta_carregamento") 
	private String txPendenciaDtaCarregamento;
	
	@Column(name = "tx_situacao_ce_master") 
	private String txSituacaoCeMaster;
	
	@Column(name = "tx_email_taxa_informativo") 
	private String txEmailTaxaInformativo;
	
	@Column(name = "vl_comprimento")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlComprimento;
	
	@Column(name = "vl_altura")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlAltura;

	@Column(name = "vl_largura")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlLargura;
	
	@Column(name = "vl_m3_pack")
	@NumberFormat(pattern = "#,##0.000")
	private Double vlM3Pack;
	
	@Column(name = "vl_peso_extrato_desova")
	@NumberFormat(pattern = "#,##0.000")
	private Double vlPesoExtratoDesova;
	
	@Column(name = "vl_mercadoria_invoice")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlMercadoriaInvoice;
	
	@Column(name = "tx_volume") 
	private String txVolume;
	
	@Column(name = "vl_qtde_volume") 
	private Integer vlQtdeVolume;
	
	@Column(name = "tx_container") 
	//@NotEmpty(message = "Container campo obrigatório!")
	@Size(max = 20, message = "Container tamanho máximo de 20 caracteres") 
	private String txContainer; 
	 
	@Column(name = "dt_saida") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtSaida; 
	
	@Column(name = "dt_entrada_cd") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtEntradaCd;
	
	@Column(name = "dt_fim_desova") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtFimDesova;

	@OneToMany(mappedBy = "cdProposta", targetEntity = TabPropostaGrupoAcessoObj.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	List<TabClienteGrupoAcessoObj> listPropostaGrupoAcesso;
	
	
	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txProposta))
			txProposta = txProposta.toUpperCase();		
		if (!Validator.isBlankOrNull(txDtaCanal))
			txDtaCanal = txDtaCanal.toUpperCase();
	}

}
