package br.com.jtpsolution.modulos.mantran.obj;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TabMantranCteObj {

	private String key;
	private String CGC_CPF_Remetente;
	private String Razao_Social_Remetente;
	private String NR_NF;
	private String CGC_CPF_Cliente;
	private String Razao_Social_Cliente;
	private String CGC_CPF_Destinatario;
	private String IE_Destinatario;
	private String Razao_Social_Destinatario;
	private String CGC_CPF_Faturamento;
	private String Razao_Social_Faturamento;
	private String Endereco_Entrega;
	private String Cidade_Entrega;
	private String UF_Entrega;
	private String CEP_Destinatario;
	private String CEP_REF_Destino;
	private String UF_Origem;
	private String CEP_REF_Origem;
	private String DT_Emissao;
	private Integer ID_Volume;
	private Integer QT_Volume;
	private Float Peso_NF;
	private Float Cubagem;
	private Float Peso_Calculo;
	private Float VR_Total_NF;
	private String Bairro_Entrega;
	private String NR_DI;
	private String CEP_Origem;
	
}
