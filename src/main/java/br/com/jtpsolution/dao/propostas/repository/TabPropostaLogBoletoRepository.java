package br.com.jtpsolution.dao.propostas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.propostas.TabPropostaLogBoletoObj;



public interface TabPropostaLogBoletoRepository extends JpaRepository<TabPropostaLogBoletoObj, Integer> {



}