package br.com.jtpsolution.modulos.propostas.service;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.jtpsolution.dao.propostas.TabPropostaDocumentosObj;
import br.com.jtpsolution.dao.propostas.TabPropostaNovaObj;
import br.com.jtpsolution.dao.propostas.TabPropostaObj;
import br.com.jtpsolution.dao.propostas.VwTabPropostaDocumentosLtlObj;
import br.com.jtpsolution.dao.propostas.VwTabPropostaObj;
import br.com.jtpsolution.dao.propostas.repository.TabPropostaDocumentosRepository;
import br.com.jtpsolution.dao.propostas.repository.TabPropostaNovaRepository;
import br.com.jtpsolution.dao.propostas.repository.TabPropostaRepository;
import br.com.jtpsolution.dao.propostas.repository.VwTabPropostaDocumentosLtlRepository;
import br.com.jtpsolution.dao.propostas.repository.VwTabPropostaDocumentosRepository;
import br.com.jtpsolution.dao.propostas.repository.VwTabPropostaRepository;
import br.com.jtpsolution.dao.util.log.TabLogObj;
import br.com.jtpsolution.exceptions.ErrosConstraintException;
import br.com.jtpsolution.security.DadosUser;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralUtil;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.log.LogBean;
import br.com.jtpsolution.util.logErro.JTPLogErroBean;

@Service
public class TabPropostaService {

	@Autowired
	private JTPLogErroBean logErroBean;
	
	@Autowired
	private TabPropostaRepository  tabRepository;
	
	@Autowired
	private TabPropostaNovaRepository  tabNovaRepository;
	
	@Autowired
	private TabPropostaDocumentosRepository  tabPropostaDocumentosRepository;
	
	@Autowired
	private VwTabPropostaDocumentosRepository  tabVwPropostaDocumentosRepository;
	
	@Autowired
	private VwTabPropostaDocumentosLtlRepository  tabVwPropostaDocumentosLtlRepository;
	
	@Autowired
	private VwTabPropostaRepository  tabVwRepository;
		
	@Autowired
	private UsuarioBean tabUsuarioService;
	
	@Autowired
	private LogBean tabLogBeanService;

	/*
	@Autowired 
	private TabFiltro.....Bean tabFiltroService;
	
	*/
	
	
	public List<TabPropostaObj> listarAguardandoDatConclusao() {
		return tabRepository.findByListAgConclusaoDATQuery();
		
	}
	
	public List<TabPropostaObj> listar() {
		return tabRepository.findAll();
		
	}

	public List<TabPropostaObj> listarDtaSConclusaoTransito() {
		return tabRepository.findByConclusaoTransitoNullQuery();
		
	}

	public List<TabPropostaObj> listar(Integer cdStatus) {
		return tabRepository.findByConclusaoTransitoNullQuery();
		
	}

	public List<VwTabPropostaObj> listarPropostaDoctosPendentes(String txCheckUploadDoctos) {
		
		DadosUser user  = tabUsuarioService.DadosUsuario();
		
		return tabVwRepository.findByTxCheckUploadDoctosContainingAndCdGrupoAcessoOrderByDtPropostaAsc(txCheckUploadDoctos, user.getCdGrupoAcesso());
		
	}
	

	public List<VwTabPropostaObj> listarPropostaUploadOK(Integer ckUploadDoctos) {
		
		DadosUser user  = tabUsuarioService.DadosUsuario();
		
		return tabVwRepository.findByListCkUploadOKQuery(ckUploadDoctos, user.getCdGrupoAcesso());
		
	}
	
	public List<VwTabPropostaObj> listarPropostaUploadOKReconferencia(Integer ckUploadDoctos) {
		
		DadosUser user  = tabUsuarioService.DadosUsuario();
		
		return tabVwRepository.findByListCkUploadOKReconfereciaQuery(ckUploadDoctos, user.getCdGrupoAcesso());
		
	}
	
	public List<VwTabPropostaObj> listarPropostaDoctosAceitosSemDesisteciaVistoria() {
		
		DadosUser user  = tabUsuarioService.DadosUsuario();
		
		return tabVwRepository.findByListDoctosSemDesistenciaVistoriaQuery(user.getCdGrupoAcesso());
		
	}
	
	public List<VwTabPropostaObj> listarPropostasFaturadasPagas(boolean ckPagas) {
		
		DadosUser user  = tabUsuarioService.DadosUsuario();
		
		if (ckPagas) {
			return tabVwRepository.findByListDtBoletoPagtoPagosQuery(user.getCdGrupoAcesso());
		}else {
			return tabVwRepository.findByListDtBoletoPagtoEmAbertoQuery(user.getCdGrupoAcesso());	
		}
		
		
	}

	
	/* Modelo tela filho 
	
		public List<VwTabPropostaObj> listar(String CampoRelacionamento) {

		DadosUser user  = tabUsuarioService.DadosUsuario();

		return tabVwRepository.findByTxNrefQuery(CampoRelacionamento, user.getCdGrupoAcesso());
		
	}
	*/
	
	/* Modelo - Tela Pesquisar
			public Page<VwTabPropostaObj> telapesquisar(TabFiltroObj tabFiltroObj, Pageable pageable) {

			Page<VwTabPropostaObj> Tab = tabFiltroService.filtrar(tabFiltroObj, pageable);
			
			return Tab;

		}
	*/


	public TabPropostaObj gravar(TabPropostaObj Tab, String submitFields) {
		
		DadosUser user  = tabUsuarioService.DadosUsuario();
		
		
		
		  try {	
			  
			  //if (Tab.getTabFreehandObj() != null)
			  //Tab.setTabFreehandObj(Tab.getTabFreehandObj().getCdCliente() != null ? Tab.getTabFreehandObj() : null);
			  /*
			  if (Tab.getTabOrigemObj() != null)
			  Tab.setTabOrigemObj(Tab.getTabOrigemObj().getCdOrigem() != null ? Tab.getTabOrigemObj() : null);
			  if (Tab.getTabDestinoObj() != null)
			  Tab.setTabDestinoObj(Tab.getTabDestinoObj().getCdDestino() != null ? Tab.getTabDestinoObj() : null);
			  if (Tab.getTabColoaderDespachanteObj() != null)
			  Tab.setTabColoaderDespachanteObj(Tab.getTabColoaderDespachanteObj().getCdCliente() != null ? Tab.getTabColoaderDespachanteObj() : null);
			  if (Tab.getTabUsuarioAceiteObj() != null)
			  Tab.setTabUsuarioAceiteObj(Tab.getTabUsuarioAceiteObj().getCdUsuario() != null ? Tab.getTabUsuarioAceiteObj() : null);
			  if (Tab.getTabStatusObj() != null)
			  Tab.setTabStatusObj(Tab.getTabStatusObj().getCdStatus() != null ? Tab.getTabStatusObj() : null);			  
			  if (Tab.getTabPerfilVeiculoObj() != null)
			  Tab.setTabPerfilVeiculoObj(Tab.getTabPerfilVeiculoObj().getCdPerfilVeiculo() != null ? Tab.getTabPerfilVeiculoObj() : null);
			  */
			  
			  //Tab = tabRepository.save(Tab);
			  Tab =	tabRepository.save(Tab);
			  		
			  tabLogBeanService.addLog(getClass().getSimpleName(), String.valueOf(Tab.getCdProposta()), user.getCdUsuario(), submitFields);
		   
		  }catch (ConstraintViolationException ex) {
			  
			 throw new ErrosConstraintException(GeneralUtil.MessageExceptionConstraint(ex), ex.getConstraintViolations());			  
		  }
		  
		
		return Tab;
		
	}
	
	
	public TabPropostaObj gravar(TabPropostaObj Tab) {
		
		  try {	
			  Tab = tabRepository.save(Tab);
		   
		  }catch (ConstraintViolationException ex) {
			  
			 throw new ErrosConstraintException(GeneralUtil.MessageExceptionConstraint(ex), ex.getConstraintViolations());			  
		  }
		  
		
		return Tab;
		
	}
	
	
	public TabPropostaDocumentosObj gravarDocumento(TabPropostaDocumentosObj Tab) {
		
		boolean ckAlteracao = false;
				
		DadosUser user  = tabUsuarioService.DadosUsuario();

		
		TabPropostaDocumentosObj tAtual = new TabPropostaDocumentosObj();
		TabPropostaDocumentosObj tNovo = Tab;
		
		//tNovo.setCdUsuario(null);
		List<TabLogObj> listaLog = null;
		
		if (Tab.getCdPropostaDocumento() != null && !Validator.isBlankOrNull(Tab.getCdPropostaDocumento())) {
			ckAlteracao = true;
			//Log de campos -- Sempre antes da gravação por causa da persistencia.
			tAtual = tabPropostaDocumentosRepository.findOne(Tab.getCdPropostaDocumento());
			//listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
			listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdPropostaDocumento()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
		}
		
		  try {	
		   tNovo = tabPropostaDocumentosRepository.save(tNovo);			  
		  }catch (ConstraintViolationException ex) {
			  
			 throw new ErrosConstraintException(GeneralUtil.MessageExceptionConstraint(ex), ex.getConstraintViolations());			  
		  }
		  
			//Log de campos
		  if (!ckAlteracao) {
			//tabLogBeanService.LogBean(String.valueOf(tNovo.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  	tabLogBeanService.LogBean(String.valueOf(tNovo.getCdPropostaDocumento()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  }else {
			  tabLogBeanService.GravarListaLog(listaLog);
		  }
		return tNovo;
		
	}
	
	
	public TabPropostaObj consultarSimple(Integer cdProposta) {
		
		TabPropostaObj Tab = null;
		Tab = tabRepository.findOne(cdProposta);
			
	 
	   return Tab;
	 
	}

	public TabPropostaObj consultar(Integer cdProposta) {
		
		DadosUser user  = tabUsuarioService.DadosUsuario();	
		TabPropostaObj Tab = tabRepository.findByCdPropostaAndCdGrupoAcessoQuery(cdProposta, user.getCdGrupoAcesso());
			
	 
	   return Tab;
	 
	}
	
	
	public TabPropostaNovaObj consultarSimples(Integer cdProposta) {
		
		
		TabPropostaNovaObj Tab = tabNovaRepository.findByCdPropostaQuery(cdProposta);
			
	 
	   return Tab;
	 
	}
	
	public VwTabPropostaObj consultarProposta(Integer cdProposta) {
		
		
		VwTabPropostaObj Tab = tabVwRepository.findByCdProposta(cdProposta);
			
	 
	   return Tab;
	 
	}	
	
	
	public VwTabPropostaObj consultar(String txHashProposta) {
		
		DadosUser user  = tabUsuarioService.DadosUsuario();	
		VwTabPropostaObj Tab = tabVwRepository.findByTxHashPropostaAndCdGrupoAcesso(txHashProposta, user.getCdGrupoAcesso());
	 
	   return Tab;
	 
	}
	
	public List<TabPropostaDocumentosObj> listaDocumentos(Integer cdProposta) {
		
		List<TabPropostaDocumentosObj> Tab = tabPropostaDocumentosRepository.findByCdProposta(cdProposta);
	 
	   return Tab;
	 
	}
	
	
	public List<VwTabPropostaDocumentosLtlObj> listaDocumentosLtl(Integer cdProposta) {
		
		List<VwTabPropostaDocumentosLtlObj> Tab = tabVwPropostaDocumentosLtlRepository.findByCdProposta(cdProposta);
	 
	   return Tab;
	 
	}
	
	public TabPropostaDocumentosObj consultarDocumento(Integer cdPropostaDocumento) {
		
		TabPropostaDocumentosObj Tab = tabPropostaDocumentosRepository.findOne(cdPropostaDocumento);
	 
	   return Tab;
	 
	}

	
	/* Se tela tiver excluir
		public void excluir(Integer CdProposta) {

		   DadosUser user  = tabUsuarioService.DadosUsuario();

		   TabPropostaObj Tab = tabRepository.findOne(CdProposta);
		   Tab.setDtCancelamento(new Date());
		   Tab.setCdUsuarioCancelamento(user.getCdUsuario());
		   tabRepository.save(Tab);
			 
		}
		
	*/	
	
	
	
	public void excluirDocumento(Integer cdPropostaDocumento) {

	  
	   tabPropostaDocumentosRepository.delete(cdPropostaDocumento);
		 
	}
	

}
