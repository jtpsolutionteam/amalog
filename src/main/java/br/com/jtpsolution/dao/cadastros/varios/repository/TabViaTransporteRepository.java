package br.com.jtpsolution.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.cadastros.varios.TabViaTransporteObj;



public interface TabViaTransporteRepository extends JpaRepository<TabViaTransporteObj, Integer> {



}