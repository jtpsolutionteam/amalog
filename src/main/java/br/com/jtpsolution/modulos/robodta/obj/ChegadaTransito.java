package br.com.jtpsolution.modulos.robodta.obj;

import java.io.Serializable;
import java.time.LocalDateTime;


public class ChegadaTransito implements Serializable{

	private String txResultado;
	private String dtChegadaTransito;
	private NomeCpf responsavel;
	private String txInterveniente;
	
	public String getTxResultado() {
		return txResultado;
	}
	public void setTxResultado(String txResultado) {
		this.txResultado = txResultado;
	}
	
	public NomeCpf getResponsavel() {
		return responsavel;
	}
	public void setResponsavel(NomeCpf responsavel) {
		this.responsavel = responsavel;
	}
	public String getTxInterveniente() {
		return txInterveniente;
	}
	public void setTxInterveniente(String txInterveniente) {
		this.txInterveniente = txInterveniente;
	}
	public String getDtChegadaTransito() {
		return dtChegadaTransito;
	}
	public void setDtChegadaTransito(String dtChegadaTransito) {
		this.dtChegadaTransito = dtChegadaTransito;
	}
	
	
	
}
