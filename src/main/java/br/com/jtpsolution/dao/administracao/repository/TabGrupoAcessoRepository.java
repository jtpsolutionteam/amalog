package br.com.jtpsolution.dao.administracao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.administracao.TabGrupoAcessoObj;



public interface TabGrupoAcessoRepository extends JpaRepository<TabGrupoAcessoObj, Integer> {

	@Query("select t from TabGrupoAcessoObj t order by t.txGrupoAcesso")
	List<TabGrupoAcessoObj> findListGrupoAcessoQuery();

}