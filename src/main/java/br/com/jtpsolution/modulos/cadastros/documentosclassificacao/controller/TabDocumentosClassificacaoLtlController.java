package br.com.jtpsolution.modulos.cadastros.documentosclassificacao.controller;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.cadastros.varios.TabDocumentosClassificacaoLtlObj;
import br.com.jtpsolution.dao.cadastros.varios.TabTipoCarregamentoLtlObj;
import br.com.jtpsolution.dao.cadastros.varios.VwTabDocumentosClassificacaoLtlObj;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabTipoCarregamentoLtlRepository;
import br.com.jtpsolution.mensagens.TabRetornoSucessoObj;
import br.com.jtpsolution.modulos.cadastros.documentosclassificacao.bean.TabFiltroDocumentosClassificacaoObj;
import br.com.jtpsolution.modulos.cadastros.documentosclassificacao.service.TabDocumentosClassificacaoLtlService;
import br.com.jtpsolution.modulos.util.geralcontroller.allController;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.regras.RegrasBean;

@Controller
@RequestMapping("/documentosclassificacaoltl")
public class TabDocumentosClassificacaoLtlController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabDocumentosClassificacaoLtlService tabService;
	
	@Autowired
	private RegrasBean regrasBean;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_CADASTROS+"/documentosclassificacao/documentosclassificacaoltl";
	

 	
 	
 	@GetMapping	
	public ModelAndView pesquisa(TabFiltroDocumentosClassificacaoObj tabFiltroDocumentosClassificacaoObj, HttpServletRequest request) {
		
		tabFiltroDocumentosClassificacaoObj = verificaFiltro(tabFiltroDocumentosClassificacaoObj, request);
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject(new TabDocumentosClassificacaoLtlObj());			
		mv.addObject("tabFiltroDocumentosClassificacaoObj", tabFiltroDocumentosClassificacaoObj);
		mv.addObject("listadados", retornoFiltro(tabFiltroDocumentosClassificacaoObj));
		
		
		return mv;
	}
	
	private List<?> retornoFiltro(TabFiltroDocumentosClassificacaoObj tabFiltroDocumentosClassificacaoObj) {
 		
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(VwTabDocumentosClassificacaoLtlObj.class);
		
		adicionarFiltro(tabFiltroDocumentosClassificacaoObj, criteria);
 		
 		return criteria.list();
 	}


	private TabFiltroDocumentosClassificacaoObj verificaFiltro(TabFiltroDocumentosClassificacaoObj tabFiltroDocumentosClassificacaoObj, HttpServletRequest request) {
		
		try {
			
			if (Validator.isObjetoNull(tabFiltroDocumentosClassificacaoObj)) {
				if (request.getSession().getAttribute(getClass().getSimpleName()) != null) {					
					String jsonFiltro = request.getSession().getAttribute(getClass().getSimpleName()).toString();
					ObjectMapper mapper = new ObjectMapper();
					tabFiltroDocumentosClassificacaoObj = mapper.readValue(jsonFiltro, TabFiltroDocumentosClassificacaoObj.class);	
				}
			}else {
				request.getSession().setAttribute(getClass().getSimpleName(),
						GeneralParser.convertObjJson(tabFiltroDocumentosClassificacaoObj));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return tabFiltroDocumentosClassificacaoObj;
	}

	
	private void adicionarFiltro(TabFiltroDocumentosClassificacaoObj filtro, Criteria criteria) {
		
		  boolean ckFiltro = false;
		  
		  if (filtro != null) {
			  
			  
			  if (!StringUtils.isEmpty(filtro.getTxDoctoClassificacaoFiltro())) {
				  criteria.add(Restrictions.like("txDoctoClassificacao", "%"+filtro.getTxDoctoClassificacaoFiltro()+"%"));
				  ckFiltro = true;
			  }
			  
			  
			  criteria.addOrder(Order.asc("txDoctoClassificacao"));
		  }
	  }
 	
	
	@RequestMapping(value = "/gravarmodal", method = RequestMethod.POST)
	public @ResponseBody List<?> gravarmodal(@Validated TabDocumentosClassificacaoLtlObj tabDocumentosClassificacaoLtlObj, Errors erros, HttpServletRequest request) {

		List<ObjectError> error = new ArrayList<ObjectError>();
		if (erros.hasErrors()) {
		  //mv.addObject("listaErros", erros.getAllErrors());
		  error.addAll(erros.getAllErrors());
		  return error;
		}else {
			error = regrasBean.verificaregras(getClass().getSimpleName(),request);
			if (!error.isEmpty()) {
				  return error;	
			}	  
		}
		
	    TabDocumentosClassificacaoLtlObj Tab = tabService.gravar(tabDocumentosClassificacaoLtlObj);
	   
	    List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();		
	    TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
	    tSuccess.setCd_mensagem(1);
	    tSuccess.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
	    success.add(tSuccess);
	    
		return success;
	}
	
	
	@RequestMapping(value = "/novo")
	public ModelAndView novo(HttpServletRequest request) {
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		mv.addObject(new TabDocumentosClassificacaoLtlObj());
		mv.addObject("tabFiltroDocumentosClassificacaoObj", verificaFiltro(new TabFiltroDocumentosClassificacaoObj(), request));
		mv.addObject("listadados", retornoFiltro(verificaFiltro(new TabFiltroDocumentosClassificacaoObj(), request)));
		mv.addObject("txEdit", "edit");
		return mv;
	}
	

	@RequestMapping(value = "/consultar/{CdDoctoClassificacao}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable Integer CdDoctoClassificacao, HttpServletRequest request) {
	
	   TabDocumentosClassificacaoLtlObj TabView = tabService.consultar(CdDoctoClassificacao);
	   TabFiltroDocumentosClassificacaoObj tF = verificaFiltro(new TabFiltroDocumentosClassificacaoObj(), request);

	   
		ModelAndView mv = new ModelAndView(txUrlTela);	
		if (TabView != null) {
		  mv.addObject("tabFiltroDocumentosClassificacaoObj",tF);	
		  mv.addObject("tabDocumentosClassificacaoLtlObj",TabView);
		  mv.addObject("listadados", retornoFiltro(verificaFiltro(tF, request)));
		  mv.addObject("txEdit", "edit");
		}else {
		  mv.addObject("tabFiltroDocumentosClassificacaoObj",tF);	
		  mv.addObject(new TabDocumentosClassificacaoLtlObj());	
		  mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
		  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}
	
	
	@RequestMapping(value = "/excluir/{CdDoctoClassificacao}", method = RequestMethod.GET)
	public @ResponseBody String excluir(@PathVariable Integer CdDoctoClassificacao, HttpServletRequest request) {

		tabService.excluir(CdDoctoClassificacao);

		return "OK!";
	}
	
	
	@Autowired
	private TabTipoCarregamentoLtlRepository tabTipoCarregamentoRepository;
	
	@ModelAttribute("selectcdtipocarregamento")
	public List<TabTipoCarregamentoLtlObj> selectcdtipocarregamento() {
		return tabTipoCarregamentoRepository.findAll();
	}
	
}
