package br.com.jtpsolution.dao.administracao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.administracao.TabMenuSubObj;



public interface TabMenuSubRepository extends JpaRepository<TabMenuSubObj, Integer> {


	  @Query("select t from TabMenuSubObj t where t.cdMenu = ?1 and t.cdOrdem >= ?2 and cdSubmenu not in(?3) order by cdOrdem")
	  List<TabMenuSubObj> findListarOrdemQuery(Integer cdMenu, Integer cdOrdem, Integer cdSubmenu);
	 
	

}