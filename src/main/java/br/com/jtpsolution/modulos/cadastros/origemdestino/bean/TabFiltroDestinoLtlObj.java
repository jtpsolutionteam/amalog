package br.com.jtpsolution.modulos.cadastros.origemdestino.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TabFiltroDestinoLtlObj {

	
	private Integer cdEmpresaFiltro;
	
	private Integer cdImpExpFiltro;
	
	private Integer cdViaTransporteFiltro;
	
	
	
	
}
