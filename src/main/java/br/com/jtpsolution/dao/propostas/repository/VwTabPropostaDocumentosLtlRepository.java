package br.com.jtpsolution.dao.propostas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.propostas.VwTabPropostaDocumentosLtlObj;



public interface VwTabPropostaDocumentosLtlRepository extends JpaRepository<VwTabPropostaDocumentosLtlObj, Integer> {

	List<VwTabPropostaDocumentosLtlObj> findByCdProposta(Integer cdProposta);

}