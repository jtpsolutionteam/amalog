package br.com.jtpsolution.modulos.cadastros.ltl.controller;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.cadastros.empresa.TabEmpresaObj;
import br.com.jtpsolution.dao.cadastros.varios.TabDestinoLtlObj;
import br.com.jtpsolution.dao.cadastros.varios.TabLtlObj;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabDestinoLtlRepository;
import br.com.jtpsolution.mensagens.TabRetornoSucessoObj;
import br.com.jtpsolution.modulos.cadastros.empresa.service.TabEmpresaService;
import br.com.jtpsolution.modulos.cadastros.ltl.bean.TabFiltroLtlObj;
import br.com.jtpsolution.modulos.cadastros.ltl.service.TabLtlService;
import br.com.jtpsolution.modulos.util.geralcontroller.allController;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.regras.RegrasBean;

@Controller
@RequestMapping("/ltl")
public class TabLtlController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabLtlService tabService;
	
	@Autowired
	private RegrasBean regrasBean;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_CADASTROS+"/ltl/ltl";
	
 	
	@GetMapping
	public ModelAndView pesquisa(TabFiltroLtlObj tabFiltroLtlObj, HttpServletRequest request) {
		
		tabFiltroLtlObj = verificaFiltro(tabFiltroLtlObj, request);
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject(new TabLtlObj());			
		mv.addObject("tabFiltroLtlObj", tabFiltroLtlObj);
		mv.addObject("listadados", retornoFiltro(tabFiltroLtlObj));
		mv.addObject("selectcddestino", selectcdtipodestino(tabFiltroLtlObj.getCdEmpresaFiltro(),
				tabFiltroLtlObj.getCdImpExpFiltro(), tabFiltroLtlObj.getCdViaTransporteFiltro()));
		return mv;
	}
	
	private List<?> retornoFiltro(TabFiltroLtlObj tabFiltroLtlObj) {
 		
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(TabLtlObj.class);
		
		adicionarFiltro(tabFiltroLtlObj, criteria);
 		
 		return criteria.list();
 	}


	private TabFiltroLtlObj verificaFiltro(TabFiltroLtlObj tabFiltroLtlObj, HttpServletRequest request) {
		
		try {
			
			if (Validator.isObjetoNull(tabFiltroLtlObj)) {
				if (request.getSession().getAttribute(getClass().getSimpleName()) != null) {					
					String jsonFiltro = request.getSession().getAttribute(getClass().getSimpleName()).toString();
					ObjectMapper mapper = new ObjectMapper();
					tabFiltroLtlObj = mapper.readValue(jsonFiltro, TabFiltroLtlObj.class);	
				}
			}else {
				request.getSession().setAttribute(getClass().getSimpleName(),
						GeneralParser.convertObjJson(tabFiltroLtlObj));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return tabFiltroLtlObj;
	}

	
	private void adicionarFiltro(TabFiltroLtlObj filtro, Criteria criteria) {
		
		  boolean ckFiltro = false;
		  
		  if (filtro != null) {
			  
			  if (!StringUtils.isEmpty(filtro.getCdEmpresaFiltro()) && !StringUtils.isEmpty(filtro.getCdImpExpFiltro()) && !StringUtils.isEmpty(filtro.getCdViaTransporteFiltro())) {

				    criteria.add(Restrictions.eq("tabEmpresaObj.cdEmpresa", filtro.getCdEmpresaFiltro()));
				    criteria.add(Restrictions.eq("cdImpexp", filtro.getCdImpExpFiltro()));
					criteria.add(Restrictions.eq("tabViaTransporteObj.cdViaTransporte", filtro.getCdViaTransporteFiltro()));
					ckFiltro = true;
			  }
			  
			  
			  if (!ckFiltro) {
				  criteria.add(Restrictions.eq("tabViaTransporteObj.cdViaTransporte", 0));
			  }
			  
			  criteria.addOrder(Order.asc("vlDe"));
		  }
	  }
	
	
 	
	
	@RequestMapping(value = "/gravarmodal", method = RequestMethod.POST)
	public @ResponseBody List<?> gravarmodal(@Validated TabLtlObj tabLtlObj, Errors erros, HttpServletRequest request) {

		List<ObjectError> error = new ArrayList<ObjectError>();
		if (erros.hasErrors()) {
		  //mv.addObject("listaErros", erros.getAllErrors());
		  error.addAll(erros.getAllErrors());
		  return error;
		}else {
			error = regrasBean.verificaregras(getClass().getSimpleName(),request);
			if (!error.isEmpty()) {
				  return error;	
			}	  
		}
		
	    TabLtlObj Tab = tabService.gravar(tabLtlObj);
	   
	    List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();		
	    TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
	    tSuccess.setCd_mensagem(1);
	    tSuccess.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
	    success.add(tSuccess);
	    
		return success;
	}
	
	
	@RequestMapping(value = "/novo/{cdEmpresa}/{cdImpexp}/{cdViaTransporte}")
	public ModelAndView novo(@PathVariable Integer cdEmpresa, @PathVariable Integer cdImpexp, @PathVariable Integer cdViaTransporte, HttpServletRequest request) {
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		mv.addObject(new TabLtlObj());
		mv.addObject("tabFiltroLtlObj", verificaFiltro(new TabFiltroLtlObj(), request));
		mv.addObject("listadados", retornoFiltro(verificaFiltro(new TabFiltroLtlObj(), request)));
		mv.addObject("selectcddestino", selectcdtipodestino(cdEmpresa,
				cdImpexp, cdViaTransporte));
		mv.addObject("txEdit", "edit");
		return mv;
	}
	

	@RequestMapping(value = "/consultar/{CdLtl}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable Integer CdLtl, HttpServletRequest request) {
	
	   TabLtlObj TabView = tabService.consultar(CdLtl);
	   TabFiltroLtlObj tF = verificaFiltro(new TabFiltroLtlObj(), request);

	   
		ModelAndView mv = new ModelAndView(txUrlTela);	
		if (TabView != null) {
		  mv.addObject("tabFiltroLtlObj",tF);	
		  mv.addObject("tabLtlObj",TabView);
		  mv.addObject("listadados", retornoFiltro(verificaFiltro(tF, request)));
		  mv.addObject("selectcddestino", selectcdtipodestino(TabView.getTabEmpresaObj().getCdEmpresa(),
					TabView.getCdImpexp(), TabView.getTabViaTransporteObj().getCdViaTransporte()));
		  mv.addObject("txEdit", "edit");
		}else {
		  mv.addObject("tabFiltroLtlObj",tF);	
		  mv.addObject(new TabLtlObj());	
		  mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
		  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}
	
	/*
	@RequestMapping(value = "/excluir/{CdLtl}", method = RequestMethod.GET)
	public @ResponseBody String excluir(@PathVariable Integer CdLtl, HttpServletRequest request) {

		tabService.excluir(CdLtl);

		return "OK!";
	}*/
	
	@Autowired
	private TabDestinoLtlRepository tabDestinoLtlRepository;
	
	public List<TabDestinoLtlObj> selectcdtipodestino(Integer cdEmpresa, Integer cdTipoCarregamento,
			Integer cdViaTransporte) {

		return tabDestinoLtlRepository
				.findByTabEmpresaObjCdEmpresaAndCdImpexpAndTabViaTransporteObjCdViaTransporteOrderByTxDestinoLtlAsc(
						cdEmpresa, cdTipoCarregamento, cdViaTransporte);
	}
	
	@Autowired
	private TabEmpresaService tabEmpresaService;
	
	@ModelAttribute("selectcdempresa")	
	public List<TabEmpresaObj> selectcdempresa() {
		return tabEmpresaService.listar();
	}
	
}
