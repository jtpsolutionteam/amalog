package br.com.jtpsolution.dao.cadastros.varios.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.cadastros.varios.VwTabDocumentosClassificacaoObj;



public interface VwTabDocumentosClassificacaoRepository extends JpaRepository<VwTabDocumentosClassificacaoObj, Integer> {

	@Query("select t from VwTabDocumentosClassificacaoObj t order by t.cdDoctoClassificacao")
	List<VwTabDocumentosClassificacaoObj> findOrderByCdDoctoClassificacaoQuery();

	@Query("select t from VwTabDocumentosClassificacaoObj t where t.cdTipoDta = ?1 order by t.txDoctoClassificacao")
	List<VwTabDocumentosClassificacaoObj> findOrderByCdClassificacaoTipoDtaQuery(Integer cdTipoDta);

	@Query("select t from VwTabDocumentosClassificacaoObj t where t.cdTipoDta = ?1 and t.ckValidar = ?2 order by t.txDoctoClassificacao")
	List<VwTabDocumentosClassificacaoObj> findOrderByCdClassificacaoTipoDtaValidarQuery(Integer cdTipoDta, Integer ckValidar);

	@Query("select t from VwTabDocumentosClassificacaoObj t where t.cdTipoDta = ?1 order by t.txDoctoClassificacao")
	List<VwTabDocumentosClassificacaoObj> findOrderByCdClassificacaoTipoDtaValidarQuery(Integer cdTipoDta);

}