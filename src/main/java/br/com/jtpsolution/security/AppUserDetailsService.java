package br.com.jtpsolution.security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.jtpsolution.dao.administracao.vw.VwTabMenuObj;
import br.com.jtpsolution.dao.cadastros.usuario.TabUsuarioObj;
import br.com.jtpsolution.dao.cadastros.usuario.repository.TabUsuarioRepository;
import br.com.jtpsolution.modulos.administrador.service.TabMenuService;

@Service
public class AppUserDetailsService implements UserDetailsService {

	@Autowired
	private TabUsuarioRepository tabUsuarioRepository;
	
	@Autowired
	private TabMenuService tabMenuService;
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		
		TabUsuarioObj tUsuario = tabUsuarioRepository.findByTxEmailAndCkAtivo(email, 1);
		
		List<VwTabMenuObj> listMenus = null;
				
		if (tUsuario == null) {
			throw new UsernameNotFoundException("Usuário e/ou senha inválidos!");
		}else {
			listMenus = tabMenuService.listar(tUsuario.getCdGrupoVisao());
		}
		
		return new DadosUser(tUsuario.getTxEmail(), 
				tUsuario.getTxSenha(), 
				AuthorityUtils.createAuthorityList(), 
				tUsuario.getCdUsuario(), 				 
				tUsuario.getCdGrupoVisao(), 
				tUsuario.getTxApelido(), 
				tUsuario.getTxEmail(),
				tUsuario.getCdGrupoAcesso(), 
				listMenus,
				tUsuario.getCdIdioma(),
				tUsuario.getTabEmpresaObj().getCdEmpresa());
	}

}
