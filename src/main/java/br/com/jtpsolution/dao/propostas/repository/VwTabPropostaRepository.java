package br.com.jtpsolution.dao.propostas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.propostas.TabPropostaObj;
import br.com.jtpsolution.dao.propostas.VwTabPropostaObj;



public interface VwTabPropostaRepository extends JpaRepository<VwTabPropostaObj, Integer> {
	
	@Query("select t from VwTabPropostaObj t where t.cdProposta = ?1 and t.cdGrupoAcesso = ?2")
	List<VwTabPropostaObj> findByListCdPropostaQuery(Integer cdProposta, Integer cdGrupoAcesso);
	
	VwTabPropostaObj findByCdPropostaAndCdGrupoAcesso(Integer cdProposta, Integer cdGrupoAcesso);
	
	VwTabPropostaObj findByCdProposta(Integer cdProposta);
	
	VwTabPropostaObj findByCdPropostaAndCdUsuario(Integer cdProposta, Integer cdUsuario);
	
	VwTabPropostaObj findByTxHashPropostaAndCdGrupoAcesso(String txHashProposta, Integer cdGrupoAcesso);

	VwTabPropostaObj findByTxBl(String txBl);
	
	VwTabPropostaObj findByTxBlAndTxTipoProposta(String txBl, String txTipoProposta);
	
	VwTabPropostaObj findByTxDiAndTxTipoProposta(String txDi, String txTipoProposta);
	
	VwTabPropostaObj findByTxReserva(String txReversa);
	
	List<VwTabPropostaObj> findByTxCheckUploadDoctosContainingAndCdGrupoAcessoOrderByDtPropostaAsc(String txCheckUploadDoctos, Integer cdGrupoAcesso);

	@Query("select t from VwTabPropostaObj t where t.ckUploadDoctos = ?1 and t.cdGrupoAcesso = ?2 and t.dtLiberacaoDocumentos is null and t.txCheckUploadDoctos not like '%pendência%' and ckReconferencia = 0 and (txBl is not null or txReserva is not null) and txTerminalMar = 'Terminal'")
	List<VwTabPropostaObj> findByListCkUploadOKQuery(Integer ckUploadDoctos, Integer cdGrupoAcesso);

	@Query("select t from VwTabPropostaObj t where t.ckUploadDoctos = ?1 and t.cdGrupoAcesso = ?2 and t.dtLiberacaoDocumentos is null and t.txCheckUploadDoctos not like '%pendência%' and ckReconferencia = 1 and (txBl is not null or txReserva is not null) and txTerminalMar = 'Terminal' order by t.dtProposta")
	List<VwTabPropostaObj> findByListCkUploadOKReconfereciaQuery(Integer ckUploadDoctos, Integer cdGrupoAcesso);

	@Query("select t from VwTabPropostaObj t where t.cdGrupoAcesso = ?1 and t.dtLiberacaoDocumentos is not null and t.ckDesistenciaVistoria = 2 order by t.dtLiberacaoDocumentos")
	List<VwTabPropostaObj> findByListDoctosSemDesistenciaVistoriaQuery(Integer cdGrupoAcesso);

	
	@Query("select t from VwTabPropostaObj t where t.dtBoletoVencto is not null and t.dtBoletoPagto is null and t.cdGrupoAcesso = ?1")
	List<VwTabPropostaObj> findByListDtBoletoPagtoEmAbertoQuery(Integer cdGrupoAcesso);

	@Query("select t from VwTabPropostaObj t where t.dtBoletoVencto is not null and t.dtBoletoPagto is not null and t.cdGrupoAcesso = ?1")
	List<VwTabPropostaObj> findByListDtBoletoPagtoPagosQuery(Integer cdGrupoAcesso);
	
}