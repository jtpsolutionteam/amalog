package br.com.jtpsolution.modulos.registro.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.jtpsolution.dao.cadastros.coloader.TabColoaderDespachanteObj;
import br.com.jtpsolution.dao.cadastros.coloader.repository.TabColoaderDespachanteRepository;
import br.com.jtpsolution.dao.cadastros.usuario.TabUsuarioObj;
import br.com.jtpsolution.dao.cadastros.usuario.repository.TabUsuarioRepository;

@Service
public class TabRegistroColoaderService {

	
	@Autowired
	private TabUsuarioRepository  tabUsuarioRepository;
	
	@Autowired 
	private TabColoaderDespachanteRepository tabRepository;
	
	
	public TabColoaderDespachanteObj gravar(TabColoaderDespachanteObj tabColoaderDespachanteObj) {
		
		
		TabColoaderDespachanteObj Tab = tabRepository.save(tabColoaderDespachanteObj);
		
		return Tab;
	}
	
	
	public TabColoaderDespachanteObj consultar(String txCnpj) {
		
		
		TabColoaderDespachanteObj Tab = tabRepository.findByTxCnpj(txCnpj);
		
		return Tab;
	}
	
	
	
	
	
	public List<TabUsuarioObj> listar() {
		return tabUsuarioRepository.findAll();
		
	}
	
	public TabUsuarioObj gravar(TabUsuarioObj Tab) {
		

		TabUsuarioObj tNovo =  tabUsuarioRepository.save(Tab);			  
		 
		return tNovo;
		
	}

	public TabUsuarioObj consultar(Integer CdUsuario) {
	 TabUsuarioObj Tab = tabUsuarioRepository.findOne(CdUsuario);
	 
	   return Tab;
	 
	}
	
	public TabUsuarioObj consultarEmail(String txEmail) {
		   TabUsuarioObj Tab = tabUsuarioRepository.findByTxEmail(txEmail);
		 
		   return Tab;
		 
	}
	

	
	
	


	

	
}
