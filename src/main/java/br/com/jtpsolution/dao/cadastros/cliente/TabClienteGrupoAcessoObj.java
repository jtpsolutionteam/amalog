package br.com.jtpsolution.dao.cadastros.cliente;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import br.com.jtpsolution.Constants;

@Entity
@Table(name = "tab_cliente_grupo_acesso", schema = Constants.SCHEMA)
public class TabClienteGrupoAcessoObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_cliente_grupo_acesso")
	// @NotNull(message = "ClienteGrupoAcesso campo obrigatório!")
	private Integer cdClienteGrupoAcesso;

	@Column(name = "cd_cliente")
	// @NotNull(message = "Cliente campo obrigatório!")
	private Integer cdCliente;

	@Column(name = "cd_grupo_acesso")
	// @NotNull(message = "GrupoAcesso campo obrigatório!")
	private Integer cdGrupoAcesso;
	
	@Column(name = "ck_cliente")
	private Integer ckCliente;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
	}

	public Integer getCdClienteGrupoAcesso() {
		return cdClienteGrupoAcesso;
	}

	public void setCdClienteGrupoAcesso(Integer cdClienteGrupoAcesso) {
		this.cdClienteGrupoAcesso = cdClienteGrupoAcesso;
	}

	public Integer getCdCliente() {
		return cdCliente;
	}

	public void setCdCliente(Integer cdCliente) {
		this.cdCliente = cdCliente;
	}

	public Integer getCdGrupoAcesso() {
		return cdGrupoAcesso;
	}

	public void setCdGrupoAcesso(Integer cdGrupoAcesso) {
		this.cdGrupoAcesso = cdGrupoAcesso;
	}

	public Integer getCkCliente() {
		return ckCliente;
	}

	public void setCkCliente(Integer ckCliente) {
		this.ckCliente = ckCliente;
	}

}
