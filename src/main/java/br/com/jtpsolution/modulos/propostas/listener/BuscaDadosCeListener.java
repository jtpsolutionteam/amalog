package br.com.jtpsolution.modulos.propostas.listener;

import java.util.Date;

import javax.persistence.PreUpdate;

import br.com.jtpsolution.dao.propostas.TabPropostaObj;
import br.com.jtpsolution.modulos.robosiscarga.obj.TabCeMercanteObj;
import br.com.jtpsolution.modulos.robosiscarga.service.RoboSiscargaService;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.Validator;

public class BuscaDadosCeListener {

	// @Autowired
	RoboSiscargaService roboSiscargaService = new RoboSiscargaService();

	@PreUpdate
	public void buscarCE(TabPropostaObj tabPropostaObj) {

	  if (Validator.isBlankOrNull(tabPropostaObj.getDtDtaSolicitacao())) {	
		if (!Validator.isBlankOrNull(tabPropostaObj.getTxNumeroCe())
				&& Validator.isBlankOrNull(tabPropostaObj.getDtAtracacao())) {		
			
			    tabPropostaObj = verificaSiscarga(tabPropostaObj);
			    
		}else if (!Validator.isBlankOrNull(tabPropostaObj.getTxNumeroCe()) && !Validator.isBlankOrNull(tabPropostaObj.getDtAtracacao())) {
			if (!tabPropostaObj.getTxSituacaoCeMaster().contains("ARMAZENADA"))	{
			   tabPropostaObj = verificaSiscarga(tabPropostaObj);
			}
		}					
	  }
	}
	
	
	private TabPropostaObj verificaSiscarga(TabPropostaObj tabPropostaObj) {
		
		
		TabCeMercanteObj Tab = roboSiscargaService.getNrCeMercante(tabPropostaObj.getTxNumeroCe());

		if (Tab != null) {

			if (!Validator.isBlankOrNull(Tab.getTxBl())) {

				tabPropostaObj.setTxBl(Tab.getTxBl());
				tabPropostaObj.setTxNavio(Tab.getTabEscalaObj().getTxNavio());
				tabPropostaObj.setTxViagem(Tab.getTabEscalaObj().getTxViagem());
				tabPropostaObj.setTxLocalAtracacao(Tab.getTxTerminalDescarregamento());
				tabPropostaObj.setTxRegime(Tab.getTxModalidadeFrete());
				tabPropostaObj.setTxPortoOrigem(Tab.getTxPortoOrigem());
				tabPropostaObj.setTxEscala(Tab.getTabEscalaObj().getTxEscala());
				tabPropostaObj.setTxStatusConsultaSiscarga(Tab.getTxStatusConsultaSiscarga());

				if (!Validator.isBlankOrNull(Tab.getTabEscalaObj().getDtPrevisaoAtracacao())) {
					Date dt = GeneralParser.parseDateTime("dd/MM/yyyy HH:mm:ss",
							Tab.getTabEscalaObj().getDtPrevisaoAtracacao().substring(0, 19));
					tabPropostaObj.setDtPrevisaoAtracacao(dt);
				}

				if (!Validator.isBlankOrNull(Tab.getTabEscalaObj().getDtAtracacao())) {
					Date dt = GeneralParser.parseDateTime("dd/MM/yyyy HH:mm:ss",
							Tab.getTabEscalaObj().getDtAtracacao().substring(0, 19));
					tabPropostaObj.setDtAtracacao(dt);
				}
				
				if (!Validator.isBlankOrNull(Tab.getTxSituacao())) {

					if (Tab.getTxSituacao().toUpperCase().contains("ARMAZENADA") || Tab.getTxSituacao().toUpperCase().contains("DESCARREGADA")) {							
						tabPropostaObj.setTxSituacaoCeMaster(Tab.getTxSituacao());							
					}else {
						if (!Validator.isBlankOrNull(Tab.getTabCeMercanteMasterObj().getTxSituacao())) {							
							if (Tab.getTxSituacao().toUpperCase().contains("ARMAZENADA") || Tab.getTxSituacao().toUpperCase().contains("DESCARREGADA")) {	
							  tabPropostaObj.setTxSituacaoCeMaster(Tab.getTabCeMercanteMasterObj().getTxSituacao());
							}
						}
					}
					
					
				}


			}else {
				tabPropostaObj.setTxSituacaoCeMaster("Erro ao tentar acessar o Robo Siscarga");
			}
		}
		
		return tabPropostaObj;
		
	}

}
