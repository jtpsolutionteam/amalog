package br.com.jtpsolution.modulos.cadastros.valoresfixos.service;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.jtpsolution.dao.cadastros.varios.TabValoresFixosObj;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabValoresFixosRepository;
import br.com.jtpsolution.dao.util.log.TabLogObj;
import br.com.jtpsolution.exceptions.ErrosConstraintException;
import br.com.jtpsolution.security.DadosUser;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralUtil;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.log.LogBean;
import br.com.jtpsolution.util.logErro.JTPLogErroBean;

@Service
public class TabValoresFixosService {

	@Autowired
	private JTPLogErroBean logErroBean;
	
	@Autowired
	private TabValoresFixosRepository  tabRepository;
	
	@Autowired
	private UsuarioBean tabUsuarioService;
	
	@Autowired
	private LogBean tabLogBeanService;


	
	public List<TabValoresFixosObj> listar() {
		return tabRepository.findAll();
		
	}



	public TabValoresFixosObj gravar(TabValoresFixosObj Tab) {
		
		boolean ckAlteracao = false;
				
		DadosUser user  = tabUsuarioService.DadosUsuario();

		
		TabValoresFixosObj tAtual = new TabValoresFixosObj();
		TabValoresFixosObj tNovo = Tab;
		
		//tNovo.setCdUsuario(null);
		List<TabLogObj> listaLog = null;
		
		if (Tab.getCdTabela() != null && !Validator.isBlankOrNull(Tab.getCdTabela())) {
			ckAlteracao = true;
			//Log de campos -- Sempre antes da gravação por causa da persistencia.
			tAtual = tabRepository.findOne(Tab.getCdTabela());
			//listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
			listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdTabela()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
		}
		
		  try {	
		   tNovo = tabRepository.save(tNovo);			  
		  }catch (ConstraintViolationException ex) {
			  
			 throw new ErrosConstraintException(GeneralUtil.MessageExceptionConstraint(ex), ex.getConstraintViolations());			  
		  }
		  
			//Log de campos
		  if (!ckAlteracao) {
			//tabLogBeanService.LogBean(String.valueOf(tNovo.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  	tabLogBeanService.LogBean(String.valueOf(tNovo.getCdTabela()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  }else {
			  tabLogBeanService.GravarListaLog(listaLog);
		  }
		return tNovo;
		
	}

	public TabValoresFixosObj consultar(Integer cdTabela) {
	 TabValoresFixosObj Tab = tabRepository.findOne(cdTabela);
	 
	   return Tab;
	 
	}

	
	/* Se tela tiver excluir
		public void excluir(Integer CdTabela) {

		   DadosUser user  = tabUsuarioService.DadosUsuario();

		   TabValoresFixosObj Tab = tabRepository.findOne(CdTabela);
		   Tab.setDtCancelamento(new Date());
		   Tab.setCdUsuarioCancelamento(user.getCdUsuario());
		   tabRepository.save(Tab);
			 
		}
		
	*/	
	

	
}
