package br.com.jtpsolution.dao.propostas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.propostas.VwTabPropostaDashboardObj;



public interface VwTabPropostaDashboardRepository extends JpaRepository<VwTabPropostaDashboardObj, Integer> {

	List<VwTabPropostaDashboardObj> findByTxCheckUploadDoctosContainingAndCdGrupoAcessoOrderByDtPropostaAsc(String txCheckUploadDoctos, Integer cdGrupoAcesso);

	@Query("select t from VwTabPropostaNewObj t where t.ckUploadDoctos = ?1 and t.cdGrupoAcesso = ?2 and t.dtLiberacaoDocumentos is null and t.txCheckUploadDoctos not like '%pendência%' and ckReconferencia = 0 and (txBl is not null or txReserva is not null) and txTerminalMar = 'Terminal'")
	List<VwTabPropostaDashboardObj> findByListCkUploadOKQuery(Integer ckUploadDoctos, Integer cdGrupoAcesso);

	@Query("select t from VwTabPropostaNewObj t where t.ckUploadDoctos = ?1 and t.cdGrupoAcesso = ?2 and t.dtLiberacaoDocumentos is null and t.txCheckUploadDoctos not like '%pendência%' and ckReconferencia = 1 and (txBl is not null or txReserva is not null) and txTerminalMar = 'Terminal' order by t.dtProposta")
	List<VwTabPropostaDashboardObj> findByListCkUploadOKReconfereciaQuery(Integer ckUploadDoctos, Integer cdGrupoAcesso);

	@Query("select t from VwTabPropostaNewObj t where t.cdGrupoAcesso = ?1 and t.dtLiberacaoDocumentos is not null and t.ckDesistenciaVistoria = 2 order by t.dtLiberacaoDocumentos")
	List<VwTabPropostaDashboardObj> findByListDoctosSemDesistenciaVistoriaQuery(Integer cdGrupoAcesso);
	
	@Query("select t from VwTabPropostaNewObj t where t.dtBoletoVencto is not null and t.dtBoletoPagto is null and t.cdGrupoAcesso = ?1 and t.ckBoletoCancelado is null and t.txTipoProposta = ?2")
	List<VwTabPropostaDashboardObj> findByListDtBoletoPagtoEmAbertoQuery(Integer cdGrupoAcesso, String txTipoProposta);

	@Query("select t from VwTabPropostaNewObj t where t.dtBoletoVencto is not null and t.dtBoletoPagto is not null and t.cdGrupoAcesso = ?1 and t.txTipoProposta = ?2")
	List<VwTabPropostaDashboardObj> findByListDtBoletoPagtoPagosQuery(Integer cdGrupoAcesso, String txTipoProposta);

	//LTL
	
	
	@Query("select t from VwTabPropostaNewObj t where t.ckUploadDoctos = ?1 and t.cdGrupoAcesso = ?2 and t.dtLiberacaoDocumentos is null and t.txCheckUploadDoctos not like '%pendência%' and t.ckReconferencia = 0 and t.txTipoProposta = 'LTL'")
	List<VwTabPropostaDashboardObj> findByListCkUploadOKLtlQuery(Integer ckUploadDoctos, Integer cdGrupoAcesso);

	@Query("select t from VwTabPropostaNewObj t where t.ckUploadDoctos = ?1 and t.cdGrupoAcesso = ?2 and t.dtLiberacaoDocumentos is null and t.txCheckUploadDoctos not like '%pendência%' and ckReconferencia = 1 and t.txTipoProposta = 'LTL' order by t.dtProposta")
	List<VwTabPropostaDashboardObj> findByListCkUploadOKReconfereciaLtlQuery(Integer ckUploadDoctos, Integer cdGrupoAcesso);

	List<VwTabPropostaDashboardObj> findByTxCheckUploadDoctosContainingAndTxTipoPropostaAndCdGrupoAcessoOrderByDtPropostaAsc(String txCheckUploadDoctos, String txTipoProposta, Integer cdGrupoAcesso);


}