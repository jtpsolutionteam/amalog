package br.com.jtpsolution.dao.propostas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.propostas.TabPropostaEnvioEmailTaxaObj;



public interface TabPropostaEnvioEmailTaxaRepository extends JpaRepository<TabPropostaEnvioEmailTaxaObj, Integer> {

	List<TabPropostaEnvioEmailTaxaObj> findByCdProposta(Integer cdProposta);
	
	//TabPropostaEnvioEmailTaxaObj findByCdPropostaAndTxEmail(Integer cdProposta, String txEmail);
	

	List<TabPropostaEnvioEmailTaxaObj> findByCdPropostaAndTxEmail(Integer cdProposta, String txEmail);
}