package br.com.jtpsolution.modulos.microled.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.com.jtpsolution.dao.cadastros.varios.TabDestinoObj;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabDestinoRepository;
import br.com.jtpsolution.dao.conexaomicroled.connectmicroled;
import br.com.jtpsolution.dao.conexaorest.restconnect;
import br.com.jtpsolution.dao.propostas.TabPropostaMicroledObj;
import br.com.jtpsolution.dao.propostas.TabPropostaObj;
import br.com.jtpsolution.dao.propostas.repository.TabPropostaMicroledRepository;
import br.com.jtpsolution.dao.propostas.repository.TabPropostaRepository;
import br.com.jtpsolution.modulos.microled.obj.TabAverbacaoMicroledObj;
import br.com.jtpsolution.modulos.microled.obj.TabStatusMicroledObj;
import br.com.jtpsolution.modulos.microled.obj.VwTabAvariaCsObj;
import br.com.jtpsolution.modulos.microled.obj.VwTabConsultaCargaItemObj;
import br.com.jtpsolution.modulos.microled.obj.VwTabEtapasAvBlCntrObj;
import br.com.jtpsolution.modulos.robodta.obj.FluxoDeclaracaoEntrada;
import br.com.jtpsolution.modulos.robodta.service.RoboDtaService;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.GeneralUtil;
import br.com.jtpsolution.util.Validator;

@Service
public class MicroledService {

	@Autowired
	private RoboDtaService roboDtaService;

	@Autowired
	private TabDestinoRepository tabDestinoRepository;

	@Autowired
	private TabPropostaMicroledRepository tabPropostaMicroledRepository;

	@Autowired
	private TabPropostaRepository tabPropostaRepository;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		MicroledService c = new MicroledService();
		VwTabConsultaCargaItemObj t = c.consultaCargaItem(716213);
		System.out.println(t.getTxLote());
		System.out.println(t.getVlPesoApurado());
		System.out.println(t.getVlPesoBruto());
		System.out.println(t.getTxMapa());

		/*
		 * System.out.println(t.getTxMicroledMercadoria());
		 * 
		 * List<VwTabAvariaCsObj> tv = c.consultaAvarias(716213); for (VwTabAvariaCsObj
		 * atual : tv) {
		 * 
		 * System.out.println(atual.getTxAvaria()); }
		 */

	}

	@Scheduled(cron = "0 0/20 6-23 * * ?")
	public void statusLote() {

		try {
			List<TabPropostaObj> list = tabPropostaRepository.findByListStatusMicroledQuery();

			for (TabPropostaObj atual : list) {

				if (!Validator.isBlankOrNull(atual.getTxLote())) {
					TabStatusMicroledObj tab = new TabStatusMicroledObj();
					tab.setLoteNumero(atual.getTxLote());

					Gson g = new GsonBuilder().disableHtmlEscaping().create();
					String txJson = g.toJson(tab);

					String json = restconnect.enviaRestPost("http://amalog.com.br:8084/valida", txJson);

					System.out.println(
							"Microled Status Lote :" + atual.getCdProposta() + " - Lote :" + atual.getTxLote());

					if (json.contains("wsvalidaSaidaCsResult")) {

						JSONObject jsonObj = new JSONObject(json);

						String txJ = jsonObj.getString("wsvalidaSaidaCsResult");

						jsonObj = new JSONObject(txJ);

						TabPropostaObj Tab = tabPropostaRepository.findOne(atual.getCdProposta());

						if (Tab != null) {

							String txStatus = jsonObj.getString("message");
							Tab.setTxStatusLoteMicroled(txStatus);
							Tab.setDtUltimoStatusLoteMicroled(new Date());
							tabPropostaRepository.save(Tab);

							System.out.println("Microled Status Lote :" + atual.getCdProposta() + " - Lote :"
									+ atual.getTxLote() + " - " + txStatus);

						}
					}
				}

				Thread.sleep(1000);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Erro StatusLote: " + e.getMessage());
		}

	}

	public VwTabConsultaCargaItemObj consultaCargaItem(Integer cdLote) {

		Connection conn = connectmicroled.getConnectionSimpleSql();
		VwTabConsultaCargaItemObj Tab = new VwTabConsultaCargaItemObj();
		try {

			String StrSql = "select distinct autonum, nimpcnpj, nimportador, mercadoria, t.lote,quantidade,embalagem, t.id_conteiner,t.dt_inicio_desova, t.dt_fim_desova,t.dt_entrada,t.dt_saida,termo_avaria,"
					+ "dt_termo,qtde_avaria,peso_avaria,peso_bruto,peso_apurado,dif,difperc, dt_cadastro, gr_paga, bloqueio_bl, icms_sefaz, mapa, bloqueio_cntr, siscarga from VW_CONSULTA_CARGA_ITEM t "
					+ "inner join vw_consulta_carga tc on " + "tc.lote = t.lote "
					+ "left join vw_etapas_av_bl_cntr tet on " + "tet.lote = t.lote " + "where t.lote = ?";
			PreparedStatement prmt = conn.prepareStatement(StrSql);
			prmt.setInt(1, cdLote);

			ResultSet rs = prmt.executeQuery();

			double vlPesoBrutoTotal = 0;
			double vlPesoApuradoTotal = 0;
			double vlPesoAvariaTotal = 0;
			String txVolumes = "";
			Integer vlQtdeVolumes = 0;
			Integer vlQtdeVolumesAvaria = 0;
			double vlDifPesoTotal = 0;
			double vlDifPorcentTotal = 0;
			String txCnpjImportador = "";
			String txImportador = "";
			String txGrPaga = "";
			String txBloqueioBl = "";
			String txIcmsSefaz = "";
			String txMapa = "";
			String txBloqueioCNTR = "";
			String txSiscarga = "";

			while (rs.next()) {

				vlPesoAvariaTotal += rs.getDouble("peso_avaria");
				vlPesoApuradoTotal += rs.getDouble("peso_apurado");
				vlPesoBrutoTotal += rs.getDouble("peso_bruto");
				vlQtdeVolumesAvaria += rs.getInt("qtde_avaria");
				vlQtdeVolumes += rs.getInt("quantidade");
				txVolumes += vlQtdeVolumes + " " + rs.getString("embalagem") + ", ";
				vlDifPesoTotal += rs.getDouble("dif");
				vlDifPorcentTotal += rs.getDouble("difperc");
				txCnpjImportador = rs.getString("nimpcnpj");
				txImportador = rs.getString("nimportador");
				txMapa = rs.getString("mapa");
				txGrPaga = rs.getString("gr_paga");
				txBloqueioBl = rs.getString("bloqueio_bl");
				txIcmsSefaz = rs.getString("icms_sefaz");
				txBloqueioCNTR = rs.getString("bloqueio_cntr");
				txSiscarga = rs.getString("siscarga");

				Tab.setTxMicroledMercadoria(rs.getString("mercadoria"));
				Tab.setTxLote(rs.getInt("lote"));
				Tab.setVlQtdeVolume(vlQtdeVolumes);
				Tab.setTxVolume(txVolumes);
				Tab.setTxContainer(rs.getString("id_conteiner"));
				Tab.setDtInicioDesova(rs.getTimestamp("dt_inicio_desova"));
				Tab.setDtFimDesova(rs.getTimestamp("dt_fim_desova"));
				Tab.setDtSaida(rs.getTimestamp("dt_saida"));
				Tab.setCdTermoAvaria(rs.getInt("termo_avaria"));
				Tab.setDtTermoAvaria(rs.getTimestamp("dt_termo"));
				Tab.setVlQtdeAvaria(vlQtdeVolumesAvaria);
				Tab.setVlPesoAvaria(vlPesoAvariaTotal);
				Tab.setVlPesoBruto(vlPesoBrutoTotal);
				Tab.setVlPesoApurado(vlPesoApuradoTotal);
				Tab.setVlDiferencaPeso(vlDifPesoTotal);
				Tab.setVlDiferencaPercentual(vlDifPorcentTotal);
				Tab.setTxCnpjImportador(txCnpjImportador);
				Tab.setTxImportador(txImportador);
				Tab.setDtInicioDesova(rs.getTimestamp("dt_inicio_desova"));
				Tab.setDtEntrada(rs.getTimestamp("dt_entrada"));
				Tab.setDtDesconsolidacao(rs.getTimestamp("dt_cadastro"));
				Tab.setTxMapa(txMapa);
				Tab.setTxBloqueioBl(txBloqueioBl);
				Tab.setTxBloqueioCNTR(txBloqueioCNTR);
				Tab.setTxIcmsSefaz(txIcmsSefaz);
				Tab.setTxSiscarga(txSiscarga);
				Tab.setTxGrPaga(txGrPaga);

				// System.out.println(rs.getString("peso_apurado"));
			}

		} catch (Exception ex) {

			ex.printStackTrace();
		}

		return Tab;

	}

	public List<VwTabAvariaCsObj> consultaAvarias(Integer cdLote) {

		Connection conn = connectmicroled.getConnectionSimpleSql();
		List<VwTabAvariaCsObj> lst = new ArrayList<VwTabAvariaCsObj>();
		try {

			String StrSql = "select * from vw_avaria_cs where bl = ?";
			PreparedStatement prmt = conn.prepareStatement(StrSql);
			prmt.setInt(1, cdLote);

			ResultSet rs = prmt.executeQuery();

			while (rs.next()) {
				VwTabAvariaCsObj Tab = new VwTabAvariaCsObj();
				Tab.setTxLote(rs.getInt("bl"));
				Tab.setCdItem(rs.getInt("item"));
				Tab.setTxEmbalagem(rs.getString("embalagem"));
				Tab.setTxTermoAvaria(rs.getString("termo"));
				Tab.setDtTermoAvaria(rs.getString("data_termo"));
				Tab.setTxAvaria(rs.getString("avarias"));
				lst.add(Tab);
			}

		} catch (Exception ex) {

			ex.printStackTrace();
		}

		return lst;
	}

	public String enviarAverbacao(TabPropostaObj tabPropostaObj) {

		FluxoDeclaracaoEntrada fluxo = roboDtaService.consultaValoresDta(tabPropostaObj.getTxNumeroDta());

		TabPropostaMicroledObj tabMicroled = new TabPropostaMicroledObj();

		String json = "";

		if (fluxo != null) {
			// System.out.println(fluxo.getVlMercadoriaDolar());
			// System.out.println(fluxo.getVlMercadoriaMoedaNacional());

			TabAverbacaoMicroledObj tabAverbacao = new TabAverbacaoMicroledObj();
			tabAverbacao.setCeMercante(tabPropostaObj.getTxNumeroCe());
			tabAverbacao.setCnpjImportador(GeneralUtil.TiraNaonumero(tabPropostaObj.getTxCnpjImportador()));
			tabAverbacao.setCnpjDespachante("29551937000137");
			tabAverbacao.setQuantidadeVolumes(tabPropostaObj.getVlQtdeVolume());
			tabAverbacao.setVolumeCarga(tabPropostaObj.getVlPesoExtratoDesova());
			tabAverbacao.setQuantidadeCntr(0);
			tabAverbacao.setCanalSiscomex(tabPropostaObj.getTxDtaCanal().replace("CANAL ", ""));
			String[] txDta = tabPropostaObj.getTxNumeroDta().split("\\/");
			if (txDta.length > 1) {
				// System.out.println(txDta[1]);
				tabAverbacao.setNumeroDocumento(GeneralUtil.TiraNaonumero(txDta[1]) + "/2020");
			} else {
				// System.out.println(txDta[0]);
				tabAverbacao.setNumeroDocumento(
						GeneralUtil.TiraNaonumero(txDta[0].substring(2, txDta[0].length())) + "/2020");
			}
			tabAverbacao.setTipoDocumento("DTA");
			tabAverbacao.setDataDocumento(GeneralParser.format_dateBR2(tabPropostaObj.getDtDtaSolicitacao()));
			if (tabPropostaObj.getTxSituacaoCeMaster().contains("8931364")) {
				tabAverbacao.setCodigoReparticao("8931364");
			} else if (tabPropostaObj.getTxSituacaoCeMaster().contains("8933206")) {
				tabAverbacao.setCodigoReparticao("8933206");
			} else if (tabPropostaObj.getTxSituacaoCeMaster().contains("8931319")) {
				tabAverbacao.setCodigoReparticao("8931319");
			}

			tabAverbacao.setMoedaOrigem("DOLAR");
			tabAverbacao.setValorCifOrigem(GeneralParser.parseDouble(fluxo.getVlMercadoriaDolar()));
			tabAverbacao.setValorCifReais(GeneralParser.parseDouble(fluxo.getVlMercadoriaMoedaNacional()));
			tabAverbacao.setEndEmails(tabPropostaObj.getTxEmailTaxaInformativo());

			// Verifica o codigo ibge destino
			TabDestinoObj tabDestino = tabDestinoRepository.findOne(tabPropostaObj.getTabDestinoObj().getCdDestino());

			tabAverbacao.setDestinoCarga(String.valueOf(tabDestino.getCdCodigoIbge()));

			if (!Validator.isBlankOrNull(tabAverbacao.getCodigoReparticao())) {
				Gson g = new GsonBuilder().disableHtmlEscaping().create();
				String txJson = g.toJson(tabAverbacao);
				System.out.println(txJson);

				try {
					json = restconnect.enviaRestPost("http://amalog.com.br:8084/averbacao", txJson);

					System.out.println(json);

					tabMicroled.setCdProposta(tabPropostaObj.getCdProposta());
					tabMicroled.setTxJsonEnvio(txJson);
					tabMicroled.setTxJsonRetorno(json);
					tabMicroled.setDtCriacao(new Date());
					tabPropostaMicroledRepository.save(tabMicroled);

				} catch (Exception ex) {
					System.out.println(
							"Erro JSon Averbação: " + tabPropostaObj.getCdProposta() + " - " + ex.getMessage());
				}
			}
		}

		return json;
	}

}
