package br.com.jtpsolution.modulos.mantran.obj;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TabMantranDoctosObj {

	/*
	
	ID_Arquivo_PAS
		CD_Classificacao string(2)
		DS_Classificacao string(100)
		Nome_Completo_Documento string(2000) ==> enviar por exemplo: http://www.servidor.com.br/nomearquivo.pdf
		DT_Upload
		Usuario_Upload string(100)
		Pendencia string(1000)
		DT_Validacao
		Usuario_Validacao string(100)
		Observacao string(1000)
	
	*/
	private Integer ID_Arquivo_PAS;	
	private String CD_Classificacao;
	private String DS_Classificacao;
	private String Nome_Completo_Documento;	
	private String DT_Upload;
	private String Usuario_Upload;
	private String Pendencia;
	private String DT_Validacao;
	private String Usuario_Validacao;
	private String Observacao;
	
	
	
	
}
