package br.com.jtpsolution.modulos.mantran.obj;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TabMantranDtaObj {

	
	private long ID;
	private String BL_Filhote;
	private String Lote_Filhote;
	private String DT_Solicitacao_DTA;
	private String Destino_Carga;
	private String CD_Importador;
	private String CD_Tipo_DTA;
	private String NR_DTA;
	private String Canal;
	private String DT_Registro_DTA;
	private String DT_Parametrizacao_DTA;
	private String DT_Inicio_Transito;
	private String DT_Chegada_Destino;
	private String DT_Conclusao_Transito;
	private String Mercadoria;
	private Float VR_Cubagem;
	private Float VR_Peso;
	private Float VR_Volumes;
	private Float VR_Mercadoria;
	private String FL_Hubport_Inativo;
	private String FL_Bloqueio;
	private String Navio_Viagem;
	private String DT_Indicacao_DTA;
	private String DT_Cadastro;
	private String NR_Container;
	private String DT_Entrada_Container;
	private String DT_Desconsolidacao;
	private String DT_Desova;
	private String CD_Indicador_NVOCC;
	private String CD_Coloader;
	private String FL_Hubport_Freehand;
	private String FL_RO;
	private String FL_FMA;
	private String CD_IMO; // (0, 1, 2 onde 0=ONU 1=CLASSE 2-RISCO)
	private String FL_Anvisa;
	private String DT_Averbacao;
	private String DT_Saida_Terminal;
	private String FL_Averbou;
	private String FL_Icms_Sefaz;
	private String FL_Desembaracada;
	private String FL_Mapa_Madeira;
	private String FL_GR_Paga;
	private String FL_SistCarga;
	private String FL_Bloqueio_BL;
	private String Key;
	private String UF_Destino;
	private String NR_FMA;
	private String FL_Canal_Verde;
	private String CD_Expedidor;
	private String NR_PROPOSTA_PAS;
	private String E_Mail_Faturamento;
	private Float VR_Peso_Bruto;
	private Float VR_Diferenca_Peso;
	private Float VR_Diferenca_Peso_Percent;
	private String DT_Inicio_Desova;
	private String DS_VOLUME;
	private String DT_Inicio_Carregamento;
	private String FL_Dedicado;
	private String FL_Empilhavel;
	private String Ultimo_Status;
	private String DT_Prev_Carregamento;
	
	
	List<TabMantranDoctosObj> Arquivos;
	
	
	
}
