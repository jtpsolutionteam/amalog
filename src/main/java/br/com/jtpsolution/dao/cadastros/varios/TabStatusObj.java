package br.com.jtpsolution.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_status", schema = Constants.SCHEMA)
public class TabStatusObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_status")
	// @NotNull(message = "Status campo obrigatório!")
	private Integer cdStatus;

	@Column(name = "tx_status")
	// @NotEmpty(message = "Status campo obrigatório!")
	@Size(max = 45, message = "Status tamanho máximo de 45 caracteres")
	private String txStatus;

	
	@Column(name = "tx_tipo_status")
	private String txTipoStatus;

	
	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txStatus))
			txStatus = txStatus.toUpperCase();
	}

	

}
