package br.com.jtpsolution.modulos.cadastros.dedicado.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TabFiltroDedicadoObj {

	private Integer cdDestinoFiltro;
	
}
