package br.com.jtpsolution.dao.propostas;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.springframework.format.annotation.NumberFormat;

import br.com.jtpsolution.Constants;

@Entity
@Table(name = "tab_descontos", schema = Constants.SCHEMA)
public class TabDescontosObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_desconto")
	// @NotNull(message = "Desconto campo obrigatório!")
	private Integer cdDesconto;

	@Column(name = "vl_de")
	// @NotEmpty(message = "De campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlDe;

	@Column(name = "vl_ate")
	// @NotEmpty(message = "Ate campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlAte;

	@Column(name = "vl_desconto")
	// @NotEmpty(message = "Desconto campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlDesconto;

	@Column(name = "vl_peso")
	// @NotEmpty(message = "Peso campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlPeso;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
	}

	public Integer getCdDesconto() {
		return cdDesconto;
	}

	public void setCdDesconto(Integer cdDesconto) {
		this.cdDesconto = cdDesconto;
	}


	public void setVlPeso(Double vlPeso) {
		this.vlPeso = vlPeso;
	}

	public Double getVlDe() {
		return vlDe;
	}

	public void setVlDe(Double vlDe) {
		this.vlDe = vlDe;
	}

	public Double getVlAte() {
		return vlAte;
	}

	public void setVlAte(Double vlAte) {
		this.vlAte = vlAte;
	}

	public Double getVlDesconto() {
		return vlDesconto;
	}

	public void setVlDesconto(Double vlDesconto) {
		this.vlDesconto = vlDesconto;
	}

	public Double getVlPeso() {
		return vlPeso;
	}

	
	

}
