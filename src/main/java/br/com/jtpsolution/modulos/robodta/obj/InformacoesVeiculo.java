package br.com.jtpsolution.modulos.robodta.obj;

import java.io.Serializable;


public class InformacoesVeiculo implements Serializable{

	private String txResultado;
	private String dtPartidaProcedencia;
	private NomeCpf responsavel;
	
	private String txInterveniente;

	public String getTxResultado() {
		return txResultado;
	}

	public void setTxResultado(String txResultado) {
		this.txResultado = txResultado;
	}


	public NomeCpf getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(NomeCpf responsavel) {
		this.responsavel = responsavel;
	}

	public String getTxInterveniente() {
		return txInterveniente;
	}

	public void setTxInterveniente(String txInterveniente) {
		this.txInterveniente = txInterveniente;
	}

	public String getDtPartidaProcedencia() {
		return dtPartidaProcedencia;
	}

	public void setDtPartidaProcedencia(String dtPartidaProcedencia) {
		this.dtPartidaProcedencia = dtPartidaProcedencia;
	}
	
	
}
