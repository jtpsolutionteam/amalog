package br.com.jtpsolution.dao.administracao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_campos_idioma", schema = Constants.SCHEMA)
public class TabCamposIdiomaObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_campo_idioma")
	// @NotNull(message = "CampoIdioma campo obrigatório!")
	private Integer cdCampoIdioma;

	@ManyToOne
	@JoinColumn(name = "cd_tela")
	private TabTelasObj tabTelasObj;

	@Column(name = "tx_campo")
	// @NotEmpty(message = "Campo campo obrigatório!")
	@Size(max = 100, message = "Campo tamanho máximo de 100 caracteres")
	private String txCampo;

	@Column(name = "tx_obj_campo")
	// @NotEmpty(message = "Objcampo campo obrigatório!")
	@Size(max = 50, message = "Objcampo tamanho máximo de 50 caracteres")
	private String txObjCampo;

	@Column(name = "cd_idioma")
	// @NotEmpty(message = "Idioma campo obrigatório!")
	@Size(max = 2, message = "Idioma tamanho máximo de 2 caracteres")
	private String cdIdioma;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {		
		
	}

}
