package br.com.jtpsolution.dao.propostas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.propostas.TabPropostaGrupoAcessoObj;



public interface TabPropostaGrupoAcessoRepository extends JpaRepository<TabPropostaGrupoAcessoObj, Integer> {

	TabPropostaGrupoAcessoObj findByCdPropostaAndCdGrupoAcesso(Integer cdProposta, Integer cdGrupoAcesso);
	
	TabPropostaGrupoAcessoObj findByCdPropostaAndCdGrupoAcessoAndCkColoader(Integer cdProposta, Integer cdGrupoAcesso, Integer ckColoader);

}