package br.com.jtpsolution.dao.administracao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.administracao.TabGrupoVisaoObj;

public interface TabGrupoVisaoRepository extends JpaRepository<TabGrupoVisaoObj, Integer> {

}
