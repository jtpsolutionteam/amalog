package br.com.jtpsolution.dao.cadastros.cliente.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.cadastros.cliente.TabClienteObj;


public interface TabClienteRepository extends JpaRepository<TabClienteObj, Integer> {

	TabClienteObj findByTxCnpj(String txCnpj);
	
	List<TabClienteObj> findByCdCliente(Integer cdCliente);

}