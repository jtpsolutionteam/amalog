package br.com.jtpsolution.dao.propostas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.propostas.TabPropostaAvariasObj;



public interface TabPropostaAvariasRepository extends JpaRepository<TabPropostaAvariasObj, Integer> {

    List<TabPropostaAvariasObj> findByCdProposta(Integer cdProposta);

}