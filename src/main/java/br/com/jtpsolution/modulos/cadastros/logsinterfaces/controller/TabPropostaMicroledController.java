package br.com.jtpsolution.modulos.cadastros.logsinterfaces.controller;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.propostas.TabPropostaMantranObj;
import br.com.jtpsolution.dao.propostas.TabPropostaMicroledObj;
import br.com.jtpsolution.modulos.cadastros.logsinterfaces.bean.TabFiltroPropostaMantranObj;
import br.com.jtpsolution.modulos.cadastros.logsinterfaces.bean.TabFiltroPropostaMicroledObj;
import br.com.jtpsolution.modulos.cadastros.logsinterfaces.service.TabPropostaMantranService;
import br.com.jtpsolution.modulos.cadastros.logsinterfaces.service.TabPropostaMicroledService;
import br.com.jtpsolution.modulos.util.geralcontroller.allController;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.regras.RegrasBean;

@Controller
@RequestMapping("/logmicroled")
public class TabPropostaMicroledController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabPropostaMicroledService tabService;
	
	@Autowired
	private RegrasBean regrasBean;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_CADASTROS+"/logsinterfaces/logmicroled";
	
	
 	
 	@GetMapping
	public ModelAndView pesquisa(TabFiltroPropostaMicroledObj tabFiltroPropostaMicroledObj, HttpServletRequest request) {
		
 		tabFiltroPropostaMicroledObj = verificaFiltro(tabFiltroPropostaMicroledObj, request);
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject(new TabPropostaMicroledObj());			
		mv.addObject("tabFiltroPropostaMicroledObj", tabFiltroPropostaMicroledObj);
		mv.addObject("listadados", retornoFiltro(tabFiltroPropostaMicroledObj));
		
		
		return mv;
	}
	
	private List<?> retornoFiltro(TabFiltroPropostaMicroledObj tabFiltroPropostaMicroledObj) {
 		
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(TabPropostaMicroledObj.class);
		
		adicionarFiltro(tabFiltroPropostaMicroledObj, criteria);
 		
 		return criteria.list();
 	}


	private TabFiltroPropostaMicroledObj verificaFiltro(TabFiltroPropostaMicroledObj tabFiltroPropostaMicroledObj, HttpServletRequest request) {
		
		try {
			
			if (Validator.isObjetoNull(tabFiltroPropostaMicroledObj)) {
				if (request.getSession().getAttribute(getClass().getSimpleName()) != null) {					
					String jsonFiltro = request.getSession().getAttribute(getClass().getSimpleName()).toString();
					ObjectMapper mapper = new ObjectMapper();
					tabFiltroPropostaMicroledObj = mapper.readValue(jsonFiltro, TabFiltroPropostaMicroledObj.class);	
				}
			}else {
				request.getSession().setAttribute(getClass().getSimpleName(),
						GeneralParser.convertObjJson(tabFiltroPropostaMicroledObj));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return tabFiltroPropostaMicroledObj;
	}

	
	private void adicionarFiltro(TabFiltroPropostaMicroledObj filtro, Criteria criteria) {
		
		  boolean ckFiltro = false;
		  
		  if (filtro != null) {
			  
			  if (!StringUtils.isEmpty(filtro.getCdPropostaFiltro())) {
				  criteria.add(Restrictions.eq("cdProposta", filtro.getCdPropostaFiltro()));
				  ckFiltro = true;
			  }
			  
			  if (!ckFiltro) {
				  criteria.add(Restrictions.eq("cdProposta", 0));
			  }
			  
			  criteria.addOrder(Order.desc("dtCriacao"));
		  }
	  }
	

	@RequestMapping(value = "/consultar/{CdCodigo}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable Integer CdCodigo, HttpServletRequest request) {
	
	   TabPropostaMicroledObj TabView = tabService.consultar(CdCodigo);
	   TabFiltroPropostaMicroledObj tF = verificaFiltro(new TabFiltroPropostaMicroledObj(), request);

	   
		ModelAndView mv = new ModelAndView(txUrlTela);	
		if (TabView != null) {
		  mv.addObject("tabFiltroPropostaMicroledObj",tF);	
		  mv.addObject("tabPropostaMicroledObj",TabView);
		  mv.addObject("listadados", retornoFiltro(verificaFiltro(tF, request)));
		  mv.addObject("txEdit", "edit");
		}else {
		  mv.addObject("tabFiltroPropostaMicroledObj",tF);	
		  mv.addObject(new TabPropostaMicroledObj());	
		  mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
		  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}
	
	
	
}
