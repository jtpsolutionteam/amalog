package br.com.jtpsolution.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_via_transporte", schema = Constants.SCHEMA)
public class TabViaTransporteObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_via_transporte")
	// @NotNull(message = "ViaTransporte campo obrigatório!")
	private Integer cdViaTransporte;

	@Column(name = "tx_via_transporte")
	// @NotEmpty(message = "ViaTransporte campo obrigatório!")
	@Size(max = 45, message = "ViaTransporte tamanho máximo de 45 caracteres")
	private String txViaTransporte;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txViaTransporte))
			txViaTransporte = txViaTransporte.toUpperCase();
	}

}
