package br.com.jtpsolution.util.permissaonegada.controller;

import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.jtpsolution.modulos.util.geralcontroller.allController;
import br.com.jtpsolution.util.linux.linuxService;


@Controller
public class PermissaoNegadaController extends allController {

	

	@RequestMapping("/permissaonegada/{txPermissaoNegada}")
	public ModelAndView permissaonegada(@PathVariable String txPermissaoNegada) {
		
		//roboDtaService.executaRobo();
		
		ModelAndView mv = new ModelAndView("util/permissaonegada/permissaonegada");
		
		mv.addObject("txPermissaoNegada", txPermissaoNegada);
		
		  /*
		  System.out.println( System.getProperty("os.name") );
			
			String txOS = System.getProperty("os.name").toLowerCase();
	        if (!txOS.contains("windows")) {
	        	try {
				
					String tx = new linuxService().consultarServico("ps -ef| grep robo-siscomex-carga-1.0.0.jar").replaceAll("     ", " ").replaceAll(" ", ";");
					System.out.println(tx);
					
					String[] str = tx.split(";");
					System.out.println(str[1]);

					new linuxService().consultarServico("kill -9 "+str[1]);
					
	        	} catch (IOException e) {
					// TODO Auto-generated catch block
					System.out.println(e.getMessage());
				}
	        }
			*/
		
		
		
		
		
		return mv;
		
		
	}
	
	
	
}
