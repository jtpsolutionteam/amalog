package br.com.jtpsolution.modulos.administrador.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.jtpsolution.dao.administracao.TabRegrasTipoObj;
import br.com.jtpsolution.dao.administracao.repository.TabRegrasTipoRepository;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.log.LogBean;
import br.com.jtpsolution.util.logErro.JTPLogErroBean;
	

@Service
public class TabRegrasTipoService {

	@Autowired
	private JTPLogErroBean logErroBean;
	
	@Autowired
	private TabRegrasTipoRepository  tabRepository;
	
	@Autowired
	private UsuarioBean tabUsuarioService;
	
	@Autowired
	private LogBean tabLogBeanService;

	
	
	public List<TabRegrasTipoObj> listar() {
		return tabRepository.findAll();
		
	}
	
	
}
