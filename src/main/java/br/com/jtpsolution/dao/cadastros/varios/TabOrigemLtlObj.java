package br.com.jtpsolution.dao.cadastros.varios;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.NumberFormat;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_origem_ltl", schema = Constants.SCHEMA)
public class TabOrigemLtlObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_origem_ltl")
	// @NotNull(message = "OrigemLtl campo obrigatório!")
	private Integer cdOrigemLtl;

	@Column(name = "tx_origem_ltl")
	// @NotEmpty(message = "OrigemLtl campo obrigatório!")
	@Size(max = 45, message = "OrigemLtl tamanho máximo de 45 caracteres")
	private String txOrigemLtl;

	@Column(name = "ck_ativo")
	// @NotNull(message = "ckAtivo campo obrigatório!")
	private Integer ckAtivo;

	@Column(name = "vl_importacao")
	// @NotEmpty(message = "Importacao campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlImportacao;

	@Column(name = "vl_exportacao")
	// @NotEmpty(message = "Exportacao campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlExportacao;

	@Column(name = "ck_rota_amalog")
	// @NotNull(message = "ckRotaAmalog campo obrigatório!")
	private Integer ckRotaAmalog;
	
	@Column(name = "vl_qtde_dias")
	private Integer vlQtdeDias;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txOrigemLtl))
			txOrigemLtl = txOrigemLtl.toUpperCase();
	}

}
