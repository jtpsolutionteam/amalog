package br.com.jtpsolution.dao.cadastros.varios;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import br.com.jtpsolution.Constants;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_taxa_dolar", schema = Constants.SCHEMA)
public class TabTaxaDolarObj {

	@Id	
	@Column(name = "cd_taxa")
	private Integer cdTaxa;
	
	@Column(name = "vl_taxa_dolar")
	// @NotEmpty(message = "TaxaDolar campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlTaxaDolar;
	
	@Column(name = "dt_taxa")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtTaxa;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
	}

}
