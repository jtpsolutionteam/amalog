package br.com.jtpsolution.modulos.propostas.service;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.jtpsolution.dao.propostas.TabPropostaEnvioEmailTaxaObj;
import br.com.jtpsolution.dao.propostas.repository.TabPropostaEnvioEmailTaxaRepository;
import br.com.jtpsolution.dao.util.log.TabLogObj;
import br.com.jtpsolution.exceptions.ErrosConstraintException;
import br.com.jtpsolution.security.DadosUser;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralUtil;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.log.LogBean;
import br.com.jtpsolution.util.logErro.JTPLogErroBean;

@Service
public class TabPropostaEnvioEmailTaxaService {

	@Autowired
	private JTPLogErroBean logErroBean;
	
	@Autowired
	private TabPropostaEnvioEmailTaxaRepository  tabRepository;
	
	@Autowired
	private UsuarioBean tabUsuarioService;
	
	@Autowired
	private LogBean tabLogBeanService;


	
	
	public List<TabPropostaEnvioEmailTaxaObj> listar(Integer cdProposta) {
		return tabRepository.findByCdProposta(cdProposta);
		
	}
	


	public TabPropostaEnvioEmailTaxaObj gravar(TabPropostaEnvioEmailTaxaObj Tab) {
		
		TabPropostaEnvioEmailTaxaObj tNovo =  tabRepository.save(Tab);			  
		  
		return tNovo;
		
	}

	public TabPropostaEnvioEmailTaxaObj consultar(Integer CdPropostaEnvioEmail) {
	 TabPropostaEnvioEmailTaxaObj Tab = tabRepository.findOne(CdPropostaEnvioEmail);
	 
	   return Tab;
	 
	}

	
	/* Se tela tiver excluir
		public void excluir(Integer CdPropostaEnvioEmail) {

		   DadosUser user  = tabUsuarioService.DadosUsuario();

		   TabPropostaEnvioEmailTaxaObj Tab = tabRepository.findOne(CdPropostaEnvioEmail);
		   Tab.setDtCancelamento(new Date());
		   Tab.setCdUsuarioCancelamento(user.getCdUsuario());
		   tabRepository.save(Tab);
			 
		}
		
	*/	
	

	
}
