package br.com.jtpsolution.modulos.propostas.listener;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.PreUpdate;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.jtpsolution.dao.propostas.TabPropostaObj;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.mailthymeleaf.EmailSend;

public class EnviaEmailInformativoListener {

	
	@Autowired
	EmailSend emailSend;
	 
	@PreUpdate
	public void enviaEmailInformativo(TabPropostaObj tabPropostaObj) {
		
		if (!Validator.isBlankOrNull(tabPropostaObj.getTxEmailTaxaInformativo()) && Validator.isBlankOrNull(tabPropostaObj.getDtEnvioEmailInformativo())) {
			
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("txEmailTaxaInformativo",tabPropostaObj.getTxEmailTaxaInformativo());
			model.put("txBomDia",GeneralParser.mostraBomDiaTardeNoite());
			model.put("txProposta",tabPropostaObj.getTxProposta());
	        model.put("txLote", tabPropostaObj.getTxLote());
	        model.put("txBl", tabPropostaObj.getTxBl());
	        model.put("txNumeroDta", tabPropostaObj.getTxNumeroDta());
	        model.put("vlTaxaInformativo", tabPropostaObj.getVlTaxaInformativo());
	        model.put("txImportador", tabPropostaObj.getTxImportador()+" - "+tabPropostaObj.getTxCnpjImportador());
	        
	       if (emailSend.sendMail("mailinformativotaxa",tabPropostaObj.getTxEmailTaxaInformativo(), "Amalog/Bandeirantes - Informativo DTA HUB PORT", model)) {
	    	   tabPropostaObj.setDtEnvioEmailInformativo(new Date());
	       }
			
		}
		
		
		
	}
	
}
