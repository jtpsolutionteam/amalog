package br.com.jtpsolution.modulos.cadastros.logsinterfaces.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.jtpsolution.dao.propostas.TabPropostaMantranObj;
import br.com.jtpsolution.dao.propostas.repository.TabPropostaMantranRepository;
import br.com.jtpsolution.security.DadosUser;
import br.com.jtpsolution.security.UsuarioBean;

@Service
public class TabPropostaMantranService {

	@Autowired
	private TabPropostaMantranRepository tabRepository;

	@Autowired
	private UsuarioBean tabUsuarioService;

	public TabPropostaMantranObj gravar(TabPropostaMantranObj Tab) {

		boolean ckAlteracao = false;

		DadosUser user = tabUsuarioService.DadosUsuario();

		TabPropostaMantranObj tNovo = tabRepository.save(Tab);

		return tNovo;

	}

	public TabPropostaMantranObj consultar(Integer CdCodigo) {
		TabPropostaMantranObj Tab = tabRepository.findOne(CdCodigo);

		return Tab;

	}

}
