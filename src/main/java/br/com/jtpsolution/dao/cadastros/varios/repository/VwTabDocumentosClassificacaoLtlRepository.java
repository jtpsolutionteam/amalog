package br.com.jtpsolution.dao.cadastros.varios.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.cadastros.varios.VwTabDocumentosClassificacaoLtlObj;
import br.com.jtpsolution.dao.cadastros.varios.VwTabDocumentosClassificacaoObj;



public interface VwTabDocumentosClassificacaoLtlRepository extends JpaRepository<VwTabDocumentosClassificacaoLtlObj, Integer> {

	@Query("select t from VwTabDocumentosClassificacaoLtlObj t order by t.cdDoctoClassificacao")
	List<VwTabDocumentosClassificacaoLtlObj> findOrderByCdDoctoClassificacaoQuery();

	@Query("select t from VwTabDocumentosClassificacaoLtlObj t where t.cdTipo = ?1 order by t.txDoctoClassificacao")
	List<VwTabDocumentosClassificacaoLtlObj> findOrderByCdClassificacaoTipoQuery(Integer cdTipo);

	@Query("select t from VwTabDocumentosClassificacaoLtlObj t where t.cdTipo = ?1 and t.ckValidar = ?2 order by t.txDoctoClassificacao")
	List<VwTabDocumentosClassificacaoLtlObj> findOrderByCdClassificacaoTipoValidarQuery(Integer cdTipo, Integer ckValidar);

	@Query("select t from VwTabDocumentosClassificacaoLtlObj t where t.cdTipo = ?1 order by t.txDoctoClassificacao")
	List<VwTabDocumentosClassificacaoLtlObj> findOrderByCdClassificacaoTipoValidarQuery(Integer cdTipo);

}