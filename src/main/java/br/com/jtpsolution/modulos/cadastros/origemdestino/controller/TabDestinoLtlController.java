package br.com.jtpsolution.modulos.cadastros.origemdestino.controller;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.cadastros.cliente.repository.TabClienteRepository;
import br.com.jtpsolution.dao.cadastros.empresa.TabEmpresaObj;
import br.com.jtpsolution.dao.cadastros.varios.TabDestinoLtlObj;
import br.com.jtpsolution.dao.cadastros.varios.TabDestinoObj;
import br.com.jtpsolution.dao.cadastros.varios.TabLtlObj;
import br.com.jtpsolution.mensagens.TabRetornoSucessoObj;
import br.com.jtpsolution.modulos.cadastros.empresa.service.TabEmpresaService;
import br.com.jtpsolution.modulos.cadastros.ltl.bean.TabFiltroLtlObj;
import br.com.jtpsolution.modulos.cadastros.origemdestino.bean.TabFiltroDestinoLtlObj;
import br.com.jtpsolution.modulos.cadastros.origemdestino.service.TabDestinoLtlService;
import br.com.jtpsolution.modulos.util.geralcontroller.allController;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.regras.RegrasBean;

@Controller
@RequestMapping("/destinoltl")
public class TabDestinoLtlController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabDestinoLtlService tabService;
	
	@Autowired
	private RegrasBean regrasBean;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_CADASTROS+"/origemdestino/destinoltl";
 	
 	
	@GetMapping
	public ModelAndView pesquisa(TabFiltroDestinoLtlObj tabFiltroDestinoLtlObj, HttpServletRequest request) {
		
		tabFiltroDestinoLtlObj = verificaFiltro(tabFiltroDestinoLtlObj, request);
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject(new TabDestinoLtlObj());			
		mv.addObject("tabFiltroDestinoLtlObj", tabFiltroDestinoLtlObj);
		mv.addObject("listadados", retornoFiltro(tabFiltroDestinoLtlObj));
		
		
		return mv;
	}
	
	private List<?> retornoFiltro(TabFiltroDestinoLtlObj tabFiltroDestinoLtlObj) {
 		
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(TabDestinoLtlObj.class);
		
		adicionarFiltro(tabFiltroDestinoLtlObj, criteria);
 		
 		return criteria.list();
 	}


	private TabFiltroDestinoLtlObj verificaFiltro(TabFiltroDestinoLtlObj tabFiltroDestinoLtlObj, HttpServletRequest request) {
		
		try {
			
			if (Validator.isObjetoNull(tabFiltroDestinoLtlObj)) {
				if (request.getSession().getAttribute(getClass().getSimpleName()) != null) {					
					String jsonFiltro = request.getSession().getAttribute(getClass().getSimpleName()).toString();
					ObjectMapper mapper = new ObjectMapper();
					tabFiltroDestinoLtlObj = mapper.readValue(jsonFiltro, TabFiltroDestinoLtlObj.class);	
				}
			}else {
				request.getSession().setAttribute(getClass().getSimpleName(),
						GeneralParser.convertObjJson(tabFiltroDestinoLtlObj));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return tabFiltroDestinoLtlObj;
	}

	
	private void adicionarFiltro(TabFiltroDestinoLtlObj filtro, Criteria criteria) {
		
		  boolean ckFiltro = false;
		  
		  if (filtro != null) {
			  
			  if (!StringUtils.isEmpty(filtro.getCdEmpresaFiltro()) && !StringUtils.isEmpty(filtro.getCdImpExpFiltro()) && !StringUtils.isEmpty(filtro.getCdViaTransporteFiltro())) {

			    criteria.add(Restrictions.eq("tabEmpresaObj.cdEmpresa", filtro.getCdEmpresaFiltro()));
			    criteria.add(Restrictions.eq("cdImpexp", filtro.getCdImpExpFiltro()));
				criteria.add(Restrictions.eq("tabViaTransporteObj.cdViaTransporte", filtro.getCdViaTransporteFiltro()));
				ckFiltro = true;
			  }
			  
			  if (!ckFiltro) {
				  criteria.add(Restrictions.eq("cdImpexp", 0));
			  }
			  
			  criteria.addOrder(Order.asc("txDestinoLtl"));
		  }
	  }
	
	
	
	@RequestMapping(value = "/gravarmodal", method = RequestMethod.POST)
	public @ResponseBody List<?> gravarmodal(@Validated TabDestinoLtlObj tabDestinoLtlObj, Errors erros, HttpServletRequest request) {

		List<ObjectError> error = new ArrayList<ObjectError>();
		if (erros.hasErrors()) {
		  //mv.addObject("listaErros", erros.getAllErrors());
		  error.addAll(erros.getAllErrors());
		  return error;
		}else {
			error = regrasBean.verificaregras(getClass().getSimpleName(),request);
			if (!error.isEmpty()) {
				  return error;	
			}	  
		}
		
	    TabDestinoLtlObj Tab = tabService.gravar(tabDestinoLtlObj);
	   
	    List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();		
	    TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
	    tSuccess.setCd_mensagem(1);
	    tSuccess.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
	    success.add(tSuccess);
	    
		return success;
	}
	
	
	@RequestMapping(value = "/novo")
	public ModelAndView novo(HttpServletRequest request) {
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		mv.addObject(new TabDestinoLtlObj());
		mv.addObject("tabFiltroDestinoLtlObj", verificaFiltro(new TabFiltroDestinoLtlObj(), request));
		mv.addObject("listadados", retornoFiltro(verificaFiltro(new TabFiltroDestinoLtlObj(), request)));
		mv.addObject("txEdit", "edit");
		return mv;
	}
	

	
	@RequestMapping(value = "/consultar/{cdDestinoLtl}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable Integer cdDestinoLtl, HttpServletRequest request) {
	
	   TabDestinoLtlObj TabView = tabService.consultar(cdDestinoLtl);
	   TabFiltroDestinoLtlObj tF = verificaFiltro(new TabFiltroDestinoLtlObj(), request);

	   
		ModelAndView mv = new ModelAndView(txUrlTela);	
		if (TabView != null) {
		  mv.addObject("tabFiltroDestinoLtlObj",tF);	
		  mv.addObject("tabDestinoLtlObj",TabView);
		  mv.addObject("listadados", retornoFiltro(verificaFiltro(tF, request)));
		  mv.addObject("txEdit", "edit");
		}else {
		  mv.addObject("tabFiltroDestinoLtlObj",tF);	
		  mv.addObject(new TabDestinoLtlObj());	
		  mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
		  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}


	
	/*
	@RequestMapping(value = "/excluir/{CdDestino}", method = RequestMethod.GET)
	public @ResponseBody String excluir(@PathVariable Integer CdDestino, HttpServletRequest request) {

		tabService.excluir(CdDestino);

		return "OK!";
	}*/
	
	@Autowired
	private TabClienteRepository tabClienteRepository;
	
	@ModelAttribute("selectcdcliente")
	public List<?> selectcdcliente() {

		return tabClienteRepository.findAll(GeneralParser.sortByIdAsc("txCliente"));
	}
	
	
	@Autowired
	private TabEmpresaService tabEmpresaService;
	
	@ModelAttribute("selectcdempresa")	
	public List<TabEmpresaObj> selectcdempresa() {
		return tabEmpresaService.listar();
	}
}
