package br.com.jtpsolution.modulos.login.controller;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import br.com.jtpsolution.security.DadosUser;

@Controller
public class LoginController {

	
	
	@GetMapping("/login")
	public String login(@AuthenticationPrincipal DadosUser user) {
	   if (user != null) {		  
		
		   return "redirect:/dashboard";
		   
	   }
		return "login/login";
	}
	
	
	
	
}
