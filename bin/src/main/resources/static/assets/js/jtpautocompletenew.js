/*
var options = {
		url: function(fieldValue) {
			return '/usuarios/lkgerente?txGerente='+fieldValue;
		},
		getValue: 'txNome',
		minCharNumber: 3,
		//requestDelay: 300,
		ajaxSettings: {
			contentType: 'application/json'
		},

    template: {
        type: "description",
        fields: {
            description: "txApelido"
        }
    },

    list: {
        match: {
            enabled: true
        }
    },

    theme: "plate-dark"
};

$("#txGerente").easyAutocomplete(options);

*/


var JTPAutocomplete = JTPAutocomplete || {};

JTPAutocomplete.Autocomplete = (function() {
	
	function Autocomplete(inputField) {
		this.skuOuNomeInput = $(inputField);		
	}
	
	Autocomplete.prototype.iniciar = function() {
		var options = {
			url: function(skuOuNome) {
				return '/usuarios/lkgerente?txGerente=' + skuOuNome;
			},
			getValue: 'txNome',
			minCharNumber: 3,
			requestDelay: 300,
			ajaxSettings: {
				contentType: 'application/json'
			},
			template: {
		        type: "description",
		        fields: {
		            description: "txApelido"
		        }
		    },

		    list: {
		        match: {
		            enabled: true
		        }
		    },

		    theme: "plate-dark"
		};
		
		this.skuOuNomeInput.easyAutocomplete(options);
	}
	
	return Autocomplete
	
}());

$(function() {
	
	var autocomplete = new JTPAutocomplete.Autocomplete('.js-gerente-input');
	autocomplete.iniciar();
	
})