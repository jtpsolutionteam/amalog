package br.com.jtpsolution.dao.propostas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.propostas.TabPropostaMicroledObj;



public interface TabPropostaMicroledRepository extends JpaRepository<TabPropostaMicroledObj, Integer> {



}