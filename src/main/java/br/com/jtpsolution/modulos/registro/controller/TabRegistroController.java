package br.com.jtpsolution.modulos.registro.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.cadastros.cliente.VwTabClienteGrupoAcessoObj;
import br.com.jtpsolution.dao.cadastros.usuario.TabUsuarioObj;
import br.com.jtpsolution.modulos.registro.service.TabRegistroService;
import br.com.jtpsolution.security.GeraPass;
import br.com.jtpsolution.util.Validator;

@Controller
@RequestMapping("/registro")
public class TabRegistroController{


	@Autowired
	private TabRegistroService tabService;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_REGISTRO+"/registro";
	
	@GetMapping
	public ModelAndView novo() {
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		mv.addObject(new TabUsuarioObj());
		return mv;
	}
	
	
	@RequestMapping(value = "/gravar", method = RequestMethod.POST)
	public ModelAndView gravar(@Validated TabUsuarioObj tabUsuarioObj, Errors erros) {
		ModelAndView mv = new ModelAndView(txUrlTela);
		
		if (erros.hasErrors()) {
		  mv.addObject("listaErros", erros.getAllErrors());
		  return mv;
		}
		
		if (Validator.isBlankOrNull(tabUsuarioObj.getTxCnpj())) {
			mv.addObject("tabUsuarioObj",tabUsuarioObj);
			mv.addObject("txMensagem", "CNPJ campo obrigatório!");
			mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
			return mv;	
		}
		
		TabUsuarioObj tabUsuario = tabService.consultarEmail(tabUsuarioObj.getTxEmail());
			
		if (tabUsuario != null) {
			mv.addObject("tabUsuarioObj",tabUsuarioObj);
			mv.addObject("txMensagem", "Email já cadastrado no sistema!");
			mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
			return mv;	
		}
		
		
		VwTabClienteGrupoAcessoObj tabCliente = tabService.consultarCnpjCkCliente(tabUsuarioObj.getTxCnpj(), 1);
		
		Integer cdGrupoAcesso = 0;
		
		if (tabCliente != null) {
			cdGrupoAcesso = tabCliente.getCdGrupoAcesso();
			tabUsuarioObj.setCdGrupoAcesso(cdGrupoAcesso);
			tabUsuarioObj.setCdGrupoVisao(3);
			tabUsuarioObj.setCkAtivo(1);			
		}else {
			mv.addObject("tabUsuarioObj",tabUsuarioObj);
			mv.addObject("txMensagem", "CNPJ não cadastrado ou sem permissão em nosso Sistema! Solicite o cadastro no numero 13 3219.1571");
			mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
			return mv;	
		}
		
		
		
		if (!tabUsuarioObj.getTxEmail().equals(tabUsuarioObj.getTxConfEmail())) {
			mv.addObject("tabUsuarioObj",tabUsuarioObj);
			mv.addObject("txMensagem", "Emails diferente!");
			mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
			return mv;			
		}
		

		if (!tabUsuarioObj.getTxSenha().equals(tabUsuarioObj.getTxConfSenha())) {
			mv.addObject("tabUsuarioObj",tabUsuarioObj);
			mv.addObject("txMensagem", "Senhas diferente!");
			mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
			return mv;						
		}

		
		if (tabUsuarioObj.getCdUsuario() != null) {
		  TabUsuarioObj TabViewSimples = tabService.consultar(tabUsuarioObj.getCdUsuario());
		  if (TabViewSimples == null) {
			  tabUsuarioObj.setTxSenha(new GeraPass().BCryptPass(tabUsuarioObj.getTxSenha()));
		  }else {
			  //System.out.println(TabViewSimples.getTxSenha());
			  //System.out.println(tabUsuarioObj.getTxSenha());
			  
			  if (Validator.isBlankOrNull(tabUsuarioObj.getTxSenha())) {
				  tabUsuarioObj.setTxSenha(TabViewSimples.getTxSenha());				   
			  }else {
				  tabUsuarioObj.setTxSenha(new GeraPass().BCryptPass(tabUsuarioObj.getTxSenha()));
			  }
		  }		  
		}else {
			tabUsuarioObj.setTxSenha(new GeraPass().BCryptPass(tabUsuarioObj.getTxSenha()));
		}
		
	    TabUsuarioObj Tab = tabService.gravar(tabUsuarioObj);
	   
	    TabUsuarioObj TabView = tabService.consultar(Tab.getCdUsuario());
				
	    mv = new ModelAndView("redirect:/login");		
		return mv;
	}
   
	/*
	@RequestMapping(value = "/pesquisar/{txPesquisar}", method = RequestMethod.GET)
	public ModelAndView pesquisar(@PathVariable String txPesquisar) {
	
	   TabUsuarioObj TabView = tabService.pesquisa(txPesquisar);

		ModelAndView mv = new ModelAndView(txUrlTela);	
		if (TabView != null) {
		  mv.addObject("tabUsuarioObj",TabView);
		}else {
		  mv.addObject(new TabUsuarioObj());	
		  mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
		  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		
		return mv;
	}*/
	
}
