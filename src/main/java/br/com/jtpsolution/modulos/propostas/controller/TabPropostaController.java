package br.com.jtpsolution.modulos.propostas.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.TransactionUtilBean;
import br.com.jtpsolution.dao.cadastros.cliente.TabClienteObj;
import br.com.jtpsolution.dao.cadastros.cliente.VwTabClienteGrupoAcessoObj;
import br.com.jtpsolution.dao.cadastros.cliente.repository.TabClienteRepository;
import br.com.jtpsolution.dao.cadastros.cliente.repository.VwTabClienteGrupoAcessoRepository;
import br.com.jtpsolution.dao.cadastros.empresa.TabEmpresaObj;
import br.com.jtpsolution.dao.cadastros.usuario.TabUsuarioObj;
import br.com.jtpsolution.dao.cadastros.varios.TabDedicadoObj;
import br.com.jtpsolution.dao.cadastros.varios.TabDestinoImpexpObj;
import br.com.jtpsolution.dao.cadastros.varios.TabDestinoObj;
import br.com.jtpsolution.dao.cadastros.varios.TabInformativoObj;
import br.com.jtpsolution.dao.cadastros.varios.TabOrigemImpexpObj;
import br.com.jtpsolution.dao.cadastros.varios.TabOrigemObj;
import br.com.jtpsolution.dao.cadastros.varios.TabPerfilVeiculoObj;
import br.com.jtpsolution.dao.cadastros.varios.TabStatusObj;
import br.com.jtpsolution.dao.cadastros.varios.TabTaxaDolarObj;
import br.com.jtpsolution.dao.cadastros.varios.TabTipoCarregamentoObj;
import br.com.jtpsolution.dao.cadastros.varios.TabValoresFixosObj;
import br.com.jtpsolution.dao.cadastros.varios.VwTabDocumentosClassificacaoObj;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabDedicadoRepository;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabDestinoImpexpRepository;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabDestinoRepository;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabInformativoRepository;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabOrigemImpexpRepository;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabOrigemRepository;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabPerfilVeiculoRepository;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabStatusRepository;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabTaxaDolarRepository;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabTipoCarregamentoRepository;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabValoresFixosRepository;
import br.com.jtpsolution.dao.propostas.TabDescontosObj;
import br.com.jtpsolution.dao.propostas.TabPropostaAvariasObj;
import br.com.jtpsolution.dao.propostas.TabPropostaDocumentosObj;
import br.com.jtpsolution.dao.propostas.TabPropostaGrupoAcessoObj;
import br.com.jtpsolution.dao.propostas.TabPropostaNovaObj;
import br.com.jtpsolution.dao.propostas.TabPropostaObj;
import br.com.jtpsolution.dao.propostas.repository.TabDescontosRepository;
import br.com.jtpsolution.dao.propostas.repository.TabPropostaDocumentosRepository;
import br.com.jtpsolution.mensagens.TabRetornoSucessoObj;
import br.com.jtpsolution.modulos.cadastros.cliente.service.TabClienteService;
import br.com.jtpsolution.modulos.cadastros.documentosclassificacao.service.TabDocumentosClassificacaoService;
import br.com.jtpsolution.modulos.mantran.service.InterfaceMantranService;
import br.com.jtpsolution.modulos.microled.obj.VwTabAvariaCsObj;
import br.com.jtpsolution.modulos.microled.obj.VwTabConsultaCargaItemObj;
import br.com.jtpsolution.modulos.microled.service.MicroledService;
import br.com.jtpsolution.modulos.propostas.service.TabPropostaAvariasService;
import br.com.jtpsolution.modulos.propostas.service.TabPropostaGrupoAcessoService;
import br.com.jtpsolution.modulos.propostas.service.TabPropostaService;
import br.com.jtpsolution.modulos.util.geralcontroller.allController;
import br.com.jtpsolution.security.DadosUser;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.CalcRandom;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.GeneralUtil;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.mailthymeleaf.EmailSend;
import br.com.jtpsolution.util.regras.RegrasBean;

@Controller
@RequestMapping("/proposta")
public class TabPropostaController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabPropostaService tabService;

	@Autowired
	private RegrasBean regrasBean;

	@Autowired
	private TabValoresFixosRepository tabValoresFixosRepository;

	@Autowired
	private TabDestinoRepository tabDestinoRepository;

	@Autowired
	private TabOrigemRepository tabOrigemRepository;

	@Autowired
	private VwTabClienteGrupoAcessoRepository tabVwClienteGrupoClienteRepository;

	@Autowired
	private TabDescontosRepository tabDescontosRepository;

	@Autowired
	private TabDocumentosClassificacaoService tabDocumentosClassificacaoService;

	@Autowired
	private TabPropostaDocumentosRepository tabPropostaDocumentosRepository;

	@Autowired
	private TabInformativoRepository tabInformativoRepository;

	@Autowired
	private TabPropostaAvariasService tabPropostaAvariasService;

	@Autowired
	private TransactionUtilBean transactionUtilBean;

	@Autowired
	private VwTabClienteGrupoAcessoRepository vwTabClienteGrupoAcessoRepository;

	@Autowired
	private TabClienteRepository tabClienteRepository;

	@Autowired
	private EmailSend emailSend;

	@Autowired
	private TabPropostaGrupoAcessoService tabPropostaGrupoAcessoService;

	@Autowired
	private TabTaxaDolarRepository tabTaxaDolarRepository;

	@Autowired
	private TabDedicadoRepository tabDedicadoRepository;

	@Autowired
	private InterfaceMantranService interfaceMantranService;

	private String txUrlTela = Constants.TEMPLATE_PATH_PROPOSTA + "/proposta";

	@RequestMapping(value = "/novo")
	public ModelAndView novo(HttpServletRequest request) {

		// permissaoEndpoint("/proposta", request);

		ModelAndView mv = new ModelAndView(txUrlTela);
		listCamposIdioma(mv);
		mv.addObject(new TabPropostaObj());
		mv.addObject(new TabPropostaDocumentosObj());
		mv.addObject("selectcdcliente", selectcdcliente(null));
		mv.addObject("selectcdfreehand", selectcdfreehand(null));
		return mv;
	}

	@RequestMapping(value = "/gravar", method = RequestMethod.POST)
	public ModelAndView gravar(@Validated TabPropostaObj tabPropostaObj, Errors erros, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView(txUrlTela);
		listCamposIdioma(mv);
		mv.addObject(new TabPropostaDocumentosObj());
		adicionaMV(mv, tabPropostaObj);

		// interfaceMantranService.enviaMantran(tabPropostaObj.getCdProposta());

		DadosUser user = tabUsuariosService.DadosUsuario();

		TabTaxaDolarObj tabTaxaDolar = tabTaxaDolarRepository.findOne(1);

		if (erros.hasErrors()) {
			// adicionaMV(mv, tabPropostaObj);
			mv.addObject("listaErros", erros.getAllErrors());
			mv.addObject("selectcdcliente", selectcdcliente(tabPropostaObj));
			mv.addObject("selectcdfreehand", selectcdfreehand(tabPropostaObj));
			return mv;
		} else {
			List<ObjectError> error = new ArrayList<ObjectError>();
			error = regrasBean.verificaregras(getClass().getSimpleName(), request);
			if (!error.isEmpty()) {
				mv.addObject("listaErros", error);
				mv.addObject("selectcdcliente", selectcdcliente(tabPropostaObj));
				mv.addObject("selectcdfreehand", selectcdfreehand(tabPropostaObj));
				return mv;
			}
		}

		TabPropostaObj Tab = new TabPropostaObj();
		tabPropostaObj.setTxTipoProposta("DTA");

		if (tabPropostaObj.getCdProposta() != null) {

			TabDestinoObj tabDestino = tabDestinoRepository.findOne(tabPropostaObj.getTabDestinoObj().getCdDestino());
			TabOrigemObj tabOrigem = tabOrigemRepository.findOne(tabPropostaObj.getTabOrigemObj().getCdOrigem());

			// Verifica a Informativo
			TabInformativoObj tabInfo = tabInformativoRepository
					.findByCdCliente(tabPropostaObj.getTabClienteObj().getCdCliente());
			if (tabInfo == null) {
				tabInfo = tabInformativoRepository.findByCdCliente(999999);
			}

			Integer cdTipoCarregamento = tabPropostaObj.getTabTipoCarregamentoObj().getCdTipoCarregamento();
			
			// Verifica tabela valores fixos
			Integer cdImpExp = 1; //Importação
			if (cdTipoCarregamento == 2 || cdTipoCarregamento == 4) {
				cdImpExp = 2;
			}
			
			TabValoresFixosObj tabValoresFixos = tabValoresFixosRepository.findByTabEmpresaObjCdEmpresaAndCdImpexpAndTxTipo(2, cdImpExp, "DTA"); // DTA Band

			

			Integer ckRotaAmalog = 0;
			double vlDestino = 0;
			if (cdTipoCarregamento == 1 || cdTipoCarregamento == 3) {
				vlDestino = tabDestino.getVlDtaImportacao();
				tabPropostaObj.setVlTaxaInformativo(tabInfo.getVlInformativo());
				ckRotaAmalog = tabDestino.getCkRotaAmalog();
			} else {
				vlDestino = tabOrigem.getVlExportacao();
				tabPropostaObj.setVlTaxaInformativo(null);
				ckRotaAmalog = tabOrigem.getCkRotaAmalog();
			}

			/*
			 * if (ckRotaAmalog == 2 && tabPropostaObj.getCdCliente() != 5 &&
			 * tabPropostaObj.getCdCliente() != 32) { mv.addObject("tabProposta",
			 * tabPropostaObj); mv.addObject("txMensagem",
			 * "Esta rota estará liberada a partir de 04/05/2020!");
			 * mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER); return mv; }
			 */

			if (!Validator.isBlankOrNull(tabPropostaObj.getTabFreehandObj())) { // Freehand

				if (Validator.isBlankOrNull(tabPropostaObj.getVlCargaMoeda())) {
					mv.addObject("tabProposta", tabPropostaObj);
					mv.addObject("txMensagem", "Goods Value required field!");
					mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
					return mv;
				} else {
					tabPropostaObj.setCdImportadorDiretoColoader(1);
					tabPropostaObj.setTxContainer("0");
				}

			} else {

				if (Validator.isBlankOrNull(tabPropostaObj.getCdImportadorDiretoColoader())) {
					mv.addObject("tabProposta", tabPropostaObj);
					mv.addObject("txMensagem", "Importador/Exportador Direto ou Coloader ? campo obrigatório!");
					mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
					adicionaMV(mv, tabPropostaObj);
					return mv;
				}
			}

			if (tabPropostaObj.getCdImportadorDiretoColoader() == 2
					&& Validator.isBlankOrNull(tabPropostaObj.getTabColoaderDespachanteObj())) {
				mv.addObject("tabProposta", tabPropostaObj);
				if (user.getCdIdioma().equals("EN")) {
					mv.addObject("txMensagem", "Coloader required field!");
				} else {
					mv.addObject("txMensagem", "Coloader campo obrigatório!");
				}
				mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
				adicionaMV(mv, tabPropostaObj);
				return mv;
			}

			/*
			 * if (ckRotaAmalog == 2 &&
			 * Validator.isBlankOrNull(tabPropostaObj.getTxContainer()) &&
			 * Validator.isBlankOrNull(tabPropostaObj.getCdFreehand()) &&
			 * (tabPropostaObj.getCdTipoCarregamento() == 1 ||
			 * tabPropostaObj.getCdTipoCarregamento() == 3)) { mv.addObject("tabProposta",
			 * tabPropostaObj); mv.addObject("txMensagem",
			 * "No. Container campo obrigatório!"); mv.addObject("tipoMensagem",
			 * Constants.TYPE_NOTIFY_DANGER); mv.addObject("selectcdorigem",
			 * selectcdtipoorigem(tabPropostaObj.getCdTipoCarregamento()));
			 * mv.addObject("selectcddestino",
			 * selectcdtipodestino(tabPropostaObj.getCdTipoCarregamento()));
			 * mv.addObject("selectcdcliente",
			 * selectcdcliente(tabPropostaObj.getCdCliente()));
			 * mv.addObject("selectcdfreehand",
			 * selectcdfreehand(tabPropostaObj.getCdFreehand())); return mv; }
			 */

			if (ckRotaAmalog == 1 && Validator.isBlankOrNull(tabPropostaObj.getTabFreehandObj())
					&& Validator.isBlankOrNull(tabPropostaObj.getVlCarga())) {
				mv.addObject("tabProposta", tabPropostaObj);
				mv.addObject("txMensagem", "Valor Mercadoria campo obrigatório!");
				mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
				adicionaMV(mv, tabPropostaObj);
				return mv;
			}

			if (ckRotaAmalog == 2 && tabPropostaObj.getVlPeso() > 7000 && user.getCdGrupoAcesso() > 1) {
				mv.addObject("tabProposta", tabPropostaObj);
				if (user.getCdIdioma().equals("EN")) {
					mv.addObject("txMensagem", "Weight exceeds the boarding rule! Please contact Bandeirantes");
				} else {
					mv.addObject("txMensagem",
							"Peso superior a regra de embarque! Favor entrar em contato com a Bandeirantes");
				}
				mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
				adicionaMV(mv, tabPropostaObj);
				return mv;
			}

			if (ckRotaAmalog == 2 && tabPropostaObj.getVlM3() > 20 && user.getCdGrupoAcesso() > 1) {
				mv.addObject("tabProposta", tabPropostaObj);
				if (user.getCdIdioma().equals("EN")) {
					mv.addObject("txMensagem", "Cubage above the boarding rule! Please contact Bandeirantes");
				} else {
					mv.addObject("txMensagem",
							"Cubagem superior a regra de embarque! Favor entrar em contato com a Bandeirantes");
				}
				mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
				adicionaMV(mv, tabPropostaObj);
				return mv;
			}

			if (tabPropostaObj.getVlFretePrevisto() == null && tabPropostaObj.getTabDestinoObj().getCdDestino() == 1) {

				mv.addObject("tabProposta", tabPropostaObj);
				mv.addObject("txMensagem", "Rota Betim - MG não será mais utilizada!");
				mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
				adicionaMV(mv, tabPropostaObj);
				return mv;
			}

			Integer ckFreteExclusivo = (tabPropostaObj.getCkFreteExclusivo() != null
					? tabPropostaObj.getCkFreteExclusivo()
					: 2); // 2 = Não
			double vlPeso = (tabPropostaObj.getVlPeso() != null ? tabPropostaObj.getVlPeso() : 0);
			double vlM3 = (tabPropostaObj.getVlM3() != null ? tabPropostaObj.getVlM3() : 0);
			// double vlDiferenca = (tabPropostaObj.getVlDiferenca() != null ?
			// tabPropostaObj.getVlDiferenca() : 0);
			double vlCarga = (tabPropostaObj.getVlCarga() != null ? tabPropostaObj.getVlCarga() : 0);

			if (!Validator.isBlankOrNull(tabPropostaObj.getTabFreehandObj())) { // freehand
				vlCarga = (tabPropostaObj.getVlCargaMoeda() * tabTaxaDolar.getVlTaxaDolar());
			}

			double vlFretePeso = 0;
			double vlAliqIcms = 0;
			if (!Validator.isBlankOrNull(tabDestino.getVlIcms())) {
				vlAliqIcms = tabDestino.getVlIcms();
			}else if (!Validator.isBlankOrNull(tabValoresFixos.getVlIcms())) {
				vlAliqIcms = tabValoresFixos.getVlIcms();
			}
			double vlAliqAdvalorem = 0;
			if (!Validator.isBlankOrNull(tabValoresFixos.getVlAdvalorem())) {
				vlAliqAdvalorem = tabValoresFixos.getVlAdvalorem();
			}

			double vlAdvalorem = 0;
			double vlDespacho = 0;
			if (tabPropostaObj.getCdProposta() != null && user.getCdGrupoVisao() == 2) {
				if (!Validator.isBlankOrNull(tabPropostaObj.getVlDespacho())) {
					vlDespacho = tabPropostaObj.getVlDespacho();
				}

			} else {
				if (tabPropostaObj.getVlDespacho() != null) {
					if (tabPropostaObj.getVlDespacho() > 0) {
						vlDespacho = tabPropostaObj.getVlDespacho();
					}
				} else if (tabValoresFixos.getVlDespacho() != null) {
					vlDespacho = tabValoresFixos.getVlDespacho();
				}
			}

			double vlCat = (tabPropostaObj.getVlCat() != null ? tabPropostaObj.getVlCat() : 0);
			double vlItr = (tabPropostaObj.getVlItr() != null ? tabPropostaObj.getVlItr() : 0);
			double vlPedagio = 0;
			if (tabPropostaObj.getVlPedagio() != null) {
				if (tabPropostaObj.getVlPedagio() > 0) {
					vlPedagio = tabPropostaObj.getVlPedagio();
				}
			} else if (tabValoresFixos.getVlPedagio() != null) { 
				vlPedagio = tabValoresFixos.getVlPedagio();
			}

			double vlAdeme = (tabPropostaObj.getVlAdeme() != null ? tabPropostaObj.getVlAdeme() : 0);
			double vlOutros = (tabPropostaObj.getVlOutros() != null ? tabPropostaObj.getVlOutros() : 0);
			double vlTaxaEntrega = (tabPropostaObj.getVlTaxaEntrega() != null ? tabPropostaObj.getVlTaxaEntrega() : 0);
			double vlTaxaColeta = (tabPropostaObj.getVlTaxaColeta() != null ? tabPropostaObj.getVlTaxaColeta() : 0);
			double vlDescarga = (tabPropostaObj.getVlDescarga() != null ? tabPropostaObj.getVlDescarga() : 0);
			double vlEstadia = (tabPropostaObj.getVlEstadia() != null ? tabPropostaObj.getVlEstadia() : 0);
			double vlEscolta = (tabPropostaObj.getVlEscolta() != null ? tabPropostaObj.getVlEscolta() : 0);
			double vlEstacionamento = (tabPropostaObj.getVlEstacionamento() != null
					? tabPropostaObj.getVlEstacionamento()
					: 0);
			double vlEmissaoDta = (tabPropostaObj.getVlEmissaoDta() != null ? tabPropostaObj.getVlEmissaoDta() : 0);
			double vlAjudante = (tabPropostaObj.getVlAjudante() != null ? tabPropostaObj.getVlAjudante() : 0);
			double vlMonitaramento = (tabPropostaObj.getVlMonitoramento() != null ? tabPropostaObj.getVlMonitoramento()
					: 0);
			double vlImoAdesivagem = (tabPropostaObj.getVlImoAdesivagem() != null ? tabPropostaObj.getVlImoAdesivagem()
					: 0);
			double vlImpostosSuspensos = (tabPropostaObj.getVlImpostosSuspensos() != null
					? tabPropostaObj.getVlImpostosSuspensos()
					: 0);
			double vlDevolucaoContainer = (tabPropostaObj.getVlDevolucaoContainer() != null
					? tabPropostaObj.getVlDevolucaoContainer()
					: 0);
			double vlDedicado = 0;

			if (vlCarga > 1000000 && user.getCdGrupoVisao() > 2) {
				mv.addObject("tabProposta", tabPropostaObj);
				mv.addObject("txMensagem", "Valor maior de 1.000.000,00! Entre em contato com o suporte Amalog");
				mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
				adicionaMV(mv, tabPropostaObj);
				return mv;
			}

			if (user.getCdGrupoVisao() > 2) { // Se não for Amalog sempre Não!
				tabPropostaObj.setCkFreteExclusivo(2);
			}

			double vlM3Pack = 0;
			boolean ckEfetivo = false;
			if (!Validator.isBlankOrNull(tabPropostaObj.getVlComprimento()) || !Validator.isBlankOrNull(tabPropostaObj.getVlAltura())
					|| !Validator.isBlankOrNull(tabPropostaObj.getVlLargura())) {
				if (tabPropostaObj.getVlComprimento() > 0 && tabPropostaObj.getVlAltura() > 0
						&& tabPropostaObj.getVlLargura() > 0) {
					vlM3Pack = (tabPropostaObj.getVlComprimento() * tabPropostaObj.getVlAltura()
							* tabPropostaObj.getVlLargura());
					ckEfetivo = true;
				}
			}
			if (vlM3Pack > 0) {
				vlM3 = vlM3Pack;
				tabPropostaObj.setVlM3Pack(vlM3Pack);

			} else {
				tabPropostaObj.setVlM3Pack(null);
			}

			if (tabPropostaObj.getVlPesoExtratoDesova() != null) {
				if (tabPropostaObj.getVlPesoExtratoDesova() > 0) {
					vlPeso = tabPropostaObj.getVlPesoExtratoDesova();
				}
			}

			if (tabPropostaObj.getVlMercadoriaInvoice() != null) {
				if (tabPropostaObj.getVlMercadoriaInvoice() > 0) {
					vlCarga = tabPropostaObj.getVlMercadoriaInvoice();
				}
			}

			double vlBase = (vlPeso / 300);
			if (vlBase < vlM3) {
				vlBase = vlM3;
			}

			if (vlBase < 1) {
				vlBase = 1;
			}

			TabPerfilVeiculoObj tabPerfilVeiculoObj = new TabPerfilVeiculoObj();
			if (vlPeso > 6000 || vlM3 > 20) {
				tabPerfilVeiculoObj.setCdPerfilVeiculo(2);
				tabPropostaObj.setTabPerfilVeiculoObj(tabPerfilVeiculoObj);
			} else {
				tabPerfilVeiculoObj.setCdPerfilVeiculo(1);
				tabPropostaObj.setTabPerfilVeiculoObj(tabPerfilVeiculoObj);
			}

			if (vlPeso > 15000 || vlM3 > 50) {
				mv.addObject("tabProposta", tabPropostaObj);
				if (user.getCdIdioma().equals("EN")) {
					mv.addObject("txMensagem", "Weight/M3 exceeds the permitted!");
				} else {
					mv.addObject("txMensagem", "Peso/M3 excedem o permitido!");
				}
				mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
				adicionaMV(mv, tabPropostaObj);
				return mv;
			}

			double vlFinal = (vlBase);
			vlFretePeso = (vlDestino * vlFinal);

			double vlFretePesoSdesconto = 0;
			double vlDesconto = 0;
			double vlPorcentDesconto = 0;
			if (tabPropostaObj.getTabPerfilVeiculoObj().getCdPerfilVeiculo() == 2 && ckFreteExclusivo > 1
					&& ckRotaAmalog == 1) { // Carreta terá
				// desconto
				// e for
				// rota
				// amalog

				List<TabDescontosObj> listDescontos = tabDescontosRepository.findByPrecosQuery(vlBase);
				vlPorcentDesconto = listDescontos.get(0).getVlDesconto();

				vlFretePesoSdesconto = vlFretePeso;
				vlDesconto = ((vlFretePeso * vlPorcentDesconto) / 100);
				vlFretePeso = (vlFretePeso - vlDesconto);
			} else {

				if (request.getParameter("submitFields").contains("vlPeso")) {
					vlDesconto = 0;
				} else {

					if (!Validator.isBlankOrNull(tabPropostaObj.getVlDesconto())) {

						vlFretePesoSdesconto = vlFretePeso;
						vlDesconto = tabPropostaObj.getVlDesconto();
						vlFretePeso = (vlFretePesoSdesconto - vlDesconto);
					}
				}

			}

			if (vlCarga > 0) {
				if (vlAliqAdvalorem > 0) {
				  vlAdvalorem = ((vlCarga * vlAliqAdvalorem) / 100) / 100;
				}
				tabPropostaObj.setVlAdvalorem(vlAdvalorem);
			}

			double vlRemonte = 0;
			if (!Validator.isBlankOrNull(tabPropostaObj.getCkNaoRemonte())) {
				if (tabPropostaObj.getCkNaoRemonte() == 2) {
					vlRemonte = vlFretePeso;
				}
			}

			double vlImoCarga = 0;
			if (!Validator.isBlankOrNull(tabPropostaObj.getCkImoPerigoso())) {
				if (tabPropostaObj.getCkImoPerigoso() == 1) {
				  if (!Validator.isBlankOrNull(tabValoresFixos.getVlTaxaImoPerigoso())) {
					  vlImoCarga = (((vlFretePeso + vlRemonte) * tabValoresFixos.getVlTaxaImoPerigoso()) / 100);
				  }
				}
			}

			double vlGris = 0;
			if (ckRotaAmalog == 1) {
				if (Validator.isBlankOrNull(tabPropostaObj.getVlMercadoriaInvoice())) {
					if (!Validator.isBlankOrNull(tabDestino.getVlGris())) {
						vlGris = ((vlCarga * tabDestino.getVlGris()) / 100);
					} else if (!Validator.isBlankOrNull(tabValoresFixos.getVlTaxaGris())) { 
						vlGris = ((vlCarga * tabValoresFixos.getVlTaxaGris()) / 100);
					}
				} else {
					if (!Validator.isBlankOrNull(tabDestino.getVlGris())) {
						vlGris = ((tabPropostaObj.getVlMercadoriaInvoice() * tabDestino.getVlGris()) / 100);
					} else if (!Validator.isBlankOrNull(tabValoresFixos.getVlTaxaGris())) {
						vlGris = ((tabPropostaObj.getVlMercadoriaInvoice() * tabValoresFixos.getVlTaxaGris()) / 100);
					}
				}

			} else {
				if (tabPropostaObj.getCkFreteExclusivo() == 1) {
					if (Validator.isBlankOrNull(tabPropostaObj.getVlMercadoriaInvoice())) {
						if (!Validator.isBlankOrNull(tabDestino.getVlGris())) {
							vlGris = ((vlCarga * tabDestino.getVlGris()) / 100);
						}else if (!Validator.isBlankOrNull(tabValoresFixos.getVlTaxaGris())) {
						   vlGris = ((vlCarga * tabValoresFixos.getVlTaxaGris()) / 100);
						}
					} else {
						if (!Validator.isBlankOrNull(tabDestino.getVlGris())) {
						   vlGris = ((tabPropostaObj.getVlMercadoriaInvoice() * tabDestino.getVlGris()) / 100);
						}else if (!Validator.isBlankOrNull(tabValoresFixos.getVlTaxaGris())) {
						   vlGris = ((tabPropostaObj.getVlMercadoriaInvoice() * tabValoresFixos.getVlTaxaGris()) / 100);
						}
					}
				}
			}

			if (tabPropostaObj.getCkFreteExclusivo() == 1) {

				List<TabDedicadoObj> listDedicado = tabDedicadoRepository.findByPrecosDedicadoQuery(
						tabPropostaObj.getTabOrigemObj().getCdOrigem(),
						tabPropostaObj.getTabDestinoObj().getCdDestino(), vlBase);

				if (!listDedicado.isEmpty()) {
					vlFretePeso = listDedicado.get(0).getVlValor();
				}

				if (!Validator.isBlankOrNull(tabPropostaObj.getVlDesconto())) {
					vlFretePesoSdesconto = vlFretePeso;
					vlFretePeso = (vlFretePeso - tabPropostaObj.getVlDesconto());

				}

			}

			double vlSubtotal = (vlFretePeso + vlRemonte + vlAdvalorem + vlDespacho + vlCat + vlItr + vlPedagio
					+ vlAdeme + vlOutros + vlTaxaEntrega + vlTaxaColeta + vlDescarga + vlGris + vlEstadia + vlEscolta
					+ vlEstacionamento + vlEmissaoDta + vlAjudante + vlImoCarga + vlMonitaramento + vlImoAdesivagem
					+ vlImpostosSuspensos + vlDevolucaoContainer);

			// double vlIcms =
			// (((((vlSubtotal*vlAliqIcms)/100)+vlSubtotal)*vlAliqIcms)/100);

			double vlBasevlIcms = ((vlSubtotal / 88) * 100);
			if (!Validator.isBlankOrNull(tabDestino.getVlIcms())) {
				double vlAliq = (100-tabDestino.getVlIcms());
				vlBasevlIcms = ((vlSubtotal / vlAliq) * 100);
			}else if (!Validator.isBlankOrNull(tabValoresFixos.getVlIcms())) {
				double vlAliq = (100-tabValoresFixos.getVlIcms());
				vlBasevlIcms = ((vlSubtotal / vlAliq) * 100);				
			}

			if (tabPropostaObj.getTabTipoCarregamentoObj().getCdTipoCarregamento() == 2
					|| tabPropostaObj.getTabTipoCarregamentoObj().getCdTipoCarregamento() == 4) {
				vlBasevlIcms = vlSubtotal;
				tabPropostaObj.setVlIcms(0.0);
				tabPropostaObj.setVlAliqIcms(0.0);
				tabPropostaObj.setVlBaseIcms(0.0);
				tabPropostaObj.setVlFrete(vlBasevlIcms);

			} else {

				if (!Validator.isBlankOrNull(tabPropostaObj.getTabFreehandObj())) { // Freehand
					vlBasevlIcms = vlBasevlIcms + ((vlBasevlIcms * 10) / 100);
					tabPropostaObj.setVlFreteMoeda(vlBasevlIcms / tabTaxaDolar.getVlTaxaDolar());
					tabPropostaObj.setVlCargaMoeda(vlCarga / tabTaxaDolar.getVlTaxaDolar());
				}

				tabPropostaObj.setVlAliqIcms(vlAliqIcms);
				tabPropostaObj.setVlIcms(vlBasevlIcms - vlSubtotal);
				tabPropostaObj.setVlBaseIcms(vlBasevlIcms);
			}

			if (ckRotaAmalog == 2) {

				tabPropostaObj.setVlEmissaoDta(0.0);
				tabPropostaObj.setTxNomePagador("Bandeirantes Deicmar Logística Integrada S/A");
				tabPropostaObj.setTxEndereco("Av Eduardo Pereira Guinle");
				tabPropostaObj.setTxNumero("0");
				tabPropostaObj.setTxBairro("Docas");
				tabPropostaObj.setTxCidade("Santos");
				tabPropostaObj.setTxUf("SP");
				tabPropostaObj.setTxCnpj("58.188.756/0001-96");
				tabPropostaObj.setTxCep("11013-250");
				tabPropostaObj.setTxEmailPagador("transitoimpo@band-deicmar.com.br");

				if (tabPropostaObj.getDtAceite() == null) {
					TabUsuarioObj tabUsuario = new TabUsuarioObj();
					tabUsuario.setCdUsuario(user.getCdUsuario());
					tabPropostaObj.setTabUsuarioAceiteObj(tabUsuario);
					tabPropostaObj.setDtAceite(new Date());
					TabStatusObj tabStatusObj = new TabStatusObj();
					tabStatusObj.setCdStatus(3);
					tabPropostaObj.setTabStatusObj(tabStatusObj);
				}
			}

			tabPropostaObj.setCkRotaAmalog(ckRotaAmalog);
			tabPropostaObj.setVlFreteValor(vlRemonte);
			tabPropostaObj.setVlValor(vlDestino);
			tabPropostaObj.setVlGris(vlGris);
			tabPropostaObj.setVlImoCarga(vlImoCarga);
			tabPropostaObj.setVlDespacho(vlDespacho);
			tabPropostaObj.setVlPedagio(vlPedagio);
			tabPropostaObj.setVlBase(vlBase);
			tabPropostaObj.setVlFinal(vlFinal);
			tabPropostaObj.setVlFretePeso(vlFretePeso);
			// tabPropostaObj.setVlAliqIcms(vlAliqIcms);
			// tabPropostaObj.setVlIcms(vlBasevlIcms-vlSubtotal);
			// tabPropostaObj.setVlBaseIcms(vlBasevlIcms);
			tabPropostaObj.setVlSubtotal(vlSubtotal);
			tabPropostaObj.setTxHashProposta(tabPropostaObj.getTxHashProposta());
			tabPropostaObj.setVlFretePesoSdesconto(vlFretePesoSdesconto);
			tabPropostaObj.setVlDesconto(vlDesconto);

			if (!ckEfetivo) {
				tabPropostaObj.setVlFretePrevisto(vlBasevlIcms);
			} else {
				tabPropostaObj.setVlFrete(vlBasevlIcms);
			}

			Tab = tabService.gravar(tabPropostaObj, request.getParameter("submitFields"));

			// Adiciona as Permissões pela Lista de Permissões do Cliente o Armazem
			if (cdTipoCarregamento == 1 || cdTipoCarregamento == 3) {
				if (tabDestino.getTabArmazemObj() != null) {
					AdicionaListaPermissaoPropostaGrupoAcesso(Tab.getCdProposta(),
							tabDestino.getTabArmazemObj().getCdCliente());
				}
			} else if (cdTipoCarregamento == 2 || cdTipoCarregamento == 4) {
				if (tabOrigem.getTabArmazemObj() != null) {
					AdicionaListaPermissaoPropostaGrupoAcesso(Tab.getCdProposta(),
							tabOrigem.getTabArmazemObj().getCdCliente());
				}
			}

		} else {

			CalcRandom calc = new CalcRandom();
			tabPropostaObj.setTxHashProposta(calc.getloginAleatoria(30));
			tabPropostaObj.setDtProposta(new Date());
			TabUsuarioObj tabUsuario = new TabUsuarioObj();
			tabUsuario.setCdUsuario(user.getCdUsuario());
			tabPropostaObj.setTabUsuarioObj(tabUsuario);
			TabStatusObj tabStatusObj = new TabStatusObj();
			tabStatusObj.setCdStatus(1);
			tabPropostaObj.setTabStatusObj(tabStatusObj);
			tabPropostaObj.setDtValidade(GeneralParser.retornaDataPosteriorDiasCorridos(new Date(), 40));
			TabEmpresaObj tabEmpresaObj = new TabEmpresaObj();
			tabEmpresaObj.setCdEmpresa(user.getCdEmpresa());
			tabPropostaObj.setTabEmpresaObj(tabEmpresaObj);

			Tab = tabService.gravar(tabPropostaObj, request.getParameter("submitFields"));
			if (Validator.isBlankOrNull(tabPropostaObj.getTxProposta())) {
				Tab.setTxProposta("AMA" + GeneralParser.setFormatZeroEsq(String.valueOf(Tab.getCdProposta()), 5) + "/"
						+ GeneralParser.format_date("yy", new Date()));
			}

			Tab = tabService.gravar(Tab, request.getParameter("submitFields"));

			// Adiciona as Permissões pela Lista de Permissões do Cliente
			AdicionaListaPermissaoPropostaGrupoAcesso(Tab.getCdProposta(), Tab.getTabClienteObj().getCdCliente());

			// Adiciona as Permissões pela Lista de Permissões do Freehand
			if (user.getCdGrupoVisao() == 8) {
				AdicionaListaPermissaoPropostaGrupoAcesso(Tab.getCdProposta(), Tab.getTabFreehandObj().getCdCliente());
			}
		}

		// Adiciona Permissão Coloader
		if (Tab.getTabColoaderDespachanteObj() != null) {
			if (!Validator.isBlankOrNull(Tab.getTabColoaderDespachanteObj().getCdCliente())) {
				AdicionaPermissaoPropostaGrupoAcessoColoader(Tab.getCdProposta(),
						Tab.getTabColoaderDespachanteObj().getCdCliente());
			}
		} 

			TabPropostaNovaObj TabView = tabService.consultarSimples(Tab.getCdProposta());
		    
		
			//List<TabPropostaDocumentosObj> listDocumentos = tabService.listaDocumentos(Tab.getCdProposta());
	
			adicionaMV(mv, tabPropostaObj);
			mv.addObject("listaDocumentos", TabView.getListPropostaDocumentos());
			mv.addObject("tabPropostaObj", TabView);
			if (user.getCdIdioma().equals("EN")) {
				mv.addObject("txMensagem", "Registration added / changed successfully!");
			} else {
				mv.addObject("txMensagem", Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
			}
			mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_SUCCESS);
			mv.addObject("listaAvarias", resultListAvarias(Tab.getCdProposta()));
		return mv;
	}

	private void AdicionaListaPermissaoPropostaGrupoAcesso(Integer cdProposta, Integer cdCliente) {

		List<VwTabClienteGrupoAcessoObj> lst = vwTabClienteGrupoAcessoRepository.findByCdCliente(cdCliente);

		for (VwTabClienteGrupoAcessoObj atual : lst) {

			TabPropostaGrupoAcessoObj Tab = new TabPropostaGrupoAcessoObj();
			Tab.setCdProposta(cdProposta);
			Tab.setCdGrupoAcesso(atual.getCdGrupoAcesso());
			tabPropostaGrupoAcessoService.gravar(Tab);
		}
	}

	private void adicionaMV(ModelAndView mv, TabPropostaObj tabPropostaObj) {

		if (tabPropostaObj.getTabTipoCarregamentoObj() != null) {
			mv.addObject("selectcdorigem",
					selectcdtipoorigem(tabPropostaObj.getTabTipoCarregamentoObj().getCdTipoCarregamento()));
			mv.addObject("selectcddestino",
					selectcdtipodestino(tabPropostaObj.getTabTipoCarregamentoObj().getCdTipoCarregamento()));
			mv.addObject("selectcddoctoclassificacao",
					selectcdDoctoClassificacao(tabPropostaObj.getTabTipoCarregamentoObj().getCdTipoCarregamento()));
		}
		mv.addObject("selectcdcliente", selectcdcliente(tabPropostaObj));
		mv.addObject("selectcdfreehand", selectcdfreehand(tabPropostaObj));

	}

	private void AdicionaPermissaoPropostaGrupoAcessoColoader(Integer cdProposta, Integer cdCliente) {

		// transactionUtilBean.queryInsertUpdate("delete from TabPropostaGrupoAcessoObj
		// t where t.cdProposta = "+cdProposta+" and t.ckColoader = 1");

		VwTabClienteGrupoAcessoObj vwTabCliente = tabVwClienteGrupoClienteRepository
				.findByCdClienteAndCkCliente(cdCliente, 1);

		if (vwTabCliente != null) {
			TabPropostaGrupoAcessoObj tabPropostaGrupoAcessoObj = tabPropostaGrupoAcessoService
					.consultarPropostaGrupoAcesso(cdProposta, vwTabCliente.getCdGrupoAcesso(), true);

			if (tabPropostaGrupoAcessoObj == null) {
				transactionUtilBean.queryInsertUpdate("delete from TabPropostaGrupoAcessoObj t where t.cdProposta = "
						+ cdProposta + " and t.ckColoader = 1");
				TabPropostaGrupoAcessoObj Tab = new TabPropostaGrupoAcessoObj();
				Tab.setCdProposta(cdProposta);
				Tab.setCdGrupoAcesso(vwTabCliente.getCdGrupoAcesso());
				Tab.setCkColoader(1);
				tabPropostaGrupoAcessoService.gravar(Tab);
			}
		}
	}

	@RequestMapping(value = "/consultar/{cdProposta}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable Integer cdProposta, HttpServletRequest request) {

		// permissaoEndpoint("/proposta", request);

		TabPropostaObj TabView = tabService.consultar(cdProposta);

		// interfaceMantranService.InterfaceMantran(cdProposta);

		// List<VwTabPropostaDocumentosObj> listDocumentos =
		// tabService.listaDocumentos(cdProposta);

		ModelAndView mv = new ModelAndView(txUrlTela);
		listCamposIdioma(mv);
		if (TabView != null) {
			adicionaMV(mv, TabView);
			mv.addObject("tabPropostaObj", TabView);
			mv.addObject(new TabPropostaDocumentosObj());
			mv.addObject("listaDocumentos", TabView.getListPropostaDocumentos());
			mv.addObject("cdProposta", TabView.getCdProposta());
			mv.addObject("listaAvarias", resultListAvarias(TabView.getCdProposta()));
		} else {
			adicionaMV(mv, TabView);
			mv.addObject(new TabPropostaObj());
			mv.addObject("listaDocumentos", TabView.getListPropostaDocumentos());
			mv.addObject(new TabPropostaDocumentosObj());
			mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
			mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}

	@RequestMapping(value = "/propostaaceite/{cdProposta}", method = RequestMethod.GET)
	public ModelAndView propostaaceite(@PathVariable Integer cdProposta) {

		TabPropostaObj TabView = tabService.consultar(cdProposta);

		ModelAndView mv = new ModelAndView(txUrlTela);
		listCamposIdioma(mv);
		if (TabView != null) {
			adicionaMV(mv, TabView);
			mv.addObject(new TabPropostaDocumentosObj());
			mv.addObject("listaDocumentos", TabView.getListPropostaDocumentos());
			mv.addObject("tabPropostaObj", TabView);
			mv.addObject("txEdit", "propostaaceite");
			mv.addObject("cdProposta", TabView.getCdProposta());
			mv.addObject("listaAvarias", resultListAvarias(TabView.getCdProposta()));
		} else {
			mv.addObject(new TabPropostaDocumentosObj());
			mv.addObject(new TabPropostaObj());
			mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
			mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}

	@RequestMapping(value = "/gravarmodal", method = RequestMethod.POST)
	public @ResponseBody List<?> gravarmodal(@Validated TabPropostaObj tabPropostaObj, Errors erros,
			HttpServletRequest request) {

		DadosUser user = tabUsuariosService.DadosUsuario();

		List<ObjectError> error = new ArrayList<ObjectError>();

		if (Validator.isBlankOrNull(tabPropostaObj.getTxEndereco())
				|| Validator.isBlankOrNull(tabPropostaObj.getTxNumero())
				|| Validator.isBlankOrNull(tabPropostaObj.getTxBairro())
				|| Validator.isBlankOrNull(tabPropostaObj.getTxCidade())
				|| Validator.isBlankOrNull(tabPropostaObj.getTxUf())
				|| Validator.isBlankOrNull(tabPropostaObj.getTxCnpj())
				|| Validator.isBlankOrNull(tabPropostaObj.getTxCep())
				|| Validator.isBlankOrNull(tabPropostaObj.getTxEmailPagador())) {

			ObjectError objErro = new ObjectError("txImportador", "Todos os campos são obrigatórios!");
			error.add(objErro);
			return error;

		}

		if (Validator.isBlankOrNull(tabPropostaObj.getCdImportadorDiretoColoader())) {
			ObjectError objErro = new ObjectError("txImportador", "Importador Direto ou Coloader ? campo obrigatório!");
			error.add(objErro);
			return error;
		}

		/*
		 * if (!Validator.isValidEmail(tabPropostaObj.getTxEmailTaxaInformativo())) {
		 * ObjectError objErro = new ObjectError("txImportador",
		 * "Email Recebimento Taxa Informativo inválido!"); error.add(objErro); return
		 * error; }
		 */

		/*
		 * if (!Validator.isValidEmail(tabPropostaObj.getTxEmailPagador())) {
		 * ObjectError objErro = new ObjectError("txImportador",
		 * "Email Pagador inválido!"); error.add(objErro); return error; }
		 */

		/*
		 * if (!Validator.isValidEmail(tabPropostaObj.getTxEmailFollowup())) {
		 * ObjectError objErro = new ObjectError("txImportador",
		 * "Email Followup inválido!"); error.add(objErro); return error; }
		 */

		/*
		 * if (!Validator.isValidEmail(tabPropostaObj.getTxEmailProgramacao())) {
		 * ObjectError objErro = new ObjectError("txImportador",
		 * "Email Programação inválido!"); error.add(objErro); return error; }
		 */
		TabUsuarioObj tabUsuarioObj = new TabUsuarioObj();
		tabUsuarioObj.setCdUsuario(user.getCdUsuario());
		tabPropostaObj.setTabUsuarioAceiteObj(tabUsuarioObj);
		tabPropostaObj.setDtAceite(new Date());
		TabStatusObj tabStatusObj = new TabStatusObj();
		tabStatusObj.setCdStatus(3);
		tabPropostaObj.setTabStatusObj(tabStatusObj);
		TabPropostaObj Tab = tabService.gravar(tabPropostaObj, request.getParameter("submitFields"));

		List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();
		TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
		tSuccess.setCd_mensagem(1);
		tSuccess.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
		success.add(tSuccess);

		return success;
	}

	@RequestMapping(value = "/gravardadosdocumentos", method = RequestMethod.POST)
	public @ResponseBody List<?> gravardadosdocumentos(@Validated TabPropostaObj tabPropostaObj, Errors erros,
			HttpServletRequest request) {

		DadosUser user = tabUsuariosService.DadosUsuario();

		List<ObjectError> error = new ArrayList<ObjectError>();
		if (erros.hasErrors()) {
			// mv.addObject("listaErros", erros.getAllErrors());
			error.addAll(erros.getAllErrors());
			return error;
		} else {
			error = regrasBean.verificaregras(getClass().getSimpleName(), request);
			if (!error.isEmpty()) {
				return error;
			}
		}

		/*
		 * if (Validator.isBlankOrNull(tabPropostaObj.getTxTerminalMar())) { ObjectError
		 * objErro = new ObjectError("txTerminalMar",
		 * "Carga encontra-se no Terminal ou no Mar ? Campo obrigatório!");
		 * error.add(objErro); return error; }
		 */

		/*
		 * if (tabPropostaObj.getTxTerminalMar().equals("Mar")) { ObjectError objErro =
		 * new ObjectError("txTerminalMar", "Favor verificar com o seu terminal!");
		 * error.add(objErro); return error; }
		 */

		/*
		 * if (tabPropostaObj.getTxTerminalMar().equals("Terminal") &&
		 * Validator.isBlankOrNull(tabPropostaObj.getCkDesistenciaVistoria())) {
		 * ObjectError objErro = new ObjectError("txTerminalMar",
		 * "Desistência de Vistoria ? Campo Obrigatório!"); error.add(objErro); return
		 * error; }
		 * 
		 * if (tabPropostaObj.getCkDesistenciaVistoria() == 1 &&
		 * Validator.isBlankOrNull(tabPropostaObj.getTxBl())) { ObjectError objErro =
		 * new ObjectError("txTerminalMar", "HBL campo Obrigatório!");
		 * error.add(objErro); return error; }
		 */

		/*
		 * if (tabPropostaObj.getCkDesistenciaVistoria() == 1 &&
		 * Validator.isBlankOrNull(tabPropostaObj.getTxLote())) { ObjectError objErro =
		 * new ObjectError("txTerminalMar", "Lote campo Obrigatório!");
		 * error.add(objErro); return error; }
		 */

		/*
		 * if (Validator.isBlankOrNull(tabPropostaObj.getTxTerminalMar()) ||
		 * Validator.isBlankOrNull(tabPropostaObj.getCkDesistenciaVistoria())) {
		 * 
		 * ObjectError objErro = new ObjectError("txTerminalMar",
		 * "Desistência de Vistoria e HBL são campos obrigatórios!");
		 * error.add(objErro); return error;
		 * 
		 * }
		 */

		TabPropostaObj Tab = tabService.consultar(tabPropostaObj.getCdProposta());

		if ((tabPropostaObj.getTabTipoCarregamentoObj().getCdTipoCarregamento() == 1
				|| tabPropostaObj.getTabTipoCarregamentoObj().getCdTipoCarregamento() == 3)
				&& !Validator.isBlankOrNull(tabPropostaObj.getTxLote())) {

			if (tabPropostaObj.getTxTerminalMar().equals("Terminal")
					&& Validator.isBlankOrNull(tabPropostaObj.getCkDesistenciaVistoria())) {
				ObjectError objErro = new ObjectError("txTerminalMar", "Desistência de Vistoria ? Campo Obrigatório!");
				error.add(objErro);
				return error;
			}

			// Verificar Lote no Microled
			MicroledService microledService = new MicroledService();
			VwTabConsultaCargaItemObj vwConsulta = microledService
					.consultaCargaItem(GeneralParser.parseInt(tabPropostaObj.getTxLote()));

			if (Validator.isBlankOrNull(vwConsulta.getTxLote())) {
				ObjectError objErro = new ObjectError("txTerminalMar", "Numero de Lote Inválido!");
				error.add(objErro);
				return error;
			} else {

				if (vwConsulta.getVlPesoApurado() == 0) {
					ObjectError objErro = new ObjectError("txTerminalMar", "Peso Apurado Zerado!");
					error.add(objErro);
					return error;
				}

				Tab.setVlPesoBruto(vwConsulta.getVlPesoBruto());
				Tab.setVlPesoExtratoDesova(vwConsulta.getVlPesoApurado());
				Tab.setVlQtdeVolume(vwConsulta.getVlQtdeVolume());
				// Tab.setVlQtdeAvaria(vwConsulta.getVlQtdeAvaria());
				Tab.setTxVolume(vwConsulta.getTxVolume());
				Tab.setTxMicroledMercadoria(vwConsulta.getTxMicroledMercadoria());
				Tab.setTxContainer(vwConsulta.getTxContainer());
				Tab.setDtInicioDesova(vwConsulta.getDtInicioDesova());
				Tab.setDtFimDesova(vwConsulta.getDtFimDesova());
				Tab.setDtSaida(vwConsulta.getDtSaida());
				Tab.setCdTermoAvaria(vwConsulta.getCdTermoAvaria());
				Tab.setDtTermoAvaria(vwConsulta.getDtTermoAvaria());
				Tab.setVlDiferencaPeso(vwConsulta.getVlDiferencaPeso());
				Tab.setVlPesoAvaria(vwConsulta.getVlPesoAvaria());
				Tab.setDtEntradaCd(vwConsulta.getDtEntrada());
				// Tab.setVlDiferencaPercentual(vwConsulta.getVlDiferencaPercentual());
				Tab.setVlDiferencaPercentual(GeneralParser.porcentagemEntreDoisValores(vwConsulta.getVlPesoApurado(),
						vwConsulta.getVlPesoBruto()));
				Tab.setDtDesconsolidacao(vwConsulta.getDtDesconsolidacao());

				// Incluir a avaria
				incluirAvarias(tabPropostaObj.getCdProposta(), tabPropostaObj.getTxLote());

			}

			Tab.setCkDesistenciaVistoria(tabPropostaObj.getCkDesistenciaVistoria());
			/*
			 * if (!Validator.isBlankOrNull(vwConsulta.getDtInicioDesova())) {
			 * Tab.setTxTerminalMar("Terminal"); }else { Tab.setTxTerminalMar("Mar"); }
			 */

		} else if ((tabPropostaObj.getTabTipoCarregamentoObj().getCdTipoCarregamento() == 2
				|| tabPropostaObj.getTabTipoCarregamentoObj().getCdTipoCarregamento() == 4)) { // Exportação
			Tab.setCkDesistenciaVistoria(1);
			Tab.setTxTerminalMar("Terminal");
		} else {
			Tab.setCkDesistenciaVistoria(tabPropostaObj.getCkDesistenciaVistoria());
			Tab.setTxTerminalMar(tabPropostaObj.getTxTerminalMar());
		}

		Tab.setTxReserva(tabPropostaObj.getTxReserva());
		Tab.setTxDue(tabPropostaObj.getTxDue());
		Tab.setTxNfs(tabPropostaObj.getTxNfs());
		Tab.setTxLote(tabPropostaObj.getTxLote());
		Tab.setTxBl(tabPropostaObj.getTxBl());
		tabService.gravar(Tab, request.getParameter("submitFields"));

		List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();
		TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
		tSuccess.setCd_mensagem(1);
		tSuccess.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
		success.add(tSuccess);

		return success;
	}

	private void enviaemailinformativo(TabPropostaObj tab) {

	}

	@RequestMapping(value = "/consultardocumento/{cdPropostaDocumento}/{cdProposta}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable Integer cdPropostaDocumento, @PathVariable Integer cdProposta,
			HttpServletRequest request) {

		TabPropostaObj TabView = tabService.consultar(cdProposta);

		// List<TabPropostaDocumentosObj> listDocumentos =
		// tabService.listaDocumentos(cdProposta);

		TabPropostaDocumentosObj tabDocumentos = tabService.consultarDocumento(cdPropostaDocumento);

		ModelAndView mv = new ModelAndView(txUrlTela);
		listCamposIdioma(mv);
		if (TabView != null) {
			adicionaMV(mv, TabView);
			mv.addObject("tabPropostaObj", TabView);
			mv.addObject("tabPropostaDocumentosObj", tabDocumentos);
			mv.addObject("listaDocumentos", TabView.getListPropostaDocumentos());
			mv.addObject("cdProposta", TabView.getCdProposta());
			mv.addObject("txEdit", "propostadocumentos");
			mv.addObject("listaAvarias", resultListAvarias(TabView.getCdProposta()));
		} else {
			mv.addObject(new TabPropostaObj());
			mv.addObject("listaDocumentos", TabView.getListPropostaDocumentos());
			mv.addObject(new TabPropostaDocumentosObj());
			mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
			mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}

	@RequestMapping(value = "/gravarmodaldocumentos", headers = ("content-type=multipart/*"), method = RequestMethod.POST)
	public @ResponseBody List<?> gravarmodaldocumentos(@RequestParam("file") MultipartFile[] inputFiles,
			@Validated TabPropostaDocumentosObj tabPropostaDocumentosObj, Errors erros, HttpServletRequest request) {

		ModelAndView mv = new ModelAndView(txUrlTela);
		listCamposIdioma(mv);

		List<ObjectError> error = new ArrayList<ObjectError>();
		if (erros.hasErrors()) {
			// mv.addObject("listaErros", erros.getAllErrors());
			error.addAll(erros.getAllErrors());
			return error;
		} /*
			 * else { error = regrasBean.verificaregras(getClass().getSimpleName(),request);
			 * if (!error.isEmpty()) { return error; } }
			 */

		DadosUser user = tabUsuariosService.DadosUsuario();

		// FileInfo fileInfo = new FileInfo();
		String originalFilename = "";

		Integer cdProposta = GeneralParser.parseInt(request.getParameter("cdProposta"));

		if (tabPropostaDocumentosObj.getCdProposta() != null && tabPropostaDocumentosObj.getCdValidado() == null) {
			TabPropostaDocumentosObj tabP = tabPropostaDocumentosRepository
					.findByCdPropostaAndTabDocumentosClassificacaoObjCdDoctoClassificacao(cdProposta,
							tabPropostaDocumentosObj.getTabDocumentosClassificacaoObj().getCdDoctoClassificacao());

			if (tabP != null) {
				ObjectError obj = new ObjectError("cdClassificacao", "Um Documento já existe com essa classificação!");
				error.add(obj);
				return error;
			}
		}

		boolean ckUpload = false;
		for (MultipartFile inputFile : inputFiles) {

			if (!inputFile.isEmpty()) {
				try {

					if (!Validator
							.validarStringCaracterEspecial(inputFile.getOriginalFilename().replaceAll(" ", "_"))) {
						ObjectError obj = new ObjectError("cdClassificacao",
								"Nome inválido por conter caracteres especiais! [!#@$%¨&*].*;|áéíóúçãõ~");
						error.add(obj);
						return error;
					}

					ckUpload = true;
					// CalcRandom c = new CalcRandom();
					// String txRef= c.getloginAleatoria(10);

					originalFilename = inputFile.getOriginalFilename().replaceAll(" ", "_");

					String pathDir = Constants.PATH_UPLOAD + "propostas/" + cdProposta + "/";

					File destinationFile = new File(pathDir);
					if (!destinationFile.exists()) {
						destinationFile.mkdirs();
					}

					File arquivo = new File(pathDir + originalFilename);
					if (arquivo.exists()) {

						GeneralUtil g = new GeneralUtil();
						g.DeletarArquivo(pathDir + originalFilename);
					}

					if (inputFile.getBytes() != null) {
						tabPropostaDocumentosObj.setDtCriacao(new Date());
						TabUsuarioObj tabUsu = new TabUsuarioObj();
						tabUsu.setCdUsuario(user.getCdUsuario());
						tabPropostaDocumentosObj.setTabUsuarioCriacaoObj(tabUsu);
						tabPropostaDocumentosObj.setTxNomeArquivo(originalFilename);
						// tabDoctosAnexosObj.setTxArquivo(encodedString);
						tabPropostaDocumentosObj
								.setTxUrl("/proposta/download/" + cdProposta + "?txNomeArquivo=" + originalFilename);
						tabPropostaDocumentosObj = tabService.gravarDocumento(tabPropostaDocumentosObj);

						FileOutputStream outStream = null; // new FileOutputStream(pathDir + originalFilename);
						byte[] bufferedbytes = new byte[1024];

						BufferedInputStream fileInputStream = new BufferedInputStream(inputFile.getInputStream());
						outStream = new FileOutputStream(arquivo);
						int count = 0;
						while ((count = fileInputStream.read(bufferedbytes)) != -1) {
							outStream.write(bufferedbytes, 0, count);
						}
						outStream.close();
						// fos.write(inputFile.getBytes());
						// fos.close();

					} else {
						// Erro
					}

				} catch (Exception e) {
					e.printStackTrace();
					// Erro
				}

				Integer ckUploadDoctosAceitos = ckTodosDoctosAceitos(cdProposta);
				verificaUpload(cdProposta, request.getParameter("submitFields"), ckUploadDoctosAceitos);

			} else {

				verificaUpload(cdProposta, request.getParameter("submitFields"), 0);

				if (user.getCdGrupoAcesso() < 3) { // Amalog

					tabPropostaDocumentosObj.setDtValidacao(new Date());
					TabUsuarioObj tabUsu = new TabUsuarioObj();
					tabUsu.setCdUsuario(user.getCdUsuario());
					tabPropostaDocumentosObj.setTabUsuarioValidacaoObj(tabUsu);

					tabPropostaDocumentosObj = tabService.gravarDocumento(tabPropostaDocumentosObj);

					TabPropostaObj tabProposta = tabService.consultar(cdProposta);
					if (tabProposta.getCkUploadDoctos() == 1) {
						Integer ckUploadDoctosAceitos = ckTodosDoctosAceitos(cdProposta);

						if (ckUploadDoctosAceitos == 1) {
							tabProposta.setDtLiberacaoDocumentos(new Date());
							TabStatusObj tabStatusObj = new TabStatusObj();
							tabStatusObj.setCdStatus(4);
							tabProposta.setTabStatusObj(tabStatusObj);
							tabProposta.setTxCheckUploadDoctos(
									"Documentos analisados com sucesso! Proposta liberada para geração da DTA conforme prazo de Deadline.");
							tabService.gravar(tabProposta, request.getParameter("submitFields"));
						} else if (ckUploadDoctosAceitos == 2) {
							tabProposta.setDtLiberacaoDocumentos(null);
							TabStatusObj tabStatusObj = new TabStatusObj();
							tabStatusObj.setCdStatus(3);
							tabProposta.setTabStatusObj(tabStatusObj);
							tabProposta.setTxCheckUploadDoctos("Existem documentos com pendências!");
							tabService.gravar(tabProposta, request.getParameter("submitFields"));
						} else {

							if (tabProposta.getTxTerminalMar().equals("Terminal")
									&& tabProposta.getCkDesistenciaVistoria() == 1) {
								tabProposta.setDtLiberacaoDocumentos(null);
								TabStatusObj tabStatusObj = new TabStatusObj();
								tabStatusObj.setCdStatus(3);
								tabProposta.setTabStatusObj(tabStatusObj);
								tabProposta.setTxCheckUploadDoctos("Documentos em analise!");
								tabService.gravar(tabProposta, request.getParameter("submitFields"));
							} else {
								if (!Validator.isBlankOrNull(tabProposta.getTxReserva())) {
									tabProposta.setDtLiberacaoDocumentos(null);
									TabStatusObj tabStatusObj = new TabStatusObj();
									tabStatusObj.setCdStatus(3);
									tabProposta.setTabStatusObj(tabStatusObj);
									tabProposta.setCkDesistenciaVistoria(1);
									tabProposta.setTxCheckUploadDoctos("Documentos em analise!");
									tabService.gravar(tabProposta, request.getParameter("submitFields"));
								} else {
									tabProposta.setDtLiberacaoDocumentos(null);
									TabStatusObj tabStatusObj = new TabStatusObj();
									tabStatusObj.setCdStatus(3);
									tabProposta.setTabStatusObj(tabStatusObj);
									tabProposta.setTxCheckUploadDoctos(
											"Documentos entrarão em analise somente após estarem no Terminal!");
									tabService.gravar(tabProposta, request.getParameter("submitFields"));
								}
							}
						}
					}
				} else {
					tabPropostaDocumentosObj.setDtCriacao(new Date());
					TabUsuarioObj tabUsu = new TabUsuarioObj();
					tabUsu.setCdUsuario(user.getCdUsuario());
					tabPropostaDocumentosObj.setTabUsuarioCriacaoObj(tabUsu);
					tabPropostaDocumentosObj = tabService.gravarDocumento(tabPropostaDocumentosObj);

				}

			}
		}

		List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();
		TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
		tSuccess.setCd_mensagem(1);
		tSuccess.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
		success.add(tSuccess);

		return success;
	}

	private void verificaUpload(Integer cdProposta, String submitFields, Integer ckValidado) {

		TabPropostaObj tabProposta = tabService.consultar(cdProposta);
		if (tabProposta != null) {
			String listDocs = ckTodosDoctosUpload(cdProposta,
					tabProposta.getTabTipoCarregamentoObj().getCdTipoCarregamento());
			if (Validator.isBlankOrNull(listDocs)) {

				if (ckValidado == 2) {
					tabProposta.setCkReconferencia(1);
				} else {
					tabProposta.setCkReconferencia(0);
				}

				tabProposta.setCkUploadDoctos(1);
				tabProposta.setTxCheckUploadDoctos(
						"Upload	de todos os Documentos feitos com sucesso! Aguardando análise de Documentos - Prazo de até 24");

			} else {
				tabProposta.setCkUploadDoctos(0);
				tabProposta.setTxCheckUploadDoctos("Documentos faltantes: " + listDocs);
			}
			tabService.gravar(tabProposta, submitFields);
		}
	}

	@RequestMapping(value = "/excluirdocumento/{cdPropostaDocumento}", method = RequestMethod.GET)
	public @ResponseBody String excluirdocumento(@PathVariable Integer cdPropostaDocumento) {

		TabPropostaDocumentosObj TabView = tabService.consultarDocumento(cdPropostaDocumento);

		String pathDir = Constants.PATH_UPLOAD + "propostas/" + TabView.getCdProposta() + "/";

		String txNomeArquivo = TabView.getTxNomeArquivo();

		File arquivo = new File(pathDir + txNomeArquivo);
		if (arquivo.exists()) {

			GeneralUtil g = new GeneralUtil();
			g.DeletarArquivo(pathDir + txNomeArquivo);
		}

		tabService.excluirDocumento(cdPropostaDocumento);

		return "OK";
	}

	@RequestMapping(value = "/download/{cdProposta}")
	public @ResponseBody HttpEntity<InputStreamResource> getFile(@PathVariable Integer cdProposta,
			@RequestParam String txNomeArquivo) {

		String pathDir = Constants.PATH_UPLOAD + "propostas/" + cdProposta + "/";
		MediaType m = null;
		File destinationFile = new File(pathDir + txNomeArquivo);
		InputStreamResource resource = null;
		try {

			resource = new InputStreamResource(new FileInputStream(destinationFile));

			if (txNomeArquivo.toLowerCase().contains(".jpg")) {
				m = MediaType.IMAGE_JPEG;
			} else if (txNomeArquivo.toLowerCase().contains(".gif")) {
				m = MediaType.IMAGE_GIF;
			} else if (txNomeArquivo.toLowerCase().contains(".png")) {
				m = MediaType.IMAGE_PNG;
			} else if (txNomeArquivo.toLowerCase().contains(".pdf")) {
				m = MediaType.APPLICATION_PDF;
			} else {
				m = MediaType.APPLICATION_OCTET_STREAM;
			}

		} catch (Exception ex) {
			System.out.println("Erro Download Arquivo: " + pathDir + txNomeArquivo);
		}

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + txNomeArquivo)
				.contentType(m).contentLength(destinationFile.length()).body(resource);
	}

	private String ckTodosDoctosUpload(Integer cdProposta, Integer cdTipoDta) {

		String listDocs = "";
		List<VwTabDocumentosClassificacaoObj> listDoctos = tabDocumentosClassificacaoService.listar(cdTipoDta, 1);

		for (VwTabDocumentosClassificacaoObj atual : listDoctos) {

			TabPropostaDocumentosObj tabP = tabPropostaDocumentosRepository
					.findByCdPropostaAndTabDocumentosClassificacaoObjCdDoctoClassificacao(cdProposta,
							atual.getCdDoctoClassificacao());

			if (tabP == null) {
				listDocs += atual.getTxDoctoClassificacao() + "; ";
			}
		}

		// Valida se está no terminal
		/*
		 * TabPropostaObj tabProposta = tabService.consultarSimple(cdProposta);
		 * 
		 * 
		 * if (!Validator.isBlankOrNull(tabProposta.getTxTerminalMar())) { if
		 * (tabProposta.getTxTerminalMar().equals("Mar")) { listDocs = "N"; } }
		 */

		return listDocs;
	}

	private List<TabPropostaAvariasObj> resultListAvarias(Integer cdProposta) {

		List<TabPropostaAvariasObj> lstAvarias = tabPropostaAvariasService.listar(cdProposta);

		return lstAvarias;

	}

	private Integer ckTodosDoctosAceitos(Integer cdProposta) {

		Integer ckOK = 1;

		List<TabPropostaDocumentosObj> listDoctos = tabService.listaDocumentos(cdProposta);

		for (TabPropostaDocumentosObj atual : listDoctos) {

			if (atual.getCdValidado() != null) {
				if (atual.getCdValidado() == 2) {
					ckOK = 2;
				}
			} else {
				if (ckOK < 2) {
					ckOK = 0;
				}
			}
		}

		return ckOK;
	}

	private void incluirAvarias(Integer cdProposta, String txLote) {

		transactionUtilBean.queryInsertUpdate("delete from TabPropostaAvariasObj t where t.cdProposta = " + cdProposta);

		MicroledService microledService = new MicroledService();
		List<VwTabAvariaCsObj> lst = microledService.consultaAvarias(GeneralParser.parseInt(txLote));

		for (VwTabAvariaCsObj atual : lst) {

			TabPropostaAvariasObj Tab = new TabPropostaAvariasObj();
			Tab.setCdItem(atual.getCdItem());
			Tab.setCdProposta(cdProposta);
			Tab.setDtTermo(atual.getDtTermoAvaria());
			Tab.setTxAvaria(atual.getTxAvaria());
			Tab.setTxEmbalagem(atual.getTxEmbalagem());
			Tab.setTxTermo(atual.getTxTermoAvaria());
			Tab.setTxLote(String.valueOf(atual.getTxLote()));
			tabPropostaAvariasService.gravar(Tab);

		}

	}

	// Modelo

	@Autowired
	private TabTipoCarregamentoRepository tabTipoCarregamentoRepository;

	@ModelAttribute("selectcdtipocarregamento")
	public List<TabTipoCarregamentoObj> selectcdtipocarregamento() {
		return tabTipoCarregamentoRepository.findAll();
	}

	@Autowired
	private TabDestinoImpexpRepository tabDestinoImpexpRepository;

	public List<TabDestinoImpexpObj> selectcdtipodestino(Integer cdTipoCarregamento) {

		if (cdTipoCarregamento != null) {
			if (cdTipoCarregamento == 1 || cdTipoCarregamento == 3) {
				return tabDestinoImpexpRepository.findByCkAtivoQuery(1, 1);
			} else {
				return tabDestinoImpexpRepository.findByCkAtivoQuery(2, 1);
			}
		}

		return null;
	}

	@Autowired
	private TabOrigemImpexpRepository tabOrigemImpexpRepository;

	public List<TabOrigemImpexpObj> selectcdtipoorigem(Integer cdTipoCarregamento) {

		if (cdTipoCarregamento != null) {
			if (cdTipoCarregamento == 1 || cdTipoCarregamento == 3) {
				return tabOrigemImpexpRepository.findByCkAtivoQuery(1, 1);
			} else {
				return tabOrigemImpexpRepository.findByCkAtivoQuery(2, 1);
			}
		}

		return null;
	}

	@Autowired
	private TabPerfilVeiculoRepository tabPerfilVeiculoRepository;

	@ModelAttribute("selectcdperfilveiculo")
	public List<TabPerfilVeiculoObj> selectcdperfilveiculo() {
		return tabPerfilVeiculoRepository.findAll();
	}

	@Autowired
	private TabStatusRepository tabStatusRepository;

	@ModelAttribute("selectcdstatus")
	public List<TabStatusObj> selectcdstatus() {
		return tabStatusRepository.findByTxTipoStatusQuery("DTA");
	}

	// @ModelAttribute("selectcdcliente")
	public List<?> selectcdcliente(TabPropostaObj tabPropostaObj) {

		DadosUser user = tabUsuariosService.DadosUsuario();
		if (tabPropostaObj != null) {
			if (tabPropostaObj.getTabClienteObj() != null) {
				if (user.getCdGrupoVisao() == 8) {
					return vwTabClienteGrupoAcessoRepository.findByCdGrupoAcessoOrderByTxClienteAsc(1);
				} else {
					if (tabPropostaObj.getTabClienteObj().getCdCliente() == null) {
						return vwTabClienteGrupoAcessoRepository
								.findByCdGrupoAcessoOrderByTxClienteAsc(user.getCdGrupoAcesso());
					} else {
						if (user.getCdGrupoAcesso() == 1 || user.getCdGrupoAcesso() == 2) {
							return vwTabClienteGrupoAcessoRepository
									.findByCdGrupoAcessoOrderByTxClienteAsc(user.getCdGrupoAcesso());
						} else {
							return tabClienteRepository
									.findByCdCliente(tabPropostaObj.getTabClienteObj().getCdCliente());
						}
					}
				}
			} else {
				return vwTabClienteGrupoAcessoRepository
						.findByCdGrupoAcessoOrderByTxClienteAsc(user.getCdGrupoAcesso());
			}
		} else {
			return vwTabClienteGrupoAcessoRepository.findByCdGrupoAcessoOrderByTxClienteAsc(user.getCdGrupoAcesso());
		}

	}

	public List<?> selectcdfreehand(TabPropostaObj tabPropostaObj) {

		DadosUser user = tabUsuariosService.DadosUsuario();
		if (tabPropostaObj != null) {

			if (tabPropostaObj.getTabFreehandObj() == null) {
				return vwTabClienteGrupoAcessoRepository
						.findByCdGrupoAcessoOrderByTxClienteAsc(user.getCdGrupoAcesso());
			} else {
				if (user.getCdGrupoAcesso() == 1 || user.getCdGrupoAcesso() == 2) {
					return vwTabClienteGrupoAcessoRepository
							.findByCdGrupoAcessoOrderByTxClienteAsc(user.getCdGrupoAcesso());
				} else {
					return tabClienteRepository.findByCdCliente(tabPropostaObj.getTabFreehandObj().getCdCliente());
				}
			}
		} else {
			return vwTabClienteGrupoAcessoRepository.findByCdGrupoAcessoOrderByTxClienteAsc(user.getCdGrupoAcesso());
		}

	}

	// @ModelAttribute("selectcddoctoclassificacao")
	public List<VwTabDocumentosClassificacaoObj> selectcdDoctoClassificacao(Integer cdTipoDta) {
		return tabDocumentosClassificacaoService.listar(cdTipoDta);
	}

	@Autowired
	private TabClienteService tabClienteService;

	@ModelAttribute("selectcdcoloaderdespachante")
	public List<TabClienteObj> selectcdcoloaderdespachante() {

		return tabClienteService.listar();
	}

}
