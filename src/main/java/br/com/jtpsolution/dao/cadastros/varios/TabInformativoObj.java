package br.com.jtpsolution.dao.cadastros.varios;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.springframework.format.annotation.NumberFormat;

import br.com.jtpsolution.Constants;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_informativo", schema = Constants.SCHEMA)
public class TabInformativoObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_informativo")
	// @NotNull(message = "Informativo campo obrigatório!")
	private Integer cdInformativo;

	@Column(name = "cd_cliente")
	// @NotNull(message = "Cliente campo obrigatório!")
	private Integer cdCliente;

	@Column(name = "vl_informativo")
	// @NotEmpty(message = "Informativo campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlInformativo;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
	}

}
