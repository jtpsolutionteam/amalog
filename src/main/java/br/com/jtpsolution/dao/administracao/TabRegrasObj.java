package br.com.jtpsolution.dao.administracao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.jtpsolution.Constants;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_regras", schema = Constants.SCHEMA)
public class TabRegrasObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_regra")
//@NotNull(message = "Regra campo obrigatório!")
	private Integer cdRegra;

	@Column(name = "cd_tipo")
	@NotNull(message = "Tipo campo obrigatório!")
	private Integer cdTipo;

	@Column(name = "tx_campo")
	@NotEmpty(message = "Campo campo obrigatório!")
	@Size(max = 50, message = "Campo tamanho máximo de 50 caracteres")
	private String txCampo;

	@Column(name = "tx_objcampo")
	@NotEmpty(message = "Objcampo campo obrigatório!")
	@Size(max = 50, message = "Objcampo tamanho máximo de 50 caracteres")
	private String txObjcampo;

	@Column(name = "cd_tela")
	@NotNull(message = "Tela campo obrigatório!")
	private Integer cdTela;

	@Column(name = "cd_grupo_visao")
	@NotNull(message = "GrupoVisao campo obrigatório!")
	private Integer cdGrupoVisao;

	@Column(name = "tx_campo_cond")
//@NotEmpty(message = "CampoCond campo obrigatório!")
	@Size(max = 80, message = "CampoCond tamanho máximo de 80 caracteres")
	private String txCampoCond;

	@Column(name = "tx_objcampo_cond")
//@NotEmpty(message = "ObjcampoCond campo obrigatório!")
	@Size(max = 30, message = "ObjcampoCond tamanho máximo de 30 caracteres")
	private String txObjcampoCond;

	@Column(name = "tx_mensagem")
//@NotEmpty(message = "Mensagem campo obrigatório!")
	@Size(max = 1000, message = "Mensagem tamanho máximo de 1000 caracteres")
	private String txMensagem;

	@Column(name = "tx_aux")
//@NotEmpty(message = "Aux campo obrigatório!")
	@Size(max = 200, message = "Aux tamanho máximo de 200 caracteres")
	private String txAux;

	@Column(name = "tx_email")
//@NotEmpty(message = "Email campo obrigatório!")
	@Size(max = 2000, message = "Email tamanho máximo de 2000 caracteres")
	private String txEmail;

	@Column(name = "cd_ordem")
	@NotNull(message = "Ordem campo obrigatório!")
	private Integer cdOrdem;

	@Column(name = "ck_ignora")
	@NotNull(message = "ckIgnora campo obrigatório!")
	private Integer ckIgnora;

	@Column(name = "tx_ignora")
//@NotEmpty(message = "Ignora campo obrigatório!")
	@Size(max = 50, message = "Ignora tamanho máximo de 50 caracteres")
	private String txIgnora;

	@Column(name = "tx_campo_chave")
	private String txCampoChave;
	
	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {

	}



}
