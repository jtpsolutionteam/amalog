package br.com.jtpsolution.modulos.cadastros.logsinterfaces.controller;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.propostas.TabPropostaMantranObj;
import br.com.jtpsolution.modulos.cadastros.logsinterfaces.bean.TabFiltroPropostaMantranObj;
import br.com.jtpsolution.modulos.cadastros.logsinterfaces.service.TabPropostaMantranService;
import br.com.jtpsolution.modulos.util.geralcontroller.allController;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.regras.RegrasBean;

@Controller
@RequestMapping("/logdtamantran")
public class TabPropostaMantranController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabPropostaMantranService tabService;
	
	@Autowired
	private RegrasBean regrasBean;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_CADASTROS+"/logsinterfaces/logdtamantra";
	
	
 	
 	@GetMapping
	public ModelAndView pesquisa(TabFiltroPropostaMantranObj tabFiltroPropostaMantranObj, HttpServletRequest request) {
		
		tabFiltroPropostaMantranObj = verificaFiltro(tabFiltroPropostaMantranObj, request);
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject(new TabPropostaMantranObj());			
		mv.addObject("tabFiltroPropostaMantranObj", tabFiltroPropostaMantranObj);
		mv.addObject("listadados", retornoFiltro(tabFiltroPropostaMantranObj));
		
		
		return mv;
	}
	
	private List<?> retornoFiltro(TabFiltroPropostaMantranObj tabFiltroPropostaMantranObj) {
 		
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(TabPropostaMantranObj.class);
		
		adicionarFiltro(tabFiltroPropostaMantranObj, criteria);
 		
 		return criteria.list();
 	}


	private TabFiltroPropostaMantranObj verificaFiltro(TabFiltroPropostaMantranObj tabFiltroPropostaMantranObj, HttpServletRequest request) {
		
		try {
			
			if (Validator.isObjetoNull(tabFiltroPropostaMantranObj)) {
				if (request.getSession().getAttribute(getClass().getSimpleName()) != null) {					
					String jsonFiltro = request.getSession().getAttribute(getClass().getSimpleName()).toString();
					ObjectMapper mapper = new ObjectMapper();
					tabFiltroPropostaMantranObj = mapper.readValue(jsonFiltro, TabFiltroPropostaMantranObj.class);	
				}
			}else {
				request.getSession().setAttribute(getClass().getSimpleName(),
						GeneralParser.convertObjJson(tabFiltroPropostaMantranObj));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return tabFiltroPropostaMantranObj;
	}

	
	private void adicionarFiltro(TabFiltroPropostaMantranObj filtro, Criteria criteria) {
		
		  boolean ckFiltro = false;
		  
		  if (filtro != null) {
			  
			  if (!StringUtils.isEmpty(filtro.getCdPropostaFiltro())) {
				  criteria.add(Restrictions.eq("cdProposta", filtro.getCdPropostaFiltro()));
				  ckFiltro = true;
			  }
			  
			  if (!ckFiltro) {
				  criteria.add(Restrictions.eq("cdProposta", 0));
			  }
			  
			  criteria.addOrder(Order.desc("dtCriacao"));
		  }
	  }
	

	@RequestMapping(value = "/consultar/{CdCodigo}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable Integer CdCodigo, HttpServletRequest request) {
	
	   TabPropostaMantranObj TabView = tabService.consultar(CdCodigo);
	   TabFiltroPropostaMantranObj tF = verificaFiltro(new TabFiltroPropostaMantranObj(), request);

	   
		ModelAndView mv = new ModelAndView(txUrlTela);	
		if (TabView != null) {
		  mv.addObject("tabFiltroPropostaMantranObj",tF);	
		  mv.addObject("tabPropostaMantranObj",TabView);
		  mv.addObject("listadados", retornoFiltro(verificaFiltro(tF, request)));
		  mv.addObject("txEdit", "edit");
		}else {
		  mv.addObject("tabFiltroPropostaMantranObj",tF);	
		  mv.addObject(new TabPropostaMantranObj());	
		  mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
		  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}
	
	
	
}
