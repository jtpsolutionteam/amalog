package br.com.jtpsolution.dao.propostas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.propostas.TabPropostaDocumentosObj;



public interface TabPropostaDocumentosRepository extends JpaRepository<TabPropostaDocumentosObj, Integer> {

	List<TabPropostaDocumentosObj> findByCdProposta(Integer cdProposta);

	TabPropostaDocumentosObj findByCdPropostaAndTabDocumentosClassificacaoObjCdDoctoClassificacao(Integer cdProposta, Integer cdClassificacao);
}