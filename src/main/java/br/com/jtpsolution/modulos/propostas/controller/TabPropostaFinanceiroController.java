package br.com.jtpsolution.modulos.propostas.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.cadastros.varios.TabFinanceiroStatusObj;
import br.com.jtpsolution.dao.cadastros.varios.TabStatusObj;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabFinanceiroStatusRepository;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabStatusRepository;
import br.com.jtpsolution.dao.propostas.TabPropostaDocumentosObj;
import br.com.jtpsolution.dao.propostas.TabPropostaObj;
import br.com.jtpsolution.dao.propostas.VwTabPropostaDocumentosObj;
import br.com.jtpsolution.dao.propostas.VwTabPropostaObj;
import br.com.jtpsolution.mensagens.TabRetornoSucessoObj;
import br.com.jtpsolution.modulos.propostas.bean.TabFiltroPropostaObj;
import br.com.jtpsolution.modulos.propostas.service.TabPropostaService;
import br.com.jtpsolution.modulos.util.geralcontroller.allController;
import br.com.jtpsolution.security.DadosUser;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.regras.RegrasBean;

@Controller
@RequestMapping("/propostafinanceiro")
public class TabPropostaFinanceiroController extends allController {

	@Autowired
	private TabPropostaService tabService;

	@Autowired
	private RegrasBean regrasBean;

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private UsuarioBean tabUsuarioService;

	private String txUrlTela = Constants.TEMPLATE_PATH_PROPOSTA + "/propostafinanceiro";

	@GetMapping
	public ModelAndView pesquisa(TabFiltroPropostaObj tabFiltroPropostaObj, HttpServletRequest request) {

		tabFiltroPropostaObj = verificaFiltro(tabFiltroPropostaObj, request);

		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject(new TabPropostaObj());
		mv.addObject("tabFiltroPropostaObj", tabFiltroPropostaObj);
		mv.addObject("listadados", retornoFiltro(tabFiltroPropostaObj));

		return mv;
	}

	private List<?> retornoFiltro(TabFiltroPropostaObj tabFiltroPropostaObj) {

		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(VwTabPropostaObj.class);

		adicionarFiltro(tabFiltroPropostaObj, criteria);

		return criteria.list();
	}

	private TabFiltroPropostaObj verificaFiltro(TabFiltroPropostaObj tabFiltroPropostaObj, HttpServletRequest request) {

		try {

			if (Validator.isObjetoNull(tabFiltroPropostaObj)) {
				if (request.getSession().getAttribute(getClass().getSimpleName()) != null) {
					String jsonFiltro = request.getSession().getAttribute(getClass().getSimpleName()).toString();
					ObjectMapper mapper = new ObjectMapper();
					tabFiltroPropostaObj = mapper.readValue(jsonFiltro, TabFiltroPropostaObj.class);
				}
			} else {
				request.getSession().setAttribute(getClass().getSimpleName(),
						GeneralParser.convertObjJson(tabFiltroPropostaObj));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return tabFiltroPropostaObj;
	}

	private void adicionarFiltro(TabFiltroPropostaObj filtro, Criteria criteria) {

		boolean ckFiltro = false;

		DadosUser user = tabUsuarioService.DadosUsuario();

		criteria.add(Restrictions.eq("cdGrupoAcesso", user.getCdGrupoAcesso()));

		if (filtro != null) {

			if (!StringUtils.isEmpty(filtro.getTxPropostaFiltro())) {
				criteria.add(Restrictions.like("txProposta", "%" + filtro.getTxPropostaFiltro() + "%"));
				ckFiltro = true;
			}

			if (!StringUtils.isEmpty(filtro.getDtPropostaInicialFiltro())
					&& !StringUtils.isEmpty(filtro.getDtPropostaFinalFiltro())) {
				criteria.add(Restrictions.between("dtProposta", filtro.getDtPropostaInicialFiltro(),
						filtro.getDtPropostaFinalFiltro()));
				ckFiltro = true;
			}

			/*
			 * if (!ckFiltro) { criteria.add(Restrictions.eq("cdStatus", -1)); }
			 */
			if (!StringUtils.isEmpty(filtro.getCdStatusFiltro())) {
				criteria.add(Restrictions.eq("cdStatus", filtro.getCdStatusFiltro()));
				ckFiltro = true;
			}

			if (!StringUtils.isEmpty(filtro.getTxStatusCondicaoFiltro())) {

				if (filtro.getTxStatusCondicaoFiltro().equals("dtPagtoComissao")) {
					criteria.add(Restrictions.isNotNull("vlEmissaoDta"));
					criteria.add(Restrictions.isNotNull("dtBoletoPagto"));
				}

				if (filtro.getTxStatusCondicaoFiltro().equals("dtBoletoVencto")) {
					criteria.add(Restrictions.sqlRestriction("date(dt_boleto_vencto) < date(now())"));
					criteria.add(Restrictions.isNull("dtBoletoPagto"));
				}

				if (filtro.getTxStatusCondicaoFiltro().equals("dtBoletoPagto")) {
					criteria.add(Restrictions.sqlRestriction("date(dt_boleto_vencto) >= date(now())"));
					criteria.add(Restrictions.isNull("dtBoletoPagto"));
				}

				if (filtro.getTxStatusCondicaoFiltro().contains("ckdt")) {
					criteria.add(Restrictions.isNotNull(filtro.getTxStatusCondicaoFiltro().replace("ck", "")));
				} else {
					if (!filtro.getTxStatusCondicaoFiltro().equals("dtBoletoVencto")
							&& !filtro.getTxStatusCondicaoFiltro().equals("dtBoletoPagto")) {
						criteria.add(Restrictions.isNull(filtro.getTxStatusCondicaoFiltro()));
					}
				}

				criteria.add(Restrictions.isNotNull("txNumeroDta"));
				criteria.add(Restrictions.isNotNull("dtAceite"));
				ckFiltro = true;
			}

			if (!ckFiltro) {
				criteria.add(Restrictions.isNull("cdProposta"));
			}

			criteria.addOrder(Order.desc("dtProposta"));
		}
	}

	@RequestMapping(value = "/gravarmodal", method = RequestMethod.POST)
	public @ResponseBody List<?> gravarmodal(@Validated TabPropostaObj tabPropostaObj, Errors erros,
			HttpServletRequest request) {

		List<ObjectError> error = new ArrayList<ObjectError>();

		if (Validator.isBlankOrNull(tabPropostaObj.getTxImportador())
				|| Validator.isBlankOrNull(tabPropostaObj.getTxEndereco())
				|| Validator.isBlankOrNull(tabPropostaObj.getTxNumero())
				|| Validator.isBlankOrNull(tabPropostaObj.getTxBairro())
				|| Validator.isBlankOrNull(tabPropostaObj.getTxCidade())
				|| Validator.isBlankOrNull(tabPropostaObj.getTxUf())
				|| Validator.isBlankOrNull(tabPropostaObj.getTxCnpj())
				|| Validator.isBlankOrNull(tabPropostaObj.getTxCep())
				|| Validator.isBlankOrNull(tabPropostaObj.getTxEmailPagador())) {

			ObjectError objErro = new ObjectError("txImportador", "Todos os campos são obrigatórios!");
			error.add(objErro);
			return error;

		}

		if (!Validator.isValidEmail(tabPropostaObj.getTxEmailPagador())) {
			ObjectError objErro = new ObjectError("txImportador", "Email Pagador inválido!");
			error.add(objErro);
			return error;
		}

		TabPropostaObj Tab = tabService.consultarSimple(tabPropostaObj.getCdProposta());

		if (Tab != null) {
			Tab.setTxImportador(tabPropostaObj.getTxImportador());
			Tab.setTxEndereco(tabPropostaObj.getTxEndereco());
			Tab.setTxNumero(tabPropostaObj.getTxNumero());
			Tab.setTxBairro(tabPropostaObj.getTxBairro());
			Tab.setTxCidade(tabPropostaObj.getTxCidade());
			Tab.setTxUf(tabPropostaObj.getTxUf());
			Tab.setTxCnpj(tabPropostaObj.getTxCnpj());
			Tab.setTxCep(tabPropostaObj.getTxCep());
			Tab.setTxEmailPagador(tabPropostaObj.getTxEmailPagador());
			Tab.setDtBoletoVencto(null);
		}
		Tab = tabService.gravar(Tab, request.getParameter("submitFields"));

		List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();
		TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
		tSuccess.setCd_mensagem(1);
		tSuccess.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
		success.add(tSuccess);

		return success;
	}

	
	@RequestMapping(value = "/baixamanual", method = RequestMethod.POST)
	public @ResponseBody List<?> baixamanual(@Validated TabPropostaObj tabPropostaObj, Errors erros,
			HttpServletRequest request) {

		List<ObjectError> error = new ArrayList<ObjectError>();

		if (Validator.isBlankOrNull(tabPropostaObj.getTxImportador())
				|| Validator.isBlankOrNull(tabPropostaObj.getTxEndereco())
				|| Validator.isBlankOrNull(tabPropostaObj.getTxNumero())
				|| Validator.isBlankOrNull(tabPropostaObj.getTxBairro())
				|| Validator.isBlankOrNull(tabPropostaObj.getTxCidade())
				|| Validator.isBlankOrNull(tabPropostaObj.getTxUf())
				|| Validator.isBlankOrNull(tabPropostaObj.getTxCnpj())
				|| Validator.isBlankOrNull(tabPropostaObj.getTxCep())
				|| Validator.isBlankOrNull(tabPropostaObj.getTxEmailPagador())) {

			ObjectError objErro = new ObjectError("txImportador", "Todos os campos são obrigatórios!");
			error.add(objErro);
			return error;

		}

		if (!Validator.isValidEmail(tabPropostaObj.getTxEmailPagador())) {
			ObjectError objErro = new ObjectError("txImportador", "Email Pagador inválido!");
			error.add(objErro);
			return error;
		}

		TabPropostaObj Tab = tabService.consultarSimple(tabPropostaObj.getCdProposta());

		if (Tab != null) {
			Tab.setTxImportador(tabPropostaObj.getTxImportador());
			Tab.setTxEndereco(tabPropostaObj.getTxEndereco());
			Tab.setTxNumero(tabPropostaObj.getTxNumero());
			Tab.setTxBairro(tabPropostaObj.getTxBairro());
			Tab.setTxCidade(tabPropostaObj.getTxCidade());
			Tab.setTxUf(tabPropostaObj.getTxUf());
			Tab.setTxCnpj(tabPropostaObj.getTxCnpj());
			Tab.setTxCep(tabPropostaObj.getTxCep());
			Tab.setTxEmailPagador(tabPropostaObj.getTxEmailPagador());
			Tab.setDtBoletoVencto(new Date());
			Tab.setDtBoletoPagto(new Date());
		}
		Tab = tabService.gravar(Tab, request.getParameter("submitFields"));

		List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();
		TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
		tSuccess.setCd_mensagem(1);
		tSuccess.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
		success.add(tSuccess);

		return success;
	}

	
	
	@RequestMapping(value = "/consultar/{cdProposta}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable Integer cdProposta, HttpServletRequest request) {

		TabPropostaObj TabView = tabService.consultar(cdProposta);

		ModelAndView mv = new ModelAndView(txUrlTela);
		if (TabView != null) {
			mv.addObject("listadados", retornoFiltro(verificaFiltro(new TabFiltroPropostaObj(), request)));
			mv.addObject("tabPropostaObj", TabView);
			mv.addObject("tabFiltroPropostaObj", verificaFiltro(new TabFiltroPropostaObj(), request));
			mv.addObject("txEdit", "propostaaceite");
		}
		return mv;
	}

	@RequestMapping("/baixapagtocomissao/{cdProposta}/{txAcao}")
	public @ResponseBody String gravarcondicao(@PathVariable Integer cdProposta, @PathVariable String txAcao, HttpServletRequest request) {

		TabPropostaObj Tab = tabService.consultarSimple(cdProposta);

		if (Tab != null) {

			if (txAcao.equals("on")) {
				Tab.setDtPagtoComissao(new Date());
			} else {
				Tab.setDtPagtoComissao(null);
			}

			tabService.gravar(Tab, request.getParameter("submitFields"));

		}

		return "OK";
	}

	@Autowired
	private TabStatusRepository tabStatusRepository;

	@ModelAttribute("selectcdstatus")
	public List<TabStatusObj> selectcdstatus() {
		return tabStatusRepository.findAll(GeneralParser.sortByIdAsc("txStatus"));
	}

	@Autowired
	private TabFinanceiroStatusRepository tabFinanceiroStatusRepository;

	@ModelAttribute("selecttxstatuscondicao")
	public List<TabFinanceiroStatusObj> selecttxstatuscondicao() {
		return tabFinanceiroStatusRepository.findAll(GeneralParser.sortByIdAsc("txFinancStatus"));
	}

}
