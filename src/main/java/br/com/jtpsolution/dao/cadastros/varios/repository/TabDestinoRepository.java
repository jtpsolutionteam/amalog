package br.com.jtpsolution.dao.cadastros.varios.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.cadastros.varios.TabDestinoObj;



public interface TabDestinoRepository extends JpaRepository<TabDestinoObj, Integer> {

	@Query("select t from TabDestinoObj t where t.ckAtivo = ?1 order by t.txDestino")
	List<TabDestinoObj> findByCkAtivoQuery(Integer ckAtivo);

}