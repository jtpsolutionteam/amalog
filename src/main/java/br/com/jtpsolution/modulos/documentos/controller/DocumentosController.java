package br.com.jtpsolution.modulos.documentos.controller;

import java.io.File;
import java.io.FileInputStream;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.jtpsolution.Constants;

@Controller
@RequestMapping("/documentos")
public class DocumentosController {

	
	@RequestMapping(value = "/download/{cdProposta}")
	public @ResponseBody HttpEntity<InputStreamResource> getFile(@PathVariable Integer cdProposta,
			@RequestParam String txNomeArquivo) {

		String pathDir = Constants.PATH_UPLOAD + "propostas/" + cdProposta + "/";
		MediaType m = null;
		File destinationFile = new File(pathDir + txNomeArquivo);
		InputStreamResource resource = null;
		try {

			resource = new InputStreamResource(new FileInputStream(destinationFile));

			if (txNomeArquivo.toLowerCase().contains(".jpg")) {
				m = MediaType.IMAGE_JPEG;
			} else if (txNomeArquivo.toLowerCase().contains(".gif")) {
				m = MediaType.IMAGE_GIF;
			} else if (txNomeArquivo.toLowerCase().contains(".png")) {
				m = MediaType.IMAGE_PNG;
			} else if (txNomeArquivo.toLowerCase().contains(".pdf")) {
				m = MediaType.APPLICATION_PDF;
			} else {
				m = MediaType.APPLICATION_OCTET_STREAM;
			}

		} catch (Exception ex) {
			System.out.println("Erro Download Arquivo: " + pathDir + txNomeArquivo);
		}

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + txNomeArquivo)
				.contentType(m).contentLength(destinationFile.length()).body(resource);
	}
	
}
