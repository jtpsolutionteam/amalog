package br.com.jtpsolution.dao.cadastros.coloader.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.cadastros.coloader.TabColoaderDespachanteObj;



public interface TabColoaderDespachanteRepository extends JpaRepository<TabColoaderDespachanteObj, Integer> {

	TabColoaderDespachanteObj findByTxCnpj(String txCnpj);

}