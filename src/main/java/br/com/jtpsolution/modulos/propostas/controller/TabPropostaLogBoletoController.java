package br.com.jtpsolution.modulos.propostas.controller;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.propostas.TabPropostaLogBoletoObj;
import br.com.jtpsolution.mensagens.TabRetornoSucessoObj;
import br.com.jtpsolution.modulos.propostas.bean.TabFiltroPropostaLogBoletoObj;
import br.com.jtpsolution.modulos.propostas.service.TabPropostaLogBoletoService;
import br.com.jtpsolution.modulos.util.geralcontroller.allController;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.regras.RegrasBean;

@Controller
@RequestMapping("/propostalogboleto")
public class TabPropostaLogBoletoController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabPropostaLogBoletoService tabService;
	
	@Autowired
	private RegrasBean regrasBean;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_PROPOSTA+"/propostalogboleto";
 	
 	@GetMapping
	public ModelAndView pesquisa(TabFiltroPropostaLogBoletoObj tabFiltroPropostaLogBoletoObj, HttpServletRequest request) {
		
		tabFiltroPropostaLogBoletoObj = verificaFiltro(tabFiltroPropostaLogBoletoObj, request);
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject(new TabPropostaLogBoletoObj());			
		mv.addObject("tabFiltroPropostaLogBoletoObj", tabFiltroPropostaLogBoletoObj);
		mv.addObject("listadados", retornoFiltro(tabFiltroPropostaLogBoletoObj));
		
		
		return mv;
	}
	
	private List<?> retornoFiltro(TabFiltroPropostaLogBoletoObj tabFiltroPropostaLogBoletoObj) {
 		
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(TabPropostaLogBoletoObj.class);
		
		adicionarFiltro(tabFiltroPropostaLogBoletoObj, criteria);
 		
 		return criteria.list();
 	}


	private TabFiltroPropostaLogBoletoObj verificaFiltro(TabFiltroPropostaLogBoletoObj tabFiltroPropostaLogBoletoObj, HttpServletRequest request) {
		
		try {
			
			if (Validator.isObjetoNull(tabFiltroPropostaLogBoletoObj)) {
				if (request.getSession().getAttribute(getClass().getSimpleName()) != null) {					
					String jsonFiltro = request.getSession().getAttribute(getClass().getSimpleName()).toString();
					ObjectMapper mapper = new ObjectMapper();
					tabFiltroPropostaLogBoletoObj = mapper.readValue(jsonFiltro, TabFiltroPropostaLogBoletoObj.class);	
				}
			}else {
				request.getSession().setAttribute(getClass().getSimpleName(),
						GeneralParser.convertObjJson(tabFiltroPropostaLogBoletoObj));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return tabFiltroPropostaLogBoletoObj;
	}

	
	private void adicionarFiltro(TabFiltroPropostaLogBoletoObj filtro, Criteria criteria) {
		
		  boolean ckFiltro = false;
		  
		  if (filtro != null) {
			  
			  if (!StringUtils.isEmpty(filtro.getCdPropostaFiltro())) {
				  criteria.add(Restrictions.eq("cdProposta", filtro.getCdPropostaFiltro()));
				  ckFiltro = true;
			  }
			  
			  if (!ckFiltro) {
				  criteria.add(Restrictions.eq("cdProposta", -1));
			  }
			  
			  criteria.addOrder(Order.asc("dtVencto"));
		  }
	  }
 	
	
	@RequestMapping(value = "/gravarmodal", method = RequestMethod.POST)
	public @ResponseBody List<?> gravarmodal(@Validated TabPropostaLogBoletoObj tabPropostaLogBoletoObj, Errors erros, HttpServletRequest request) {

		List<ObjectError> error = new ArrayList<ObjectError>();
		if (erros.hasErrors()) {
		  //mv.addObject("listaErros", erros.getAllErrors());
		  error.addAll(erros.getAllErrors());
		  return error;
		}else {
			error = regrasBean.verificaregras(getClass().getSimpleName(),request);
			if (!error.isEmpty()) {
				  return error;	
			}	  
		}
		
	    TabPropostaLogBoletoObj Tab = tabService.gravar(tabPropostaLogBoletoObj);
	   
	    List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();		
	    TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
	    tSuccess.setCd_mensagem(1);
	    tSuccess.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
	    success.add(tSuccess);
	    
		return success;
	}
	
	
	@RequestMapping(value = "/novo")
	public ModelAndView novo(HttpServletRequest request) {
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		mv.addObject(new TabPropostaLogBoletoObj());
		mv.addObject("tabFiltroPropostaLogBoletoObj", verificaFiltro(new TabFiltroPropostaLogBoletoObj(), request));
		mv.addObject("listadados", retornoFiltro(verificaFiltro(new TabFiltroPropostaLogBoletoObj(), request)));
		mv.addObject("txEdit", "edit");
		return mv;
	}
	

	@RequestMapping(value = "/consultar/{CdPropostaLogBoleto}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable Integer CdPropostaLogBoleto, HttpServletRequest request) {
	
	   TabPropostaLogBoletoObj TabView = tabService.consultar(CdPropostaLogBoleto);
	   TabFiltroPropostaLogBoletoObj tF = verificaFiltro(new TabFiltroPropostaLogBoletoObj(), request);

	   
		ModelAndView mv = new ModelAndView(txUrlTela);	
		if (TabView != null) {
		  mv.addObject("tabFiltroPropostaLogBoletoObj",tF);	
		  mv.addObject("tabPropostaLogBoletoObj",TabView);
		  mv.addObject("listadados", retornoFiltro(verificaFiltro(tF, request)));
		  mv.addObject("txEdit", "edit");
		}else {
		  mv.addObject("tabFiltroPropostaLogBoletoObj",tF);	
		  mv.addObject(new TabPropostaLogBoletoObj());	
		  mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
		  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}
	
	

	
}
