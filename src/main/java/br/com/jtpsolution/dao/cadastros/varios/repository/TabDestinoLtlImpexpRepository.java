package br.com.jtpsolution.dao.cadastros.varios.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.cadastros.varios.TabDestinoLtlImpexpObj;



public interface TabDestinoLtlImpexpRepository extends JpaRepository<TabDestinoLtlImpexpObj, Integer> {


	@Query("select t from TabDestinoLtlImpexpObj t where t.cdImpexp = ?1 and t.tabDestinoLtlObj.ckAtivo = ?2 order by t.tabDestinoLtlObj.txDestinoLtl")
	List<TabDestinoLtlImpexpObj> findByCkAtivoQuery(Integer cdImpexp, Integer ckAtivo);
	
	
}