package br.com.jtpsolution.modulos.mantran.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.com.jtpsolution.dao.cadastros.cliente.TabClienteObj;
import br.com.jtpsolution.dao.conexaorest.restconnect;
import br.com.jtpsolution.dao.propostas.TabPropostaDocumentosObj;
import br.com.jtpsolution.dao.propostas.TabPropostaMantranObj;
import br.com.jtpsolution.dao.propostas.TabPropostaObj;
import br.com.jtpsolution.dao.propostas.VwTabPropostaObj;
import br.com.jtpsolution.dao.propostas.repository.TabPropostaMantranRepository;
import br.com.jtpsolution.dao.propostas.repository.TabPropostaRepository;
import br.com.jtpsolution.modulos.cadastros.cliente.service.TabClienteService;
import br.com.jtpsolution.modulos.mantran.obj.TabMantranCteObj;
import br.com.jtpsolution.modulos.mantran.obj.TabMantranDoctosObj;
import br.com.jtpsolution.modulos.mantran.obj.TabMantranDtaObj;
import br.com.jtpsolution.modulos.microled.obj.VwTabConsultaCargaItemObj;
import br.com.jtpsolution.modulos.microled.service.MicroledService;
import br.com.jtpsolution.modulos.propostas.service.TabPropostaService;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.GeneralUtil;
import br.com.jtpsolution.util.Validator;

@Service
public class InterfaceMantranService {

	@Autowired
	private TabPropostaService tabPropostaService;

	@Autowired
	private TabClienteService tabClienteService;

	@Autowired
	private TabPropostaMantranRepository tabPropostaMantranRepository;

	@Autowired
	private TabPropostaRepository tabPropostaRepository;

	@Scheduled(cron = "0 0/20 6-23 * * ?")
	public void enviaMantran() {

		try {
			List<TabPropostaObj> listProposta = tabPropostaRepository.findByAceiteLoteConclusaoTransitoNullQuery();
			//List<TabPropostaObj> listProposta = tabPropostaRepository.findByCdPropostaQuery(10431);
			
			for (TabPropostaObj atual : listProposta) {

				if (!Validator.isBlankOrNull(atual.getTxLote())) {

					System.out.println("Inicio envio Mantran: " + atual.getCdProposta());

					// Verificar no Microled
					if (atualizaLote(atual.getCdProposta())) {

						TabPropostaObj Tab = tabPropostaService.consultarSimple(atual.getCdProposta());

						String txEnvioMantran = InterfaceMantranDTA(atual.getCdProposta());
						System.out.println("json DTA Mantran: " + txEnvioMantran);
						if (txEnvioMantran.contains("ID")) {
							JSONObject json = new JSONObject(txEnvioMantran);

							if (!json.isNull("ID")) {

								Tab.setCdIdMantran(json.getInt("ID"));

							} else if (!json.isNull("MENSAGEM_ERRO")) {
								Tab.setTxErroMantran(json.getString("MENSAGEM_ERRO"));
							} else {
								Tab.setTxErroMantran("Erro resquest DTA Mantran");
							}

							tabPropostaService.gravar(Tab);

						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Erro Interface Mantran");
		}
	}

	public void enviaMantran(Integer cdProposta) {

		try {
			TabPropostaObj listProposta = tabPropostaRepository.findOne(cdProposta);

			

				if (!Validator.isBlankOrNull(listProposta.getTxLote())) {

					System.out.println("Inicio envio Mantran: " + listProposta.getCdProposta());

					// Verificar no Microled
					if (atualizaLote(listProposta.getCdProposta())) {

						TabPropostaObj Tab = tabPropostaService.consultarSimple(listProposta.getCdProposta());

						String txEnvioMantran = InterfaceMantranDTA(listProposta.getCdProposta());
						System.out.println("json DTA Mantran: " + txEnvioMantran);
						if (txEnvioMantran.contains("ID")) {
							JSONObject json = new JSONObject(txEnvioMantran);

							if (!json.isNull("ID")) {
								
								if (json.getInt("ID") > 0) {
								  Tab.setCdIdMantran(json.getInt("ID"));
								}

							} else if (!json.isNull("MENSAGEM_ERRO")) {
								Tab.setTxErroMantran(json.getString("MENSAGEM_ERRO"));
							} else {
								Tab.setTxErroMantran("Erro resquest DTA Mantran");
							}

							tabPropostaService.gravar(Tab);

						}
					}
				}
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Erro Interface Mantran");
		}
	}

	private boolean atualizaLote(Integer cdProposta) {

		try {
			TabPropostaObj Tab = tabPropostaService.consultarSimple(cdProposta);

			if (Tab != null) {

				MicroledService microledService = new MicroledService();
				VwTabConsultaCargaItemObj vwConsulta = microledService
						.consultaCargaItem(GeneralParser.parseInt(Tab.getTxLote()));

				if (!Validator.isBlankOrNull(vwConsulta.getTxLote())) {
					Tab.setVlPesoBruto(vwConsulta.getVlPesoBruto());
					Tab.setVlPesoExtratoDesova(vwConsulta.getVlPesoApurado());
					Tab.setVlQtdeVolume(vwConsulta.getVlQtdeVolume());
					// Tab.setVlQtdeAvaria(vwConsulta.getVlQtdeAvaria());
					Tab.setTxVolume(vwConsulta.getTxVolume());
					Tab.setTxMicroledMercadoria(vwConsulta.getTxMicroledMercadoria());
					Tab.setTxContainer(vwConsulta.getTxContainer());
					Tab.setDtInicioDesova(vwConsulta.getDtInicioDesova());
					Tab.setDtFimDesova(vwConsulta.getDtFimDesova());
					Tab.setDtSaida(vwConsulta.getDtSaida());
					Tab.setCdTermoAvaria(vwConsulta.getCdTermoAvaria());
					Tab.setDtTermoAvaria(vwConsulta.getDtTermoAvaria());
					Tab.setVlDiferencaPeso(vwConsulta.getVlDiferencaPeso());
					Tab.setVlPesoAvaria(vwConsulta.getVlPesoAvaria());
					Tab.setDtEntradaCd(vwConsulta.getDtEntrada());
					// Tab.setVlDiferencaPercentual(vwConsulta.getVlDiferencaPercentual());
					Tab.setVlDiferencaPercentual(GeneralParser.porcentagemEntreDoisValores(vwConsulta.getVlPesoApurado(),
							vwConsulta.getVlPesoBruto()));
					Tab.setDtDesconsolidacao(vwConsulta.getDtDesconsolidacao());
					Tab.setTxImportador(vwConsulta.getTxImportador());
					Tab.setTxCnpjImportador(vwConsulta.getTxCnpjImportador());
					Tab.setTxMapa(vwConsulta.getTxMapa());
					Tab.setTxIcmsSefaz(vwConsulta.getTxIcmsSefaz());
					Tab.setTxSiscarga(vwConsulta.getTxSiscarga());
					Tab.setTxBloqueioBl(vwConsulta.getTxBloqueioBl());
					Tab.setTxBloqueioCntr(vwConsulta.getTxBloqueioCNTR());
					Tab.setTxGrPaga(vwConsulta.getTxGrPaga());
	
					tabPropostaService.gravar(Tab);
					return true;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			return false;
		}

		return false;

	}

	public String InterfaceMantranDTA(Integer cdProposta) {

		String txRetorno = "";
		String txJson = "";

		VwTabPropostaObj tabProposta = null;
		try {

			tabProposta = tabPropostaService.consultarProposta(cdProposta);

			if (tabProposta != null) {

				TabMantranDtaObj tM1 = new TabMantranDtaObj();
				
				if (!Validator.isBlankOrNull(tabProposta.getCdIdMantran())) {
					if (tabProposta.getCdIdMantran() > 0) {
						tM1.setID(tabProposta.getCdIdMantran());
					}					
				}
				
				// tM1.setKey("jhg4j56hgK7JH9S0"); //Teste
				tM1.setKey("028kj86hgK7JH9S0");
				// tM1.setID("");
				tM1.setBL_Filhote(tabProposta.getTxBl());
				tM1.setLote_Filhote(tabProposta.getTxLote());
				tM1.setDT_Solicitacao_DTA(GeneralParser.format_datetimeBRMySql(tabProposta.getDtDtaSolicitacao()));
				tM1.setDestino_Carga(tabProposta.getTxDestino());
				
				if (tabProposta.getTxCnpjImportador() != null) {
				  if (tabProposta.getTxCnpjImportador().length() > 14) {
					tM1.setCD_Importador(GeneralUtil.TiraNaonumero(tabProposta.getTxCnpjImportador()).substring(0,14)); 
				  }else {
				    tM1.setCD_Importador(GeneralUtil.TiraNaonumero(tabProposta.getTxCnpjImportador()));
				  }
				}
				tM1.setCD_Tipo_DTA(
						GeneralUtil.setFormatZeroEsq(String.valueOf(tabProposta.getCdTipoCarregamento()), 2));
				tM1.setNR_DTA(tabProposta.getTxNumeroDta());
				tM1.setCanal(tabProposta.getTxDtaCanal());
				tM1.setDT_Registro_DTA(GeneralParser.format_datetimeBRMySql(tabProposta.getDtDtaRegistro()));
				tM1.setDT_Parametrizacao_DTA(
						GeneralParser.format_datetimeBRMySql(tabProposta.getDtDtaParametrizacao()));
				tM1.setDT_Inicio_Transito(GeneralParser.format_datetimeBRMySql(tabProposta.getDtDtaInicioTransito()));
				tM1.setDT_Chegada_Destino(GeneralParser.format_datetimeBRMySql(tabProposta.getDtDtaChegadaTransito()));
				tM1.setDT_Conclusao_Transito(
						GeneralParser.format_datetimeBRMySql(tabProposta.getDtDtaConclusaoTransito()));
				String txMercadoria = tabProposta.getTxMercadoria();
				if (txMercadoria.length() > 100) {
					txMercadoria = txMercadoria.substring(0, 95) + "...";
					tM1.setMercadoria(txMercadoria);
				} else {
					tM1.setMercadoria(txMercadoria);
				}
				tM1.setVR_Cubagem(tabProposta.getVlM3Pack() != null ? tabProposta.getVlM3Pack().floatValue() : null);
				tM1.setVR_Peso(
						tabProposta.getVlPesoExtratoDesova() != null ? tabProposta.getVlPesoExtratoDesova().floatValue()
								: null);
				tM1.setVR_Volumes(
						tabProposta.getVlQtdeVolume() != null ? tabProposta.getVlQtdeVolume().floatValue() : null);
				tM1.setVR_Mercadoria(
						tabProposta.getVlMercadoriaInvoice() != null ? tabProposta.getVlMercadoriaInvoice().floatValue()
								: null);
				tM1.setFL_Hubport_Inativo(null);
				tM1.setFL_Bloqueio(null);
				String txNavio = tabProposta.getTxNavio() + "/" + tabProposta.getTxViagem();
				if (txNavio.length() > 50) {
					tM1.setNavio_Viagem(txNavio.substring(0, 49));
				} else {
					tM1.setNavio_Viagem(txNavio);
				}
				tM1.setDT_Indicacao_DTA(null);
				tM1.setDT_Cadastro(null);
				tM1.setNR_Container(tabProposta.getTxContainer());
				tM1.setDT_Entrada_Container(GeneralParser.format_datetimeBRMySql(tabProposta.getDtEntradaCd()));
				tM1.setDT_Desconsolidacao(GeneralParser.format_datetimeBRMySql(tabProposta.getDtDesconsolidacao()));
				tM1.setDT_Inicio_Desova(GeneralParser.format_datetimeBRMySql(tabProposta.getDtInicioDesova()));
				tM1.setDT_Desova(GeneralParser.format_datetimeBRMySql(tabProposta.getDtFimDesova()));
				tM1.setDT_Inicio_Carregamento(GeneralParser.format_datetimeBRMySql(tabProposta.getDtDtaCarregamento()));

				TabClienteObj tabCliente = tabClienteService.consultar(tabProposta.getCdCliente());
				tM1.setCD_Indicador_NVOCC(GeneralUtil.TiraNaonumero(tabCliente.getTxCnpj()));

				if (!Validator.isBlankOrNull(tabProposta.getCdColoaderDespachante())) {
					tabCliente = tabClienteService.consultar(tabProposta.getCdColoaderDespachante());
					tM1.setCD_Coloader(GeneralUtil.TiraNaonumero(tabCliente.getTxCnpj()));
				} else {
					tM1.setCD_Coloader(null);
				}

				tM1.setFL_Hubport_Freehand(null);
				tM1.setFL_RO(null);
				tM1.setFL_FMA(null);
				if (tabProposta.getCkImoPerigoso() != null) {
				 tM1.setCD_IMO(tabProposta.getCkImoPerigoso() == 1 ? "1" : "0"); // (0, 1, 2 onde 0=ONU 1=CLASSE 2-RISCO)
				}else {
				 tM1.setCD_IMO("0");	
				}
				if (tabProposta.getCkAnvisa() != null) {
				 tM1.setFL_Anvisa(tabProposta.getCkAnvisa() == 1 ? "S" : "N");
				}else {
				 tM1.setFL_Anvisa("N");
				}
				tM1.setDT_Averbacao(GeneralParser.format_datetimeBRMySql(tabProposta.getDtAverbacao()));
				tM1.setFL_Averbou(tabProposta.getDtAverbacao() != null ? "S" : "N");
				tM1.setDT_Saida_Terminal(GeneralParser.format_datetimeBRMySql(tabProposta.getDtSaida()));
				tM1.setFL_Icms_Sefaz(tabProposta.getTxIcmsSefaz().equals("SIM") ? "S" : "N");
				tM1.setFL_Desembaracada(GeneralParser.format_datetimeBRMySql(tabProposta.getDtDtaDesembaraco()));
				tM1.setFL_Mapa_Madeira(tabProposta.getTxMapa().equals("SIM") ? "S" : "N");
				tM1.setFL_GR_Paga(tabProposta.getTxGrPaga().equals("SIM") ? "S" : "N");
				tM1.setFL_SistCarga(tabProposta.getTxSiscarga().equals("SIM") ? "S" : "N");
				tM1.setFL_Bloqueio_BL(tabProposta.getTxBloqueioBl().equals("SIM") ? "S" : "N");
				tM1.setUF_Destino(null);
				tM1.setNR_FMA(null);
				tM1.setCD_Expedidor(null);
				tM1.setDS_VOLUME(tabProposta.getTxVolume());
				tM1.setNR_PROPOSTA_PAS(tabProposta.getTxProposta());
				tM1.setE_Mail_Faturamento(tabProposta.getTxEmailTaxaInformativo());
				tM1.setVR_Peso_Bruto(
						tabProposta.getVlPesoBruto() != null ? tabProposta.getVlPesoBruto().floatValue() : null);
				tM1.setVR_Diferenca_Peso(
						tabProposta.getVlDiferencaPeso() != null ? tabProposta.getVlDiferencaPeso().floatValue()
								: null);
				tM1.setVR_Diferenca_Peso_Percent(tabProposta.getVlDiferencaPercentual() != null
						? tabProposta.getVlDiferencaPercentual().floatValue()
						: null);
				
				if (tabProposta.getCkFreteExclusivo() != null) {
				  tM1.setFL_Dedicado(tabProposta.getCkFreteExclusivo() == 1 ? "S" : "N");
				}
				
				if (tabProposta.getCkNaoRemonte() != null) {
				 tM1.setFL_Empilhavel(tabProposta.getCkNaoRemonte() == 1 ? "S" : "N");
				}else {
					tM1.setFL_Empilhavel("S");	
				}
				
				tM1.setUltimo_Status(tabProposta.getTxStatus());
				tM1.setDT_Prev_Carregamento(GeneralParser.format_datetimeBRMySql(tabProposta.getDtPrevCarregamento()));

				if (!Validator.isBlankOrNull(tabProposta.getTxDtaCanal())) {
					if (tabProposta.getTxDtaCanal().toUpperCase().contains("VERDE")) {
						tM1.setFL_Canal_Verde("S");
					}
				}
				if (!Validator.isBlankOrNull(tabProposta.getTxSituacaoCeMaster())) {
					if (tabProposta.getTxSituacaoCeMaster().contains("8933206")) {
						tM1.setCD_Expedidor("58188756002210");
					} else if (tabProposta.getTxSituacaoCeMaster().contains("8931364")) {
						tM1.setCD_Expedidor("58188756000277");
					} else if (tabProposta.getTxSituacaoCeMaster().contains("8932759")) {
						tM1.setCD_Expedidor("58188756001249");
					}

				}

				List<TabPropostaDocumentosObj> lDoctos = tabPropostaService.listaDocumentos(cdProposta);

				List<TabMantranDoctosObj> lMantranDoctos = new ArrayList<TabMantranDoctosObj>();
				for (TabPropostaDocumentosObj atual : lDoctos) {

					TabMantranDoctosObj tDoctos = new TabMantranDoctosObj();
					String txUrl = atual.getTxUrl().replace("/proposta/", "https://amalog.com.br/documentos/");
					tDoctos.setNome_Completo_Documento(txUrl);
					tDoctos.setCD_Classificacao(String.valueOf(atual.getTabDocumentosClassificacaoObj().getCdDoctoClassificacao()));
					tDoctos.setDS_Classificacao(atual.getTabDocumentosClassificacaoObj().getTxDoctoClassificacao());
					tDoctos.setDT_Upload(GeneralParser.format_datetimeBRMySql(atual.getDtCriacao()));
					tDoctos.setDT_Validacao(GeneralParser.format_datetimeBRMySql(atual.getDtValidacao()));
					tDoctos.setID_Arquivo_PAS(atual.getCdPropostaDocumento());
					tDoctos.setObservacao(atual.getTxObs());
					tDoctos.setPendencia(atual.getTxPendencia());
					tDoctos.setUsuario_Upload(atual.getTabUsuarioCriacaoObj().getTxApelido());
					tDoctos.setUsuario_Validacao(atual.getTabUsuarioValidacaoObj().getTxApelido());
					lMantranDoctos.add(tDoctos);
				}

				tM1.setArquivos(lMantranDoctos);

				Gson g = new GsonBuilder().disableHtmlEscaping().create();
				txJson = g.toJson(tM1);
				// System.out.println(txJson);

				if (Validator.isBlankOrNull(tabProposta.getCdIdMantran())) {
					txRetorno = restconnect.enviaRestPost("http://189.108.172.20:91/api/dta", txJson);
					System.out.println("Mantran DTA POST: " + tabProposta.getCdProposta());
				} else if (tabProposta.getCdIdMantran() == 0) {
					txRetorno = restconnect.enviaRestPost("http://189.108.172.20:91/api/dta", txJson);
					System.out.println("Mantran DTA POST: " + tabProposta.getCdProposta());					
				}else {	
					if (tabProposta.getCdIdMantran() > 0) {
						txRetorno = restconnect.enviaRestPut(
								"http://189.108.172.20:91/api/dta/" + tabProposta.getCdIdMantran(), txJson);
						System.out.println("Mantran DTA PUT: " + tabProposta.getCdProposta() + " - ID:"
								+ tabProposta.getCdIdMantran());
					}
				}

				if (txRetorno.contains("existe um DTA para com mesmo")
						|| txRetorno.contains("mas apenas alterado (PUT)")) {

					txRetorno = restconnect.enviaRestGet(
							"http://189.108.172.20:91/api/dta/028kj86hgK7JH9S0/bl_filhote/" + tabProposta.getTxBl());

					JSONObject json = new JSONObject(txRetorno);
					if (!json.isNull("ID")) {

						Integer id = json.getInt("ID");
						System.out.println("Mantran DTA GET: " + tabProposta.getCdProposta() + " - ID:" + id);

						txRetorno = restconnect.enviaRestPut("http://189.108.172.20:91/api/dta/" + id, txJson);
						System.out.println("Mantran DTA PUT: " + tabProposta.getCdProposta() + " - ID:" + id);
					}

				}

				// Inicio - Gravar log Mantran
				gravaLogMantran(tabProposta.getCdProposta(), txJson, txRetorno);
				// Fim - Gravar log Mantran

			}

		} catch (Exception ex) {
			// ex.printStackTrace();
			String txErro = "Erro request DTA Mantran: " + ex.getMessage() + " - " + tabProposta.getCdProposta();
			System.out.println("Erro request DTA Mantran: " + ex.getMessage() + " - " + txErro);
			System.out.println("Json Enviado: " + txJson);
			gravaLogMantran(tabProposta.getCdProposta(), txJson, txErro);
		}
		return txRetorno;

	}

	private void gravaLogMantran(Integer cdProposta, String txJsonEnvio, String txJsonRetorno) {

		TabPropostaMantranObj tab = new TabPropostaMantranObj();
		tab.setCdProposta(cdProposta);
		tab.setTxJsonEnvio(txJsonEnvio);
		tab.setTxJsonRetorno(txJsonRetorno);
		tab.setDtCriacao(new Date());
		tabPropostaMantranRepository.save(tab);

	}

	public String InterfaceMantranCTE(Integer cdProposta) {

		String txJson = "";

		TabPropostaObj tabProposta = tabPropostaService.consultar(cdProposta);

		if (tabProposta != null) {

			TabMantranCteObj tM1 = new TabMantranCteObj();
			tM1.setKey("jhg4j56hgK7JH9S0");
			tM1.setCGC_CPF_Remetente(tabProposta.getTxCnpj());
			tM1.setRazao_Social_Remetente(tabProposta.getTxNomePagador());
			tM1.setNR_NF(null);
			tM1.setCGC_CPF_Cliente(tabProposta.getTxCnpj());
			tM1.setRazao_Social_Cliente(tabProposta.getTxNomePagador());
			tM1.setCGC_CPF_Destinatario(null);
			tM1.setIE_Destinatario(null);
			tM1.setRazao_Social_Destinatario(null);
			tM1.setCGC_CPF_Faturamento(tabProposta.getTxCnpj());
			tM1.setRazao_Social_Faturamento(tabProposta.getTxNomePagador());
			tM1.setEndereco_Entrega(null);
			tM1.setCidade_Entrega(null);
			tM1.setUF_Entrega(null);
			tM1.setCEP_Destinatario(null);
			tM1.setCEP_REF_Destino(null);
			tM1.setUF_Origem(tabProposta.getTabOrigemObj().getTxOrigem());
			tM1.setCEP_REF_Origem(null);
			tM1.setDT_Emissao(GeneralParser.format_dateBR2(tabProposta.getDtDtaSolicitacao()));
			tM1.setID_Volume(null);
			tM1.setQT_Volume(tabProposta.getVlQtdeVolume());
			tM1.setPeso_NF(tabProposta.getVlPesoExtratoDesova().floatValue());
			tM1.setCubagem(tabProposta.getVlM3Pack().floatValue());
			tM1.setPeso_Calculo(null);
			tM1.setVR_Total_NF(null);
			tM1.setBairro_Entrega(null);
			tM1.setNR_DI(tabProposta.getTxNumeroDta());

			tM1.setCEP_Origem(null);

			Gson g = new GsonBuilder().disableHtmlEscaping().create();
			txJson = g.toJson(tM1);
			System.out.println(txJson);
			
			//177.70.122.154:39533

		}

		return txJson;

	}

}
