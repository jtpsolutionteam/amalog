package br.com.jtpsolution.dao.cadastros.coloader;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.br.CNPJ;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_coloader_despachante", schema = Constants.SCHEMA)
public class TabColoaderDespachanteObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_coloader")
	// @NotNull(message = "Coloader campo obrigatório!")
	private Integer cdColoader;

	@Column(name = "tx_nome")
	@NotEmpty(message = "Razão Social campo obrigatório!")
	@Size(max = 100, message = "Razão Social tamanho máximo de 100 caracteres")
	private String txNome;

	@Column(name = "tx_nomesimples")
	@NotEmpty(message = "1o. Nome Empresa campo obrigatório!")
	@Size(max = 50, message = "1o. Nome Empresa tamanho máximo de 50 caracteres")
	private String txNomesimples;
	
	@Column(name = "tx_endereco")
	@NotEmpty(message = "Endereço campo obrigatório!")
	@Size(max = 200, message = "Endereço tamanho máximo de 200 caracteres")
	private String txEndereco;

	@Column(name = "tx_cidade")
	@NotEmpty(message = "Cidade campo obrigatório!")
	@Size(max = 50, message = "Cidade tamanho máximo de 50 caracteres")
	private String txCidade;

	@Column(name = "tx_uf")
	@NotEmpty(message = "Uf campo obrigatório!")
	@Size(max = 2, message = "Uf tamanho máximo de 2 caracteres")
	private String txUf;

	@CNPJ
	@Column(name = "tx_cnpj")
	@NotEmpty(message = "CNPJ campo obrigatório!")
	@Size(max = 20, message = "CNPJ tamanho máximo de 20 caracteres")
	private String txCnpj;

	@Column(name = "tx_email")
	@NotEmpty(message = "Email campo obrigatório!")
	@Size(max = 50, message = "Email tamanho máximo de 50 caracteres")
	private String txEmail;

	@Column(name = "tx_telefone")
	@NotEmpty(message = "Telefone campo obrigatório!")
	@Size(max = 15, message = "Telefone tamanho máximo de 15 caracteres")
	private String txTelefone;
	
	@Transient
	@NotEmpty(message = "Senha campo obrigatório!")
	private String txSenha;

	@Transient
	@NotEmpty(message = "Confirmação Email campo obrigatório!")
	private String txConfEmail;

	@Transient
	@NotEmpty(message = "Confirmação Senha campo obrigatório!")
	private String txConfSenha;

	
	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txNome))
			txNome = txNome.toUpperCase();		
		if (!Validator.isBlankOrNull(txEndereco))
			txEndereco = txEndereco.toUpperCase();
		if (!Validator.isBlankOrNull(txCidade))
			txCidade = txCidade.toUpperCase();
		if (!Validator.isBlankOrNull(txUf))
			txUf = txUf.toUpperCase();
		if (!Validator.isBlankOrNull(txCnpj))
			txCnpj = txCnpj.toUpperCase();
		if (!Validator.isBlankOrNull(txEmail))
			txEmail = txEmail.toUpperCase();
		if (!Validator.isBlankOrNull(txTelefone))
			txTelefone = txTelefone.toUpperCase();
	}

}
