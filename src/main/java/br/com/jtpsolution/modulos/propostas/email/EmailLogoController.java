package br.com.jtpsolution.modulos.propostas.email;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.jtpsolution.dao.propostas.TabPropostaEnvioEmailTaxaObj;
import br.com.jtpsolution.dao.propostas.repository.TabPropostaEnvioEmailTaxaRepository;

@Controller
@RequestMapping("/emaillogo")
public class EmailLogoController {

	@Autowired
	private TabPropostaEnvioEmailTaxaRepository tabPropostaEnvioEmailTaxaRepository;
	
	@GetMapping("/download/{cdProposta}/{txEmail}")
	public @ResponseBody String getFileOld(@PathVariable Integer cdProposta,
			@PathVariable String txEmail) {

		//System.out.println("Email Download: "+cdProposta+" - "+txEmail);
		
		ckAbertura(cdProposta, txEmail.replace(".com", ".com.br"));
		
		/*
		String txNomeArquivo = "logo-bandeirantes-logistica-integrada.png";
		String pathDir = Constants.PATH_UPLOAD + "logo/";
		MediaType m = null;
		File destinationFile = new File(pathDir + txNomeArquivo);
		InputStreamResource resource = null;
		try {

			resource = new InputStreamResource(new FileInputStream(destinationFile));

			if (txNomeArquivo.toLowerCase().contains(".jpg")) {
				m = MediaType.IMAGE_JPEG;
			} else if (txNomeArquivo.toLowerCase().contains(".gif")) {
				m = MediaType.IMAGE_GIF;
			} else if (txNomeArquivo.toLowerCase().contains(".png")) {
				m = MediaType.IMAGE_PNG;
			} else if (txNomeArquivo.toLowerCase().contains(".pdf")) {
				m = MediaType.APPLICATION_PDF;
			} else {
				m = MediaType.APPLICATION_OCTET_STREAM;
			}
			
			ckAbertura(cdProposta, txEmail);
			
		} catch (Exception ex) {
			System.out.println("Erro Download Arquivo: " + pathDir + txNomeArquivo);
		}

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + txNomeArquivo)
				.contentType(m).contentLength(destinationFile.length()).body(resource);
		*/	
		
		return "OK";
	}
	
	
	@GetMapping("/download/{cdProposta}")
	public @ResponseBody String getFile(@PathVariable Integer cdProposta,
			@RequestParam String txEmail) {

		//System.out.println("Email Download: "+cdProposta+" - "+txEmail);
		
		ckAbertura(cdProposta, txEmail);
		
		/*
		String txNomeArquivo = "logo-bandeirantes-logistica-integrada.png";
		String pathDir = Constants.PATH_UPLOAD + "logo/";
		MediaType m = null;
		File destinationFile = new File(pathDir + txNomeArquivo);
		InputStreamResource resource = null;
		try {

			resource = new InputStreamResource(new FileInputStream(destinationFile));

			if (txNomeArquivo.toLowerCase().contains(".jpg")) {
				m = MediaType.IMAGE_JPEG;
			} else if (txNomeArquivo.toLowerCase().contains(".gif")) {
				m = MediaType.IMAGE_GIF;
			} else if (txNomeArquivo.toLowerCase().contains(".png")) {
				m = MediaType.IMAGE_PNG;
			} else if (txNomeArquivo.toLowerCase().contains(".pdf")) {
				m = MediaType.APPLICATION_PDF;
			} else {
				m = MediaType.APPLICATION_OCTET_STREAM;
			}
			
			ckAbertura(cdProposta, txEmail);
			
		} catch (Exception ex) {
			System.out.println("Erro Download Arquivo: " + pathDir + txNomeArquivo);
		}

		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + txNomeArquivo)
				.contentType(m).contentLength(destinationFile.length()).body(resource);
		*/	
		
		return "OK";
	}
	
	
	private void ckAbertura(Integer cdProposta, String txEmail) {
		
		new Thread() {			
			public void run() {
				//System.out.println("Email Abertura: "+cdProposta+" - "+txEmail);
				List<TabPropostaEnvioEmailTaxaObj> list = tabPropostaEnvioEmailTaxaRepository.findByCdPropostaAndTxEmail(cdProposta, txEmail);
				
				for (TabPropostaEnvioEmailTaxaObj atual: list) {
					//System.out.println("Email Achou: "+cdProposta+" - "+txEmail);
					if (atual.getDtAberto() == null) {
					 TabPropostaEnvioEmailTaxaObj Tab = new TabPropostaEnvioEmailTaxaObj();						 
					 Tab.setCdPropostaEnvioEmail(atual.getCdPropostaEnvioEmail());
					 Tab.setCdProposta(atual.getCdProposta());
					 Tab.setCkEnviado(atual.getCkEnviado());
					 Tab.setTxEmail(atual.getTxEmail());
					 Tab.setDtEnvio(atual.getDtEnvio());
					 Tab.setCkAberto(1);
					 Tab.setDtAberto(new Date());
					 tabPropostaEnvioEmailTaxaRepository.save(Tab);
					}					
				}
			}
		}.start();
	}
	
}
