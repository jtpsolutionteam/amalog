package br.com.jtpsolution.modulos.iugu;

import java.util.Date;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.conexaorest.restconnect;
import br.com.jtpsolution.dao.propostas.TabPropostaObj;
import br.com.jtpsolution.dao.propostas.repository.TabPropostaRepository;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.Validator;

@Service
//@EnableScheduling
public class VerificaPagamentosIuguBean {

	
	@Autowired
	private TabPropostaRepository tabPropostaRepository;
	
	@Scheduled(cron = "0 0/60 6-23 * * ?")
	public void baixapagamentos() {
		
		try {
		
		List<TabPropostaObj> lst = tabPropostaRepository.findByListDtBoletoPagtoEmAbertoQuery();
		
		for (TabPropostaObj atual : lst) {
			
			if (!Validator.isBlankOrNull(atual.getTxBoletoId())) {
				String retorno = restconnect.enviaRestGet("https://api.iugu.com/v1/invoices/"+atual.getTxBoletoId()+"?api_token="+Constants.IUGU_KEY_PRODUCAO);
				
				//System.out.println(retorno);
				
				JSONObject json = new JSONObject(retorno);
				
				if (!json.isNull("status")) {
					
					String txStatus = json.getString("status");
					//System.out.println(txStatus);
					
					if (txStatus.equals("paid")) {
						
						String dtPagto = json.getString("paid_at").substring(0,10);
						//System.out.println(dtPagto);
						
						atualizaPagamentoProposta(atual.getCdProposta(),GeneralParser.parseDate("yyyy-MM-dd", dtPagto), null);
						
					}else if (txStatus.equals("canceled")) {
						
						atualizaPagamentoProposta(atual.getCdProposta(), null, 1);
						
					}
					
				}
				
				Thread.sleep(2000);
			}
			
		}
		
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println("Erro Baixa de Pagamento - IUGU");
		}
	}
	
	
	private void atualizaPagamentoProposta(Integer cdProposta, Date dtPagto, Integer ckCancelado) {
		
		TabPropostaObj tab = tabPropostaRepository.findOne(cdProposta);
		
		if (tab != null) {
			if (!Validator.isBlankOrNull(dtPagto)) {
			  tab.setDtBoletoPagto(dtPagto);
			}else if (!Validator.isBlankOrNull(ckCancelado)) {
			  tab.setCkBoletoCancelado(ckCancelado);
			}
			tabPropostaRepository.save(tab);
		}
		
		
	}
	
	
	
}
