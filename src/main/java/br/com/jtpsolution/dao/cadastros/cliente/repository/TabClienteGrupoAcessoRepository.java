package br.com.jtpsolution.dao.cadastros.cliente.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.cadastros.cliente.TabClienteGrupoAcessoObj;



public interface TabClienteGrupoAcessoRepository extends JpaRepository<TabClienteGrupoAcessoObj, Integer> {

	List<TabClienteGrupoAcessoObj> findByCdCliente(Integer cdCliente);

}