package br.com.jtpsolution.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.cadastros.varios.TabValoresFixosObj;



public interface TabValoresFixosRepository extends JpaRepository<TabValoresFixosObj, Integer> {

	TabValoresFixosObj findByTabEmpresaObjCdEmpresaAndCdImpexpAndTxTipo(Integer cdEmpresa, Integer cdImpexp, String txTipo);
	
	

}