package br.com.jtpsolution.modulos.administrador.service;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.jtpsolution.dao.administracao.TabMenuObj;
import br.com.jtpsolution.dao.administracao.repository.TabMenuRepository;
import br.com.jtpsolution.dao.administracao.repository.VwTabMenuRepository;
import br.com.jtpsolution.dao.administracao.vw.VwTabMenuObj;
import br.com.jtpsolution.dao.util.log.TabLogObj;
import br.com.jtpsolution.exceptions.ErrosConstraintException;
import br.com.jtpsolution.security.DadosUser;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralUtil;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.log.LogBean;
import br.com.jtpsolution.util.logErro.JTPLogErroBean;

@Service
public class TabMenuService {

	@Autowired
	private JTPLogErroBean logErroBean;
	
	@Autowired
	private TabMenuRepository  tabRepository;
	
	@Autowired
	private VwTabMenuRepository  tabVwRepository;
	

	@Autowired
	private UsuarioBean tabUsuarioService;
	
	@Autowired
	private LogBean tabLogBeanService;


	
	
	public List<VwTabMenuObj> listar(Integer cdGrupoVisao) {
		return tabVwRepository.findOrderByCdOrdemQuery(cdGrupoVisao);
		
	}
	


	public TabMenuObj gravar(TabMenuObj Tab) {
		
		boolean ckAlteracao = false;
				
		DadosUser user  = tabUsuarioService.DadosUsuario();

		
		TabMenuObj tAtual = new TabMenuObj();
		TabMenuObj tNovo = Tab;
		
		//tNovo.setCdUsuario(null);
		List<TabLogObj> listaLog = null;
		
		if (Tab.getCdMenu() != null && !Validator.isBlankOrNull(Tab.getCdMenu())) {
			ckAlteracao = true;
			//Log de campos -- Sempre antes da gravação por causa da persistencia.
			tAtual = tabRepository.findOne(Tab.getCdMenu());
			//listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
			listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdMenu()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
		}
		
		  try {	
		   tNovo = tabRepository.save(tNovo);			  
		  }catch (ConstraintViolationException ex) {
			  
			 throw new ErrosConstraintException(GeneralUtil.MessageExceptionConstraint(ex), ex.getConstraintViolations());			  
		  }
		  
			//Log de campos
		  if (!ckAlteracao) {
			//tabLogBeanService.LogBean(String.valueOf(tNovo.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  	tabLogBeanService.LogBean(String.valueOf(tNovo.getCdMenu()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  }else {
			  tabLogBeanService.GravarListaLog(listaLog);
		  }
		return tNovo;
		
	}

	public TabMenuObj consultar(Integer CdMenu) {
	 TabMenuObj Tab = tabRepository.findOne(CdMenu);
	 
	   return Tab;
	 
	}

	
	
		public void excluir(Integer cdMenu) {

		   DadosUser user  = tabUsuarioService.DadosUsuario();
		   
		   tabRepository.delete(cdMenu);
			 
		}
		

	
}
