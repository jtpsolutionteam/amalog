package br.com.jtpsolution.modulos.robodta.obj;

import java.io.Serializable;


public class ArmazenamentoComum implements Serializable{

	private String dtArmzenamento;
	private NomeCpf responsavel;
	private String txInterveniente;
	
	
	public NomeCpf getResponsavel() {
		return responsavel;
	}
	public void setResponsavel(NomeCpf responsavel) {
		this.responsavel = responsavel;
	}
	public String getTxInterveniente() {
		return txInterveniente;
	}
	public void setTxInterveniente(String txInterveniente) {
		this.txInterveniente = txInterveniente;
	}
	public String getDtArmzenamento() {
		return dtArmzenamento;
	}
	public void setDtArmzenamento(String dtArmzenamento) {
		this.dtArmzenamento = dtArmzenamento;
	}
	
	
	
}
