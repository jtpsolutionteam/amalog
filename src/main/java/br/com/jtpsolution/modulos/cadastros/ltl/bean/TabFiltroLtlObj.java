package br.com.jtpsolution.modulos.cadastros.ltl.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TabFiltroLtlObj {

	private Integer cdEmpresaFiltro;
	
	private Integer cdImpExpFiltro;
	
	private Integer cdViaTransporteFiltro;
	
	
}
