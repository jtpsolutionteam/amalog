package br.com.jtpsolution.modulos.robosiscarga.obj;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TabCeManifestoObj {

	private String txManifesto;
	
	private String txTipoManifesto;
	
	private String dtEmissaoManifesto;
	
	private String dtManifestoOperacao;
	
	private String txEmpresaNavegacao;
	
	private String txAgenciaNavegacao;
	
	private String txNavio;
	
	private String txPortoDescarragamento;
	
	private String txEscala;
	
}
