package br.com.jtpsolution.modulos.portalunico.obj;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TabDatObj {

	private String txDat;
	
	private Date dtEmissao;
	
	private String txSituacao;

}
