package br.com.jtpsolution.modulos.propostas.listener;

import javax.persistence.PreUpdate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import br.com.jtpsolution.dao.cadastros.varios.repository.TabDestinoRepository;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabInformativoRepository;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabOrigemRepository;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabValoresFixosRepository;
import br.com.jtpsolution.dao.propostas.TabPropostaObj;
import br.com.jtpsolution.dao.propostas.repository.TabDescontosRepository;
import br.com.jtpsolution.modulos.propostas.service.TabPropostaAvariasService;
import br.com.jtpsolution.security.UsuarioBean;

@Configurable
public class GravarListener {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabValoresFixosRepository tabValoresFixosRepository;

	@Autowired
	private TabDestinoRepository tabDestinoRepository;

	@Autowired
	private TabOrigemRepository tabOrigemRepository;

	@Autowired
	private TabInformativoRepository tabInformativoRepository;

	@Autowired
	private TabPropostaAvariasService tabPropostaAvariasService;

	@Autowired
	private TabDescontosRepository tabDescontosRepository;

	@PreUpdate
	public void gravar(TabPropostaObj tabPropostaObj) {

		/*
		AutowireHelper.autowire(this, this.tabUsuariosService);
		AutowireHelper.autowire(this, this.tabValoresFixosRepository);
		AutowireHelper.autowire(this, this.tabDestinoRepository);
		AutowireHelper.autowire(this, this.tabOrigemRepository);
		AutowireHelper.autowire(this, this.tabInformativoRepository);
		AutowireHelper.autowire(this, this.tabPropostaAvariasService);
		AutowireHelper.autowire(this, this.tabDescontosRepository);
	  */
		

	}

}
