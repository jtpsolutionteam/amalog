package br.com.jtpsolution.dao.cadastros.empresa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_empresa", schema = Constants.SCHEMA)
public class TabEmpresaObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_empresa")
	// @NotNull(message = "Empresa campo obrigatório!")
	private Integer cdEmpresa;

	@Column(name = "tx_empresa")
	// @NotEmpty(message = "Empresa campo obrigatório!")
	@Size(max = 100, message = "Empresa tamanho máximo de 100 caracteres")
	private String txEmpresa;

	@Column(name = "tx_cnpj")
	// @NotEmpty(message = "Cnpj campo obrigatório!")
	@Size(max = 20, message = "Cnpj tamanho máximo de 20 caracteres")
	private String txCnpj;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txEmpresa))
			txEmpresa = txEmpresa.toUpperCase();
		if (!Validator.isBlankOrNull(txCnpj))
			txCnpj = txCnpj.toUpperCase();
	}

}
