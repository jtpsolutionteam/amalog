package br.com.jtpsolution.modulos.cadastros.cliente.controller;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.administracao.TabGrupoAcessoObj;
import br.com.jtpsolution.dao.cadastros.cliente.TabClienteGrupoAcessoObj;
import br.com.jtpsolution.dao.cadastros.cliente.TabClienteObj;
import br.com.jtpsolution.dao.cadastros.cliente.VwTabClienteGrupoAcessoObj;
import br.com.jtpsolution.mensagens.TabRetornoSucessoObj;
import br.com.jtpsolution.modulos.administrador.service.TabGrupoAcessoService;
import br.com.jtpsolution.modulos.cadastros.cliente.bean.TabFiltroClienteGrupoAcessoObj;
import br.com.jtpsolution.modulos.cadastros.cliente.service.TabClienteGrupoAcessoService;
import br.com.jtpsolution.modulos.cadastros.cliente.service.TabClienteService;
import br.com.jtpsolution.modulos.util.geralcontroller.allController;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.regras.RegrasBean;

@Controller
@RequestMapping("/clientegrupoacesso")
public class TabClienteGrupoAcessoController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabClienteGrupoAcessoService tabService;
	
	@Autowired
	private RegrasBean regrasBean;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_CADASTROS+"/cliente/clientegrupoacesso";
	
 	
 	@GetMapping	
	public ModelAndView pesquisa(TabFiltroClienteGrupoAcessoObj tabFiltroClienteGrupoAcessoObj, HttpServletRequest request) {
		
		tabFiltroClienteGrupoAcessoObj = verificaFiltro(tabFiltroClienteGrupoAcessoObj, request);
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject(new TabClienteGrupoAcessoObj());			
		mv.addObject("tabFiltroClienteGrupoAcessoObj", tabFiltroClienteGrupoAcessoObj);
		mv.addObject("listadados", retornoFiltro(tabFiltroClienteGrupoAcessoObj));
		mv.addObject("cdCliente", tabFiltroClienteGrupoAcessoObj.getCdClienteFiltro());
		
		return mv;
	}
	
	private List<?> retornoFiltro(TabFiltroClienteGrupoAcessoObj tabFiltroClienteGrupoAcessoObj) {
 		
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(VwTabClienteGrupoAcessoObj.class);
		
		adicionarFiltro(tabFiltroClienteGrupoAcessoObj, criteria);
 		
 		return criteria.list();
 	}


	private TabFiltroClienteGrupoAcessoObj verificaFiltro(TabFiltroClienteGrupoAcessoObj tabFiltroClienteGrupoAcessoObj, HttpServletRequest request) {
		
		try {
			
			if (Validator.isObjetoNull(tabFiltroClienteGrupoAcessoObj)) {
				if (request.getSession().getAttribute(getClass().getSimpleName()) != null) {					
					String jsonFiltro = request.getSession().getAttribute(getClass().getSimpleName()).toString();
					ObjectMapper mapper = new ObjectMapper();
					tabFiltroClienteGrupoAcessoObj = mapper.readValue(jsonFiltro, TabFiltroClienteGrupoAcessoObj.class);	
				}
			}else {
				request.getSession().setAttribute(getClass().getSimpleName(),
						GeneralParser.convertObjJson(tabFiltroClienteGrupoAcessoObj));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return tabFiltroClienteGrupoAcessoObj;
	}

	
	private void adicionarFiltro(TabFiltroClienteGrupoAcessoObj filtro, Criteria criteria) {
		
		  boolean ckFiltro = false;
		  
		  if (filtro != null) {
			  
			  if (!StringUtils.isEmpty(filtro.getCdClienteFiltro())) {
				  criteria.add(Restrictions.eq("cdCliente", filtro.getCdClienteFiltro()));
				  ckFiltro = true;
			  }
			  
			  if (!ckFiltro) {
				  criteria.add(Restrictions.eq("cdClienteGrupoAcesso", -1));
			  }
			  
			  criteria.addOrder(Order.asc("cdGrupoAcesso"));
		  }
	  }
 	
	
	@RequestMapping(value = "/gravarmodal", method = RequestMethod.POST)
	public @ResponseBody List<?> gravarmodal(@Validated TabClienteGrupoAcessoObj tabClienteGrupoAcessoObj, Errors erros, HttpServletRequest request) {

		List<ObjectError> error = new ArrayList<ObjectError>();
		if (erros.hasErrors()) {
		  //mv.addObject("listaErros", erros.getAllErrors());
		  error.addAll(erros.getAllErrors());
		  return error;
		}else {
			error = regrasBean.verificaregras(getClass().getSimpleName(),request);
			if (!error.isEmpty()) {
				  return error;	
			}	  
		}
		
	    TabClienteGrupoAcessoObj Tab = tabService.gravar(tabClienteGrupoAcessoObj);
	   
	    List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();		
	    TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
	    tSuccess.setCd_mensagem(1);
	    tSuccess.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
	    success.add(tSuccess);
	    
		return success;
	}
	
	
	@RequestMapping(value = "/novo/{cdCliente}")
	public ModelAndView novo(@PathVariable Integer cdCliente, HttpServletRequest request) {
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		mv.addObject(new TabClienteGrupoAcessoObj());
		mv.addObject("tabFiltroClienteGrupoAcessoObj", verificaFiltro(new TabFiltroClienteGrupoAcessoObj(), request));
		mv.addObject("listadados", retornoFiltro(verificaFiltro(new TabFiltroClienteGrupoAcessoObj(), request)));
		mv.addObject("cdCliente", cdCliente);
		mv.addObject("txEdit", "edit");
		return mv;
	}
	

	@RequestMapping(value = "/consultar/{cdClienteGrupoAcesso}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable Integer cdClienteGrupoAcesso, HttpServletRequest request) {
	
	   TabClienteGrupoAcessoObj TabView = tabService.consultar(cdClienteGrupoAcesso);
	   TabFiltroClienteGrupoAcessoObj tF = verificaFiltro(new TabFiltroClienteGrupoAcessoObj(), request);

	   
		ModelAndView mv = new ModelAndView(txUrlTela);	
		if (TabView != null) {
		  mv.addObject("tabFiltroClienteGrupoAcessoObj",tF);	
		  mv.addObject("tabClienteGrupoAcessoObj",TabView);
		  mv.addObject("listadados", retornoFiltro(verificaFiltro(tF, request)));
		  mv.addObject("cdCliente", tF.getCdClienteFiltro());
		  mv.addObject("txEdit", "edit");
		}else {
		  mv.addObject("tabFiltroClienteGrupoAcessoObj",tF);	
		  mv.addObject(new TabClienteGrupoAcessoObj());	
		  mv.addObject("cdCliente", tF.getCdClienteFiltro());
		  mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
		  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}
	
	/*
	@RequestMapping(value = "/excluir/{CdClienteGrupoAcesso}", method = RequestMethod.GET)
	public @ResponseBody String excluir(@PathVariable Integer CdClienteGrupoAcesso, HttpServletRequest request) {

		tabService.excluir(CdClienteGrupoAcesso);

		return "OK!";
	}*/
	
	
	@Autowired
	private TabClienteService tabClienteService;
	
	@ModelAttribute("selectcdcliente")
	public List<TabClienteObj> selectcdcliente() {
		return tabClienteService.listar();
	}
	
	@Autowired
	private TabGrupoAcessoService tabGrupoAcessoService;
	
	@ModelAttribute("selectcdgrupoacesso")
	public List<TabGrupoAcessoObj> selectcdgrupoacesso() {
		return tabGrupoAcessoService.listar();
	}
	
}
