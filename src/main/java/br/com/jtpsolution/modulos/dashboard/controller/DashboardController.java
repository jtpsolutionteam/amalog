package br.com.jtpsolution.modulos.dashboard.controller;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.jtpsolution.dao.propostas.VwTabPropostaNewObj;
import br.com.jtpsolution.dao.propostas.VwTabPropostaObj;
import br.com.jtpsolution.dao.propostas.repository.VwTabPropostaNewRepository;
import br.com.jtpsolution.modulos.propostas.service.TabPropostaService;
import br.com.jtpsolution.modulos.robodta.service.RoboDtaService;
import br.com.jtpsolution.modulos.util.geralcontroller.allController;
import br.com.jtpsolution.security.DadosUser;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.teste.teste;


@Controller
public class DashboardController extends allController {

	@Autowired
	private UsuarioBean tabUsuarioService;
	
	@Autowired
	private TabPropostaService tabPropostaService;
	
	@Autowired
	private VwTabPropostaNewRepository vwTabPropostaNewRepository;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private teste test;

	@Autowired
	private RoboDtaService roboDtaService;

	
	@RequestMapping("/dashboard")
	public ModelAndView dashboard() {
		
		
		
		DadosUser user  = tabUsuarioService.DadosUsuario();
	
		ModelAndView mv = new ModelAndView("dashboard/dashboard");

		
      
		return mv;
		
		
	}
	
	
	
}
