package br.com.jtpsolution.modulos.robodta.obj;

public class SolicitacaoRegistro {
	private String dtSolicitacao;
	private String dtRegistro;
	private String txCPFRegistro;
	
	public String getDtSolicitacao() {
		return dtSolicitacao;
	}
	public void setDtSolicitacao(String dtSolicitacao) {
		this.dtSolicitacao = dtSolicitacao;
	}
	public String getDtRegistro() {
		return dtRegistro;
	}
	public void setDtRegistro(String dtRegistro) {
		this.dtRegistro = dtRegistro;
	}
	public String getTxCPFRegistro() {
		return txCPFRegistro;
	}
	public void setTxCPFRegistro(String txCPFRegistro) {
		this.txCPFRegistro = txCPFRegistro;
	}
	
	
}
