package br.com.jtpsolution.dao.administracao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.util.Validator;

@Entity
@Table(name = "tab_grupo_acesso", schema = Constants.SCHEMA)
public class TabGrupoAcessoObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_grupo_acesso")
	// @NotNull(message = "GrupoAcesso campo obrigatório!")
	private Integer cdGrupoAcesso;

	@Column(name = "tx_grupo_acesso")
	// @NotEmpty(message = "GrupoAcesso campo obrigatório!")
	@Size(max = 45, message = "GrupoAcesso tamanho máximo de 45 caracteres")
	private String txGrupoAcesso;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txGrupoAcesso))
			txGrupoAcesso = txGrupoAcesso.toUpperCase();
	}

	public Integer getCdGrupoAcesso() {
		return cdGrupoAcesso;
	}

	public void setCdGrupoAcesso(Integer cdGrupoAcesso) {
		this.cdGrupoAcesso = cdGrupoAcesso;
	}

	public String getTxGrupoAcesso() {
		return txGrupoAcesso;
	}

	public void setTxGrupoAcesso(String txGrupoAcesso) {
		this.txGrupoAcesso = txGrupoAcesso;
	}
	
	

}
