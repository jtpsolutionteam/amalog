package br.com.jtpsolution.modulos.cadastros.cliente.service;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.jtpsolution.dao.cadastros.cliente.TabClienteObj;
import br.com.jtpsolution.dao.cadastros.cliente.repository.TabClienteRepository;
import br.com.jtpsolution.dao.util.log.TabLogObj;
import br.com.jtpsolution.exceptions.ErrosConstraintException;
import br.com.jtpsolution.security.DadosUser;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.GeneralUtil;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.log.LogBean;
import br.com.jtpsolution.util.logErro.JTPLogErroBean;

@Service
public class TabClienteService {

	@Autowired
	private JTPLogErroBean logErroBean;
	
	@Autowired
	private TabClienteRepository  tabRepository;
	
	
	@Autowired
	private UsuarioBean tabUsuarioService;
	
	@Autowired
	private LogBean tabLogBeanService;


	
	
	public List<TabClienteObj> listar() {
		return tabRepository.findAll(GeneralParser.sortByIdAsc("txCliente"));
		
	}
	
	/* Modelo tela filho 
	
		public List<VwTabAgenteObj> listar(String CampoRelacionamento) {

		DadosUser user  = tabUsuarioService.DadosUsuario();

		return tabVwRepository.findByTxNrefQuery(CampoRelacionamento, user.getCdGrupoAcesso());
		
	}
	*/
	
	/* Modelo - Tela Pesquisar
			public Page<VwTabAgenteObj> telapesquisar(TabFiltroObj tabFiltroObj, Pageable pageable) {

			Page<VwTabAgenteObj> Tab = tabFiltroService.filtrar(tabFiltroObj, pageable);
			
			return Tab;

		}
	*/


	public TabClienteObj gravar(TabClienteObj Tab) {
		
		boolean ckAlteracao = false;
				
		DadosUser user  = tabUsuarioService.DadosUsuario();

		
		TabClienteObj tAtual = new TabClienteObj();
		TabClienteObj tNovo = Tab;
		
		//tNovo.setCdUsuario(null);
		List<TabLogObj> listaLog = null;
		
		if (Tab.getCdCliente() != null && !Validator.isBlankOrNull(Tab.getCdCliente())) {
			ckAlteracao = true;
			//Log de campos -- Sempre antes da gravação por causa da persistencia.
			tAtual = tabRepository.findOne(Tab.getCdCliente());
			//listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
			listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdCliente()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
		}
		
		  try {	
		   tNovo = tabRepository.save(tNovo);			  
		  }catch (ConstraintViolationException ex) {
			  
			 throw new ErrosConstraintException(GeneralUtil.MessageExceptionConstraint(ex), ex.getConstraintViolations());			  
		  }
		  
			//Log de campos
		  if (!ckAlteracao) {
			//tabLogBeanService.LogBean(String.valueOf(tNovo.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  	tabLogBeanService.LogBean(String.valueOf(tNovo.getCdCliente()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  }else {
			  tabLogBeanService.GravarListaLog(listaLog);
		  }
		return tNovo;
		
	}

	public TabClienteObj consultar(Integer cdCliente) {
	 TabClienteObj Tab = tabRepository.findOne(cdCliente);
	 
	   return Tab;
	 
	}

	
	/* Se tela tiver excluir
		public void excluir(Integer CdAgente) {

		   DadosUser user  = tabUsuarioService.DadosUsuario();

		   TabAgenteObj Tab = tabRepository.findOne(CdAgente);
		   Tab.setDtCancelamento(new Date());
		   Tab.setCdUsuarioCancelamento(user.getCdUsuario());
		   tabRepository.save(Tab);
			 
		}
		
	*/	

	
}
