package br.com.jtpsolution.modulos.robosiscarga.obj;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TabCeMercanteMasterObj {

	private String txCe;
	
	private String txTipoConhecimento;
	
	private String txBl;
	
	private String dtBl;
	
	private String txCeMercanteMaster;

	private String txCnpjNvocc;
	
	private String txNomeNvocc;
	
	private String txNomeTransportador;
	
	private String txCnpjTransportador;
	
	private String txPortoOrigem;
	
	private String txPortoDestino;
	
	private String txPaisProcedencia;
	
	private String txUfDestino;
	
	private String txSituacao;
	
	private String txModalidadeFrete;
	
	private String txManifesto;
	
	
	
	
	
	
	
	
}
