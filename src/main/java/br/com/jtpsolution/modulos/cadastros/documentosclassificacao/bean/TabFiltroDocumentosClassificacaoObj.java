package br.com.jtpsolution.modulos.cadastros.documentosclassificacao.bean;

public class TabFiltroDocumentosClassificacaoObj {

	private String txDoctoClassificacaoFiltro;

	public String getTxDoctoClassificacaoFiltro() {
		return txDoctoClassificacaoFiltro;
	}

	public void setTxDoctoClassificacaoFiltro(String txDoctoClassificacaoFiltro) {
		this.txDoctoClassificacaoFiltro = txDoctoClassificacaoFiltro;
	}
	
	
}
