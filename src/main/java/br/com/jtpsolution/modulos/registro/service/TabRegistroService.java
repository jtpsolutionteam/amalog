package br.com.jtpsolution.modulos.registro.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.jtpsolution.dao.cadastros.cliente.VwTabClienteGrupoAcessoObj;
import br.com.jtpsolution.dao.cadastros.cliente.repository.TabClienteRepository;
import br.com.jtpsolution.dao.cadastros.cliente.repository.VwTabClienteGrupoAcessoRepository;
import br.com.jtpsolution.dao.cadastros.usuario.TabUsuarioObj;
import br.com.jtpsolution.dao.cadastros.usuario.repository.TabUsuarioRepository;

@Service
public class TabRegistroService {

	
	@Autowired
	private TabUsuarioRepository  tabRepository;
	
	@Autowired
	private VwTabClienteGrupoAcessoRepository  tabVwClienteRepository;
	
	
	public List<TabUsuarioObj> listar() {
		return tabRepository.findAll();
		
	}
	
	public TabUsuarioObj gravar(TabUsuarioObj Tab) {
		

		TabUsuarioObj tNovo =  tabRepository.save(Tab);			  
		 
		return tNovo;
		
	}

	public TabUsuarioObj consultar(Integer CdUsuario) {
	 TabUsuarioObj Tab = tabRepository.findOne(CdUsuario);
	 
	   return Tab;
	 
	}
	
	public TabUsuarioObj consultarEmail(String txEmail) {
		   TabUsuarioObj Tab = tabRepository.findByTxEmail(txEmail);
		 
		   return Tab;
		 
		}
	

	
	public List<VwTabClienteGrupoAcessoObj> consultarCnpj(String txCnpj) {
		   
		   return tabVwClienteRepository.findByTxCnpj(txCnpj);
		 
	}
	
	public VwTabClienteGrupoAcessoObj consultarCnpjCkCliente(String txCnpj, Integer ckCliente) {
		   
		   return tabVwClienteRepository.findByTxCnpjAndCkCliente(txCnpj, ckCliente);
		 
	}
	
	


	
	/* Se tela tiver excluir
		public void excluir(Integer CdUsuario) {

		   DadosUser user  = tabUsuarioService.DadosUsuario();

		   TabUsuarioObj Tab = tabRepository.findOne(CdUsuario);
		   Tab.setDtCancelamento(new Date());
		   Tab.setCdUsuarioCancelamento(user.getCdUsuario());
		   tabRepository.save(Tab);
			 
		}
		
	*/	
	
	
	

	
}
