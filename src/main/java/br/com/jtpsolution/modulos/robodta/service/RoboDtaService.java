package br.com.jtpsolution.modulos.robodta.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.jtpsolution.dao.cadastros.usuario.TabUsuarioObj;
import br.com.jtpsolution.dao.cadastros.varios.TabDocumentosClassificacaoObj;
import br.com.jtpsolution.dao.cadastros.varios.TabStatusObj;
import br.com.jtpsolution.dao.conexaorest.restconnect;
import br.com.jtpsolution.dao.propostas.TabPropostaDocumentosObj;
import br.com.jtpsolution.dao.propostas.TabPropostaEnvioEmailTaxaObj;
import br.com.jtpsolution.dao.propostas.TabPropostaObj;
import br.com.jtpsolution.dao.propostas.repository.TabPropostaDocumentosRepository;
import br.com.jtpsolution.dao.propostas.repository.TabPropostaRepository;
import br.com.jtpsolution.modulos.mantran.service.InterfaceMantranService;
import br.com.jtpsolution.modulos.microled.service.MicroledService;
import br.com.jtpsolution.modulos.propostas.service.TabPropostaEnvioEmailTaxaService;
import br.com.jtpsolution.modulos.robodta.obj.FluxoDeclaracaoEntrada;
import br.com.jtpsolution.modulos.robodta.obj.FluxoDeclaracaoTransito;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.GeneralUtil;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.mailthymeleaf.EmailSend;

@Service
public class RoboDtaService {

	@Autowired
	private TabPropostaRepository tabPropostaRepository;

	@Autowired
	private TabPropostaDocumentosRepository tabPropostaDocumentosRepository;

	@Autowired
	private TabPropostaEnvioEmailTaxaService tabPropostaEnvioEmailTaxaService;

	@Autowired
	private EmailSend emailSend;
	
	@Autowired
	private InterfaceMantranService interfaceMantranService;
	
	@Autowired
	private MicroledService microledService;
	
	
	@Scheduled(cron = "0 0/10 6-23 * * ?")
	public void executaRobo() {

		try {
			
		 // if (GeneralUtil.getProducao()) {
			 System.out.println("DTA Robo:" +new Date());
			
			List<TabPropostaObj> listProposta = tabPropostaRepository.findByConclusaoTransitoNullQuery();

			for (TabPropostaObj atual : listProposta) {
				boolean ckFluxo = false;
				boolean ckDesembaraco = false;

				 System.out.println("DTA No: "+atual.getTxNumeroDta());

				if (!Validator.isBlankOrNull(atual.getTxNumeroDta())) {

					FluxoDeclaracaoTransito fluxo = consultarDta(GeneralUtil.setTiranaoNumero(atual.getTxNumeroDta()));
					
					System.out.println("Fluxo DTA: "+fluxo);
					
					if (fluxo != null) {

						System.out.println("DTA INICIO: "+atual.getTxNumeroDta());
						
						if (fluxo.getSolicitacaoRegistro() != null) {

							if (fluxo.getSolicitacaoRegistro().getDtSolicitacao() != null) {

								TabPropostaObj tabProposta = tabPropostaRepository.findOne(atual.getCdProposta());

								if (fluxo.getSolicitacaoRegistro() != null) {
									tabProposta.setDtDtaSolicitacao(GeneralParser.StringLocalDateTimeToDate(
											fluxo.getSolicitacaoRegistro().getDtSolicitacao()));
									TabStatusObj tabStatusObj = new TabStatusObj();
									tabStatusObj.setCdStatus(5);
									tabProposta.setTabStatusObj(tabStatusObj);

									if (fluxo.getSolicitacaoRegistro().getDtRegistro() != null) {
										tabProposta.setDtDtaRegistro(GeneralParser.StringLocalDateTimeToDate(
												fluxo.getSolicitacaoRegistro().getDtRegistro()));										
										tabStatusObj.setCdStatus(6);
										tabProposta.setTabStatusObj(tabStatusObj);
									}

									if (atual.getDtDtaSolicitacao() == null) {
										ckFluxo = true;																			
									} else {

										TabPropostaDocumentosObj tabP = tabPropostaDocumentosRepository
												.findByCdPropostaAndTabDocumentosClassificacaoObjCdDoctoClassificacao(atual.getCdProposta(), 28);
										if (tabP == null) {
											ckFluxo = true;
										}
										TabPropostaDocumentosObj tabD = tabPropostaDocumentosRepository
												.findByCdPropostaAndTabDocumentosClassificacaoObjCdDoctoClassificacao(atual.getCdProposta(), 29);
										if (tabD == null) {
											if (!Validator.isBlankOrNull(atual.getDtDtaDesembaraco())) {
												ckDesembaraco = true;
											}
										}

									}

									if (atual.getDtDtaRegistro() == null) {
										ckFluxo = true;
									}

								}

								if (fluxo.getSelecaoParametrizada() != null) {
									tabProposta.setDtDtaParametrizacao(GeneralParser.StringLocalDateTimeToDate(
											fluxo.getSelecaoParametrizada().getDtParametrizacao()));																	
									TabStatusObj tabStatusObj = new TabStatusObj();
									tabStatusObj.setCdStatus(7);
									tabProposta.setTabStatusObj(tabStatusObj);
									if (fluxo.getSelecaoParametrizada().getTxCanal() != null)
										tabProposta.setTxDtaCanal(fluxo.getSelecaoParametrizada().getTxCanal());

									if (atual.getDtDtaParametrizacao() == null) {
										ckFluxo = true;
									}

								}

								if (fluxo.getDesembaraco() != null) {
									tabProposta.setDtDtaDesembaraco(GeneralParser
											.StringLocalDateTimeToDate(fluxo.getDesembaraco().getDtDesembaraco()));
									TabStatusObj tabStatusObj = new TabStatusObj();
									tabStatusObj.setCdStatus(8);
									tabProposta.setTabStatusObj(tabStatusObj);


									if (atual.getDtDtaDesembaraco() == null) {
										ckFluxo = true;
									}

								}

								if (fluxo.getCarregamento() != null) {
									tabProposta.setDtDtaCarregamento(GeneralParser
											.StringLocalDateTimeToDate(fluxo.getCarregamento().getDtCarregamento()));
									TabStatusObj tabStatusObj = new TabStatusObj();
									tabStatusObj.setCdStatus(9);
									tabProposta.setTabStatusObj(tabStatusObj);

									if (atual.getDtDtaCarregamento() == null) {
										ckFluxo = true;
									}

								}

								if (fluxo.getInformacoesVeiculo() != null) {
									if (fluxo.getInformacoesVeiculo().getDtPartidaProcedencia() != null) {
										tabProposta.setDtDtaInicioTransito(GeneralParser.StringLocalDateTimeToDate(
												fluxo.getInformacoesVeiculo().getDtPartidaProcedencia()));
										TabStatusObj tabStatusObj = new TabStatusObj();
										tabStatusObj.setCdStatus(10);
										tabProposta.setTabStatusObj(tabStatusObj);

									}
									if (atual.getDtDtaInicioTransito() == null) {
										ckFluxo = true;
									}
								}

								if (fluxo.getChegadaTransito() != null) {
									tabProposta.setDtDtaChegadaTransito(GeneralParser.StringLocalDateTimeToDate(
											fluxo.getChegadaTransito().getDtChegadaTransito()));
									TabStatusObj tabStatusObj = new TabStatusObj();
									tabStatusObj.setCdStatus(11);
									tabProposta.setTabStatusObj(tabStatusObj);


									if (atual.getDtDtaChegadaTransito() == null) {
										ckFluxo = true;
									}
								}
								
								if (fluxo.getArmazenamento() != null) {
									tabProposta.setDtArmazenagem(GeneralParser.StringLocalDateTimeToDate(
											fluxo.getArmazenamento().getRegistro().getDtArmzenamento()));
									TabStatusObj tabStatusObj = new TabStatusObj();
									tabStatusObj.setCdStatus(11);
									tabProposta.setTabStatusObj(tabStatusObj);


									if (atual.getDtArmazenagem() == null) {
										ckFluxo = true;
									}
								}

								if (fluxo.getConclusaoTransito() != null) {
									tabProposta.setDtDtaConclusaoTransito(GeneralParser.StringLocalDateTimeToDate(
											fluxo.getConclusaoTransito().getDtConclusaoTransito()));
									TabStatusObj tabStatusObj = new TabStatusObj();
									tabStatusObj.setCdStatus(12);
									tabProposta.setTabStatusObj(tabStatusObj);


									if (atual.getDtDtaConclusaoTransito() == null) {
										ckFluxo = true;
									}
								}

								
								if (ckFluxo 
										&& Validator.isBlankOrNull(tabProposta.getDtEnvioEmailInformativo())) {

									String txEmail = tabProposta.getTxEmailTaxaInformativo()
											+ ";faturamentodta@band-deicmar.com.br";

									String[] arrEmail = txEmail.split("\\;");

									for (int i = 0; i <= arrEmail.length - 1; i++) {
										if (Validator.isValidEmail(arrEmail[i].trim())) {
											
											String txEmailArray = arrEmail[i].trim();
											
											Map<String, Object> model = new HashMap<String, Object>();
											model.put("cdProposta", tabProposta.getCdProposta());
											model.put("txEmail", txEmailArray);
											model.put("txEmailTaxaInformativo", txEmailArray);
											model.put("txBomDia", GeneralParser.mostraBomDiaTardeNoite());
											model.put("txProposta", tabProposta.getTxProposta());
											model.put("txLote", tabProposta.getTxLote());
											model.put("txBl", tabProposta.getTxBl());
											model.put("txNumeroDta", tabProposta.getTxNumeroDta());
											model.put("vlTaxaInformativo", GeneralParser.doubleToStringMoney(
													tabProposta.getVlTaxaInformativo().doubleValue()));
											model.put("txImportador", tabProposta.getTxImportador() + " - "
													+ tabProposta.getTxCnpjImportador());
											
											Date dtEnvio = new Date();
											if (emailSend.sendMail("mailinformativotaxa", txEmailArray,
													"Amalog/Bandeirantes - Informativo DTA HUB PORT", model)) {
												tabProposta.setDtEnvioEmailInformativo(new Date());
												System.out.println("Email="+tabProposta.getCdProposta()+"-"+txEmailArray+"-"+1);
												AdicionaEmail(tabProposta.getCdProposta(), txEmailArray, dtEnvio, 1);
											} else {
												AdicionaEmail(tabProposta.getCdProposta(), txEmailArray, dtEnvio, 2);
											}
										}
									}
								}
								
								
								//Envia as atualizações para o Mantran
								/*
								String txEnvioMantran = interfaceMantranService.InterfaceMantranDTA(tabProposta.getCdProposta()); 
								System.out.println("json DTA Mantran: "+txEnvioMantran);
								if (txEnvioMantran.contains("ID")) {
									JSONObject json = new JSONObject(txEnvioMantran);
									
									if (!json.isNull("ID")) {							
										
									    tabProposta.setCdIdMantran(json.getInt("ID"));
									    
									}else if (!json.isNull("MENSAGEM_ERRO")) {																		
										tabProposta.setTxErroMantran(json.getString("MENSAGEM_ERRO"));
									}else {									
										tabProposta.setTxErroMantran("Erro resquest DTA Mantran");
									}
								}
								
								if (!Validator.isBlankOrNull(tabProposta.getCdIdMantran())) {
								  if (tabProposta.getCdIdMantran() == 0) {
									tabProposta.setCdIdMantran(null);
								  }
								}
								*/
								
								//Envia Microled Averbacao
								
								   if (Validator.isBlankOrNull(tabProposta.getDtAverbacao())) {
									 tabProposta.setDtEnvioAverbacao(new Date());  
								    
								     System.out.println("Microled Averbacao: "+tabProposta.getCdProposta());
								     String retornoMicroled = microledService.enviarAverbacao(tabProposta);
								     
								     if (retornoMicroled.contains("Averbacao concluída")) {
								    	 tabProposta.setDtAverbacao(new Date());
								    	 tabProposta.setTxStatusAverbacao("Averbacao concluída");
								     }else {
								    	 if (!retornoMicroled.contains("Erro envio")) {
								    	 JSONObject j = new JSONObject(retornoMicroled);
								    	 
								    	 j = new JSONObject(j.getString("wsaverbacaoLoteResult"));
								    	 
								    	 if (!j.isNull("message")) {
								    		 tabProposta.setTxStatusAverbacao(j.getString("message"));
								    	 }
								    	} 
								     }
								     System.out.println("Microled Averbacao: "+retornoMicroled);
								   }
								
								
								tabProposta.setDtDtaUltimaConsulta(new Date());
								tabPropostaRepository.save(tabProposta);
								
								System.out.println("DTA FIM: "+atual.getTxNumeroDta());
								
								
								//Se tiver concluido o transito envia novamente
								/*
								if (fluxo.getConclusaoTransito() != null) {
								 txEnvioMantran = interfaceMantranService.InterfaceMantranDTA(tabProposta.getCdProposta()); 
									System.out.println("json DTA Mantran: "+txEnvioMantran);
									if (txEnvioMantran.contains("ID")) {
										JSONObject json = new JSONObject(txEnvioMantran);
										if (!json.isNull("ID")) {							
												
										    tabProposta.setCdIdMantran(json.getInt("ID"));
										    
										}else if (!json.isNull("MENSAGEM_ERRO")) {																		
											tabProposta.setTxErroMantran(json.getString("MENSAGEM_ERRO"));
										}else {									
											tabProposta.setTxErroMantran("Erro resquest DTA Mantran");
										}
									}
								}
									*/
								// System.out.println(atual.getTxProposta());

								// Fluxo
								if (ckFluxo) {
									RoboExtratoDta(tabProposta.getCdProposta(), 1, tabProposta.getTxNumeroDta());
									RoboExtratoDta(tabProposta.getCdProposta(), 3, tabProposta.getTxNumeroDta());
								}

								// Desembaraço
								if (ckDesembaraco) {
									RoboExtratoDta(tabProposta.getCdProposta(), 2, tabProposta.getTxNumeroDta());
								}
								
								
								
							}
						}
					}
					Thread.sleep(2000);

				}
			}
		  //}
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
			System.out.println("Erro Robo DTA: "+e.getMessage());
		}

	}

	private void AdicionaEmail(Integer cdProposta, String txEmail, Date dtEnvio, Integer ckEnviado) {

		try {
			TabPropostaEnvioEmailTaxaObj Tab = new TabPropostaEnvioEmailTaxaObj();
			Tab.setCdProposta(cdProposta);
			Tab.setCkEnviado(ckEnviado);
			Tab.setDtEnvio(dtEnvio);
			Tab.setTxEmail(txEmail);

			tabPropostaEnvioEmailTaxaService.gravar(Tab);
		} catch (Exception ex) {

			System.out.println("Erro gravação email log" + ex.getMessage());
		}
	}

	private FluxoDeclaracaoTransito consultarDta(String txNumeroDta) {

		FluxoDeclaracaoTransito jsonObj = null;

		try {
			String json = restconnect
					.enviaRestGet("http://amalog.com.br:8081/transporte/aduaneiro/fluxo/declaracao/" + txNumeroDta);

			ObjectMapper mapper = new ObjectMapper();

			jsonObj = mapper.readValue(json, FluxoDeclaracaoTransito.class);

			return jsonObj;

		} catch (Exception ex) {
			System.out.println("Erro envio fluxo DTA "+txNumeroDta);
		}

		return jsonObj;

	}

	public void RoboExtratoDta(Integer cdProposta, Integer cdTipo, String txNumeroDta) {

		try {
			String txUrl = "http://amalog.com.br:8081/transporte/aduaneiro/extrato/fluxo/"
					+ GeneralUtil.setTiranaoNumero(txNumeroDta);
			String txNomeArquivo = "dtafluxo" + GeneralUtil.setTiranaoNumero(txNumeroDta) + ".pdf";
			Integer cdClassificacao = 28; // Fluxo
			if (cdTipo == 2) { // Desembaraço
				txUrl = "http://amalog.com.br:8081/transporte/aduaneiro/extrato/certificado/desembaraco/"
						+ GeneralUtil.setTiranaoNumero(txNumeroDta);
				cdClassificacao = 29;
				txNomeArquivo = "dtadesembaraco" + GeneralUtil.setTiranaoNumero(txNumeroDta) + ".pdf";
			} else if (cdTipo == 3) { // Declaracao
				txUrl = "http://amalog.com.br:8081/transporte/aduaneiro/extrato/declaracao/"
						+ GeneralUtil.setTiranaoNumero(txNumeroDta);
				cdClassificacao = 34;
				txNomeArquivo = "dtadeclaracao" + GeneralUtil.setTiranaoNumero(txNumeroDta) + ".pdf";
			}

			URL url = new URL(txUrl);
			String pathDir = "/Amalog/files/propostas/" + cdProposta + "/";

			File file = new File(pathDir);

			if (!file.exists()) {
				file.mkdirs();
			}

			file = new File(pathDir + txNomeArquivo);

			if (file.exists()) {
				file.delete();
			}

			InputStream is = url.openStream();
			FileOutputStream fos = new FileOutputStream(file);

			int bytes = 0;

			boolean ckIs = false;

			while ((bytes = is.read()) != -1) {
				fos.write(bytes);
				ckIs = true;
			}

			is.close();

			fos.close();

			if (ckIs) {

				TabPropostaDocumentosObj tabP = tabPropostaDocumentosRepository
						.findByCdPropostaAndTabDocumentosClassificacaoObjCdDoctoClassificacao(cdProposta, cdClassificacao);
				if (tabP == null) {

					TabPropostaDocumentosObj tab = new TabPropostaDocumentosObj();
					tab.setCdProposta(cdProposta);
					TabDocumentosClassificacaoObj tabDocto = new TabDocumentosClassificacaoObj();
					tabDocto.setCdDoctoClassificacao(cdClassificacao);
					tab.setTabDocumentosClassificacaoObj(tabDocto);
					TabUsuarioObj tabUsu = new TabUsuarioObj();
					tabUsu.setCdUsuario(2);
					tab.setTabUsuarioCriacaoObj(tabUsu);
					tab.setDtCriacao(new Date());
					tab.setCdValidado(1);
					tab.setTxNomeArquivo(txNomeArquivo);
					tab.setTxUrl("/proposta/download/" + cdProposta + "?txNomeArquivo=" + txNomeArquivo);
					tabPropostaDocumentosRepository.save(tab);

				} else {
					tabP.setCdProposta(cdProposta);
					TabDocumentosClassificacaoObj tabDocto = new TabDocumentosClassificacaoObj();
					tabDocto.setCdDoctoClassificacao(cdClassificacao);
					tabP.setTabDocumentosClassificacaoObj(tabDocto);
					TabUsuarioObj tabUsu = new TabUsuarioObj();
					tabUsu.setCdUsuario(2);
					tabP.setTabUsuarioCriacaoObj(tabUsu);
					tabP.setDtCriacao(new Date());
					tabP.setCdValidado(1);
					tabP.setTxNomeArquivo(txNomeArquivo);
					tabP.setTxUrl("/proposta/download/" + cdProposta + "?txNomeArquivo=" + txNomeArquivo);
					tabPropostaDocumentosRepository.save(tabP);

				}

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());

		}

	}

	
	public FluxoDeclaracaoEntrada consultaValoresDta(String txNumeroDta) {

		FluxoDeclaracaoEntrada jsonObj = null;

		try {
			String json = restconnect
					.enviaRestGet("http://amalog.com.br:8081/transporte/aduaneiro/fluxo/declaracao/entrada/" + GeneralUtil.TiraNaonumero(txNumeroDta));

			ObjectMapper mapper = new ObjectMapper();

			jsonObj = mapper.readValue(json, FluxoDeclaracaoEntrada.class);

			return jsonObj;

		} catch (Exception ex) {

		}

		return jsonObj;

	}
	
	
}
