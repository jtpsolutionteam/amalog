package br.com.jtpsolution.modulos.cadastros.dedicado.controller;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.cadastros.varios.TabDedicadoObj;
import br.com.jtpsolution.dao.cadastros.varios.TabDestinoObj;
import br.com.jtpsolution.dao.cadastros.varios.TabOrigemObj;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabDestinoRepository;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabOrigemRepository;
import br.com.jtpsolution.mensagens.TabRetornoSucessoObj;
import br.com.jtpsolution.modulos.cadastros.dedicado.bean.TabFiltroDedicadoObj;
import br.com.jtpsolution.modulos.cadastros.dedicado.service.TabDedicadoService;
import br.com.jtpsolution.modulos.util.geralcontroller.allController;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.regras.RegrasBean;

@Controller
@RequestMapping("/dedicado")
public class TabDedicadoController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabDedicadoService tabService;
	
	@Autowired
	private RegrasBean regrasBean;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_CADASTROS+"/dedicado/dedicado";
	
	
 	
 	@GetMapping
	public ModelAndView pesquisa(TabFiltroDedicadoObj tabFiltroDedicadoObj, HttpServletRequest request) {
		
		tabFiltroDedicadoObj = verificaFiltro(tabFiltroDedicadoObj, request);
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject(new TabDedicadoObj());			
		mv.addObject("tabFiltroDedicadoObj", tabFiltroDedicadoObj);
		mv.addObject("listadados", retornoFiltro(tabFiltroDedicadoObj));
		
		
		return mv;
	}
	
	private List<?> retornoFiltro(TabFiltroDedicadoObj tabFiltroDedicadoObj) {
 		
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(TabDedicadoObj.class);
		
		adicionarFiltro(tabFiltroDedicadoObj, criteria);
 		
 		return criteria.list();
 	}


	private TabFiltroDedicadoObj verificaFiltro(TabFiltroDedicadoObj tabFiltroDedicadoObj, HttpServletRequest request) {
		
		try {
			
			if (Validator.isObjetoNull(tabFiltroDedicadoObj)) {
				if (request.getSession().getAttribute(getClass().getSimpleName()) != null) {					
					String jsonFiltro = request.getSession().getAttribute(getClass().getSimpleName()).toString();
					ObjectMapper mapper = new ObjectMapper();
					tabFiltroDedicadoObj = mapper.readValue(jsonFiltro, TabFiltroDedicadoObj.class);	
				}
			}else {
				request.getSession().setAttribute(getClass().getSimpleName(),
						GeneralParser.convertObjJson(tabFiltroDedicadoObj));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return tabFiltroDedicadoObj;
	}

	
	private void adicionarFiltro(TabFiltroDedicadoObj filtro, Criteria criteria) {
		
		  boolean ckFiltro = false;
		  
		  if (filtro != null) {
			  
			  if (!StringUtils.isEmpty(filtro.getCdDestinoFiltro())) {
				  criteria.add(Restrictions.eq("tabDestinoObj.cdDestino", filtro.getCdDestinoFiltro()));
				  ckFiltro = true;
			  }
			  
			  
			  //criteria.addOrder(Order.asc("tabDestinoObj.txDestino"));
			  
			 
		  }
	  }
 	
	
	@RequestMapping(value = "/gravarmodal", method = RequestMethod.POST)
	public @ResponseBody List<?> gravarmodal(@Validated TabDedicadoObj tabDedicadoObj, Errors erros, HttpServletRequest request) {

		List<ObjectError> error = new ArrayList<ObjectError>();
		if (erros.hasErrors()) {
		  //mv.addObject("listaErros", erros.getAllErrors());
		  error.addAll(erros.getAllErrors());
		  return error;
		}else {
			error = regrasBean.verificaregras(getClass().getSimpleName(),request);
			if (!error.isEmpty()) {
				  return error;	
			}	  
		}
		
	    TabDedicadoObj Tab = tabService.gravar(tabDedicadoObj);
	   
	    List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();		
	    TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
	    tSuccess.setCd_mensagem(1);
	    tSuccess.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
	    success.add(tSuccess);
	    
		return success;
	}
	
	
	@RequestMapping(value = "/novo")
	public ModelAndView novo(HttpServletRequest request) {
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		mv.addObject(new TabDedicadoObj());
		mv.addObject("tabFiltroDedicadoObj", verificaFiltro(new TabFiltroDedicadoObj(), request));
		mv.addObject("listadados", retornoFiltro(verificaFiltro(new TabFiltroDedicadoObj(), request)));
		mv.addObject("txEdit", "edit");
		return mv;
	}
	

	@RequestMapping(value = "/consultar/{cdDedicado}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable Integer cdDedicado, HttpServletRequest request) {
	
	   TabDedicadoObj TabView = tabService.consultar(cdDedicado);
	   TabFiltroDedicadoObj tF = verificaFiltro(new TabFiltroDedicadoObj(), request);

	   
		ModelAndView mv = new ModelAndView(txUrlTela);	
		if (TabView != null) {
		  mv.addObject("tabFiltroDedicadoObj",tF);	
		  mv.addObject("tabDedicadoObj",TabView);
		  mv.addObject("listadados", retornoFiltro(verificaFiltro(tF, request)));
		  mv.addObject("txEdit", "edit");
		}else {
		  mv.addObject("tabFiltroDedicadoObj",tF);	
		  mv.addObject(new TabDedicadoObj());	
		  mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
		  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}
	
	
	@Autowired
	private TabOrigemRepository tabOrigemRepository;
	
	@ModelAttribute("selectcdorigem")
	public List<TabOrigemObj> selectcdorigem() {
		return tabOrigemRepository.findAll(GeneralParser.sortByIdAsc("txOrigem"));
	}

	
	@Autowired
	private TabDestinoRepository tabDestinoRepository;
	
	@ModelAttribute("selectcddestino")
	public List<TabDestinoObj> selectcddestino() {
		return tabDestinoRepository.findAll(GeneralParser.sortByIdAsc("txDestino"));
	}

	
}
