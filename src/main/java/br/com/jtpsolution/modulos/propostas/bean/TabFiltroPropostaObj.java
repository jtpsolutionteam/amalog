package br.com.jtpsolution.modulos.propostas.bean;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TabFiltroPropostaObj {

	private String txPropostaFiltro;
	
	private String txTipoCampoFiltro;
	
	private Integer ckAnvisaFiltro;
	
	private Integer ckImoPerigosoFiltro;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtPropostaInicialFiltro;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtPropostaFinalFiltro;

	private Integer cdStatusFiltro;
	
	private String txStatusCondicaoFiltro;
	
	

	
	
}
