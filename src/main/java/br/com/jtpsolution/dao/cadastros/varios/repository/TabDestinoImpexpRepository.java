package br.com.jtpsolution.dao.cadastros.varios.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.cadastros.varios.TabDestinoImpexpObj;



public interface TabDestinoImpexpRepository extends JpaRepository<TabDestinoImpexpObj, Integer> {


	@Query("select t from TabDestinoImpexpObj t where t.cdImpexp = ?1 and t.tabDestinoObj.ckAtivo = ?2 order by t.tabDestinoObj.txDestino")
	List<TabDestinoImpexpObj> findByCkAtivoQuery(Integer cdImpexp, Integer ckAtivo);
	
	
}