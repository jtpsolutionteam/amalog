package br.com.jtpsolution.dao.propostas;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.jtpsolution.Constants;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_proposta_log_boleto", schema = Constants.SCHEMA)
public class TabPropostaLogBoletoObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_proposta_log_boleto")
	// @NotNull(message = "PropostaLogBoleto campo obrigatório!")
	private Integer cdPropostaLogBoleto;

	@Column(name = "cd_proposta")
	// @NotNull(message = "Proposta campo obrigatório!")
	private Integer cdProposta;

	@Column(name = "dt_envio")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtEnvio;

	@Column(name = "dt_vencto")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtVencto;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
	}

}
