package br.com.jtpsolution.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.cadastros.varios.TabOrigemObj;



public interface TabOrigemRepository extends JpaRepository<TabOrigemObj, Integer> {



}