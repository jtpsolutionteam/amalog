package br.com.jtpsolution.dao.cadastros.varios;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;

import br.com.jtpsolution.Constants;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_dedicado", schema = Constants.SCHEMA)
public class TabDedicadoObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_dedicado")
	// @NotNull(message = "Dedicado campo obrigatório!")
	private Integer cdDedicado;

	@ManyToOne
	@JoinColumn(name = "cd_origem")
	private TabOrigemObj tabOrigemObj;

	@ManyToOne
	@JoinColumn(name = "cd_destino")
	private TabDestinoObj tabDestinoObj;

	@Column(name = "vl_de")	
	@NumberFormat(pattern = "#,##0.00")
	private double vlDe;

	@Column(name = "vl_ate")
	@NumberFormat(pattern = "#,##0.00")
	private double vlAte;

	@Column(name = "vl_valor")
	@NumberFormat(pattern = "#,##0.00")
	private double vlValor;

	@Column(name = "vl_peso")
	@NumberFormat(pattern = "#,##0.00")
	private double vlPeso;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
	}

}
