package br.com.jtpsolution.modulos.robodta.obj;

import java.io.Serializable;
import java.time.LocalDateTime;


public class IntegridadeTransito implements Serializable{

	private String txResultado;
	private String dtIntegridadeTransito;
	private NomeMatricula responsavel;
	private String txInterveniente;
	public String getTxResultado() {
		return txResultado;
	}
	public void setTxResultado(String txResultado) {
		this.txResultado = txResultado;
	}
	public String getDtIntegridadeTransito() {
		return dtIntegridadeTransito;
	}
	public void setDtIntegridadeTransito(String dtIntegridadeTransito) {
		this.dtIntegridadeTransito = dtIntegridadeTransito;
	}
	public NomeMatricula getResponsavel() {
		return responsavel;
	}
	public void setResponsavel(NomeMatricula responsavel) {
		this.responsavel = responsavel;
	}
	public String getTxInterveniente() {
		return txInterveniente;
	}
	public void setTxInterveniente(String txInterveniente) {
		this.txInterveniente = txInterveniente;
	}
	
	
	
}