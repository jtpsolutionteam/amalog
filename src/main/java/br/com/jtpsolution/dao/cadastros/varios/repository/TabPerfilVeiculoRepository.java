package br.com.jtpsolution.dao.cadastros.varios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.cadastros.varios.TabPerfilVeiculoObj;



public interface TabPerfilVeiculoRepository extends JpaRepository<TabPerfilVeiculoObj, Integer> {



}