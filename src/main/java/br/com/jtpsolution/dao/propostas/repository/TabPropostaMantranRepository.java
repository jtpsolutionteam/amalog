package br.com.jtpsolution.dao.propostas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.propostas.TabPropostaMantranObj;



public interface TabPropostaMantranRepository extends JpaRepository<TabPropostaMantranObj, Integer> {



}