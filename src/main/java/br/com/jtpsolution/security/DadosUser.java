package br.com.jtpsolution.security;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import br.com.jtpsolution.dao.administracao.vw.VwTabMenuObj;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DadosUser extends User{
	private Integer cdUsuario;
	private Integer cdGrupoVisao;
	private String txApelido;
	private String txEmail;	
	private Integer cdGrupoAcesso;
	private String cdIdioma;
	private List<VwTabMenuObj> listTabMenu;
	private Integer cdEmpresa;
	
	private static final long serialVersionUID = -8062200468272967425L;
	
	public DadosUser(String username, String password, Collection<? extends GrantedAuthority> authorities, Integer cdUsuario, Integer cdGrupoVisao, String txApelido, String txEmail, Integer cdGrupoAcesso, List<VwTabMenuObj> listTabMenu, String cdIdioma, Integer cdEmpresa) {
		super(username, password, authorities);
		this.cdUsuario = cdUsuario;
		this.cdGrupoVisao = cdGrupoVisao;
		this.txApelido = txApelido;
		this.txEmail = txEmail;
		this.cdGrupoAcesso = cdGrupoAcesso;
		this.listTabMenu = listTabMenu;
		this.cdIdioma = cdIdioma;
		this.cdEmpresa = cdEmpresa;
	}
	

}
