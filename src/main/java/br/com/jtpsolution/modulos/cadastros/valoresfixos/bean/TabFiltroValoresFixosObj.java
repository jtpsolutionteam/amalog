package br.com.jtpsolution.modulos.cadastros.valoresfixos.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TabFiltroValoresFixosObj {

	private Integer cdEmpresaFiltro;
	
	private Integer cdImpExpFiltro;
	
	private String txTipoFiltro;
	
}
