package br.com.jtpsolution.dao.propostas;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "vw_tab_proposta", schema = Constants.SCHEMA)
public class VwTabPropostaObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_proposta")
	// @NotNull(message = "Proposta campo obrigatório!")
	private Integer cdProposta;

	@Column(name = "cd_usuario")
	// @NotNull(message = "Usuario campo obrigatório!")
	private Integer cdUsuario;

	@Column(name = "cd_cliente")
	// @NotNull(message = "Agente campo obrigatório!")
	private Integer cdCliente;

	@Column(name = "dt_proposta")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtProposta;

	@Column(name = "tx_proposta")
	// @NotEmpty(message = "Proposta campo obrigatório!")
	@Size(max = 15, message = "Proposta tamanho máximo de 15 caracteres")
	private String txProposta;

	@Column(name = "cd_status")
	// @NotNull(message = "Status campo obrigatório!")
	private Integer cdStatus;

	@Column(name = "cd_tipo_carregamento")
	// @NotNull(message = "TipoCarregamento campo obrigatório!")
	private Integer cdTipoCarregamento;
	
	@Column(name = "cd_importador_direto_coloader")
	private Integer cdImportadorDiretoColoader;

	@Column(name = "cd_destino")
	// @NotNull(message = "Destino campo obrigatório!")
	private Integer cdDestino;

	@Column(name = "vl_valor")
	// @NotEmpty(message = "Valor campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlValor;

	@Column(name = "cd_perfil_veiculo")
	// @NotNull(message = "PerfilVeiculo campo obrigatório!")
	private Integer cdPerfilVeiculo;

	@Column(name = "vl_peso")
	// @NotEmpty(message = "Peso campo obrigatório!")
	@NumberFormat(pattern = "#,##0.000")
	private BigDecimal vlPeso;

	@Column(name = "vl_m3")
	// @NotEmpty(message = "M3 campo obrigatório!")
	@NumberFormat(pattern = "#,##0.000")
	private BigDecimal vlM3;

	@Column(name = "vl_base")
	// @NotEmpty(message = "Base campo obrigatório!")
	@NumberFormat(pattern = "#,##0.000")
	private BigDecimal vlBase;

	@Column(name = "vl_diferenca")
	// @NotEmpty(message = "Diferenca campo obrigatório!")
	@NumberFormat(pattern = "#,##0.000")
	private BigDecimal vlDiferenca;

	@Column(name = "vl_final")
	// @NotEmpty(message = "Final campo obrigatório!")
	@NumberFormat(pattern = "#,##0.000")
	private BigDecimal vlFinal;

	@Column(name = "vl_carga")
	// @NotEmpty(message = "Carga campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlCarga;

	@Column(name = "vl_frete_peso")
	// @NotEmpty(message = "FretePeso campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlFretePeso;

	@Column(name = "vl_frete")
	// @NotEmpty(message = "Frete campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlFrete;

	@Column(name = "vl_despacho")
	// @NotEmpty(message = "Despacho campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlDespacho;

	@Column(name = "vl_pedagio")
	// @NotEmpty(message = "Pedagio campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlPedagio;

	@Column(name = "vl_taxa_entrega")
	// @NotEmpty(message = "TaxaEntrega campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlTaxaEntrega;

	@Column(name = "vl_gris")
	// @NotEmpty(message = "Gris campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlGris;

	@Column(name = "vl_estacionamento")
	// @NotEmpty(message = "Estacionamento campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlEstacionamento;

	@Column(name = "vl_imo_carga")
	// @NotEmpty(message = "ImoCarga campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlImoCarga;

	@Column(name = "vl_impostos_suspensos")
	// @NotEmpty(message = "ImpostosSuspensos campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlImpostosSuspensos;

	@Column(name = "vl_frete_valor")
	// @NotEmpty(message = "FreteValor campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlFreteValor;

	@Column(name = "vl_cat")
	// @NotEmpty(message = "Cat campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlCat;

	@Column(name = "vl_ademe")
	// @NotEmpty(message = "Ademe campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlAdeme;

	@Column(name = "vl_taxa_coleta")
	// @NotEmpty(message = "TaxaColeta campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlTaxaColeta;

	@Column(name = "vl_estadia")
	// @NotEmpty(message = "Estadia campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlEstadia;

	@Column(name = "vl_emissao_dta")
	// @NotEmpty(message = "EmissaoDta campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlEmissaoDta;

	@Column(name = "vl_monitoramento")
	// @NotEmpty(message = "Monitoramento campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlMonitoramento;

	@Column(name = "vl_devolucao_container")
	// @NotEmpty(message = "DevolucaoContainer campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlDevolucaoContainer;

	@Column(name = "vl_advalorem")
	// @NotEmpty(message = "Advalorem campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlAdvalorem;

	@Column(name = "vl_itr")
	// @NotEmpty(message = "Itr campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlItr;

	@Column(name = "vl_outros")
	// @NotEmpty(message = "Outros campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlOutros;

	@Column(name = "vl_descarga")
	// @NotEmpty(message = "Descarga campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlDescarga;

	@Column(name = "vl_escolta")
	// @NotEmpty(message = "Escolta campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlEscolta;

	@Column(name = "vl_ajudante")
	// @NotEmpty(message = "Ajudante campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlAjudante;

	@Column(name = "vl_imo_adesivagem")
	// @NotEmpty(message = "ImoAdesivagem campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlImoAdesivagem;

	@Column(name = "vl_subtotal")
	// @NotEmpty(message = "Subtotal campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlSubtotal;

	@Column(name = "vl_base_icms")
	// @NotEmpty(message = "BaseIcms campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlBaseIcms;

	@Column(name = "vl_aliq_icms")
	// @NotEmpty(message = "AliqIcms campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlAliqIcms;

	@Column(name = "vl_icms")
	// @NotEmpty(message = "Icms campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private BigDecimal vlIcms;

	@Column(name = "dt_validade")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtValidade;

	@Column(name = "tx_cliente")
	// @NotEmpty(message = "Cliente campo obrigatório!")
	@Size(max = 100, message = "Cliente tamanho máximo de 100 caracteres")
	private String txCliente;

	@Column(name = "cd_grupo_acesso")
	// @NotNull(message = "GrupoAcesso campo obrigatório!")
	private Integer cdGrupoAcesso;

	@Column(name = "tx_usuario")
	// @NotEmpty(message = "Usuario campo obrigatório!")
	@Size(max = 100, message = "Usuario tamanho máximo de 100 caracteres")
	private String txUsuario;
	
	@Column(name = "tx_status")
	private String txStatus;
	
	@Column(name = "ck_imo_perigoso")
	@NotNull(message = "IMO Carga Perigosa campo obrigatório!")
	private Integer ckImoPerigoso;
	
	@Column(name = "ck_nao_remonte")
	@NotNull(message = "Não Remonte campo obrigatório!")
	private Integer ckNaoRemonte;
	
	@Column(name = "tx_hash_proposta")
	private String txHashProposta;

	@Column(name = "tx_mercadoria")
	@NotEmpty(message = "Descrição da Mercadoria campo obrigatório!")
	private String txMercadoria;
	
	@Column(name = "tx_importador") 
	//@NotEmpty(message = "Importador campo obrigatório!")
	@Size(max = 100, message = "Importador tamanho máximo de 100 caracteres") 
	private String txImportador; 
	 
	@Column(name = "tx_cnpj") 
	//@NotEmpty(message = "Cnpj campo obrigatório!")
	@Size(max = 20, message = "Cnpj tamanho máximo de 20 caracteres") 
	private String txCnpj; 
	 
	@Column(name = "tx_endereco") 
	//@NotEmpty(message = "Endereco campo obrigatório!")
	@Size(max = 200, message = "Endereco tamanho máximo de 200 caracteres") 
	private String txEndereco;
	
	@Column(name = "tx_numero") 
	//@NotEmpty(message = "Número campo obrigatório!")
	@Size(max = 6, message = "Número tamanho máximo de 6 caracteres") 
	private String txNumero; 
	 
	@Column(name = "tx_cidade") 
	//@NotEmpty(message = "Cidade campo obrigatório!")
	@Size(max = 50, message = "Cidade tamanho máximo de 50 caracteres") 
	private String txCidade; 
	 
	@Column(name = "tx_uf") 
	//@NotEmpty(message = "Uf campo obrigatório!")
	@Size(max = 2, message = "Uf tamanho máximo de 2 caracteres") 
	private String txUf; 
	 
	@Column(name = "tx_bairro") 
	//@NotEmpty(message = "Bairro campo obrigatório!")
	@Size(max = 50, message = "Bairro tamanho máximo de 50 caracteres") 
	private String txBairro; 
	 
	@Column(name = "tx_cep") 
	//@NotEmpty(message = "Cep campo obrigatório!")
	@Size(max = 10, message = "Cep tamanho máximo de 10 caracteres") 
	private String txCep; 
	 
	@Column(name = "tx_nome_programacao") 
	//@NotEmpty(message = "NomeProgramacao campo obrigatório!")
	@Size(max = 100, message = "Nome Programação tamanho máximo de 100 caracteres") 
	private String txNomeProgramacao; 
	 
	@Column(name = "tx_email_programacao") 
	//@NotEmpty(message = "EmailProgramacao campo obrigatório!")
	@Size(max = 100, message = "EmailProgramação tamanho máximo de 100 caracteres") 
	private String txEmailProgramacao; 
	 
	@Column(name = "tx_nome_bandeirantes") 
	//@NotEmpty(message = "NomeBandeirantes campo obrigatório!")
	@Size(max = 100, message = "Nome Bandeirantes tamanho máximo de 100 caracteres") 
	private String txNomeBandeirantes; 
	 
	@Column(name = "tx_email_bandeirantes") 
	//@NotEmpty(message = "EmailBandeirantes campo obrigatório!")
	@Size(max = 100, message = "Email Bandeirantes tamanho máximo de 100 caracteres") 
	private String txEmailBandeirantes; 
	 
	@Column(name = "tx_nome_followup") 
	//@NotEmpty(message = "NomeFollowup campo obrigatório!")
	@Size(max = 100, message = "Nome Followup tamanho máximo de 100 caracteres") 
	private String txNomeFollowup; 
	 
	@Column(name = "tx_email_followup") 
	//@NotEmpty(message = "EmailFollowup campo obrigatório!")
	@Size(max = 100, message = "Email Followup tamanho máximo de 100 caracteres") 
	private String txEmailFollowup; 
	
	@Column(name = "cd_usuario_aceite") 	
	private Integer cdUsuarioAceite; 
	
	@Column(name = "dt_aceite") 	
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtAceite;
	
	@Column(name = "tx_usuario_aceite") 	
	private String txUsuarioAceite;
	
	@Column(name = "vl_frete_peso_sdesconto")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlFretePesoSdesconto;
	
	@Column(name = "vl_desconto")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlDesconto;
	
	@Column(name = "tx_terminal_mar")	
	private String txTerminalMar;
	
	@Column(name = "ck_desistencia_vistoria")	
	private Integer ckDesistenciaVistoria;
	
	@Column(name = "tx_lote")	
	private String txLote;
	
	@Column(name = "tx_nome_pagador")	
	private String txNomePagador;

	@Column(name = "tx_cnpj_importador")	
	private String txCnpjImportador;

	@Column(name = "dt_baixa_pagto_armazenagem")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtBaixaPagtoArmazenagem;
	
	@Column(name = "ck_upload_doctos")	
	private Integer ckUploadDoctos;
	
	@Column(name = "tx_perfil_veiculo")	
	private String txPerfilVeiculo;
		
	@Column(name = "vl_altura")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlAltura;

	@Column(name = "vl_largura")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlLargura;
	
	@Column(name = "vl_peso_extrato_desova")
	@NumberFormat(pattern = "#,##0.000")
	private Double vlPesoExtratoDesova;
	
	@Column(name = "vl_mercadoria_invoice")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlMercadoriaInvoice;
	
	@Column(name = "tx_destino")
	private String txDestino;
	
	@Column(name = "vl_comprimento")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlComprimento;
	
	@Column(name = "vl_m3_pack")
	@NumberFormat(pattern = "#,##0.000")
	private Double vlM3Pack;
	
	@Column(name = "vl_frete_previsto")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlFretePrevisto;
	
	@Column(name = "dt_liberacao_documentos")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtLiberacaoDocumentos;
	
	@Column(name = "tx_numero_dta")
	private String txNumeroDta;

	@Column(name = "tx_bl")
	private String txBl;
	
	@Column(name = "ck_frete_exclusivo")
	private Integer ckFreteExclusivo;

	@Column(name = "dt_prev_carregamento")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtPrevCarregamento;
	
	@Column(name = "tx_nome_upload_docto_bl")
	private String txNomeUploadDoctoBl;
	
	@Column(name = "tx_email_upload_docto_bl")
	private String txEmailUploadDoctoBl;
	
	@Column(name = "tx_mbl") 
	//@NotEmpty(message = "Mbl campo obrigatório!")
	@Size(max = 30, message = "Mbl tamanho máximo de 30 caracteres") 
	private String txMbl; 
	 
	@Column(name = "dt_dta_solicitacao") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaSolicitacao; 
	 
	@Column(name = "dt_dta_registro") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaRegistro; 
	 
	@Column(name = "dt_dta_carregamento") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaCarregamento; 
	 
	@Column(name = "dt_dta_parametrizacao") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaParametrizacao; 
	 
	@Column(name = "tx_dta_canal") 
	//@NotEmpty(message = "DtaCanal campo obrigatório!")
	@Size(max = 30, message = "DtaCanal tamanho máximo de 30 caracteres") 
	private String txDtaCanal; 
	 
	@Column(name = "dt_dta_desembaraco") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaDesembaraco; 
	 
	@Column(name = "dt_dta_inicio_transito") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaInicioTransito; 
	 
	@Column(name = "dt_dta_chegada_transito") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaChegadaTransito; 
	 
	@Column(name = "dt_dta_conclusao_transito") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaConclusaoTransito; 

	@Column(name = "dt_dta_ultima_consulta") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtDtaUltimaConsulta; 
	
	@Column(name = "dt_averbacao") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtAverbacao; 
	
	@Column(name = "tx_check_upload_doctos") 	
	private String txCheckUploadDoctos;

	@Column(name = "tx_tipo_carregamento") 	
	private String txTipoCarregamento;

	@Column(name = "ck_quimico") 	
	private Integer ckQuimico;

	@Column(name = "tx_boleto_id") 
	private String txBoletoId;
	
	@Column(name = "dt_boleto_vencto") 
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtBoletoVencto;
	
	@Column(name = "dt_boleto_pagto") 
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtBoletoPagto;


	@Column(name = "tx_boleto_linha_digitavel") 
	private String txBoletoLinhaDigitavel;

	@Column(name = "tx_boleto_url") 
	private String txBoletoUrl;
	
	@Column(name = "tx_boleto_erros") 
	private String txBoletoErros;

	@Column(name = "tx_iugu_retornos") 
	private String txIuguRetornos;
	
	@Column(name = "ck_rota_amalog") 
	private Integer ckRotaAmalog;
	
	@Column(name = "tx_email_taxa_informativo") 
	private String txEmailTaxaInformativo;
	
	@Column(name = "dt_pagto_comissao") 
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	private Date dtPagtoComissao;
	
	@Column(name = "tx_email_pagador") 
	private String txEmailPagador;

	@Column(name = "vl_taxa_informativo")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlTaxaInformativo;
	
	@Column(name = "dt_envio_email_informativo") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtEnvioEmailInformativo; 
	
	@Column(name = "ck_anvisa") 
	private Integer ckAnvisa;
	
	@Column(name = "tx_numero_ce") 
	@Size(max = 15, message = "Numero CE tamanho máximo de 15 caracteres")
	private String txNumeroCe;
	
	@Column(name = "vl_qtde_volume") 
	private Integer vlQtdeVolume;

	@Column(name = "tx_volume") 
	private String txVolume;
	
	@Column(name = "cd_origem") 
	private Integer cdOrigem;

	@Column(name = "tx_origem") 
	private String txOrigem;

	@Column(name = "ck_lote_microled") 
	//@NotNull(message = "ckLoteMicroled campo obrigatório!")
	private Integer ckLoteMicroled; 
	 
	@Column(name = "tx_microled_mercadoria") 
	//@NotEmpty(message = "MicroledMercadoria campo obrigatório!")
	@Size(max = 200, message = "MicroledMercadoria tamanho máximo de 200 caracteres") 
	private String txMicroledMercadoria; 
	 
	@Column(name = "tx_container") 
	//@NotEmpty(message = "Container campo obrigatório!")
	@Size(max = 20, message = "Container tamanho máximo de 20 caracteres") 
	private String txContainer; 
	 
	@Column(name = "dt_inicio_desova") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtInicioDesova; 
	 
	@Column(name = "dt_fim_desova") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtFimDesova; 
	 
	@Column(name = "dt_saida") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtSaida; 
	 
	@Column(name = "cd_termo_avaria") 
	//@NotNull(message = "TermoAvaria campo obrigatório!")
	private Integer cdTermoAvaria; 
	 
	@Column(name = "dt_termo_avaria") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtTermoAvaria; 
	 
	@Column(name = "vl_diferenca_peso") 
	//@NotEmpty(message = "DiferencaPeso campo obrigatório!")
	@NumberFormat(pattern = "#,##0.000") 
	private BigDecimal vlDiferencaPeso; 
	 
	@Column(name = "vl_peso_avaria") 
	//@NotEmpty(message = "PesoAvaria campo obrigatório!")
	@NumberFormat(pattern = "#,##0.000") 
	private BigDecimal vlPesoAvaria; 
	 
	@Column(name = "vl_diferenca_percentual") 
	//@NotEmpty(message = "DiferencaPercentual campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00") 
	private BigDecimal vlDiferencaPercentual;
	
	@Column(name = "tx_viagem") 
	//@NotEmpty(message = "Viagem campo obrigatório!")
	@Size(max = 20, message = "Viagem tamanho máximo de 20 caracteres") 
	private String txViagem; 
	 
	@Column(name = "tx_navio") 
	//@NotEmpty(message = "Navio campo obrigatório!")
	@Size(max = 200, message = "Navio tamanho máximo de 200 caracteres") 
	private String txNavio; 
	 
	@Column(name = "tx_local_atracacao") 
	//@NotEmpty(message = "LocalAtracacao campo obrigatório!")
	@Size(max = 200, message = "LocalAtracacao tamanho máximo de 200 caracteres") 
	private String txLocalAtracacao; 
	 
	@Column(name = "tx_porto_origem") 
	//@NotEmpty(message = "PortoOrigem campo obrigatório!")
	@Size(max = 100, message = "PortoOrigem tamanho máximo de 100 caracteres") 
	private String txPortoOrigem; 
	 
	@Column(name = "tx_regime") 
	//@NotEmpty(message = "Regime campo obrigatório!")
	@Size(max = 45, message = "Regime tamanho máximo de 45 caracteres") 
	private String txRegime; 
	
	@Column(name = "vl_peso_bruto") 
	//@NotEmpty(message = "PesoAvaria campo obrigatório!")
	@NumberFormat(pattern = "#,##0.000") 
	private Double vlPesoBruto; 
	
	@Column(name = "dt_previsao_atracacao") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtPrevisaoAtracacao; 
	
	@Column(name = "dt_atracacao") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtAtracacao; 
	
	@Column(name = "tx_escala") 
	private String txEscala;
	
	@Column(name = "cd_status_averbacao") 
	private Integer cdStatusAverbacao;
	
	@Column(name = "tx_status_averbacao") 
	private String txStatusAverbacao;
	
	@Column(name = "tx_status_consulta_siscarga") 
	private String txStatusConsultaSiscarga;
	
	@Column(name = "tx_situacao_ce_master") 
	private String txSituacaoCeMaster;
	
	@Column(name = "cd_coloader_despachante") 
	private Integer cdColoaderDespachante;

	@Column(name = "tx_coloader_despachante") 
	private String txColoaderDespachante;

	@Column(name = "tx_reserva") 
	private String txReserva;
	
	@Column(name = "tx_due") 
	private String txDue;
	
	@Column(name = "tx_nfs") 
	private String txNfs;
	
	@Column(name = "ck_reconferencia") 
	private Integer ckReconferencia;
	
	@Column(name = "dt_entrada_cd") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtEntradaCd;
	
	@Column(name = "vl_frete_moeda")
	// @NotEmpty(message = "Frete campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlFreteMoeda;
	
	@Column(name = "cd_freehand") 
	private Integer cdFreehand;
	
	@Column(name = "vl_carga_moeda")
	//@NotNull(message = "Vl.Carga campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlCargaMoeda;
	
	@Column(name = "vl_taxa_dollar")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlTaxaDollar;
	
	@Column(name = "tx_freehand")	
	private String txFreehand;
	
	@Column(name = "tx_erro_mantran")	
	private String txErroMantran;
	
	@Column(name = "tx_tipo_proposta") 
	//@NotEmpty(message = "TipoProposta campo obrigatório!")
	@Size(max = 20, message = "TipoProposta tamanho máximo de 20 caracteres") 
	private String txTipoProposta; 
	 
	@Column(name = "cd_origem_ltl") 
	//@NotNull(message = "OrigemLtl campo obrigatório!")
	private Integer cdOrigemLtl; 
	 
	@Column(name = "cd_destino_ltl") 
	//@NotNull(message = "DestinoLtl campo obrigatório!")
	private Integer cdDestinoLtl; 
	 
	@Column(name = "tx_origem_ltl") 
	//@NotEmpty(message = "OrigemLtl campo obrigatório!")
	@Size(max = 45, message = "OrigemLtl tamanho máximo de 45 caracteres") 
	private String txOrigemLtl; 
	 
	@Column(name = "tx_destino_ltl") 
	//@NotEmpty(message = "DestinoLtl campo obrigatório!")
	@Size(max = 45, message = "DestinoLtl tamanho máximo de 45 caracteres") 
	private String txDestinoLtl; 
	
	@Column(name = "tx_di") 
	private String txDi; 
	
	@Column(name = "cd_via_transporte") 
	//@NotNull(message = "ViaTransporte campo obrigatório!")
	private Integer cdViaTransporte; 
	 
	@Column(name = "ck_ajudante") 
	//@NotNull(message = "ckAjudante campo obrigatório!")
	private Integer ckAjudante; 
	 
	@Column(name = "ck_restricao_via_acesso") 
	//@NotNull(message = "ckRestricaoViaAcesso campo obrigatório!")
	private Integer ckRestricaoViaAcesso; 
	 
	@Column(name = "tx_restricao_via_acesso") 
	//@NotEmpty(message = "RestricaoViaAcesso campo obrigatório!")
	@Size(max = 300, message = "RestricaoViaAcesso tamanho máximo de 300 caracteres") 
	private String txRestricaoViaAcesso; 
	
	@Column(name = "ck_restricao_local_descarga") 
	//@NotNull(message = "ckRestricaoViaAcesso campo obrigatório!")
	private Integer ckRestricaoLocalDescarga; 
	 
	@Column(name = "tx_restricao_local_descarga") 
	//@NotEmpty(message = "RestricaoViaAcesso campo obrigatório!")
	@Size(max = 300, message = "RestricaoLocalDescarga tamanho máximo de 300 caracteres") 
	private String txRestricaoLocalDescarga; 
	
	@Column(name = "tx_cnpj_importador_microled") 
	private String txCnpjImportadorMicroled;
	
	@Column(name = "tx_importador_microled") 
	private String txImportadorMicroled;
	
	@Column(name = "ck_boleto_cancelado") 
	private Integer ckBoletoCancelado;

	@Column(name = "cd_empresa") 
	private Integer cdEmpresa;
	
	@Column(name = "dt_desconsolidacao") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtDesconsolidacao;
	
	@Column(name = "dt_envio_averbacao") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtEnvioAverbacao;
	
	@Column(name = "tx_referencia") 
	private String txReferencia;

	@Column(name = "dt_doctos_liberados_registro_dta") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtDoctosLiberadosRegistroDta;
	
	@Column(name = "dt_pendencia_dta_carregamento") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtPendenciaDtaCarregamento;

	@Column(name = "tx_pendencia_dta_carregamento") 
	private String txPendenciaDtaCarregamento;
	
	@Column(name = "dt_armazenagem") 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtArmazenagem;
	
	@Column(name = "tx_mapa") 
	//@NotEmpty(message = "Mapa campo obrigatório!")
	//@Size(max = 3, message = "Mapa tamanho máximo de 3 caracteres") 
	private String txMapa; 
	 
	@Column(name = "tx_gr_paga") 
	//@NotEmpty(message = "GrPaga campo obrigatório!")
	//@Size(max = 3, message = "GrPaga tamanho máximo de 3 caracteres") 
	private String txGrPaga; 
	 
	@Column(name = "tx_siscarga") 
	//@NotEmpty(message = "Siscarga campo obrigatório!")
	//@Size(max = 3, message = "Siscarga tamanho máximo de 3 caracteres") 
	private String txSiscarga; 
	 
	@Column(name = "tx_bloqueio_bl") 
	//@NotEmpty(message = "BloqueioBl campo obrigatório!")
	//@Size(max = 3, message = "BloqueioBl tamanho máximo de 3 caracteres") 
	private String txBloqueioBl; 
	 
	@Column(name = "tx_bloqueio_cntr") 
	//@NotEmpty(message = "BloqueioCntr campo obrigatório!")
	//@Size(max = 3, message = "BloqueioCntr tamanho máximo de 3 caracteres") 
	private String txBloqueioCntr; 
	 
	@Column(name = "tx_icms_sefaz") 
	//@NotEmpty(message = "IcmsSefaz campo obrigatório!")
	//@Size(max = 3, message = "IcmsSefaz tamanho máximo de 3 caracteres") 
	private String txIcmsSefaz; 
	
	@Column(name = "cd_id_mantran")	
	private Integer cdIdMantran;
	
	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txProposta))
			txProposta = txProposta.toUpperCase();
		if (!Validator.isBlankOrNull(txCliente))
			txCliente = txCliente.toUpperCase();
		if (!Validator.isBlankOrNull(txUsuario))
			txUsuario = txUsuario.toUpperCase();
	}



	
}
