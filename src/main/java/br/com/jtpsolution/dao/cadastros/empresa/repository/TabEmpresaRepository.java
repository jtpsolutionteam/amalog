package br.com.jtpsolution.dao.cadastros.empresa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jtpsolution.dao.cadastros.empresa.TabEmpresaObj;



public interface TabEmpresaRepository extends JpaRepository<TabEmpresaObj, Integer> {



}