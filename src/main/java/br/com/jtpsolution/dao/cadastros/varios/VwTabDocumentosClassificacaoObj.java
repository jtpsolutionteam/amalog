package br.com.jtpsolution.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.util.Validator;

@Entity
@Table(name = "vw_tab_documentos_classificacao", schema = Constants.SCHEMA)
public class VwTabDocumentosClassificacaoObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_docto_classificacao")
	// @NotNull(message = "DoctoClassificacao campo obrigatório!")
	private Integer cdDoctoClassificacao;

	@Column(name = "tx_docto_classificacao")
	// @NotEmpty(message = "DoctoClassificacao campo obrigatório!")
	@Size(max = 100, message = "DoctoClassificacao tamanho máximo de 100 caracteres")
	private String txDoctoClassificacao;

	@Column(name = "cd_tipo_dta")
	// @NotNull(message = "TipoDta campo obrigatório!")
	private Integer cdTipoDta;
	
	@Column(name = "tx_tipo_dta")
	// @NotNull(message = "TipoDta campo obrigatório!")
	private String txTipoDta;
	
	@Column(name = "ck_validar")
	@NotNull(message = "Validar? campo obrigatório!")
	private Integer ckValidar;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txDoctoClassificacao))
			txDoctoClassificacao = txDoctoClassificacao.toUpperCase();
	}

	public Integer getCdDoctoClassificacao() {
		return cdDoctoClassificacao;
	}

	public void setCdDoctoClassificacao(Integer cdDoctoClassificacao) {
		this.cdDoctoClassificacao = cdDoctoClassificacao;
	}

	public String getTxDoctoClassificacao() {
		return txDoctoClassificacao;
	}

	public void setTxDoctoClassificacao(String txDoctoClassificacao) {
		this.txDoctoClassificacao = txDoctoClassificacao;
	}

	public Integer getCdTipoDta() {
		return cdTipoDta;
	}

	public void setCdTipoDta(Integer cdTipoDta) {
		this.cdTipoDta = cdTipoDta;
	}

	public String getTxTipoDta() {
		return txTipoDta;
	}

	public void setTxTipoDta(String txTipoDta) {
		this.txTipoDta = txTipoDta;
	}

	public Integer getCkValidar() {
		return ckValidar;
	}

	public void setCkValidar(Integer ckValidar) {
		this.ckValidar = ckValidar;
	}
	
	

}
