package br.com.jtpsolution.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.util.Validator;

@Entity
@Table(name = "tab_perfil_veiculo", schema = Constants.SCHEMA)
public class TabPerfilVeiculoObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_perfil_veiculo")
	// @NotNull(message = "PerfilVeiculo campo obrigatório!")
	private Integer cdPerfilVeiculo;

	@Column(name = "tx_perfil_veiculo")
	// @NotEmpty(message = "PerfilVeiculo campo obrigatório!")
	@Size(max = 45, message = "PerfilVeiculo tamanho máximo de 45 caracteres")
	private String txPerfilVeiculo;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txPerfilVeiculo))
			txPerfilVeiculo = txPerfilVeiculo.toUpperCase();
	}

	public Integer getCdPerfilVeiculo() {
		return cdPerfilVeiculo;
	}

	public void setCdPerfilVeiculo(Integer cdPerfilVeiculo) {
		this.cdPerfilVeiculo = cdPerfilVeiculo;
	}

	public String getTxPerfilVeiculo() {
		return txPerfilVeiculo;
	}

	public void setTxPerfilVeiculo(String txPerfilVeiculo) {
		this.txPerfilVeiculo = txPerfilVeiculo;
	}

}
