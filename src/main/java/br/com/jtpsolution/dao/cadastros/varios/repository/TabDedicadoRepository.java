package br.com.jtpsolution.dao.cadastros.varios.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.cadastros.varios.TabDedicadoObj;



public interface TabDedicadoRepository extends JpaRepository<TabDedicadoObj, Integer> {

	@Query("select t from TabDedicadoObj t where t.tabOrigemObj.cdOrigem = ?1 and t.tabDestinoObj.cdDestino = ?2 and t.vlDe <= ?3 order by t.vlDe desc")
	List<TabDedicadoObj> findByPrecosDedicadoQuery(Integer cdOrigem, Integer cdDestino, double vlBase);

}