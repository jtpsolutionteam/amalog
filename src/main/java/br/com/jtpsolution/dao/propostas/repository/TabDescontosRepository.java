package br.com.jtpsolution.dao.propostas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jtpsolution.dao.propostas.TabDescontosObj;



public interface TabDescontosRepository extends JpaRepository<TabDescontosObj, Integer> {

	@Query("select t from TabDescontosObj t where vlDe <= ?1 order by vlDe desc")
	List<TabDescontosObj> findByPrecosQuery(double vlBase);

}