package br.com.jtpsolution.modulos.microled.obj;

import java.util.Date;

import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VwTabConsultaCargaItemObj {
	
	
	private Integer autonum;
	
	private Integer txLote;

	@NumberFormat(pattern = "#,##0.000") 
	private double vlPesoBruto; 
	
	@NumberFormat(pattern = "#,##0.000") 
	private double vlPesoApurado; 

	private Integer vlQtdeVolume;
	
	private Integer vlQtdeAvaria;
	
	private String txVolume;	
	 
	private String txMicroledMercadoria; 
	 
	private String txContainer; 
	 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtInicioDesova; 
	 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtFimDesova; 
	 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtSaida; 
	 
	private Integer cdTermoAvaria; 
	 
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtTermoAvaria; 
	 
	//@NumberFormat(pattern = "#,##0.000") 
	private double vlDiferencaPeso; 
	 
	@NumberFormat(pattern = "#,##0.000") 
	private double vlPesoAvaria; 
	 
	//@NumberFormat(pattern = "#,##0.00") 
	private double vlDiferencaPercentual;
	
	private String txCnpjImportador;
	
	private String txImportador;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtEntrada; 
	
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") 
	@Temporal(TemporalType.TIMESTAMP) 
	private Date dtDesconsolidacao;
	
	private String txGrPaga;
	
	private String txBloqueioBl;

	private String txIcmsSefaz;
	
	private String txMapa;
	
	private String txBloqueioCNTR;
	
	private String txSiscarga;
}
