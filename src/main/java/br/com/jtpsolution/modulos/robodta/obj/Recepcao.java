package br.com.jtpsolution.modulos.robodta.obj;

import java.io.Serializable;


public class Recepcao implements Serializable{

	
	private String dtRecepcao;
	private NomeMatricula responsavel; 
	private String txInterveniente;
	public String getDtRecepcao() {
		return dtRecepcao;
	}
	public void setDtRecepcao(String dtRecepcao) {
		this.dtRecepcao = dtRecepcao;
	}
	public NomeMatricula getResponsavel() {
		return responsavel;
	}
	public void setResponsavel(NomeMatricula responsavel) {
		this.responsavel = responsavel;
	}
	public String getTxInterveniente() {
		return txInterveniente;
	}
	public void setTxInterveniente(String txInterveniente) {
		this.txInterveniente = txInterveniente;
	}
	
	
}
