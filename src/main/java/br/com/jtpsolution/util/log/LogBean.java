package br.com.jtpsolution.util.log;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.jtpsolution.dao.util.log.TabLogObj;
import br.com.jtpsolution.dao.util.log.repository.TabLogRepository;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.GeneralUtil;
import br.com.jtpsolution.util.Validator;

@Service
public class LogBean {

	@Autowired
	private TabLogRepository tabRepository;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		TabLogObj TabOld = new TabLogObj();
		TabOld.setCdCodigo(1);
		TabOld.setCdUsuario(1);
		TabOld.setCdFolder(1);
		TabOld.setTxRef("teste");

		TabLogObj TabNew = new TabLogObj();
		TabNew.setCdCodigo(2);
		TabNew.setCdUsuario(2);
		TabNew.setCdFolder(1);
		TabNew.setTxRef("teste2");

		new LogBean().LogBean("167161", 1, "", 0, 1, TabOld, TabNew);
	}

	public void addLog(String txService, String txRef, Integer cdUsuario, String submitFields) {

		new Thread() {
			public void run() {
				
				try {
				if (!Validator.isBlankOrNull(submitFields)) {

					String[] txFields = submitFields.split("\\|");

					for (int i = 0; i <= txFields.length - 1; i++) {

						String[] tx = txFields[i].split("\\=");

						if (tx.length > 1) {
							TabLogObj Tab = new TabLogObj();
							Tab.setCdUsuario(cdUsuario);
							Tab.setDtData(new Date());
							Tab.setCdTipo(1);
							Tab.setTxCampo(tx[0]);
							Tab.setTxRef(txRef);
							Tab.setTxService(txService);
							Tab.setTxValor(validacampo(tx[0], tx[1]));
							tabRepository.save(Tab);
						}

					}
				}
				}catch (Exception ex) {
					System.out.println("Erro: "+getClass().getSimpleName());
				}
			}
		}.start();

	}

	private String validacampo(String field, String value) {

		if (field.contains("ck")) {
			if (value.equals("1")) {
				value = "SIM";
			} else {
				value = "NÃO";
			}
		}

		return value;

	}

	public List<TabLogObj> LogBean(String tx_ref, Integer cd_usuario, String tx_service, Integer cd_linha_grid,
			Integer cd_tipo, Object objOld, Object objNew) {

		List<TabLogObj> l = new ArrayList<TabLogObj>();
		try {

			GeneralUtil u = new GeneralUtil();
			Field[] camposOld = objOld.getClass().getDeclaredFields();
			Field[] camposNew = objNew.getClass().getDeclaredFields();

			for (int i = 0; i < camposOld.length; i++) {
				// Tipo
				// Exemplo: class java.math.BigDecimal
				// System.out.println(campos[i].getType());
				camposOld[i].setAccessible(true);
				// System.out.println("Old");
				// System.out.println(camposOld[i].getName());
				// System.out.println(camposOld[i].get(objOld));
				// System.out.println(camposOld[i].getModifiers());

				camposNew[i].setAccessible(true);
				// System.out.println("New");
				// System.out.println(camposNew[i].getName());
				// System.out.println(camposNew[i].get(objNew));
				// Setar valor
				// campos[i].set(objeto, new BigDecimal("0"));

				if (camposOld[i].getName().equals(camposNew[i].getName())) {

					String field = camposOld[i].getName();
					// System.out.println(field);
					// System.out.println(camposOld[i].getType());
					// System.out.println(camposNew[i].getType());

					String valueOld = "";
					if (camposOld[i].get(objOld) != null) {
						valueOld = camposOld[i].get(objOld).toString();
					}

					String valueNew = "";
					if (camposNew[i].get(objNew) != null) {
						valueNew = camposNew[i].get(objNew).toString();
						if (camposNew[i].getType().toString().toLowerCase().contains("date")
								&& !Validator.isBlankOrNull(valueNew)) {
							
							valueNew = checkDate(valueNew);
							valueOld = checkDate(valueOld);
	
						}
					}

					if (!verificaIsFieldLookupObj(objNew, camposNew[i].getName())) {
						if (!valueOld.equals(valueNew)) {
							// System.out.println(camposNew[i].getName());
							// System.out.println(valueNew);

							TabLogObj Tab = new TabLogObj();
							Tab.setCdCodigo(null);
							Tab.setDtData(new Date());
							Tab.setTxService(tx_service);
							Tab.setCdUsuario(cd_usuario);
							Tab.setTxCampo(camposNew[i].getName());
							Tab.setTxRef(tx_ref);
							Tab.setCdTipo(cd_tipo);
							Tab.setTxValor(valueNew);
							Tab.setCdCodigoLinhaGrid(cd_linha_grid);
							l.add(Tab);
							if (cd_tipo == 1) { // Se for alterar tem que adicionar numa lista para fazer após a
												// validação pelo JPA
								// tabRepository.save(Tab);
								Gravacao(Tab);
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return l;

	}
	
	public static String checkDate(String txDate) {
		
		String txReturn = "";
		int sizeDate = txDate.length();
		if (sizeDate == 10) {
			txReturn = GeneralParser.format_dateBR2(GeneralParser.parseDate("yyyy-MM-dd",txDate));			
		}else if (sizeDate == 19) {
			txReturn = GeneralParser.format_dateHSBR(GeneralParser.parseDateTime(
					"yyyy-MM-dd HH:mm:ss",txDate));
		}else if (sizeDate > 19 && sizeDate <= 21) {
			txReturn = GeneralParser.format_dateHSBR(GeneralParser.parseDateTime(
					"yyyy-MM-dd HH:mm:ss",txDate.substring(0,19)));
		}else if (sizeDate > 21) {
			txReturn = GeneralParser
					.format_dateHSBR(GeneralParser.parseDateTime("yyyy-MM-dd HH:mm:ss",
							GeneralParser.format_dateUS(txDate,
									"yyyy-MM-dd HH:mm:ss")));
		}
		
		return txReturn;
		
	}

	private void Gravacao(TabLogObj Tab) {
		new Thread() {
			public void run() {
				tabRepository.save(Tab);
			}
		}.start();
	}

	public void GravarListaLog(final List<TabLogObj> listaLog) {
		new Thread() {
			public void run() {

				for (TabLogObj tabLog : listaLog) {
					// sua lógica

					TabLogObj Tab = new TabLogObj();
					Tab.setCdCodigo(null);
					Tab.setDtData(new Date());
					Tab.setTxService(tabLog.getTxService());
					Tab.setCdUsuario(tabLog.getCdUsuario());
					Tab.setTxCampo(tabLog.getTxCampo());
					Tab.setTxRef(tabLog.getTxRef());
					Tab.setCdTipo(tabLog.getCdTipo());
					Tab.setTxValor(tabLog.getTxValor());
					Tab.setCdCodigoLinhaGrid(tabLog.getCdCodigoLinhaGrid());
					tabRepository.save(Tab);

				}
			}
		}.start();
	}

	public void GravarLog(final String txRef, final Integer cdTipo, final Date dtData, final String txValor,
			final String txCampo, final Integer cdUsuario, final String txService) {
		new Thread() {
			public void run() {

				TabLogObj Tab = new TabLogObj();
				Tab.setCdCodigo(null);
				Tab.setDtData(dtData);
				Tab.setTxService(txService);
				Tab.setCdUsuario(cdUsuario);
				Tab.setTxCampo(txCampo);
				Tab.setTxRef(txRef);
				Tab.setCdTipo(cdTipo);
				Tab.setTxValor(txValor);
				tabRepository.save(Tab);
			}
		}.start();
	}

	private void compareObj(Object objNew, String tx_field_old, String tx_valor_old) {

		try {
			Field[] campos = objNew.getClass().getDeclaredFields();
			for (int i = 0; i < campos.length; i++) {
				campos[i].setAccessible(true);
				if (campos[i].getName().equals(tx_field_old) && !campos[i].get(objNew).equals(tx_valor_old)) {
					System.out.println("New");
					System.out.println(campos[i].getName());
					System.out.println(campos[i].get(objNew));

				} else {
					System.out.println("Old");
					System.out.println(tx_field_old);
					System.out.println(tx_valor_old);
				}

			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	private boolean verificaIsFieldLookupObj(Object objNew, String txField) {

		boolean ckLookup = false;
		try {

			if (txField.substring(0, 2).equals("tx")) {

				Field[] campos = objNew.getClass().getDeclaredFields();
				for (int i = 0; i < campos.length; i++) {
					campos[i].setAccessible(true);
					String txFieldModify = txField.replace("tx", "cd");
					if (campos[i].getName().equals(txFieldModify)) {
						ckLookup = true;
						break;
					}
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return ckLookup;
	}

}
