package br.com.jtpsolution.modulos.administrador.controller;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.administracao.TabGrupoAcessoObj;
import br.com.jtpsolution.mensagens.TabRetornoSucessoObj;
import br.com.jtpsolution.modulos.administrador.bean.TabFiltroGrupoAcessoObj;
import br.com.jtpsolution.modulos.administrador.service.TabGrupoAcessoService;
import br.com.jtpsolution.modulos.util.geralcontroller.allController;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.regras.RegrasBean;

@Controller
@RequestMapping("/admgrupoacesso")
public class TabGrupoAcessoController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabGrupoAcessoService tabService;
	
	@Autowired
	private RegrasBean regrasBean;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_ADM+"/admgrupoacesso";

 	
 	@GetMapping
	@Transactional(readOnly = true)
	public ModelAndView pesquisa(TabFiltroGrupoAcessoObj tabFiltroGrupoAcessoObj, HttpServletRequest request) {
		
		tabFiltroGrupoAcessoObj = verificaFiltro(tabFiltroGrupoAcessoObj, request);
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject(new TabGrupoAcessoObj());			
		mv.addObject("tabFiltroGrupoAcessoObj", tabFiltroGrupoAcessoObj);
		mv.addObject("listadados", retornoFiltro(tabFiltroGrupoAcessoObj));
		
		
		return mv;
	}
	
	private List<?> retornoFiltro(TabFiltroGrupoAcessoObj tabFiltroGrupoAcessoObj) {
 		
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(TabGrupoAcessoObj.class);
		
		adicionarFiltro(tabFiltroGrupoAcessoObj, criteria);
 		
 		return criteria.list();
 	}


	private TabFiltroGrupoAcessoObj verificaFiltro(TabFiltroGrupoAcessoObj tabFiltroGrupoAcessoObj, HttpServletRequest request) {
		
		try {
			
			if (Validator.isObjetoNull(tabFiltroGrupoAcessoObj)) {
				if (request.getSession().getAttribute(getClass().getSimpleName()) != null) {					
					String jsonFiltro = request.getSession().getAttribute(getClass().getSimpleName()).toString();
					ObjectMapper mapper = new ObjectMapper();
					tabFiltroGrupoAcessoObj = mapper.readValue(jsonFiltro, TabFiltroGrupoAcessoObj.class);	
				}
			}else {
				request.getSession().setAttribute(getClass().getSimpleName(),
						GeneralParser.convertObjJson(tabFiltroGrupoAcessoObj));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return tabFiltroGrupoAcessoObj;
	}

	
	private void adicionarFiltro(TabFiltroGrupoAcessoObj filtro, Criteria criteria) {
		
		  boolean ckFiltro = false;
		  
		  if (filtro != null) {
			  
			  if (!StringUtils.isEmpty(filtro.getTxGrupoAcessoFiltro())) {
				  criteria.add(Restrictions.ilike("txGrupoAcesso", "%"+filtro.getTxGrupoAcessoFiltro()+"%"));
				  ckFiltro = true;
			  }
			  
			  /*
			  if (!ckFiltro) {
				  criteria.add(Restrictions.eq("txGrupoAcesso", "-1"));
			  }*/
			  
			  criteria.addOrder(Order.asc("txGrupoAcesso"));
		  }
	  }
 	
	
	@RequestMapping(value = "/gravarmodal", method = RequestMethod.POST)
	public @ResponseBody List<?> gravarmodal(@Validated TabGrupoAcessoObj tabGrupoAcessoObj, Errors erros, HttpServletRequest request) {

		List<ObjectError> error = new ArrayList<ObjectError>();
		if (erros.hasErrors()) {
		  //mv.addObject("listaErros", erros.getAllErrors());
		  error.addAll(erros.getAllErrors());
		  return error;
		}else {
			error = regrasBean.verificaregras(getClass().getSimpleName(),request);
			if (!error.isEmpty()) {
				  return error;	
			}	  
		}
		
	    TabGrupoAcessoObj Tab = tabService.gravar(tabGrupoAcessoObj);
	   
	    List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();		
	    TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
	    tSuccess.setCd_mensagem(1);
	    tSuccess.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
	    success.add(tSuccess);
	    
		return success;
	}
	
	
	@RequestMapping(value = "/novo")
	public ModelAndView novo(HttpServletRequest request) {
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		mv.addObject(new TabGrupoAcessoObj());
		mv.addObject("tabFiltroGrupoAcessoObj", verificaFiltro(new TabFiltroGrupoAcessoObj(), request));
		mv.addObject("listadados", retornoFiltro(verificaFiltro(new TabFiltroGrupoAcessoObj(), request)));
		mv.addObject("txEdit", "edit");
		return mv;
	}
	

	@RequestMapping(value = "/consultar/{CdGrupoAcesso}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable Integer CdGrupoAcesso, HttpServletRequest request) {
	
	   TabGrupoAcessoObj TabView = tabService.consultar(CdGrupoAcesso);
	   TabFiltroGrupoAcessoObj tF = verificaFiltro(new TabFiltroGrupoAcessoObj(), request);

	   
		ModelAndView mv = new ModelAndView(txUrlTela);	
		if (TabView != null) {
		  mv.addObject("tabFiltroGrupoAcessoObj",tF);	
		  mv.addObject("tabGrupoAcessoObj",TabView);
		  mv.addObject("listadados", retornoFiltro(verificaFiltro(tF, request)));
		  mv.addObject("txEdit", "edit");
		}else {
		  mv.addObject("tabFiltroGrupoAcessoObj",tF);	
		  mv.addObject(new TabGrupoAcessoObj());	
		  mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
		  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}
	
	/*
	@RequestMapping(value = "/excluir/{CdGrupoAcesso}", method = RequestMethod.GET)
	public @ResponseBody String excluir(@PathVariable Integer CdGrupoAcesso, HttpServletRequest request) {

		tabService.excluir(CdGrupoAcesso);

		return "OK!";
	}*/
	
}
