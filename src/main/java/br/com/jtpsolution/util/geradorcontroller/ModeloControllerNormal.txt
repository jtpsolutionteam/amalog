
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.mensagens.TabRetornoSucessoObj;

@Controller
@RequestMapping("/#TabObj")
public class #Classe extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private #ServiceService tabService;
	
	@Autowired
	private RegrasBean regrasBean;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_CADASTROS+"/nomedomodulo/nometelahtml";
	
	
	@RequestMapping(value = "/novo")
	public ModelAndView novo() {
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		mv.addObject(new #TabObj());
		return mv;
	}
	
	
	@RequestMapping(value = "/gravar", method = RequestMethod.POST)
	public ModelAndView gravar(@Validated #TabObj #tabObj, Errors erros, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView(txUrlTela);
		
		if (erros.hasErrors()) {
		  mv.addObject("listaErros", erros.getAllErrors());
		  return mv;
		}else {
			List<ObjectError> error = new ArrayList<ObjectError>();
			error = regrasBean.verificaregras(getClass().getSimpleName(),request);
			if (!error.isEmpty()) {
				  mv.addObject("listaErros", error);
				  return mv;	
			}	  
		}
		
	    #TabObj Tab = tabService.gravar(#tabObj);
	    Vw#TabObj TabView = tabService.consultar(Tab.get#Field());
				
		mv.addObject("#tabObj",TabView);
		mv.addObject("txMensagem", Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
		mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_SUCCESS);
		return mv;
	}
   
	
	@RequestMapping(value = "/pesquisar/{txPesquisar}", method = RequestMethod.GET)
	public ModelAndView pesquisar(@PathVariable String txPesquisar) {
	
	   Vw#TabObj TabView = tabService.pesquisa(txPesquisar);

		ModelAndView mv = new ModelAndView(txUrlTela);	
		if (TabView != null) {
		  mv.addObject("#tabObj",TabView);
		}else {
		  mv.addObject(new #TabObj());	
		  mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
		  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		
		return mv;
	}
	
	@RequestMapping(value = "/consultar/{txConsultar}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable String txConsultar) {
	
	   Vw#TabObj TabView = tabService.consultar(txConsultar);
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		if (TabView != null) {
		  mv.addObject("#tabObj",TabView);
		}else {
		  mv.addObject(new #TabObj());	
		  mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
		  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}
	
	@RequestMapping(value = "/lkpesquisar")
	public @ResponseBody List<Vw#TabObj> lkpesquisar(String txUrlPesquisar) {
		return tabService.lkpesquisar(txUrlPesquisar);
		
	}
	
	
	//Modelo
	/*
	@ModelAttribute("selectgrupoacesso")
	public List<TabGrupoAcessoObj> selectgrupoacesso() {
		return tabGrupoAcessoService.listar();
	}*/

	
	
}
