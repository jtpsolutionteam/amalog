package br.com.jtpsolution.modulos.administrador.service;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.jtpsolution.dao.administracao.TabCamposIdiomaObj;
import br.com.jtpsolution.dao.administracao.repository.TabCamposIdiomaRepository;
import br.com.jtpsolution.dao.util.log.TabLogObj;
import br.com.jtpsolution.exceptions.ErrosConstraintException;
import br.com.jtpsolution.security.DadosUser;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.GeneralUtil;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.log.LogBean;

@Service
public class TabCamposIdiomaService {

	@Autowired
	private TabCamposIdiomaRepository tabRepository;

	@Autowired
	private UsuarioBean tabUsuarioService;

	@Autowired
	private LogBean tabLogBeanService;

	public List<TabCamposIdiomaObj> listar() {
		return tabRepository.findAll(GeneralParser.sortByIdAsc("txCampo"));

	}
	
	public List<TabCamposIdiomaObj> listarController(String txController, String cdIdioma) {
		return tabRepository.findByTabTelasObjTxControllerAndCdIdioma(txController, cdIdioma);

	}
	
	public TabCamposIdiomaObj gravar(TabCamposIdiomaObj Tab) {

		boolean ckAlteracao = false;

		DadosUser user = tabUsuarioService.DadosUsuario();

		TabCamposIdiomaObj tAtual = new TabCamposIdiomaObj();
		TabCamposIdiomaObj tNovo = Tab;

		// tNovo.setCdUsuario(null);
		List<TabLogObj> listaLog = null;

		if (Tab.getCdCampoIdioma() != null && !Validator.isBlankOrNull(Tab.getCdCampoIdioma())) {
			ckAlteracao = true;
			// Log de campos -- Sempre antes da gravação por causa da persistencia.
			tAtual = tabRepository.findOne(Tab.getCdCampoIdioma());
			// listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdUsuario()),
			// user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
			listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdCampoIdioma()), user.getCdUsuario(),
					getClass().getSimpleName(), 0, 2, tAtual, Tab);
		}

		try {
			tNovo = tabRepository.save(tNovo);
		} catch (ConstraintViolationException ex) {

			throw new ErrosConstraintException(GeneralUtil.MessageExceptionConstraint(ex),
					ex.getConstraintViolations());
		}

		// Log de campos
		if (!ckAlteracao) {
			// tabLogBeanService.LogBean(String.valueOf(tNovo.getCdUsuario()),
			// user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
			tabLogBeanService.LogBean(String.valueOf(tNovo.getCdCampoIdioma()), user.getCdUsuario(),
					getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		} else {
			tabLogBeanService.GravarListaLog(listaLog);
		}
		return tNovo;

	}

	public TabCamposIdiomaObj consultar(Integer cdCampo) {
		TabCamposIdiomaObj Tab = tabRepository.findOne(cdCampo);

		return Tab;

	}
	
	public void excluir(Integer cdCampoIdioma) {

		tabRepository.delete(cdCampoIdioma);

	}

	
}
