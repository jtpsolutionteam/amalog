package br.com.jtpsolution.modulos.robosiscarga.obj;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TabEscalaObj {

	private String txEscala;
	
	private String txNavio;
	
	private String txResponsavel;
	
	private String txBandeira;
	
	private String txTipoOperacao;
	
	private String txTransportador;
	
	private String txNacionalidade;
	
	private String txAgencia;
	
	private String dtInclusaoEscala;
	
	private String dtPrevisaoAtracacao;
	
	private String dtAtracacao;
	
	private String txLocalAtracacao;
	
	private String txResponsavelAtracacao;
	
	private String dtSolicitacaoEfetivaPasse;
	
	private String txSituacaoEscala;
	
	private String txPortoEscala;
	
	private String txViagem;
	
	
	
}
