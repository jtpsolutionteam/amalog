package br.com.jtpsolution.modulos.portalunico.service;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.conexaorest.restconnect;
import br.com.jtpsolution.modulos.portalunico.obj.TabDatObj;
import br.com.jtpsolution.util.GeneralParser;

@Service
public class RoboDatService {

	
	
	
	
	public TabDatObj consultarDat(String txDat) {

		TabDatObj tabDatObj = new TabDatObj();
		try {
			
			String txUrl = Constants.ROBO_DAT+txDat;
			String jsonDat = restconnect.enviaRestGet(txUrl);
			
			//System.out.println(jsonDat);
			
			JSONObject json = new JSONObject(jsonDat);
			
			JSONArray jsonArray = json.getJSONArray("documentosTransporte");
			
			for(int i=0; i<jsonArray.length(); i++) {

		        JSONObject jsonObject = (JSONObject) jsonArray.get(i);
		        
				if (!jsonObject.isNull("numeroDocumentoTransporte")) {				
					tabDatObj.setTxDat(jsonObject.getString("numeroDocumentoTransporte"));
					tabDatObj.setDtEmissao(GeneralParser.parseDateTime("yyyy-MM-dd",jsonObject.getString("dataEmissao")));
					tabDatObj.setTxSituacao(jsonObject.getString("situacaoAtual"));
				}
			}    
			
			
		}catch (Exception ex) {
			tabDatObj.setTxSituacao("Numero da DAT inválido!");
		}
		
		return tabDatObj;
	}
	
	
}
