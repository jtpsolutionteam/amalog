package br.com.jtpsolution.modulos.propostas.controller;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.TransactionUtilBean;
import br.com.jtpsolution.dao.cadastros.varios.TabStatusObj;
import br.com.jtpsolution.dao.cadastros.varios.repository.TabStatusRepository;
import br.com.jtpsolution.dao.propostas.TabPropostaNewObj;
import br.com.jtpsolution.dao.propostas.TabPropostaObj;
import br.com.jtpsolution.modulos.propostas.bean.TabFiltroPropostaObj;
import br.com.jtpsolution.modulos.propostas.service.TabPropostaService;
import br.com.jtpsolution.modulos.util.geralcontroller.allController;
import br.com.jtpsolution.security.DadosUser;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.regras.RegrasBean;

@Controller
@RequestMapping("/propostapesquisa")
public class TabPropostaPesquisaController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabPropostaService tabService;
	
	@Autowired
	private RegrasBean regrasBean;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private UsuarioBean tabUsuarioService;
	
	@Autowired
	private TransactionUtilBean transactionUtilBean;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_PROPOSTA+"/propostapesquisa";
	
 	@GetMapping	
	public ModelAndView pesquisa(TabFiltroPropostaObj tabFiltroPropostaObj, HttpServletRequest request) {
		
		tabFiltroPropostaObj = verificaFiltro(tabFiltroPropostaObj, request);
		
		
		ModelAndView mv = verificaPermissao(txUrlTela);
		mv.addObject(new TabPropostaObj());			
		mv.addObject("tabFiltroPropostaObj", tabFiltroPropostaObj);
		mv.addObject("listadados", retornoFiltro(tabFiltroPropostaObj));
		
		
		return mv;
	}
 	
 	
	
	private List<?> retornoFiltro(TabFiltroPropostaObj tabFiltroPropostaObj) {
 		
		//Criteria criteria = entityManager.unwrap(Session.class).createCriteria(TabPropostaNewObj.class);
		String txWhere = adicionarFiltro(tabFiltroPropostaObj);
		
		String StrSql = "select distinct t from TabPropostaNewObj t " 
				+ "join fetch t.tabUsuarioObj tabUsuarioObj  "
				+ "left join fetch t.tabOrigemObj tabOrigemObj  " 
				+ "left join fetch t.tabDestinoObj tabDestinoObj  "
				+ "left join fetch t.tabOrigemLtlObj tabOrigemLtlObj  "
				+ "left join fetch t.tabDestinoLtlObj tabDestinoLtlObj  "
				+ "left join fetch t.tabStatusObj tabStatusObj  "
				+ "left join fetch t.tabTipoCarregamentoObj tabTipoCarregamentoObj  "
				+ "left join fetch t.tabFreehandObj tabFreehandObj  "
				+ "join fetch t.tabClienteObj tabClienteObj  " 
				+ "join fetch t.listPropostaGrupoAcesso tg " + txWhere;
		
		List<TabPropostaNewObj> list = (List<TabPropostaNewObj>) transactionUtilBean.querySelect(StrSql,
				TabPropostaNewObj.class);
		
 		
 		return list;
 	}


	private TabFiltroPropostaObj verificaFiltro(TabFiltroPropostaObj tabFiltroPropostaObj, HttpServletRequest request) {
		
		try {
			
			if (Validator.isObjetoNull(tabFiltroPropostaObj)) {
				if (request.getSession().getAttribute(getClass().getSimpleName()) != null) {					
					String jsonFiltro = request.getSession().getAttribute(getClass().getSimpleName()).toString();
					ObjectMapper mapper = new ObjectMapper();
					tabFiltroPropostaObj = mapper.readValue(jsonFiltro, TabFiltroPropostaObj.class);	
				}
			}else {
				request.getSession().setAttribute(getClass().getSimpleName(),
						GeneralParser.convertObjJson(tabFiltroPropostaObj));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return tabFiltroPropostaObj;
	}

	
	private String adicionarFiltro(TabFiltroPropostaObj filtro) {
		
		  boolean ckFiltro = false;
		  
		  DadosUser user  = tabUsuarioService.DadosUsuario();	
		  
		  String txWhere = "where ";
		  
		  txWhere += "tg.cdGrupoAcesso ="+ user.getCdGrupoAcesso();
		    
		  txWhere += " and t.txTipoProposta = 'DTA'";
		    
		 // }else {
		//	criteria.add(Restrictions.eq("cdUsuario", user.getCdUsuario()));  
		 // }
		  
		  if (filtro != null) {
			  
			  if (!StringUtils.isEmpty(filtro.getTxPropostaFiltro())) {
				  txWhere += " and t.txProposta like '%"+filtro.getTxPropostaFiltro()+"%'";
				  ckFiltro = true;
			  }
			  
			  if (!StringUtils.isEmpty(filtro.getTxTipoCampoFiltro()) && !StringUtils.isEmpty(filtro.getDtPropostaInicialFiltro()) && !StringUtils.isEmpty(filtro.getDtPropostaFinalFiltro())) {
				  txWhere += " and date(t."+filtro.getTxTipoCampoFiltro()+") between '"+ GeneralParser.format_dateBRMySql(filtro.getDtPropostaInicialFiltro()) + "' and '"+GeneralParser.format_dateBRMySql(filtro.getDtPropostaFinalFiltro())+"'";
				  ckFiltro = true;
			  }
			  
			  if (!StringUtils.isEmpty(filtro.getCdStatusFiltro())) {
				  txWhere += " and tabStatusObj.cdStatus = "+filtro.getCdStatusFiltro();
				  ckFiltro = true;
			  }
			  
			  
			 
			  if (!ckFiltro) {
				  
				  txWhere += " and t.dtDtaConclusaoTransito is null";
				  Date dtAbertura = GeneralParser.retornaDataAnteriorDiasCorridos(new Date(), 50);
				  txWhere += " and date(t.dtProposta) between '"+ GeneralParser.format_dateBRMySql(dtAbertura) + "' and '"+GeneralParser.format_dateBRMySql(new Date())+"'";
				  
			  }
			  
			 txWhere += " order by t.dtProposta";
		  }
		  
		  return txWhere;
	  }
 	

	@Autowired
	private TabStatusRepository tabStatusRepository;
	
	@ModelAttribute("selectcdstatus")
	public List<TabStatusObj> selectcdstatus() {
		return tabStatusRepository.findAll();
	}
	
}
