package br.com.jtpsolution.modulos.robosiscarga.obj;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TabCeMercanteObj {

	private String txCe;
	
	private String txTipoConhecimento;
	
	private String txBl;
	
	private String dtBl;

	private String txCnpjNvocc;
	
	private String txNomeNvocc;
	
	private String txNomeTransportador;
	
	private String txCnpjTransportador;
	
	private String txPortoOrigem;
	
	private String txPortoDestino;
	
	private String txPaisProcedencia;
	
	private String txUfDestino;
	
	private String txDi;
	
	private String txSituacao;
	
	private String txModalidadeFrete;
	
	private String txCeMercanteMaster;
	
	private String txManifesto;
	
	private String txTerminalDescarregamento;
	
	private TabCeManifestoObj tabCeManifestoObj;
	
	private TabCeMercanteMasterObj tabCeMercanteMasterObj;
	
	private TabEscalaObj tabEscalaObj;
	
	private List<TabCeMercanteItemObj> lstItem;
	
	private String txStatusConsultaSiscarga;
	
	
	
	
	
}
