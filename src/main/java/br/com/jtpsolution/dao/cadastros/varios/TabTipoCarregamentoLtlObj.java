package br.com.jtpsolution.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.util.Validator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_tipo_carregamento_ltl", schema = Constants.SCHEMA)
public class TabTipoCarregamentoLtlObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_tipo_carregamento")
	// @NotNull(message = "TipoCarregamento campo obrigatório!")
	private Integer cdTipoCarregamento;

	@Column(name = "tx_tipo_carregamento")
	// @NotEmpty(message = "TipoCarregamento campo obrigatório!")
	@Size(max = 50, message = "TipoCarregamento tamanho máximo de 50 caracteres")
	private String txTipoCarregamento;

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txTipoCarregamento))
			txTipoCarregamento = txTipoCarregamento.toUpperCase();
	}

}
