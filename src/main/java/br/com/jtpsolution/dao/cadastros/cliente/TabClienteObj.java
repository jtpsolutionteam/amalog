package br.com.jtpsolution.dao.cadastros.cliente;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.util.Validator;

@Entity
@Table(name = "tab_cliente", schema = Constants.SCHEMA)
public class TabClienteObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_cliente")
	// @NotNull(message = "Cliente campo obrigatório!")
	private Integer cdCliente;

	@Column(name = "tx_cliente")
	@NotEmpty(message = "Cliente campo obrigatório!")
	@Size(max = 100, message = "Cliente tamanho máximo de 100 caracteres")
	private String txCliente;
	
	@Column(name = "tx_cnpj")
	@NotEmpty(message = "CNPJ campo obrigatório!")
	@Size(max = 20, message = "CNPJ tamanho máximo de 20 caracteres")
	private String txCnpj;
	
	

	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		if (!Validator.isBlankOrNull(txCliente))
			txCliente = txCliente.toUpperCase();
	}

	public Integer getCdCliente() {
		return cdCliente;
	}

	public void setCdCliente(Integer cdCliente) {
		this.cdCliente = cdCliente;
	}

	public String getTxCliente() {
		return txCliente;
	}

	public void setTxCliente(String txCliente) {
		this.txCliente = txCliente;
	}

	public String getTxCnpj() {
		return txCnpj;
	}

	public void setTxCnpj(String txCnpj) {
		this.txCnpj = txCnpj;
	}

	
	
}
