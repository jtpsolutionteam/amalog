package br.com.jtpsolution.modulos.cadastros.cliente.service;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.jtpsolution.dao.cadastros.cliente.TabClienteGrupoAcessoObj;
import br.com.jtpsolution.dao.cadastros.cliente.VwTabClienteGrupoAcessoObj;
import br.com.jtpsolution.dao.cadastros.cliente.repository.TabClienteGrupoAcessoRepository;
import br.com.jtpsolution.dao.cadastros.cliente.repository.VwTabClienteGrupoAcessoRepository;
import br.com.jtpsolution.dao.util.log.TabLogObj;
import br.com.jtpsolution.exceptions.ErrosConstraintException;
import br.com.jtpsolution.security.DadosUser;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralUtil;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.log.LogBean;
import br.com.jtpsolution.util.logErro.JTPLogErroBean;

@Service
public class TabClienteGrupoAcessoService {

	@Autowired
	private JTPLogErroBean logErroBean;
	
	@Autowired
	private TabClienteGrupoAcessoRepository  tabRepository;
	
	@Autowired
	private VwTabClienteGrupoAcessoRepository  tabVwRepository;
	
	
	@Autowired
	private UsuarioBean tabUsuarioService;
	
	@Autowired
	private LogBean tabLogBeanService;

	/*
	@Autowired 
	private TabFiltro.....Bean tabFiltroService;
	
	*/
	
	
	public List<VwTabClienteGrupoAcessoObj> listar(Integer cdCliente) {
		return tabVwRepository.findByCdCliente(cdCliente);
		
	}
	
	/* Modelo tela filho 
	
		public List<VwTabClienteGrupoAcessoObj> listar(String CampoRelacionamento) {

		DadosUser user  = tabUsuarioService.DadosUsuario();

		return tabVwRepository.findByTxNrefQuery(CampoRelacionamento, user.getCdGrupoAcesso());
		
	}
	*/
	
	/* Modelo - Tela Pesquisar
			public Page<VwTabClienteGrupoAcessoObj> telapesquisar(TabFiltroObj tabFiltroObj, Pageable pageable) {

			Page<VwTabClienteGrupoAcessoObj> Tab = tabFiltroService.filtrar(tabFiltroObj, pageable);
			
			return Tab;

		}
	*/


	public TabClienteGrupoAcessoObj gravar(TabClienteGrupoAcessoObj Tab) {
		
		boolean ckAlteracao = false;
				
		DadosUser user  = tabUsuarioService.DadosUsuario();

		
		TabClienteGrupoAcessoObj tAtual = new TabClienteGrupoAcessoObj();
		TabClienteGrupoAcessoObj tNovo = Tab;
		
		//tNovo.setCdUsuario(null);
		List<TabLogObj> listaLog = null;
		
		if (Tab.getCdClienteGrupoAcesso() != null && !Validator.isBlankOrNull(Tab.getCdClienteGrupoAcesso())) {
			ckAlteracao = true;
			//Log de campos -- Sempre antes da gravação por causa da persistencia.
			tAtual = tabRepository.findOne(Tab.getCdClienteGrupoAcesso());
			//listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
			listaLog = tabLogBeanService.LogBean(String.valueOf(tAtual.getCdClienteGrupoAcesso()), user.getCdUsuario(), getClass().getSimpleName(), 0, 2, tAtual, Tab);
		}
		
		  try {	
		   tNovo = tabRepository.save(tNovo);			  
		  }catch (ConstraintViolationException ex) {
			  
			 throw new ErrosConstraintException(GeneralUtil.MessageExceptionConstraint(ex), ex.getConstraintViolations());			  
		  }
		  
			//Log de campos
		  if (!ckAlteracao) {
			//tabLogBeanService.LogBean(String.valueOf(tNovo.getCdUsuario()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  	tabLogBeanService.LogBean(String.valueOf(tNovo.getCdClienteGrupoAcesso()), user.getCdUsuario(), getClass().getSimpleName(), 0, 1, tAtual, tNovo);
		  }else {
			  tabLogBeanService.GravarListaLog(listaLog);
		  }
		return tNovo;
		
	}

	public TabClienteGrupoAcessoObj consultar(Integer CdClienteGrupoAcesso) {
	 TabClienteGrupoAcessoObj Tab = tabRepository.findOne(CdClienteGrupoAcesso);
	 
	   return Tab;
	 
	}

	
	/* Se tela tiver excluir
		public void excluir(Integer CdClienteGrupoAcesso) {

		   DadosUser user  = tabUsuarioService.DadosUsuario();

		   TabClienteGrupoAcessoObj Tab = tabRepository.findOne(CdClienteGrupoAcesso);
		   Tab.setDtCancelamento(new Date());
		   Tab.setCdUsuarioCancelamento(user.getCdUsuario());
		   tabRepository.save(Tab);
			 
		}
		
	*/	
	
	
}
