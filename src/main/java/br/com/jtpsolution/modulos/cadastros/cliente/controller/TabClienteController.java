package br.com.jtpsolution.modulos.cadastros.cliente.controller;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.cadastros.cliente.TabClienteObj;
import br.com.jtpsolution.mensagens.TabRetornoSucessoObj;
import br.com.jtpsolution.modulos.cadastros.cliente.bean.TabFiltroClienteObj;
import br.com.jtpsolution.modulos.cadastros.cliente.service.TabClienteService;
import br.com.jtpsolution.modulos.util.geralcontroller.allController;
import br.com.jtpsolution.security.UsuarioBean;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.Validator;
import br.com.jtpsolution.util.regras.RegrasBean;

@Controller
@RequestMapping("/cliente")
public class TabClienteController extends allController {

	@Autowired
	private UsuarioBean tabUsuariosService;

	@Autowired
	private TabClienteService tabService;
	
	@Autowired
	private RegrasBean regrasBean;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	private String txUrlTela = Constants.TEMPLATE_PATH_CADASTROS+"/cliente/cliente";
	
	
 	
 	@GetMapping
	@Transactional(readOnly = true)
	public ModelAndView pesquisa(TabFiltroClienteObj tabFiltroClienteObj, HttpServletRequest request) {
		
		tabFiltroClienteObj = verificaFiltro(tabFiltroClienteObj, request);
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);
		mv.addObject(new TabClienteObj());			
		mv.addObject("tabFiltroClienteObj", tabFiltroClienteObj);
		mv.addObject("listadados", retornoFiltro(tabFiltroClienteObj));
		
		
		return mv;
	}
	
	private List<?> retornoFiltro(TabFiltroClienteObj tabFiltroClienteObj) {
 		
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(TabClienteObj.class);
		
		adicionarFiltro(tabFiltroClienteObj, criteria);
 		
 		return criteria.list();
 	}


	private TabFiltroClienteObj verificaFiltro(TabFiltroClienteObj tabFiltroClienteObj, HttpServletRequest request) {
		
		try {
			
			if (Validator.isObjetoNull(tabFiltroClienteObj)) {
				if (request.getSession().getAttribute(getClass().getSimpleName()) != null) {					
					String jsonFiltro = request.getSession().getAttribute(getClass().getSimpleName()).toString();
					ObjectMapper mapper = new ObjectMapper();
					tabFiltroClienteObj = mapper.readValue(jsonFiltro, TabFiltroClienteObj.class);	
				}
			}else {
				request.getSession().setAttribute(getClass().getSimpleName(),
						GeneralParser.convertObjJson(tabFiltroClienteObj));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return tabFiltroClienteObj;
	}

	
	private void adicionarFiltro(TabFiltroClienteObj filtro, Criteria criteria) {
		
		  boolean ckFiltro = false;
		  
		  if (filtro != null) {
			  
			  
			  if (!StringUtils.isEmpty(filtro.getTxClienteFiltro())) {
				  criteria.add(Restrictions.ilike("txCliente", "%"+filtro.getTxClienteFiltro()+"%"));
				  ckFiltro = true;
			  }
			  
			  /*
			  if (!ckFiltro) {
				  criteria.add(Restrictions.eq("Campo", "-1"));
			  }*/
			  
			  criteria.addOrder(Order.asc("txCliente"));
		  }
	  }
 	
	
	@RequestMapping(value = "/gravarmodal", method = RequestMethod.POST)
	public @ResponseBody List<?> gravarmodal(@Validated TabClienteObj tabClienteObj, Errors erros, HttpServletRequest request) {

		List<ObjectError> error = new ArrayList<ObjectError>();
		if (erros.hasErrors()) {
		  //mv.addObject("listaErros", erros.getAllErrors());
		  error.addAll(erros.getAllErrors());
		  return error;
		}else {
			error = regrasBean.verificaregras(getClass().getSimpleName(),request);
			if (!error.isEmpty()) {
				  return error;	
			}	  
		}
		
	    TabClienteObj Tab = tabService.gravar(tabClienteObj);
	   
	    List<TabRetornoSucessoObj> success = new ArrayList<TabRetornoSucessoObj>();		
	    TabRetornoSucessoObj tSuccess = new TabRetornoSucessoObj();
	    tSuccess.setCd_mensagem(1);
	    tSuccess.setTx_mensagem(Constants.REGISTRO_INCLUIDO_ALTERADO_SUCESSO);
	    success.add(tSuccess);
	    
		return success;
	}
	
	
	@RequestMapping(value = "/novo")
	public ModelAndView novo(HttpServletRequest request) {
		
		
		ModelAndView mv = new ModelAndView(txUrlTela);	
		mv.addObject(new TabClienteObj());
		mv.addObject("tabFiltroClienteObj", verificaFiltro(new TabFiltroClienteObj(), request));
		mv.addObject("listadados", retornoFiltro(verificaFiltro(new TabFiltroClienteObj(), request)));
		mv.addObject("txEdit", "edit");
		return mv;
	}
	

	@RequestMapping(value = "/consultar/{CdCliente}", method = RequestMethod.GET)
	public ModelAndView consultar(@PathVariable Integer CdCliente, HttpServletRequest request) {
	
	   TabClienteObj TabView = tabService.consultar(CdCliente);
	   TabFiltroClienteObj tF = verificaFiltro(new TabFiltroClienteObj(), request);

	   
		ModelAndView mv = new ModelAndView(txUrlTela);	
		if (TabView != null) {
		  mv.addObject("tabFiltroClienteObj",tF);	
		  mv.addObject("tabClienteObj",TabView);
		  mv.addObject("listadados", retornoFiltro(verificaFiltro(tF, request)));
		  mv.addObject("txEdit", "edit");
		}else {
		  mv.addObject("tabFiltroClienteObj",tF);	
		  mv.addObject(new TabClienteObj());	
		  mv.addObject("txMensagem", Constants.REGISTRO_NAO_EXISTE);
		  mv.addObject("tipoMensagem", Constants.TYPE_NOTIFY_DANGER);
		}
		return mv;
	}
	
	/*
	@RequestMapping(value = "/excluir/{CdCliente}", method = RequestMethod.GET)
	public @ResponseBody String excluir(@PathVariable Integer CdCliente, HttpServletRequest request) {

		tabService.excluir(CdCliente);

		return "OK!";
	}*/
	
}
