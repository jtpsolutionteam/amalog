package br.com.jtpsolution.dao.cadastros.varios;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.springframework.format.annotation.NumberFormat;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.dao.cadastros.empresa.TabEmpresaObj;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tab_valores_fixos", schema = Constants.SCHEMA)
public class TabValoresFixosObj {

	@Id
	@GeneratedValue
	@Column(name = "cd_tabela")
	// @NotNull(message = "Tabela campo obrigatório!")
	private Integer cdTabela;

	@Column(name = "vl_despacho")
	// @NotEmpty(message = "Despacho campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlDespacho;

	@Column(name = "vl_pedagio")
	// @NotEmpty(message = "Pedagio campo obrigatório!")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlPedagio;
	
	@Column(name = "vl_taxa_gris")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlTaxaGris;
	
	@Column(name = "vl_taxa_imo_perigoso")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlTaxaImoPerigoso;

	@Column(name = "vl_ajudante")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlAjudante;
	
	@Column(name = "vl_icms")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlIcms;
	
	@ManyToOne
	@JoinColumn(name = "cd_empresa")
	private TabEmpresaObj tabEmpresaObj;

	@Column(name = "cd_impexp")
	private Integer cdImpexp;
	
	@Column(name = "tx_tipo")
	private String txTipo;
	
	@Column(name = "vl_estacionamento")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlEstacionamento;
	
	@Column(name = "vl_advalorem")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlAdvalorem;
	
	@Column(name = "vl_peso_limite")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlPesoLimite;

	@Column(name = "vl_m3_limite")
	@NumberFormat(pattern = "#,##0.00")
	private Double vlM3Limite;
	
	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
	}

	

}
