package br.com.jtpsolution.dao.conexaorest;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.jtpsolution.Constants;
import br.com.jtpsolution.modulos.robodta.obj.FluxoDeclaracaoTransito;
import br.com.jtpsolution.util.GeneralParser;
import br.com.jtpsolution.util.Validator;

public class restconnect {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			String json = restconnect.enviaRestGet("http://amalog.com.br:8081/transporte/aduaneiro/fluxo/declaracao/2000991585");

			ObjectMapper mapper = new ObjectMapper();

			FluxoDeclaracaoTransito jsonObj = mapper.readValue(json, FluxoDeclaracaoTransito.class);

			System.out.println(jsonObj.getSolicitacaoRegistro().getDtSolicitacao());

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static String enviaRestGet(String txUrl) {

		try {

			RestTemplate restTemplate = new RestTemplate();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			
			HttpEntity<String> entity = new HttpEntity<String>(headers);
						
			String url = txUrl;
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);

			return GeneralParser.convertUTF8TOIso8859(response.getBody());

		} catch (Exception ex) {
			return "Erro envio Resquest: " + ex.getMessage();
		}

	}
	
	public static String enviaRestPut(String txUrlcontroller, String txBodyJson) {

		try {

			RestTemplate restTemplate = new RestTemplate();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			
			if (!Validator.isBlankOrNull(txBodyJson)) {
				txBodyJson = GeneralParser.convertISO8859ToUTF8(txBodyJson);
			}

			HttpEntity<String> entity = new HttpEntity<String>(txBodyJson, headers);

			String url = txUrlcontroller;
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class);

			return GeneralParser.convertUTF8TOIso8859(response.getBody());

		} catch (Exception ex) {
			return "Erro envio Resquest: " + ex.getMessage();
		}

	}
	
	
	public static String enviaRestPost(String txUrlcontroller, String txBodyJson) {

		try {
			RestTemplate restTemplate = new RestTemplate();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			if (!Validator.isBlankOrNull(txBodyJson)) {
				txBodyJson = GeneralParser.convertISO8859ToUTF8(txBodyJson);
			}
			
			HttpEntity<String> entity = new HttpEntity<String>(txBodyJson, headers);

			String url = txUrlcontroller;
			ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);

			return GeneralParser.convertUTF8TOIso8859(response.getBody());

		} catch (Exception ex) {
			return "Erro envio Resquest: " + ex.getMessage();
		}

	}

	

}
