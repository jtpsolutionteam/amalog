package br.com.jtpsolution;

import br.com.jtpsolution.util.GeneralUtil;

public class Constants {

	//Constantes relativas ao Robos
	public static final String ROBO_SISCARGA = "http://amalog.com.br:8082/siscomex/carga/siscarga";
	public static final String ROBO_DAT = "http://amalog.com.br:8083/portal-unico/sicomex/dat/";

	//Constantes relativas a IUGU
	public static final String IUGU_KEY_TESTE = "caf67af42e3a46cdceadc78bf5fcd176"; 
	public static final String IUGU_KEY_PRODUCAO = "e2cd99ef2ad85d73ad6185acda6b2e5d"; 
	public static final String IUGU_KEY_TESTEJTP = "cf4bcecbfaaad3c34edd9aefc5a5f6a2";
	
	//Constantes relativas as Notificações
    public static final String TYPE_NOTIFY_SUCCESS = "success";
    public static final String TYPE_NOTIFY_DANGER = "error";
    public static final String TYPE_NOTIFY_WARNING = "warning";
    public static final String TYPE_NOTIFY_INFO = "info";
	
	//Constantes relativas aos PATHS Templates Web
    	public static final String TEMPLATE_PATH_ADM = "modulos/administracao";
	    public static final String TEMPLATE_PATH_UTIL = "modulos/util";
	    public static final String TEMPLATE_PATH_CADASTROS = "modulos/cadastros";
	    public static final String TEMPLATE_PATH_PROPOSTA = "modulos/proposta";
	    public static final String TEMPLATE_PATH_REGISTRO = "modulos/registro";
	    public static final String TEMPLATE_PATH_MAIL = "/mail";
	   
	    
	
		// Constantes relativas a Variavel de ultimo filtro	
		public static final String ULTIMO_FILTRO = "ULTIMO_FILTRO";	
	    
	    
	// Constantes relativas ao Schema de Banco de Dados	
		public static final String SCHEMA = "revelacraque";	
	
		// Constantes relativas as Mensagens de Retorno	
		public static final String REGISTRO_INCLUIDO_ALTERADO_SUCESSO = "Registro Incluído/Alterado com sucesso!";	
		public static final String LOGIN_OK = "Login efetuado com sucesso!";
		public static final String REGISTRO_EXCLUIDO_SUCESSO = "Registro excluído com sucesso!";	
		public static final String ERRO_AO_EXCLUIR = "Erro ao excluir o registro!";
		
		public static final String REGISTRO_NAO_EXISTE = "Registro não existe!";
		public static final String FILTRO_NAO_EXISTE = "Não existem dados para o filtro desejado!";
		public static final String HASH_INVALIDO = "Hash de segurança inválido!";
		public static final String ERRO_UPLOAD = "Erro no envio do Arquivo!";
		public static final String ERRO_UPLOAD_ARQUIVO_JA_EXISTE = "Arquivo já existe!";

		
		//Constantes relativas ao path dos arquivos do Sistema
		public static final String NOME_SISTEMA = "Amalog";
		public static final String NOME_SISTEMA_LOCAL = "amalog";
		//public static final String PATH_ARQS_JAVA_LOCAL = "/Sistemas/Spring/workspace/amalog/";
		//public static final String PATH_ARQS_JAVA_LOCAL = "/Sistemas/eclipse/workspaceSTS/amalog/";
		public static final String PATH_ARQS_JAVA_LOCAL = "/Sistemas/SpringSTS/workspace/amalog/";
		
		//Constantes relativas ao upload de documentos
				public static final String PATH_UPLOAD = "/Amalog/files/";
		
}
